#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <linux/input.h>
#include <unistd.h>

#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>

#define LOGI(format, ...)                       printf("%s[%s][%i] " format "\n", __FILE__, __FUNCTION__, __LINE__,##__VA_ARGS__)


int main(int argc, char **argv)
{
    int err;
    struct libevdev *dev;
    struct libevdev_uinput *uidev;
    
    // int fd;
    
    // if(argc < 2)
    // {
    //     LOGI("");
    //     return 1;
    // }
    
    // fd = open(argv[1], O_RDWR);
    
    // if (fd < 0) {
    //     LOGI("Failed to open device");
    //     return fd;
    // }

    dev = libevdev_new();
    libevdev_set_name(dev, "fake keyboard device");

    libevdev_enable_event_type(dev, EV_KEY);
    libevdev_enable_event_code(dev, EV_KEY, KEY_F1, NULL);

    err = libevdev_uinput_create_from_device(dev,
        LIBEVDEV_UINPUT_OPEN_MANAGED,
        &uidev);

    if (err != 0)
    {
        LOGI("");
        return err;
    }

    while(1)
    {
        libevdev_uinput_write_event(uidev, EV_KEY, KEY_F1, 1);
        libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
        libevdev_uinput_write_event(uidev, EV_KEY, KEY_F1, 0);
        libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
        sleep(2);
    }

    libevdev_uinput_destroy(uidev);
    printf("Complete\n");
    
    return 0;
}