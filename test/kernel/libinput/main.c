#define _GNU_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <libudev.h>
#include <linux/input.h>
#include <sys/ioctl.h>

#include <libinput.h>
#include <libevdev/libevdev.h>

#define LOGI(format, ...)            printf("%s[%d][%s] " format "\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__ )

enum tools_backend {
    BACKEND_DEVICE,
    BACKEND_UDEV
};

struct tools_options {
    enum tools_backend backend;
    const char *device; /* if backend is BACKEND_DEVICE */
    const char *seat; /* if backend is BACKEND_UDEV */
    int grab; /* EVIOCGRAB */

    int verbose;
    int tapping;
    int drag_lock;
    int natural_scroll;
    int left_handed;
    int middlebutton;
    enum libinput_config_click_method click_method;
    enum libinput_config_scroll_method scroll_method;
    int scroll_button;
    double speed;
    int dwt;
    enum libinput_config_accel_profile profile;
};

struct tools_context {
    struct tools_options options;
    void *user_data;
};

uint32_t start_time;
struct tools_context context;
static unsigned int stop = 0;

static int
open_restricted(const char *path, int flags, void *user_data)
{
    const struct tools_context *context = user_data;
    LOGI("%s %x",path, flags);
    int fd = open(path, flags);
    LOGI("");
    if (fd < 0)
    {
        LOGI("Failed to open %s (%s)", path, strerror(errno));
    }
    // else if (context->options.grab &&
    //      ioctl(fd, EVIOCGRAB, (void*)1) == -1)
    // {
    //     LOGI("Grab requested, but failed for %s (%s)", path, strerror(errno));
    // }
    LOGI("");
    return fd < 0 ? -errno : fd;
}

static void
close_restricted(int fd, void *user_data)
{
    LOGI("");
    close(fd);
}

static const struct libinput_interface interface = {
    .open_restricted = open_restricted,
    .close_restricted = close_restricted,
};

static void
print_event_time(uint32_t time)
{
    printf("%+6.2fs ", (time - start_time) / 1000.0);
}

static void
print_event_header(struct libinput_event *ev)
{
    struct libinput_device *dev = libinput_event_get_device(ev);
    const char *type = NULL;

    switch(libinput_event_get_type(ev)) {
    case LIBINPUT_EVENT_NONE:
        LOGI("");
        abort();
    case LIBINPUT_EVENT_DEVICE_ADDED:
        type = "DEVICE_ADDED";
        break;
    case LIBINPUT_EVENT_DEVICE_REMOVED:
        type = "DEVICE_REMOVED";
        break;
    case LIBINPUT_EVENT_KEYBOARD_KEY:
        type = "KEYBOARD_KEY";
        break;
    case LIBINPUT_EVENT_POINTER_MOTION:
        type = "POINTER_MOTION";
        break;
    case LIBINPUT_EVENT_POINTER_MOTION_ABSOLUTE:
        type = "POINTER_MOTION_ABSOLUTE";
        break;
    case LIBINPUT_EVENT_POINTER_BUTTON:
        type = "POINTER_BUTTON";
        break;
    case LIBINPUT_EVENT_POINTER_AXIS:
        type = "POINTER_AXIS";
        break;
    case LIBINPUT_EVENT_TOUCH_DOWN:
        type = "TOUCH_DOWN";
        break;
    case LIBINPUT_EVENT_TOUCH_MOTION:
        type = "TOUCH_MOTION";
        break;
    case LIBINPUT_EVENT_TOUCH_UP:
        type = "TOUCH_UP";
        break;
    case LIBINPUT_EVENT_TOUCH_CANCEL:
        type = "TOUCH_CANCEL";
        break;
    case LIBINPUT_EVENT_TOUCH_FRAME:
        type = "TOUCH_FRAME";
        break;
    case LIBINPUT_EVENT_GESTURE_SWIPE_BEGIN:
        type = "GESTURE_SWIPE_BEGIN";
        break;
    case LIBINPUT_EVENT_GESTURE_SWIPE_UPDATE:
        type = "GESTURE_SWIPE_UPDATE";
        break;
    case LIBINPUT_EVENT_GESTURE_SWIPE_END:
        type = "GESTURE_SWIPE_END";
        break;
    case LIBINPUT_EVENT_GESTURE_PINCH_BEGIN:
        type = "GESTURE_PINCH_BEGIN";
        break;
    case LIBINPUT_EVENT_GESTURE_PINCH_UPDATE:
        type = "GESTURE_PINCH_UPDATE";
        break;
    case LIBINPUT_EVENT_GESTURE_PINCH_END:
        type = "GESTURE_PINCH_END";
        break;
    }

    printf("%-7s    %s  ", libinput_device_get_sysname(dev), type);
}

static void
print_key_event(struct libinput_event *ev)
{
    struct libinput_event_keyboard *k = libinput_event_get_keyboard_event(ev);
    enum libinput_key_state state;
    uint32_t key;
    const char *keyname;

    print_event_time(libinput_event_keyboard_get_time(k));
    state = libinput_event_keyboard_get_key_state(k);

    key = libinput_event_keyboard_get_key(k);
    keyname = libevdev_event_code_get_name(EV_KEY, key);
    printf("%s (%d) %s\n",
           keyname ? keyname : "???",
           key,
           state == LIBINPUT_KEY_STATE_PRESSED ? "pressed" : "released");
}

static int
handle_and_print_events(struct libinput *li)
{
    int rc = -1;
    struct libinput_event *ev;

    libinput_dispatch(li);
    while ((ev = libinput_get_event(li)))
    {
        print_event_header(ev);
        enum libinput_event_type type = libinput_event_get_type(ev);
        switch (type)
        {
            case LIBINPUT_EVENT_NONE:
            {
                LOGI("");
                abort();
                break;
            }
            // case LIBINPUT_EVENT_DEVICE_ADDED:
            // case LIBINPUT_EVENT_DEVICE_REMOVED:
            //     print_device_notify(ev);
            //     // tools_device_apply_config(libinput_event_get_device(ev), &context.options);
            //     break;
            case LIBINPUT_EVENT_KEYBOARD_KEY:
            {
                print_key_event(ev);
                break;
            }
            default:
            {
                LOGI("UNKOWN: %d", type);
                break;
            }

        }

        libinput_event_destroy(ev);
        libinput_dispatch(li);
        rc = 0;
    }
    return rc;
}

static void
sighandler(int signal, siginfo_t *siginfo, void *userdata)
{
    stop = 1;
}

int main(int argc, char ** argv)
{
    struct libinput *li = NULL;
    struct libinput_device *device;
    char *path = argv[1];
    void *userdata = NULL;
    struct timespec tp;
    LOGI("%s", path);

    struct tools_options *options = &context.options;

    context.user_data = NULL;

    memset(options, 0, sizeof(*options));
    options->tapping = -1;
    options->drag_lock = -1;
    options->natural_scroll = -1;
    options->left_handed = -1;
    options->middlebutton = -1;
    options->dwt = -1;
    options->click_method = -1;
    options->scroll_method = -1;
    options->scroll_button = -1;
    options->backend = BACKEND_UDEV;
    options->seat = "seat0";
    options->speed = 0.0;
    options->profile = LIBINPUT_CONFIG_ACCEL_PROFILE_NONE;


    li = libinput_path_create_context(&interface, context.user_data);
    if (!li)
    {
        LOGI("Failed to initialize context from %s", path);
        return 1;
    }
    LOGI("2");
    device = libinput_path_add_device(li, path);
    LOGI("");
    if (!device)
    {
        LOGI("Failed to initialized device %s", path);
        libinput_unref(li);
        li = NULL;
    }

    LOGI("");
    clock_gettime(CLOCK_MONOTONIC, &tp);
    start_time = tp.tv_sec * 1000 + tp.tv_nsec / 1000000;
//------------------------------------------------------------------------------
    LOGI("");
    struct pollfd fds;
    struct sigaction act;

    fds.fd = libinput_get_fd(li);
    fds.events = POLLIN;
    fds.revents = 0;

    memset(&act, 0, sizeof(act));
    act.sa_sigaction = sighandler;
    act.sa_flags = SA_SIGINFO;
    LOGI("");
    if (sigaction(SIGINT, &act, NULL) == -1) {
        LOGI("Failed to set up signal handling (%s)", strerror(errno));
        goto error;
    }
    LOGI("");
    if (handle_and_print_events(li))
    {
        LOGI("Expected device added events on startup but got none. Maybe you don't have the right permissions?");
    }

    while (!stop && poll(&fds, 1, -1) > -1)
    {
        handle_and_print_events(li);
    }

    LOGI("");
//------------------------------------------------------------------------------
    libinput_unref(li);
    return 0;
error:
    // udev_unref(udev);
    LOGI("");
    return 1;
}
