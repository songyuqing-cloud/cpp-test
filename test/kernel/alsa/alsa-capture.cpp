/*

This example reads from the default PCM device
and writes to standard output for 5 seconds of data.

*/
#include <stdio.h>
#include <stdint.h>
#include <string>
/* Use the newer ALSA API */
#define ALSA_PCM_NEW_HW_PARAMS_API

#include <alsa/asoundlib.h>

    typedef struct wavHeader
    {
        char riffMarker[4];
        uint32_t fileSize;
        char fileType[4];
        char formatMarker[4];
        uint32_t headerLength;
        uint16_t formatType;
        uint16_t numberOfChannels;
        uint32_t sampleRate;
        uint32_t bytesPerSecond;
        uint16_t bytesPerFrame;
        uint16_t bitsPerSample;
    } SWavHeader;

    SWavHeader *genericWavHeader(uint32_t sampleRate, uint16_t bitDepth, uint16_t numberOfChannels)
    {
        SWavHeader *header = (SWavHeader *)malloc(sizeof(SWavHeader));

        if (header == NULL)
        {
            return NULL;
        }

        memcpy(&header->riffMarker, "RIFF", 4);
        memcpy(&header->fileType, "WAVE", 4);
        memcpy(&header->formatMarker, "fmt ", 4);
        header->headerLength = 16;
        header->formatType = 1;
        header->numberOfChannels = numberOfChannels;
        header->sampleRate = sampleRate;
        header->bytesPerSecond = sampleRate * numberOfChannels * bitDepth / 8;
        header->bytesPerFrame = numberOfChannels * bitDepth / 8;
        header->bitsPerSample = bitDepth;

        return header;
    }

    int writeWavHeader(int fd, SWavHeader *header)
    {
        if (header == NULL)
        {
            return -1;
        }

        write(fd, &header->riffMarker, sizeof(header->riffMarker));
        write(fd, &header->fileSize, sizeof(header->fileSize));
        write(fd, &header->fileType, sizeof(header->fileType));
        write(fd, &header->formatMarker, sizeof(header->formatMarker));
        write(fd, &header->headerLength, sizeof(header->headerLength));
        write(fd, &header->formatType, sizeof(header->formatType));
        write(fd, &header->numberOfChannels, sizeof(header->numberOfChannels));
        write(fd, &header->sampleRate, sizeof(header->sampleRate));
        write(fd, &header->bytesPerSecond, sizeof(header->bytesPerSecond));
        write(fd, &header->bytesPerFrame, sizeof(header->bytesPerFrame));
        write(fd, &header->bitsPerSample, sizeof(header->bitsPerSample));
        write(fd, "data", 4);

        uint32_t data_size = header->fileSize - sizeof(SWavHeader);
        write(fd, &data_size, 4);

        return 0;
    }

    int recordWAV(const char *fileName, SWavHeader *header, uint32_t duration)
    {
        const char *device = "hw:0,2";
        char *buffer;
        int err;
        int size;
        int dir;
        int fd;
        unsigned int sampleRate = header->sampleRate;
        snd_pcm_t *handle;
        snd_pcm_hw_params_t *params;
        snd_pcm_uframes_t frames = 32;

        /* Open PCM device for recording (capture) */
        err = snd_pcm_open(&handle, device, SND_PCM_STREAM_CAPTURE, 0);
        if (err)
        {
            printf("Unable to open PCM device: %s\n", snd_strerror(err));
            return err;
        }

        /* Allocate a hardware parameters object. */
        snd_pcm_hw_params_alloca(&params);

        /* Fill it in with default values. */
        snd_pcm_hw_params_any(handle, params);

        /* Interleaved mode */
        err = snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
        if (err)
        {
            printf("Error setting interleaved mode: %s\n", snd_strerror(err));
            snd_pcm_close(handle);
            return err;
        }

        /* Signed 16-bit little-endian format */
        if (header->bitsPerSample == 16)
        {
            err = snd_pcm_hw_params_set_format(handle, params, SND_PCM_FORMAT_S16_LE);
        }
        else
        {
            printf("Doesn't support this format\n");
            return -1;
        }

        if (err)
        {
            printf("Error setting format: %s\n", snd_strerror(err));
            snd_pcm_close(handle);
            return err;
        }

        /* Set number of channels */
        err = snd_pcm_hw_params_set_channels(handle, params, header->numberOfChannels);
        if (err)
        {
            printf("Error setting channels: %s\n", snd_strerror(err));
            snd_pcm_close(handle);
            return err;
        }

        /* Set sample rate */
        err = snd_pcm_hw_params_set_rate_near(handle, params, &sampleRate, 0);
        if (err)
        {
            printf("Error setting sampling rate (%d): %s\n", sampleRate, snd_strerror(err));
            snd_pcm_close(handle);
            return err;
        }
        header->sampleRate = sampleRate;

        /* Set period size*/
        err = snd_pcm_hw_params_set_period_size_near(handle, params, &frames, &dir);
        if (err)
        {
            printf("Error setting period size: %s\n", snd_strerror(err));
            snd_pcm_close(handle);
            return err;
        }

        /* Write the parameters to the driver */
        err = snd_pcm_hw_params(handle, params);
        if (err < 0)
        {
            printf("Unable to set HW parameters: %s\n", snd_strerror(err));
            snd_pcm_close(handle);
            return err;
        }

        /* Use a buffer large enough to hold one period */
        err = snd_pcm_hw_params_get_period_size(params, &frames, &dir);
        if (err)
        {
            printf("Error retrieving period size: %s\n", snd_strerror(err));
            snd_pcm_close(handle);
            return err;
        }

        size = frames * header->bitsPerSample / 8 * header->numberOfChannels; /* 2 bytes/sample */
        buffer = (char *) malloc(size);
        if (!buffer)
        {
            fprintf(stdout, "Buffer error.\n");
            snd_pcm_close(handle);
            return -1;
        }

        err = snd_pcm_hw_params_get_period_time(params, &sampleRate, &dir);
        if (err)
        {
            printf("Error retrieving period time: %s\n", snd_strerror(err));
            snd_pcm_close(handle);
            free(buffer);
            return err;
        }

        uint32_t pcm_data_size = header->sampleRate * header->bytesPerFrame * duration;
        header->fileSize = pcm_data_size + sizeof(SWavHeader);

        /* Remove old file if exists */
        remove(fileName);

        /* Open file for writing data */
        fd = open(fileName, O_WRONLY | O_CREAT, 0644);
        if(fd < 0)
        {
            return -1;
        }

        /* Write file header */
        err = writeWavHeader(fd, header);
        if (err)
        {
            printf("Error writing .wav header.");
            snd_pcm_close(handle);
            free(buffer);
            close(fd);
            return err;
        }

        /* Write data */
        int loop = (duration * 1000000) / sampleRate;
        while (loop > 0)
        {
            loop--;
            err = snd_pcm_readi(handle, buffer, frames);
            if (err == -EPIPE)
            {
                /* EPIPE means overrun */
                printf("overrun occurred\n");
                snd_pcm_prepare(handle);
            }
            else if (err < 0)
            {
                printf("error from read: %s\n", snd_strerror(err));
            }
            else if (err != (int)frames)
            {
                printf("short read, read %d frames\n", err);
            }

            err = write(fd, buffer, size);
            if (err != size)
            {
              printf("short write: wrote %d bytes\n", err);
            }
        }

        close(fd);
        snd_pcm_drain(handle);
        snd_pcm_close(handle);
        free(buffer);
        return 0;
    }



int main(int argc, char **argv)
{
    long loops;
    int rc;
    int size;
    snd_pcm_t *handle;
    snd_pcm_hw_params_t *params;
    unsigned int val;
    int dir;
    snd_pcm_uframes_t frames;
    char *buffer;
    int fd = 1;
    if(argc > 2)
    {
        fd = open(argv[2], O_WRONLY | O_CREAT, 0644);
        if(fd < 0)
        {
            return -1;
        }
    }

    /* Open PCM device for recording (capture). */
    if(argc > 1)
    {
        printf("Open with %s\n", argv[1]);
        rc = snd_pcm_open(&handle, argv[1],
                      SND_PCM_STREAM_CAPTURE, 0);
    }
    else
    {
        printf("Open with hw:0,2\n");
        rc = snd_pcm_open(&handle, "hw:0,2",
                      SND_PCM_STREAM_CAPTURE, 0);
    }

    if (rc < 0)
    {
        fprintf(stderr,
                "unable to open pcm device: %s\n",
                snd_strerror(rc));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(handle, params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(handle, params,
                                 SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(handle, params,
                                 SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(handle, params, 2);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 44100;
    snd_pcm_hw_params_set_rate_near(handle, params,
                                    &val, &dir);

    /* Set period size to 32 frames. */
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(handle,
                                           params, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(handle, params);
    if (rc < 0)
    {
        fprintf(stderr,
                "unable to set hw parameters: %s\n",
                snd_strerror(rc));
        exit(1);
    }

    /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(params,
                                      &frames, &dir);
    size = frames * 4; /* 2 bytes/sample, 2 channels */
    buffer = (char *) malloc(size);

    /* We want to loop for 5 seconds */
    snd_pcm_hw_params_get_period_time(params,
                                      &val, &dir);
    loops = 5000000 / val;

    while (loops > 0)
    {
        loops--;
        rc = snd_pcm_readi(handle, buffer, frames);
        if (rc == -EPIPE)
        {
            /* EPIPE means overrun */
            fprintf(stderr, "overrun occurred\n");
            snd_pcm_prepare(handle);
        }
        else if (rc == -EBADFD)
        {
            fprintf(stderr, "PCM is not in the right state (SND_PCM_STATE_PREPARED or SND_PCM_STATE_RUNNING)\n");
        }
        else if (rc == -ESTRPIPE)
        {
            fprintf(stderr, "a suspend event occurred (stream is suspended and waiting for an application recovery)\n");
        }
        else if (rc < 0)
        {
            fprintf(stderr,
                    "error from read %d: %s\n", rc,
                    snd_strerror(rc));
        }
        else if (rc != (int)frames)
        {
            fprintf(stderr, "short read, read %d frames\n", rc);
        }
        rc = write(fd, buffer, size);
        if (rc != size)
            fprintf(stderr,
                    "short write: wrote %d bytes\n", rc);
    }

    snd_pcm_drain(handle);
    snd_pcm_close(handle);
    free(buffer);
    close(fd);


    // int err;
    // SWavHeader *header = genericWavHeader(8000, 16, 1);
    // std::string buffer = "/tmp/sh_voice.wav";

    // err = recordWAV(buffer.c_str(), header, 5);

    // if (err)
    // {
    //     printf("Error recording WAV file: %d\n", err);
    // }

    // free(header);

    return 0;
}
