#define HELLO(_a, _b) _HELLO(_a, _b)
#define _HELLO(_a, _b) \
_a##::##_b

#define NS test::oal::

int main(int argc, char ** argv) {
    HELLO(test::oal, oal_tc_001)

}
