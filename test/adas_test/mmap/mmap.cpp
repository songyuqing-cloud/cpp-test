#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/mman.h>
#include <linux/kd.h>
#include <linux/vt.h>
#include <linux/fb.h>
#include <linux/input.h>
#include <sys/user.h>

#include <string>

#define LOG_EN

#include </home/longlt3/adas/SDK_ROOT/tests/fpt/unittest/test_linux/oal/projects/public/test_utils.hpp>




int main(int argc, char **argv)
{
  int oal_fd = open("/dev/oal_cached", O_RDWR);
  int oal_fd_nc = open("/dev/oal_noncached", O_RDWR);
  
  _LOGI("mapped: 0x%lx", std::stoi(argv[1]));
  
  void *ret1 = mmap(0, std::stoi(argv[2]), PROT_READ | PROT_WRITE | PROT_EXEC, MAP_SHARED, oal_fd, std::stoi(argv[1]));
  
  _LOGI("mapped: 0x%lx", std::stoi(argv[1]));
  void *ret2 = mmap(0, std::stoi(argv[2]), PROT_READ | PROT_WRITE | PROT_EXEC, MAP_SHARED, oal_fd_nc, std::stoi(argv[1]));
  
  _LOGE("0x%lx 0x%lx 0x%lx: ", ret1, ret2, errno);
}