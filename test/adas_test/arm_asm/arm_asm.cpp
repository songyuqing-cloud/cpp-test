#include <iostream>
#include <cstdint>

int nothing(int val)
{
	val--;
	asm volatile("" ::: "memory");

    return val++;
}

int something(int val)
{
	val--;
	asm volatile("ret; nop" ::: "memory");
    return val++;
}

int main()
{
    // void *p;
    // p = reinterpret_cast<void *>(0xcb000000);
    // *reinterpret_cast<uint8_t *>(p) = 0xFF;

	nothing(1); something(2);

    return 0;
}
