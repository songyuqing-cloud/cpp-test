#ifndef MY_H
#define MY_H
#include <iostream>

class MyStatic
{
public:
    static inline int doSmt()
    {
        static int x = 0;
        x++;
        return x;
    }
};
#endif