#ifndef MY_H2
#define MY_H2

#include "static.hpp"

class M2
{
public:
    M2()
    {
        std::cout<< "M2: " << MyStatic::doSmt() << std::endl;
    }
};

#endif