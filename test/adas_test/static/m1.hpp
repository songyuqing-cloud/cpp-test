#ifndef MY_H1
#define MY_H1

#include "static.hpp"

class M1
{
public:
    M1()
    {
        std::cout<< "M1: " << MyStatic::doSmt() << std::endl;
    }
};

#endif