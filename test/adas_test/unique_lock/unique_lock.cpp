#include <thread>
#include <mutex>
#include <iostream>
#include <condition_variable>
#include <chrono>

std::mutex mtx;
std::condition_variable cv;
bool ready = false;

#define NUM 3

int main()
{
    std::thread th[NUM];
    for(int i=0; i<NUM; i++)
    {
        th[i] = std::thread([](int tid)
            {
                {
                    std::unique_lock<std::mutex> lck(mtx);
                    cv.wait(lck, [](){return ready;});
                }
                for (int i = 0; i < 10; ++i)
                {
                    /* code */
                    std::cout<< tid;
                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                }
                std::cout << std::endl;
            }, i);
    }

    ready = true;
    cv.notify_all();

    for(auto &t : th)
    {
        t.join();
    }

    return 0;
}
