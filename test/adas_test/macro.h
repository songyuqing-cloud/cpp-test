#ifndef MACRO_H
#define MACRO_H

#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <error.h>
#include <string.h>

#define _PLOGE(format, ...) do {fprintf(stderr, "%s %s p[%d-%d] " format "%s\n", __FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__, strerror(errno));} while(0)
#define _PPLOGE(format, ...) do {fprintf(stderr, "%s %s pp[%d-%d] " format "%s\n", __FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__, strerror(errno));} while(0)
#define _TLOGE(format, ...) do {fprintf(stderr, "%s %s t[%d-%d] " format "%s\n", __FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__, strerror(errno));} while(0)
#define _LOGE(format, ...) do {fprintf(stderr, "%s %s [%d] " format "%s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno));} while(0)


#define _PLOGI(format, ...) do {printf("%s %s p[%d-%d] " format "\n", __FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ );} while(0)
#define _PPLOGI(format, ...) do {printf("%s %s pp[%d-%d] " format "\n", __FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ );} while(0)
#define _TLOGI(format, ...) do {printf("%s %s t[%d-%d] " format "\n", __FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ );} while(0)
#define _LOGI(format, ...) do {printf("%s %s [%d] " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ );} while(0)

#define SLEEPMS(ms)                                                            \
{                                                                              \
  if((ms) > 0)                                                                 \
  {                                                                            \
    struct timespec req;                                                       \
    req.tv_sec = (ms) / 1000;                                                  \
    req.tv_nsec = ((ms) % 1000) * 1000000;                                     \
    nanosleep(&req, nullptr);                                                  \
  }                                                                            \
}

#endif // MACRO_H
