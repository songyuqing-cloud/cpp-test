#include <vector>
#include <iostream>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <chrono>
#include <random>

#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include <csignal>

#define LOGI(format, ...)            printf("%s %s [%d] " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#define _PLOGI(format, ...) do {printf("p[%d] %s %s [%d] " format "\n", (int)getpid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ );} while(0)
#define _PPLOGI(format, ...) do {printf("pp[%d] %s %s [%d] " format "\n", (int)getppid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ );} while(0)
#define _TLOGI(format, ...) do {printf("t[%d] %s %s [%d] " format "\n", (int)syscall(SYS_gettid), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ );} while(0)

#define LOGE(format, ...)            fprintf(stderr, "%s %s [%d] " format " %s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
#define _PLOGE(format, ...) do {fprintf(stderr, "p[%d] %s %s [%d] " format "%s\n", (int)getpid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno));} while(0)
#define _PPLOGE(format, ...) do {fprintf(stderr, "pp[%d] %s %s [%d] " format "%s\n", (int)getppid(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno));} while(0)
#define _TLOGE(format, ...) do {fprintf(stderr, "t[%d] %s %s [%d] " format "%s\n", (int)syscall(SYS_gettid), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno));} while(0)


static std::mutex mtx;
static std::condition_variable cv;
static bool ready = false;

volatile std::sig_atomic_t gSignalStatus;
static bool quit = false;

std::vector<int> gv;

int main()
{
    gv.reserve(10000);

    LOGI("%d %d", gv.size(), gv.capacity());

    std::this_thread::sleep_for(std::chrono::milliseconds(1));

    enum {
        THREAD_PUSH = 0,
        THREAD_POP,
        THREAD_MAX,
    };

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> dist(1, 100);
    static int32_t temp = 0;

    std::thread thrs [THREAD_MAX];
    LOGI("");
    thrs[THREAD_PUSH] = std::thread( [&]()
    {
        {
            std::unique_lock<std::mutex> lck(mtx);
            cv.wait(lck, [](){return ready;});
        }

        _TLOGI("Thread Go");
        while(!quit && temp < gv.capacity())
        {
            // {
            //     std::unique_lock<std::mutex> lck(mtx);
            //     cv.wait(lck, [](){return gv.empty() || quit;});
            // }
            if(quit)
            {
                break;
            }
            // gv.push_back(dist(gen));
            gv.push_back(temp++);
            // _TLOGI("%d", gv.empty() ? -1 : gv.back());
            // cv.notify_one();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        _TLOGI("Thread quit");
    });

    thrs[THREAD_POP] = std::thread( [&]()
    {
        {
            std::unique_lock<std::mutex> lck(mtx);
            cv.wait(lck, [](){return ready;});
        }
        _TLOGI("Thread Go");
        while(!quit)
        {
            // {
            //     std::unique_lock<std::mutex> lck(mtx);
            //     cv.wait(lck, [](){return !gv.empty() || quit;});
            // }
            if(!gv.empty())
            {
                _TLOGI("%d", gv.back());
                gv.pop_back();
            }
            // cv.notify_one();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        _TLOGI("Thread quit");
    });

    // Install a signal handler
    std::signal(SIGINT, [](int signal)
    {
        _TLOGI("received signal: %d", signal);
        quit = true;
        // cv.notify_all();
    });

    _TLOGI("Ready!");
    ready = true;
    cv.notify_all();

    for(auto &th : thrs)
    {
        th.join();
    }

    if(!gv.empty())
    {
        LOGI("gv size: %d", gv.size());
        for(auto const &tmp : gv)
        {
            LOGI("%d", tmp);
        }
    }
    else
    {
    }

    LOGI("QUIT: %d", temp);

    return 0;
}
