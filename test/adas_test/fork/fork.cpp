#include <iostream>
#include <cstdlib>
#include <../macro.h>

class Example
{
public:
    Example()
    {
        _PLOGI("");
        ptr = malloc(100);
    }
    void test()
    {
        _PLOGI("");
        fork();
        _PLOGI("");
    }

    ~Example()
    {
        if(ptr)
        {
            _PLOGI("");
            delete ptr;
        }
    }

void *ptr;
};

int main(void)
{
    Example ex;
    ex.test();
    return 0;
}
