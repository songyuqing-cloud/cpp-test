#include <iostream>
#include <cstring>
#include <chrono>
#include <thread>

#include <mqueue.h>

#include "../macro.h"

using namespace std;

static char const *getMqFileName() {return "/mq_temp_"; }

int main(void)
{
    // unlink message queue
    mq_unlink(getMqFileName());
    _LOGI("Creating message queue: %s", getMqFileName());
    struct mq_attr mqattr;
    mqattr.mq_maxmsg = 10;
    mqattr.mq_msgsize = sizeof(int);
    // create message queue
    mqd_t mq_id = mq_open (getMqFileName(), O_CREAT, 0600, &mqattr);
    if(mq_id == static_cast<mqd_t>(-1))
    {
        _LOGE("");
        return 1;
    }
    close(mq_id);

    int temp = 0;
    pid_t cpid = fork();

    if(cpid)
    {
        // parent
        _PLOGI("Parent");
        // std::this_thread::sleep_for(std::chrono::milliseconds(5000));

        // mq_id = mq_open (getMqFileName(), O_RDONLY | O_NONBLOCK, 0600, &mqattr);

        // int msgSize;
        // while(msgSize = mq_receive(mq_id, reinterpret_cast<char *>(&temp), mqattr.mq_msgsize, nullptr) < 0)
        // {
        //     _LOGE("");
        //     std::this_thread::sleep_for(std::chrono::milliseconds(250));
        // }
        // _TLOGI("[Received] size:%d - temp:%d", msgSize, temp);
        // temp++;
        // // msgSize = mq_send(mq_id, reinterpret_cast<char *>(&temp), sizeof(int), 0);
        // // _TLOGI("Sent: %d", msgSize);
        // mq_close(mq_id);
        // mq_unlink(getMqFileName());
    }
    else
    {
        // child
        _PLOGI("Child");
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        mq_id = mq_open (getMqFileName(), O_WRONLY, 0600, &mqattr);
        mq_unlink(getMqFileName());
        temp++;
        int msgSize;
        while(msgSize = mq_send(mq_id, reinterpret_cast<char *>(&temp), sizeof(int), 0) < 0)
        {
            _LOGE("");
            std::this_thread::sleep_for(std::chrono::milliseconds(250));
        }
        _TLOGI("[Sent] size:%d", msgSize);
        // msgSize = mq_receive(mq_id, reinterpret_cast<char *>(&temp), mqattr.mq_msgsize, nullptr);
        // _TLOGI("Received: %d : %d", temp, msgSize);
        mq_close(mq_id);
    }


    _TLOGE("");
    return 0;
}
