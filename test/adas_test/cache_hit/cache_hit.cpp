#include <iostream>
#include <vector>
#include <string>

// static const int LOOP = 50;
static const int INC_VAL = 50;
static const int INI_VAL = 50;


int main(int argc, char **argv)
{
	using namespace std;

    int loop = 0;
    if(argc > 1)
    {
        // istringstream sstream (string(argv[1]));
        // sstream >> loop;
        loop = stoi(argv[1]);
    }

    cout << "LOOP: "<< loop << endl;

    for(int i=0; i<loop; i++)
    {
        // int *x = new int [INI_VAL + (INC_VAL * i)];
        vector<int> x(INI_VAL + (INC_VAL * i));
        // x.reserve(INI_VAL + (INC_VAL * i));
        for( auto &temp : x)
        {
            temp++;
        }

        for( auto &temp : x)
        {
            temp++;
        }

        cout << "loop: " << i << endl;

        // delete [] x;
    }

	return 0;
}
