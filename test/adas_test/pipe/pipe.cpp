#include <sys/types.h>
#include <fcntl.h>    /* For O_RDWR */
#include <unistd.h>   /* For open(), creat() */
#include <stdio.h>
#include <stdlib.h>

#include <../macro.h>

/* Read characters from the pipe and echo them to stdout. */

void read_from_pipe (int file)
{
    FILE *stream;
    int c;
    stream = fdopen (file, "r");
    while ((c = fgetc (stream)) != EOF)
        putchar (c);
    fclose (stream);
}

/* Write some random text to the pipe. */

void write_to_pipe (int file)
{
    FILE *stream;
    stream = fdopen (file, "w");
    fprintf (stream, "hello, world!\n");
    fprintf (stream, "goodbye, world!\n");
    fclose (stream);
}

int main (void)
{
    pid_t pid;
    int ctop[2], ptoc[2];

    /* Create the pipe. */
    if (pipe2(ptoc, O_NONBLOCK) || pipe2(ctop, O_NONBLOCK))
    {
        fprintf (stderr, "Pipe failed.\n");
        return EXIT_FAILURE;
    }

    /* Create the child process. */
    pid = fork ();
    if (pid == (pid_t) 0)
    {
        /* This is the child process. Close other end first. */
        _PLOGE("");
        close (ptoc[1]);
        close (ctop[0]);
        SLEEPMS(50);
        _PLOGE("");
        read_from_pipe (ptoc[0]);
        _PLOGE("");
        write_to_pipe (ctop[1]);
        SLEEPMS(50);
        _PLOGE("");
        return EXIT_SUCCESS;
    }
    else if (pid < (pid_t) 0)
    {
        /* The fork failed. */
        fprintf (stderr, "Fork failed.\n");
        return EXIT_FAILURE;
    }
    else
    {
        /* This is the parent process. Close other end first. */
        _PLOGE("");
        close (ptoc[0]);
        close (ctop[1]);
        SLEEPMS(50);
        _PLOGE("");
        write_to_pipe (ptoc[1]);
        _PLOGE("");
        read_from_pipe (ctop[0]);
        SLEEPMS(50);
        _PLOGE("");
        return EXIT_SUCCESS;
    }
}
