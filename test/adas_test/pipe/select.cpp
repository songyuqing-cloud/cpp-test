#include <sys/types.h>
#include <fcntl.h>    /* For O_RDWR */
#include <unistd.h>   /* For open(), creat() */
#include <stdio.h>
#include <stdlib.h>

#include <../macro.h>

int main (void)
{
    pid_t pid;
    int p_read[2], c_read[2];
    fd_set fds;
    struct timeval tv;

    _LOGI("%d", FD_SETSIZE);

    /* Create the pipe. */
    if (pipe2(c_read, O_NONBLOCK) || pipe2(p_read, O_NONBLOCK))
    {
        fprintf (stderr, "Pipe failed.\n");
        return EXIT_FAILURE;
    }

    char buff[2];

    /* Create the child process. */
    pid = fork ();
    if (pid == (pid_t) 0)
    {
        /* This is the child process. Close other end first. */
        _PLOGE("");
        close (c_read[1]); // close write
        close (p_read[0]); // close read
        _PLOGE("");
        buff[0] = 0;
        for(int i=0; i<10; i++)
        {
            _PLOGI("loopC [%d]", i);
            FD_ZERO(&fds); // Clear FD set for select
            FD_SET(c_read[0], &fds);

            tv.tv_sec = 0;
            tv.tv_usec = 50000;
            int rs = select(c_read[0] + 1, &fds, NULL, NULL, &tv);
            if (FD_ISSET(c_read[0], &fds))
            {
                int rs = read(c_read[0], &buff, 1);
                if(rs > 0) _PLOGE("C: %d: ", buff[0]);
            }
            _PLOGE("Crs: %d: ", rs);

            buff[0] = 0;
            write(p_read[1], buff, 1);
            _PLOGE("");
            SLEEPMS(5);
        }
        return EXIT_SUCCESS;
    }
    else if (pid < (pid_t) 0)
    {
        /* The fork failed. */
        fprintf (stderr, "Fork failed.\n");
        return EXIT_FAILURE;
    }
    else
    {
        /* This is the parent process. Close other end first. */
        _PLOGE("");
        close (c_read[0]); // close read
        close (p_read[1]); // close write
        _PLOGE("");
        buff[0] = 1;
        for(int i=0; i<10; i++)
        {
            _PLOGI("loopP [%d]", i);
            FD_ZERO(&fds); // Clear FD set for select
            FD_SET(p_read[0], &fds);

            tv.tv_sec = 0;
            tv.tv_usec = 50000;
            int rs = select(p_read[0] + 1, &fds, NULL, NULL, &tv);
            if (FD_ISSET(p_read[0], &fds))
            {
                int rs = read(p_read[0], &buff, 1);
                if(rs > 0) _PLOGE("P: %d: ", buff[0]);
            }
            _PLOGE("Prs: %d: ", rs);

            buff[0] = 1;
            write(c_read[1], buff, 1);
            _PLOGE("");
            SLEEPMS(5);
        }

        SLEEPMS(1000);
        return EXIT_SUCCESS;
    }
}
