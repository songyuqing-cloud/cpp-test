#include <iostream>
#include "../macro.h"

int test1()
{
    _LOGI("");
    return 1;
}

void test2()
{
    _LOGI("");
}

int test3()
{
    _LOGI("");
    return 3;
}

int main(void)
{
    _LOGI("");
    int x = (test1(),test2(),test3());
    _LOGI("%d", x);

    return 0;
}
