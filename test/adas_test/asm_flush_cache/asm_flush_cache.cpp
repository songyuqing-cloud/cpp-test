#include <iostream>
#include <string>
#include <cstring>

#include <macro.h>
// #include <oal.h>

inline void fdcr(void *ptr, size_t size)
{
    __asm __volatile__ (
    "   mrs x3, CTR_EL0                                                     \n"
    "   ubfx  x3, x3, #16, #4                                               \n"
    "   mov x2, #4                                                          \n"
    "   lsl x2, x2, x3                                                      \n"
    "   add %[size], %[ptr], %[size]                                        \n"
    "   sub x3, x2, #1                                                      \n"
    "   bic %[ptr], %[ptr], x3                                              \n"
    "dflush_loop:                                                           \n"
    "   dc  cvac, %[ptr]                                                    \n"
    "   add %[ptr], %[ptr], x2                                              \n"
    "   cmp %[ptr], %[size]                                                 \n"
    "   b.lo    dflush_loop                                                 \n"
    "   dsb sy                                                              \n"
    :
    :   [size] "r" (size), [ptr] "r" (ptr)
    );
}

int main(int argc, char **argv)
{
    _PLOGI("");

    using namespace std;
    size_t size = 10;
    if(argc > 1)
    {
        size = stoi(argv[1]);
    }

    typedef uint32_t arr_type;

    arr_type *arr = new arr_type[size];
    memset(arr, 0, size * sizeof(arr_type));

    fdcr(arr, size * sizeof(arr_type));

    _LOGI("");

    delete [] arr;
    return 0;
}
