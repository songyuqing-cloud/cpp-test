// #include <iostream>

// using namespace std;

#define MAGIC_VAL                       0x42FF0000
#define AUTO_STEP                       0;
#define CONTINUOUS_FLAG                         VSDK_CV_MAT_CONT_FLAG
#define SUBMATRIX_FLAG                      VSDK_CV_SUBMAT_FLAG

#define MAGIC_MASK                      0xFFFF0000
#define TYPE_MASK                       0x00000FFF
#define DEPTH_MASK                      7

#define OAL_USAGE_CACHED 0x10000000
#define OAL_USAGE_NONCACHED 0x20000000
#define OAL_USAGE_FORCE_NOTFLUSH 0x40000000

#define VSDK_CV_MAT_CONT_FLAG_SHIFT  14
#define VSDK_CV_MAT_CONT_FLAG        (1 << VSDK_CV_MAT_CONT_FLAG_SHIFT)
#define VSDK_CV_IS_MAT_CONT(flags)   ((flags) & VSDK_CV_MAT_CONT_FLAG)
#define VSDK_CV_IS_CONT_MAT          VSDK_CV_IS_MAT_CONT
#define VSDK_CV_SUBMAT_FLAG_SHIFT    15
#define VSDK_CV_SUBMAT_FLAG          (1 << VSDK_CV_SUBMAT_FLAG_SHIFT)



#define VSDK_CV_AUTOSTEP             0x7fffffff

#define VSDK_CV_CN_MAX               512
#define VSDK_CV_CN_SHIFT             3
#define VSDK_CV_DEPTH_MAX            (1 << VSDK_CV_CN_SHIFT)

#define VSDK_CV_MAT_DEPTH_MASK       (VSDK_CV_DEPTH_MAX - 1)
#define VSDK_CV_MAT_DEPTH(flags)     ((flags) & VSDK_CV_MAT_DEPTH_MASK)

#define VSDK_CV_MAT_CN_MASK          ((VSDK_CV_CN_MAX - 1) << VSDK_CV_CN_SHIFT)
#define VSDK_CV_MAT_CN(flags)        ((((flags) & VSDK_CV_MAT_CN_MASK) >> VSDK_CV_CN_SHIFT) + 1)

#define VSDK_CV_ELEM_SIZE(type)      (VSDK_CV_MAT_CN(type) << ((((sizeof(size_t)/4+1)*16384|0x3a50) >> VSDK_CV_MAT_DEPTH(type)*2) & 3))

#define VSDK_CV_ELEM_SIZE1(type)     ((((sizeof(size_t)<<28)|0x8442211) >> VSDK_CV_MAT_DEPTH(type)*4) & 15)


int main(void)
{

	int x = VSDK_CV_ELEM_SIZE(MAGIC_VAL);
	return 0;
}
