#include <iostream>

using namespace std;

struct A
{
    int x;
    void test();
};

struct B
{
    int x;
    void test(B &b);
};

void test(B &a_b)
{
    cout << "hello" << endl;
}

void A::test()
{
    B b;
    b.test(*this);
}

void B::test(B &b)
{
    cout << "hello" << endl;
}

int main()
{
    A a;
    A *pa = &a;
    a.test();
    
    return 0;
}