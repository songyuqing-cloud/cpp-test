#include <string>

#ifndef _BASE64_H_
#define _BASE64_H_

std::string base64_encode(unsigned char const* , unsigned int len);
std::string base64_decode(std::string const& s);

extern void base64_encode_three(char *output, const char *input);
extern void base64_encode_last_two(char *output, const char *input);
extern void base64_encode_last_one(char *output, const char *input);
extern void base64_decode(const char *encoded_bytes, size_t encoded_byte_count,
                          uint8_t *decoded);
extern void base64url_encode_three(char *output, const char *input);
extern void base64url_encode_last_two(char *output, const char *input);
extern void base64url_encode_last_one(char *output, const char *input);
extern void base64url_decode(const char *encoded_bytes,
                             size_t encoded_byte_count, uint8_t *decoded);

#endif