/*
   base64.cpp and base64.h

   Copyright (C) 2004-2008 Ren� Nyffenegger

   This source code is provided 'as-is', without any express or implied
   warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
      claim that you wrote the original source code. If you use this source code
      in a product, an acknowledgment in the product documentation would be
      appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   Ren� Nyffenegger rene.nyffenegger@adp-gmbh.ch

*/

#include "base64.h"
#include <iostream>

static const std::string base64_chars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";


static inline bool is_base64(unsigned char c)
{
    return (isalnum(c) || (c == '+') || (c == '/'));
}

std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len)
{
    std::string ret;
    int i = 0, j = 0;
    unsigned char char_array_3[3], char_array_4[4];

    while (in_len--)
    {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3)
        {
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;

            for(i = 0; (i <4) ; i++)
            {
                ret += base64_chars[char_array_4[i]];
            }
            i = 0;
        }
    }

    if (i)
    {
        for(j = i; j < 3; j++)
        {
            char_array_3[j] = '\0';
        }

        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        char_array_4[3] = char_array_3[2] & 0x3f;

        for (j = 0; (j < i + 1); j++)
        {
            ret += base64_chars[char_array_4[j]];
        }

        while((i++ < 3))
        {
            ret += '=';
        }

    }

    return ret;

}

std::string base64_decode(std::string const& encoded_string)
{
    int in_len = encoded_string.size();
    int i = 0, j = 0, in_ = 0;
    unsigned char char_array_4[4], char_array_3[3];
    std::string ret;

    while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_]))
    {
        char_array_4[i++] = encoded_string[in_];
        in_++;
        if (i ==4)
        {
            for (i = 0; i <4; i++)
            {
                char_array_4[i] = base64_chars.find(char_array_4[i]);
            }

            char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
            char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

            for (i = 0; (i < 3); i++)
            {
                ret += char_array_3[i];
            }
            i = 0;
        }
    }

    if (i)
    {
        for (j = i; j <4; j++)
        {
            char_array_4[j] = 0;
        }

        for (j = 0; j <4; j++)
        {
            char_array_4[j] = base64_chars.find(char_array_4[j]);
        }

        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

        for (j = 0; (j < i - 1); j++)
        {
            ret += char_array_3[j];
        }
    }

    return ret;
}

extern void base64_encode_three(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    unsigned int bits1 = (unsigned char)(input[1]);
    unsigned int bits2 = (unsigned char)(input[2]);
    output[0] = base64_encode_six_bits(bits0 >> 2);
    output[1] = base64_encode_six_bits(((bits0 & 0x3) << 4) | (bits1 >> 4));
    output[2] = base64_encode_six_bits(((bits1 & 0xf) << 2) | (bits2 >> 6));
    output[3] = base64_encode_six_bits(bits2 & 0x3f);
}

extern void base64_encode_last_two(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    unsigned int bits1 = (unsigned char)(input[1]);
    output[0] = base64_encode_six_bits(bits0 >> 2);
    output[1] = base64_encode_six_bits(((bits0 & 0x3) << 4) | (bits1 >> 4));
    output[2] = base64_encode_six_bits((bits1 & 0xf) << 2);
    output[3] = '=';
    output[4] = 0;
}

extern void base64_encode_last_one(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    output[0] = base64_encode_six_bits(bits0 >> 2);
    output[1] = base64_encode_six_bits((bits0 & 0x3) << 4);
    output[2] = '=';
    output[3] = '=';
    output[4] = 0;
}

extern void base64_decode(const char *encoded_bytes, size_t encoded_byte_count,
                          uint8_t *decoded)
{
    assert(encoded_bytes != NULL);
    assert((encoded_byte_count % 4) == 0);
    assert(decoded != NULL);

    const char *follow_encoded = encoded_bytes;
    uint8_t *follow_decoded = decoded;
    for (size_t num = 0; num < (encoded_byte_count / 4) - 1; ++num)
    {
        uint8_t bits0 = base64_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64_decode_six_bits(follow_encoded[2]);
        uint8_t bits3 = base64_decode_six_bits(follow_encoded[3]);
        follow_encoded += 4;
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        *follow_decoded = ((bits2 & 0x3) << 6) | bits3;
        ++follow_decoded;
    }
    if (encoded_byte_count == 0)
        return;
    if (follow_encoded[2] == '=')
    {
        assert(follow_encoded[3] == '=');
        uint8_t bits0 = base64_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64_decode_six_bits(follow_encoded[1]);
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        assert((bits1 & 0xf) == 0);
    }
    else if (follow_encoded[3] == '=')
    {
        uint8_t bits0 = base64_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64_decode_six_bits(follow_encoded[2]);
        follow_encoded += 4;
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        assert((bits2 & 0x3) == 0);
    }
    else
    {
        uint8_t bits0 = base64_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64_decode_six_bits(follow_encoded[2]);
        uint8_t bits3 = base64_decode_six_bits(follow_encoded[3]);
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        *follow_decoded = ((bits2 & 0x3) << 6) | bits3;
        ++follow_decoded;
    }
}

extern void base64url_encode_three(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    unsigned int bits1 = (unsigned char)(input[1]);
    unsigned int bits2 = (unsigned char)(input[2]);
    output[0] = base64url_encode_six_bits(bits0 >> 2);
    output[1] = base64url_encode_six_bits(((bits0 & 0x3) << 4) | (bits1 >> 4));
    output[2] = base64url_encode_six_bits(((bits1 & 0xf) << 2) | (bits2 >> 6));
    output[3] = base64url_encode_six_bits(bits2 & 0x3f);
}

extern void base64url_encode_last_two(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    unsigned int bits1 = (unsigned char)(input[1]);
    output[0] = base64url_encode_six_bits(bits0 >> 2);
    output[1] = base64url_encode_six_bits(((bits0 & 0x3) << 4) | (bits1 >> 4));
    output[2] = base64url_encode_six_bits((bits1 & 0xf) << 2);
    output[3] = '=';
    output[4] = 0;
}

extern void base64url_encode_last_one(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    output[0] = base64url_encode_six_bits(bits0 >> 2);
    output[1] = base64url_encode_six_bits((bits0 & 0x3) << 4);
    output[2] = '=';
    output[3] = '=';
    output[4] = 0;
}

extern void base64url_decode(const char *encoded_bytes,
                             size_t encoded_byte_count, uint8_t *decoded)
{
    assert(encoded_bytes != NULL);
    assert((encoded_byte_count % 4) == 0);
    assert(decoded != NULL);

    const char *follow_encoded = encoded_bytes;
    uint8_t *follow_decoded = decoded;
    for (size_t num = 0; num < (encoded_byte_count / 4) - 1; ++num)
    {
        uint8_t bits0 = base64url_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64url_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64url_decode_six_bits(follow_encoded[2]);
        uint8_t bits3 = base64url_decode_six_bits(follow_encoded[3]);
        follow_encoded += 4;
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        *follow_decoded = ((bits2 & 0x3) << 6) | bits3;
        ++follow_decoded;
    }
    if (encoded_byte_count == 0)
        return;
    if (follow_encoded[2] == '=')
    {
        assert(follow_encoded[3] == '=');
        uint8_t bits0 = base64url_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64url_decode_six_bits(follow_encoded[1]);
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        assert((bits1 & 0xf) == 0);
    }
    else if (follow_encoded[3] == '=')
    {
        uint8_t bits0 = base64url_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64url_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64url_decode_six_bits(follow_encoded[2]);
        follow_encoded += 4;
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        assert((bits2 & 0x3) == 0);
    }
    else
    {
        uint8_t bits0 = base64url_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64url_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64url_decode_six_bits(follow_encoded[2]);
        uint8_t bits3 = base64url_decode_six_bits(follow_encoded[3]);
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        *follow_decoded = ((bits2 & 0x3) << 6) | bits3;
        ++follow_decoded;
    }
}

static char base64_encode_six_bits(unsigned int input)
{
    assert(input < 64);
    if (input < 26)
        return input + 'A';
    if (input < 52)
        return (input - 26) + 'a';
    if (input < 62)
        return (input - 52) + '0';
    if (input == 62)
        return '+';
    assert(input == 63);
    return '/';
}

static uint8_t base64_decode_six_bits(char input)
{
    if ((input >= 'A') && (input <= 'Z'))
        return input - 'A';
    if ((input >= 'a') && (input <= 'z'))
        return (input - 'a') + 26;
    if ((input >= '0') && (input <= '9'))
        return (input - '0') + 52;
    if (input == '+')
        return 62;
    assert(input == '/');
    return 63;
}

static char base64url_encode_six_bits(unsigned int input)
{
    assert(input < 64);
    if (input < 26)
        return input + 'A';
    if (input < 52)
        return (input - 26) + 'a';
    if (input < 62)
        return (input - 52) + '0';
    if (input == 62)
        return '-';
    assert(input == 63);
    return '_';
}

static uint8_t base64url_decode_six_bits(char input)
{
    if ((input >= 'A') && (input <= 'Z'))
        return input - 'A';
    if ((input >= 'a') && (input <= 'z'))
        return (input - 'a') + 26;
    if ((input >= '0') && (input <= '9'))
        return (input - '0') + 52;
    if (input == '-')
        return 62;
    assert(input == '_');
    return 63;
}