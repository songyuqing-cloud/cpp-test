/* file "GoldenRetriever.h" */

/* Copyright 2012-2015 SoundHound, Incorporated.  All rights reserved. */


#ifndef GOLDENRETRIEVER_H
#define GOLDENRETRIEVER_H


#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#include <poll.h>


class HTTPHandler
{
public:
    virtual void handleHTTPVersion(const char *version)  { }
    virtual void handleParameter(const char *key, const char *value)  { }
    virtual void handleContentByteCount(size_t byteCount)  { }
    virtual void handleHeaderDone(void)  { }
    virtual void handleContent(size_t byteCount, const void *bytes)  { }
    virtual void handleContentEnd(void)  { }

protected:
    HTTPHandler(void)  { }
    ~HTTPHandler(void)  { }
};

class HTTPRawWriter
{
protected:
    HTTPRawWriter(void)  { }

public:
    virtual ~HTTPRawWriter(void)  { }

    virtual void write(const char *to_write) = 0;
    virtual void write(const char *to_write, size_t byte_count) = 0;
    virtual ssize_t read(char *buffer, size_t byte_count) = 0;
    virtual void set_timeout_seconds(double seconds)  { }
};

class HTTPRawReader
{
protected:
    HTTPRawReader(void)  { }

public:
    virtual ~HTTPRawReader(void)  { }

    virtual char *readLine(void) = 0;
    virtual bool has_bytes(void) = 0;
    virtual ssize_t read_bytes(char *buffer, size_t byte_count) = 0;
    virtual void check_for_errors(void) = 0;
};

class HTTPSocketReader : public HTTPRawReader
{
private:
    int socket_num;
    std::vector<char> buffer;

public:
    HTTPSocketReader(int socket_num) : socket_num(socket_num)  { }
    ~HTTPSocketReader(void)
    {
        close(socket_num);
    }

    char *readLine(void)
    {
        buffer.clear();
        while (true)
        {
            char local_buffer[1];
            ssize_t num = read(socket_num, &(local_buffer[0]), 1);
            if (num != 1)
                break;
            if (local_buffer[0] == '\r')
                continue;
            if (local_buffer[0] == '\n')
                break;
            buffer.push_back(local_buffer[0]);
        }
        buffer.push_back(0);
        return &(buffer[0]);
    }
    bool has_bytes(void)
    {
        struct pollfd data;
        data.fd = socket_num;
        data.events = POLLIN;
        return (poll(&data, 1, 0) > 0);
    }
    ssize_t read_bytes(char *buffer, size_t byte_count)
    {
        return read(socket_num, buffer, byte_count);
    }
    virtual void check_for_errors(void)
    {
    }
};

class HTTPSocketWriter : public HTTPRawWriter
{
private:
    int socket_num;
    HTTPRawReader *reader;
    bool do_close;

public:
    HTTPSocketWriter(int socket_num, HTTPRawReader *reader, bool do_close) :
        socket_num(socket_num), reader(reader), do_close(do_close)
    {
        assert(reader != NULL);
    }
    ~HTTPSocketWriter(void)
    {
        if (do_close)
            close(socket_num);
    }

    void write(const char *to_write)
    {
        write(to_write, strlen(to_write));
    }
    void write(const char *to_write, size_t byte_count)
    {
        ssize_t write_result = ::write(socket_num, to_write, byte_count);
        if (write_result != byte_count)
            throw "Socket write error.";
    }
    ssize_t read(char *buffer, size_t byte_count)
    {
        return reader->read_bytes(buffer, byte_count);
    }
};

class GoldenRetriever
{
protected:
    bool have_content_length;
    unsigned long content_length;
    bool chunked;

    GoldenRetriever(void);
    ~GoldenRetriever(void)  { }

    void handle_header_line(char *this_line, HTTPHandler *handler);
    void handleParametersAndBody(HTTPHandler *handler, HTTPRawReader *reader);
    static void error(const char *format, ...);
};


#endif /* GOLDENRETRIEVER_H */
