/* file "Fetch.cpp" */

/* Copyright 2012-2015 SoundHound, Incorporated.  All rights reserved. */


#include "Fetch.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <limits.h>
#include <errno.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <sstream>
#include "openssl/ssl.h"
#include "pthread.h"
extern "C"
{
#define class CLASS
#include "include.h"
#include "string_index.h"
#include "file_parser.h"
#include "execute.h"
#include "standard_built_ins.h"
#include "trace.h"
#include "trace_channels.h"
#include "routine_instance.h"
#include "utility.h"
#undef class
}
#include "base64.h"
#define _PRINT printf
#include <utilsmacro.h>

class CannedCatalog
{
private:
    string_index *tail_index;
    std::vector<string_index *> all_tail_indexes;
    std::vector<std::string> all_file_names;

public:
    CannedCatalog(void)
    {
        tail_index = create_string_index();
        if (tail_index == NULL)
            throw "Out of memory.";
    }
    ~CannedCatalog(void)
    {
        destroy_string_index(tail_index);
        size_t name_count = all_tail_indexes.size();
        for (size_t name_num = 0; name_num < name_count; ++name_num)
            destroy_string_index(all_tail_indexes[name_num]);
    }

    FILE *open_item(const char *body, const char *url_tail,
                    const char *catalog_file_name)
    {
        string_index *body_index =
            (string_index *)(lookup_in_string_index(tail_index, url_tail));
        if (body_index == NULL)
        {
not_found:
            size_t body_length = strlen(body);
            char *buffer = new char[body_length + 100];
            if (body_length == 0)
                buffer[0] = 0;
            else
                sprintf(buffer, ", body \"%s\",", body);
            fprintf(stderr,
                    "Error: No canned output found for catalog \"%s\"%s and URL"
                    " tail \"%s\".\n", catalog_file_name, buffer, url_tail);
            delete[] buffer;
            return NULL;
        }
        void *position_pointer = lookup_in_string_index(body_index, body);
        if (position_pointer == NULL)
            goto not_found;
        const char *file_name =
            all_file_names[((size_t)position_pointer) - 1].c_str();
        if (strcmp(file_name, "api_error") == 0)
            return NULL;
        FILE *result = fopen(file_name, "rb");
        if (result == NULL)
        {
            fprintf(stderr, "Error opening file `%s' for reading: %s.\n",
                    file_name, strerror(errno));
        }
        return result;
    }

    void add_item(const char *body, const char *url_tail, const char *file_name)
    {
        string_index *body_index =
            (string_index *)(lookup_in_string_index(tail_index, url_tail));
        if (body_index == NULL)
        {
            body_index = create_string_index();
            if (body_index == NULL)
                throw "Out of memory.";
            all_tail_indexes.push_back(body_index);
            enter_into_string_index(tail_index, url_tail, body_index);
        }

        size_t position = all_file_names.size();
        all_file_names.push_back(file_name);
        enter_into_string_index(body_index, body, (void *)(position + 1));
    }
};

class SimpleFetchResult : public FetchResult
{
private:
    unsigned status_code;
    char *reason_phrase;
    bool own_reason_phrase;
    char *http_version;
    bool own_http_version;
    struct pair
    {
        char *key;
        char *value;
    };
    std::vector<pair> parameters;
    string_index *parameter_index;
    std::vector<unsigned char> content;
    size_t content_position;

public:
    SimpleFetchResult(void) : status_code(0), reason_phrase(NULL),
        own_reason_phrase(false), http_version(NULL),
        own_http_version(false), content_position(0)
    {
        parameter_index = create_string_index();
        if (parameter_index == NULL)
            throw "Out of memory.";
    }
    ~SimpleFetchResult(void)
    {
        if (own_reason_phrase)
            free(reason_phrase);
        if (own_http_version)
            free(http_version);
        size_t param_count = parameters.size();
        for (size_t param_num = 0; param_num < param_count; ++param_num)
        {
            free(parameters[param_num].key);
            free(parameters[param_num].value);
        }
        assert(parameter_index != NULL);
        destroy_string_index(parameter_index);
    }

    unsigned statusCode(void)
    {
        return status_code;
    }
    const char *reasonPhrase(void)
    {
        return reason_phrase;
    }
    const char *httpVersion(void)
    {
        return http_version;
    }
    size_t parameterCount(void)
    {
        return parameters.size();
    }
    const char *parameterKey(size_t paramNum)
    {
        assert(paramNum < parameters.size());
        return parameters[paramNum].key;
    }
    const char *parameterValue(size_t paramNum)
    {
        assert(paramNum < parameters.size());
        return parameters[paramNum].value;
    }
    const char *parameterLookup(const char *key)
    {
        if (!(exists_in_string_index(parameter_index, key)))
            return NULL;
        size_t index = (size_t)(lookup_in_string_index(parameter_index, key));
        assert(index < parameters.size());
        assert(strcmp(parameters[index].key, key) == 0);
        return parameters[index].value;
    }
    size_t contentByteCount(void)
    {
        return content.size();
    }
    const void *contentBytes(void)
    {
        return &(content[0]);
    }
    size_t readContent(void *buffer, size_t minBytes, size_t maxBytes)
    {
        if (content_position + minBytes > content.size())
        {
            throw "An attempt was made to read beyond the end of the "
            "FetchResult content.";
        }
        size_t result =
            ((content_position + maxBytes <= content.size()) ? maxBytes :
             content.size() - content_position);
        if (result > 0)
        {
            memcpy(buffer, &(content[content_position]), result);
            content_position += result;
        }
        return result;
    }
    bool contentDone(void)
    {
        return content_position == content.size();
    }

    void setStatusCode(unsigned new_status_code)
    {
        status_code = new_status_code;
    }
    void setReasonPhrase(const char *new_reason_phrase)
    {
        if (own_reason_phrase)
            free(reason_phrase);
        own_reason_phrase = false;
        reason_phrase = (char *)(malloc(strlen(new_reason_phrase) + 1));
        if (reason_phrase == NULL)
            throw "Out of memory.";
        own_reason_phrase = true;
        strcpy(reason_phrase, new_reason_phrase);
    }
    void setHTTPVersion(const char *new_http_version)
    {
        if (own_http_version)
            free(http_version);
        own_http_version = false;
        http_version = (char *)(malloc(strlen(new_http_version) + 1));
        if (http_version == NULL)
            throw "Out of memory.";
        own_http_version = true;
        strcpy(http_version, new_http_version);
    }
    void setParameter(const char *key, const char *value)
    {
        assert(key != NULL);
        assert(value != NULL);

        assert(value != NULL);
        char *new_value = (char *)(malloc(strlen(value) + 1));
        if (new_value == NULL)
            throw "Out of memory.";
        strcpy(new_value, value);

        if (exists_in_string_index(parameter_index, key))
        {
            size_t index =
                (size_t)(lookup_in_string_index(parameter_index, key));
            assert(index < parameters.size());
            free(parameters[index].value);
            parameters[index].value = new_value;
            return;
        }

        assert(key != NULL);
        char *new_key = (char *)(malloc(strlen(key) + 1));
        if (new_key == NULL)
        {
            free(new_value);
            throw "Out of memory.";
        }
        strcpy(new_key, key);

        size_t index = parameters.size();
        pair new_element;
        new_element.key = new_key;
        new_element.value = new_value;
        parameters.push_back(new_element);
        verdict the_verdict =
            enter_into_string_index(parameter_index, key, (void *)index);
        if (the_verdict != MISSION_ACCOMPLISHED)
            throw "Out of memory.";
    }
    void makeContentSpace(size_t byte_count)
    {
        content.reserve(byte_count);
    }
    void addContent(size_t byte_count, const void *bytes)
    {
        const char *typed = (const char *)bytes;
        for (size_t num = 0; num < byte_count; ++num)
            content.push_back(typed[num]);
    }
};

class SimpleFetchHandler : public FetchHandler
{
public:
    SimpleFetchHandler(SimpleFetchResult *init_result) : result(init_result)
    {
    }

    void handleStatusCode(unsigned code)
    {
        result->setStatusCode(code);
    };
    void handleReasonPhrase(const char *message)
    {
        result->setReasonPhrase(message);
    };
    void handleHTTPVersion(const char *version)
    {
        result->setHTTPVersion(version);
    };
    void handleParameter(const char *key, const char *value)
    {
        result->setParameter(key, value);
    };
    void handleContentByteCount(size_t byteCount)
    {
        result->makeContentSpace(byteCount);
    };
    void handleContent(size_t byteCount, const void *bytes)
    {
        result->addContent(byteCount, bytes);
    };
    void handleContentEnd(void)  { };

private:
    SimpleFetchResult *result;
};


static char base64_encode_six_bits(unsigned int input);
static uint8_t base64_decode_six_bits(char input);
static char base64url_encode_six_bits(unsigned int input);
static uint8_t base64url_decode_six_bits(char input);
static int open_socket_with_timeout(struct addrinfo *address_info_list,
                                    double timeout_seconds);
static CannedCatalog *find_catalog(const char *catalog_file_name,
                                   const char *executable_directory, size_t executable_directory_length);
static object *object_for_class_declaration(routine_declaration *declaration,
        context *the_context, purity_level *level, tracer *the_tracer,
        salmon_thread *root_thread, jumper *the_jumper);
static context *global_object_internal_context(object *global_object,
        jumper *the_jumper);
static CannedCatalog *fill_in_catalog_from_value(CannedCatalog *result,
        value *top_value, const char *catalog_file_name,
        const char *executable_directory);


static char default_user_agent[] = "GoldenRetriever/1.0";
static char user_agent_key[] = "User-Agent";
static char http_prefix[] = "http://";
static char https_prefix[] = "https://";
static char script_prefix[] = "script://";
static bool disabled = false;
static const char *disable_reason = NULL;
static string_index *catalog_index = NULL;
static pthread_mutex_t catalog_lock = PTHREAD_MUTEX_INITIALIZER;
static std::vector<CannedCatalog *> all_catalogs;


double Fetch::default_timeout_seconds = 10;


Fetch::Fetch(const char *init_url) : user_agent(default_user_agent),
    incremental_handler(NULL)
{
    incremental_state = IS_START;
    string_buffer_init(&incremental_buffer, 10);
    assert(init_url != NULL);
    url = (char *)(malloc(strlen(init_url) + 1));
    if (url == NULL)
        throw "Out of memory.";
    strcpy(url, init_url);

    writer = NULL;
    reader = NULL;

    timeout_seconds = default_timeout_seconds;

    parameter_index = create_string_index();
    if (parameter_index == NULL)
    {
        free(url);
        throw "Out of memory.";
    }
}

Fetch::~Fetch(void)
{
    assert(incremental_buffer.array != NULL);
    free(incremental_buffer.array);

    assert(url != NULL);
    free(url);

    if (writer != NULL)
        delete writer;
    if (reader != NULL)
        delete reader;

    assert(user_agent != NULL);
    if (user_agent != default_user_agent)
        free(user_agent);

    size_t param_count = parameters.size();
    for (size_t param_num = 0; param_num < param_count; ++param_num)
    {
        free(parameters[param_num].key);
        free(parameters[param_num].value);
    }
    assert(parameter_index != NULL);
    destroy_string_index(parameter_index);
}

void Fetch::setUserAgent(const char *new_user_agent)
{
    assert(user_agent != NULL);
    if (user_agent != default_user_agent)
        free(user_agent);

    assert(new_user_agent != NULL);
    user_agent = (char *)(malloc(strlen(new_user_agent) + 1));
    if (user_agent == NULL)
    {
        user_agent = default_user_agent;
        throw "Out of memory.";
    }
    strcpy(user_agent, new_user_agent);
}

void Fetch::setParameter(const char *key, const char *value)
{
    assert(key != NULL);
    assert(value != NULL);

    if (strcmp(key, user_agent_key) == 0)
    {
        setUserAgent(value);
        return;
    }

    assert(value != NULL);
    char *new_value = (char *)(malloc(strlen(value) + 1));
    if (new_value == NULL)
        throw "Out of memory.";
    strcpy(new_value, value);

    if (exists_in_string_index(parameter_index, key))
    {
        size_t index = (size_t)(lookup_in_string_index(parameter_index, key));
        assert(index < parameters.size());
        free(parameters[index].value);
        parameters[index].value = new_value;
        return;
    }

    assert(key != NULL);
    char *new_key = (char *)(malloc(strlen(key) + 1));
    if (new_key == NULL)
    {
        free(new_value);
        throw "Out of memory.";
    }
    strcpy(new_key, key);

    size_t index = parameters.size();
    pair new_element;
    new_element.key = new_key;
    new_element.value = new_value;
    parameters.push_back(new_element);
    verdict the_verdict =
        enter_into_string_index(parameter_index, key, (void *)index);
    if (the_verdict != MISSION_ACCOMPLISHED)
        throw "Out of memory.";
}

void Fetch::setBasicAuthentication(const char *username, const char *password)
{
#define PREFIX "Basic "

    size_t username_length = strlen(username);
    size_t password_length = strlen(password);
    size_t result_length = ((username_length + password_length + 3) / 3) * 4;

    size_t buffer_length = result_length + sizeof(PREFIX) + 1;
    char *buffer = (char *)(malloc(buffer_length));
    memcpy(buffer, PREFIX, sizeof(PREFIX) - 1);
    char *follow_out = buffer + (sizeof(PREFIX) - 1);
    size_t user_prefix_count = username_length / 3;
    const char *follow_in = username;
    for (size_t user_prefix_num = 0; user_prefix_num < user_prefix_count;
            ++user_prefix_num)
    {
        assert((follow_out + 4) < (buffer + buffer_length));
        base64_encode_three(follow_out, follow_in);
        follow_out += 4;
        follow_in += 3;
    }

    const char *follow_suffix;
    size_t suffix_count;
    char colon_buffer[3];
    switch (username_length % 3)
    {
    case 0:
    {
        colon_buffer[0] = ':';
        if (password[0] == 0)
        {
            assert((follow_out + 4) < (buffer + buffer_length));
            base64_encode_last_one(follow_out, &(colon_buffer[0]));
            follow_out += 4;
            suffix_count = 0;
        }
        else
        {
            colon_buffer[1] = password[0];
            if (password[1] == 0)
            {
                assert((follow_out + 4) < (buffer + buffer_length));
                base64_encode_last_two(follow_out, &(colon_buffer[0]));
                follow_out += 4;
                suffix_count = 0;
            }
            else
            {
                colon_buffer[2] = password[1];
                assert((follow_out + 4) < (buffer + buffer_length));
                base64_encode_three(follow_out, &(colon_buffer[0]));
                follow_out += 4;
                follow_suffix = password + 2;
                suffix_count = password_length - 2;
            }
        }
        break;
    }
    case 1:
    {
        colon_buffer[0] = follow_in[0];
        colon_buffer[1] = ':';
        if (password[0] == 0)
        {
            assert((follow_out + 4) < (buffer + buffer_length));
            base64_encode_last_two(follow_out, &(colon_buffer[0]));
            follow_out += 4;
            suffix_count = 0;
        }
        else
        {
            colon_buffer[2] = password[0];
            assert((follow_out + 4) < (buffer + buffer_length));
            base64_encode_three(follow_out, &(colon_buffer[0]));
            follow_out += 4;
            follow_suffix = password + 1;
            suffix_count = password_length - 1;
        }
        break;
    }
    case 2:
    {
        colon_buffer[0] = follow_in[0];
        colon_buffer[1] = follow_in[1];
        colon_buffer[2] = ':';
        assert((follow_out + 4) < (buffer + buffer_length));
        base64_encode_three(follow_out, &(colon_buffer[0]));
        follow_out += 4;
        follow_suffix = password;
        suffix_count = password_length;
        break;
    }
    default:
    {
        assert(false);
        break;
    }
    }

    while (suffix_count >= 3)
    {
        assert((follow_out + 4) < (buffer + buffer_length));
        base64_encode_three(follow_out, follow_suffix);
        follow_out += 4;
        follow_suffix += 3;
        suffix_count -= 3;
    }

    switch (suffix_count)
    {
    case 0:
        assert(follow_out < (buffer + buffer_length));
        *follow_out = 0;
        break;
    case 1:
        assert((follow_out + 4) < (buffer + buffer_length));
        base64_encode_last_one(follow_out, follow_suffix);
        break;
    case 2:
        assert((follow_out + 4) < (buffer + buffer_length));
        base64_encode_last_two(follow_out, follow_suffix);
        break;
    default:
        assert(false);
        break;
    }

    setParameter("Authorization", buffer);
    free(buffer);

#undef PREFIX
}

void Fetch::setTimeoutSeconds(double new_timeout_seconds)
{
    timeout_seconds = new_timeout_seconds;
}

FetchResult *Fetch::get(Method method)
{
    class Cleanup
    {
    public:
        SimpleFetchResult *result;
        ~Cleanup(void)
        {
            if (result != NULL)
                delete result;
        }
    };
    SimpleFetchResult *result = new SimpleFetchResult();
    Cleanup cleanup;
    cleanup.result = result;
    SimpleFetchHandler handler(result);
    get(&handler, method);
    cleanup.result = NULL;
    return result;
}

void Fetch::get(FetchHandler *handler, Method method)
{
    const char *method_name;
    switch (method)
    {
    case OPTIONS:
        method_name = "OPTIONS";
        break;
    case GET:
        method_name = "GET";
        break;
    case HEAD:
        method_name = "HEAD";
        break;
    case POST:
        method_name = "POST";
        break;
    case PUT:
        method_name = "PUT";
        break;
    case DELETE:
        method_name = "DELETE";
        break;
    case TRACE:
        method_name = "TRACE";
        break;
    case CONNECT:
        method_name = "CONNECT";
        break;
    default:
        assert(false);
    }
    doHeader(method_name);
    assert(reader != NULL);
    assert(writer != NULL);
    delete writer;
    writer = NULL;
    handleReply(handler);
}

FetchResult *Fetch::post(size_t contentByteCount, const void *contentBytes)
{
    SimpleFetchResult *result = new SimpleFetchResult();
    SimpleFetchHandler handler(result);
    post(contentByteCount, contentBytes, &handler);
    return result;
}

void Fetch::post(size_t contentByteCount, const void *contentBytes,
                 FetchHandler *handler)
{
    char buffer[(sizeof(size_t) * 3) + 1];
    sprintf(buffer, "%lu", (unsigned long)contentByteCount);
    setParameter("Content-Length", buffer);
    doHeader("POST");
    assert(reader != NULL);
    assert(writer != NULL);
    writer->write((const char *)contentBytes, contentByteCount);
    delete writer;
    writer = NULL;
    handleReply(handler);
}

void Fetch::startPost(const char *method, FetchHandler *handler)
{
    incremental_handler = handler;
    setParameter("Transfer-Encoding", "chunked");
    doHeader(method);
    assert(reader != NULL);
    assert(writer != NULL);
    check_for_increment_results();
}

void Fetch::continuePost(size_t contentByteCount, const void *contentBytes)
{
    check_for_increment_results();
    char buffer[(sizeof(size_t) * 3) + 1];
    sprintf(buffer, "%lx\r\n", (unsigned long)contentByteCount);
    writer->write(buffer);
    writer->write((const char *)contentBytes, contentByteCount);
    writer->write("\r\n");
}

FetchResult *Fetch::finishPost(void)
{
    SimpleFetchResult *result = new SimpleFetchResult();
    SimpleFetchHandler handler(result);
    finishPost(&handler);
    return result;
}

void Fetch::finishPost(FetchHandler *handler)
{
    assert((incremental_handler == NULL) || (incremental_handler == handler));
    writer->write("0\r\n\r\n");
    delete writer;
    writer = NULL;
    if (incremental_handler == NULL)
    {
        handleReply(handler);
    }
    else
    {
        while (true)
        {
            char buffer[2560];
            ssize_t actual = reader->read_bytes(&(buffer[0]), 2560);
            assert(actual <= 2560);
            if (actual <= 0)
                break;
            handle_incremental_bytes(actual, &(buffer[0]));
        }
        incremental_handler->handleContentEnd();
    }
}

FetchResult *Fetch::get(const char *url, const char *user_agent,
                        double timeout_seconds, Method method)
{
    Fetch fetch(url);
    if (user_agent != NULL)
        fetch.setUserAgent(user_agent);
    fetch.setTimeoutSeconds(timeout_seconds);
    return fetch.get(method);
}

void Fetch::get(FetchHandler *handler, const char *url, const char *user_agent,
                double timeout_seconds, Method method)
{
    Fetch fetch(url);
    if (user_agent != NULL)
        fetch.setUserAgent(user_agent);
    fetch.setTimeoutSeconds(timeout_seconds);
    fetch.get(handler, method);
}

FetchResult *Fetch::post(size_t contentByteCount, const void *contentBytes,
                         const char *url, const char *user_agent, double timeout_seconds)
{
    Fetch fetch(url);
    if (user_agent != NULL)
        fetch.setUserAgent(user_agent);
    fetch.setTimeoutSeconds(timeout_seconds);
    return fetch.post(contentByteCount, contentBytes);
}

void Fetch::post(size_t contentByteCount, const void *contentBytes,
                 FetchHandler *handler, const char *url, const char *user_agent,
                 double timeout_seconds)
{
    Fetch fetch(url);
    if (user_agent != NULL)
        fetch.setUserAgent(user_agent);
    fetch.setTimeoutSeconds(timeout_seconds);
    fetch.post(contentByteCount, contentBytes, handler);
}

HTTPRawWriter *Fetch::openRaw(const char *url, const char *method)
{
    Fetch *fetch = new Fetch(url);

    fetch->doPreHeader(method);
    assert(fetch->reader != NULL);
    assert(fetch->writer != NULL);

    size_t host_start;
    size_t host_length;
    unsigned port;
    size_t path_start;
    size_t path_length;
    bool is_ssl;
    bool is_script;
    parseURL(url, &host_start, &host_length, &port, &path_start, &path_length,
             &is_ssl, &is_script);

    fetch->writer->write(method);
    fetch->writer->write(" ");
    if (path_length == 0)
        fetch->writer->write("/");
    else
        fetch->writer->write(&(url[path_start]), path_length);
    fetch->writer->write(" HTTP/1.1\r\n");

    fetch->writer->write("Host: ");
    fetch->writer->write(&(url[host_start]), host_length);
    fetch->writer->write("\r\n");

    return fetch->writer;
}

void Fetch::parseURL(const char *url, size_t *host_start, size_t *host_length,
                     unsigned *port, size_t *path_start, size_t *path_length, bool *is_ssl,
                     bool *is_script)
{
    assert(url != NULL);
    assert(host_start != NULL);
    assert(host_length != NULL);
    assert(port != NULL);
    assert(path_start != NULL);
    assert(path_length != NULL);
    assert(is_ssl != NULL);

    const char *follow;
    if (strncmp(url, http_prefix, strlen(http_prefix)) == 0)
    {
        *is_ssl = false;
        if (is_script != NULL)
            *is_script = false;
        follow = url + strlen(http_prefix);
    }
    else if (strncmp(url, https_prefix, strlen(https_prefix)) == 0)
    {
        *is_ssl = true;
        if (is_script != NULL)
            *is_script = false;
        follow = url + strlen(https_prefix);
    }
    else if ((is_script != NULL) &&
             (strncmp(url, script_prefix, strlen(script_prefix)) == 0))
    {
        *is_script = true;
        follow = url + strlen(script_prefix);
    }
    else
    {
        throw "URL prefix not recognized.";
    }

    *host_start = follow - url;
    while (true)
    {
        if (*follow == 0)
        {
            *host_length = (follow - url) - *host_start;
            if ((is_script == NULL) || (!*is_script))
                *port = (*is_ssl ? 443 : 80);
            *path_start = follow - url;
            *path_length = 0;
            return;
        }
        if (*follow == ':')
        {
            *host_length = (follow - url) - *host_start;
            ++follow;
            unsigned local_port = 0;
            while ((*follow != 0) && (*follow != '/'))
            {
                char this_char = *follow;
                unsigned digit;
                if ((this_char >= '0') && (this_char <= '9'))
                    digit = this_char - '0';
                else
                    throw "Bad URL: non-digit in port.";
                if (local_port >= (((1 << 16) - digit) / 10))
                    throw "Overflow in port number.";
                local_port = ((local_port * 10) + digit);
                ++follow;
            }
            *port = local_port;
            break;
        }
        if (*follow == '/')
        {
            *host_length = (follow - url) - *host_start;
            if ((is_script == NULL) || (!*is_script))
                *port = (*is_ssl ? 443 : 80);
            break;
        }
        ++follow;
    }

    *path_start = follow - url;
    const char *end = follow;
    while (*end != 0)
        ++end;
    *path_length = end - follow;
    return;
}

void Fetch::doHeader(const char *method)
{
    LOGI("");
    doPreHeader(method);
    LOGI("");
    assert(reader != NULL);
    assert(writer != NULL);

    size_t host_start;
    size_t host_length;
    unsigned port;
    size_t path_start;
    size_t path_length;
    bool is_ssl;
    bool is_script;
    parseURL(url, &host_start, &host_length, &port, &path_start, &path_length,
             &is_ssl, &is_script);
    LOGI("%s %s %s", method, user_agent_key, user_agent);
    writer->write(method);
    writer->write(" ");
    if (path_length == 0)
        writer->write("/");
    else
        writer->write(&(url[path_start]), path_length);
    writer->write(" HTTP/1.1\r\n");

    writer->write("Host: ");
    writer->write(&(url[host_start]), host_length);
    writer->write("\r\n");

    writer->write(user_agent_key);
    writer->write(": ");
    writer->write(user_agent);
    writer->write("\r\n");

    size_t param_count = parameters.size();
    for (size_t param_num = 0; param_num < param_count; ++param_num)
    {
        writer->write(parameters[param_num].key);
        writer->write(": ");
        writer->write(parameters[param_num].value);
        writer->write("\r\n");
        LOGI("%s %s",parameters[param_num].key, parameters[param_num].value);
    }
    LOGI("%s",url);
    LOGI("%s",user_agent);
    writer->write("\r\n");
}

void Fetch::doPreHeader(const char *method)
{
    if (disabled)
    {
        fprintf(stderr,
                "ERROR: An attempt was made to hit an external API (URL %s) "
                "while such hits were disabled because %s.\n", url,
                disable_reason);
        exit(1);
    }

    size_t host_start;
    size_t host_length;
    unsigned port;
    size_t path_start;
    size_t path_length;
    bool is_ssl;
    bool is_script;
    parseURL(url, &host_start, &host_length, &port, &path_start, &path_length,
             &is_ssl, &is_script);

    class SocketWriter : public HTTPRawWriter
    {
    private:
        int socket_num;
        HTTPRawReader *reader;
        bool do_close;

    public:
        SocketWriter(int socket_num, HTTPRawReader *reader, bool do_close) :
            socket_num(socket_num), reader(reader), do_close(do_close)
        {
            assert(reader != NULL);
        }
        ~SocketWriter(void)
        {
            if (do_close)
                close(socket_num);
        }

        void write(const char *to_write)
        {
            write(to_write, strlen(to_write));
        }
        void write(const char *to_write, size_t byte_count)
        {
            ssize_t write_result = ::write(socket_num, to_write, byte_count);
            if (write_result != byte_count)
                throw "Socket write error.";
        }
        ssize_t read(char *buffer, size_t byte_count)
        {
            return reader->read_bytes(buffer, byte_count);
        }
    };

    if (is_script)
    {
        size_t token_count = 1;
        const char *follow = &(url[host_start]);
        while (follow < &(url[path_start + path_length]))
        {
            if (((*follow == ' ') || (*follow == '@')) &&
                    ((follow[1] != ' ') || (follow[1] != '@')))
            {
                ++token_count;
            }
            ++follow;
        }
        char **argv = new char *[token_count + 1];
        char *buffer = new char[((path_start + path_length) - host_start) + 1];
        memcpy(buffer, &(url[host_start]),
               (path_start + path_length) - host_start);
        buffer[(path_start + path_length) - host_start] = 0;
        argv[0] = buffer;
        size_t arg_num = 1;
        char *follow_buffer = buffer;
        while (follow_buffer <
                &(buffer[(path_start + path_length) - host_start]))
        {
            if ((*follow_buffer == ' ') || (*follow_buffer == '@'))
            {
                *follow_buffer = 0;
                if ((follow_buffer[1] != ' ') || (follow_buffer[1] != '@'))
                {
                    argv[arg_num] = &(follow_buffer[1]);
                    ++arg_num;
                }
            }
            ++follow_buffer;
        }
        assert(arg_num == token_count);
        argv[arg_num] = NULL;

        size_t length0 = strlen(argv[0]);
        if ((length0 >= 16) &&
                (strcmp(&(argv[0][length0 - 16]), "/canned_api.salm") == 0))
        {
            const char *fetch_if_not_found =
                getenv("TERRIER_CANNED_API_FETCH_IF_NOT_FOUND");
            if ((fetch_if_not_found != NULL) &&
                    (strcmp(fetch_if_not_found, "yes") == 0))
            {
                /* Let the script handle it in this case. */
                goto skip;
            }

            size_t used_argument_count = 1;

            if (arg_num < used_argument_count + 1)
                goto skip;

            if (strcmp(argv[used_argument_count], "-new-data-directory") == 0)
            {
                if (arg_num < used_argument_count + 3)
                    goto skip;
                used_argument_count += 2;
            }

            if (strcmp(argv[used_argument_count], "-real-url-base") == 0)
            {
                if (arg_num < used_argument_count + 3)
                    goto skip;
                used_argument_count += 2;
            }

            if (arg_num != used_argument_count + 2)
                goto skip;

            const char *catalog_file_name = argv[used_argument_count];
            const char *url_tail = argv[used_argument_count + 1];

            CannedCatalog *catalog =
                find_catalog(catalog_file_name, argv[0], length0 - 16);
            if (catalog == NULL)
            {
                delete[] argv;
                delete[] buffer;
                throw "Script returned non-zero exit status.";
            }

            class CannedReader : public HTTPRawReader
            {
            private:
                CannedCatalog *catalog;
                std::string catalog_file_name;
                std::string url_tail;
                std::stringstream body;
                FILE *fp;
                std::vector<char> buffer;

                void start_read(void)
                {
                    if (catalog == NULL)
                        return;
                    std::string body_string = body.str();
                    const char *body_chars = body_string.c_str();
                    size_t body_char_count = strlen(body_chars);
                    if ((body_char_count > 0) &&
                            (body_chars[body_char_count - 1] != '\n'))
                    {
                        body_string += "\n";
                        body_chars = body_string.c_str();
                    }
                    fp = catalog->open_item(body_chars, url_tail.c_str(),
                                            catalog_file_name.c_str());
                    catalog = NULL;
                    if (fp == NULL)
                        throw "Script returned non-zero exit status.";
                }

            public:
                CannedReader(CannedCatalog *catalog,
                             const char *catalog_file_name, const char *url_tail) :
                    catalog(catalog),
                    catalog_file_name(catalog_file_name),
                    url_tail(url_tail), fp(NULL)
                {
                    assert(catalog != NULL);
                }
                ~CannedReader(void)
                {
                    if (fp != NULL)
                        fclose(fp);
                }

                char *readLine(void)
                {
                    start_read();
                    buffer.clear();
                    while (true)
                    {
                        char local_buffer[1];
                        size_t num = fread(&(local_buffer[0]), 1, 1, fp);
                        if (num != 1)
                            break;
                        if (local_buffer[0] == '\r')
                            continue;
                        if (local_buffer[0] == '\n')
                            break;
                        buffer.push_back(local_buffer[0]);
                    }
                    buffer.push_back(0);
                    return &(buffer[0]);
                }
                bool has_bytes(void)
                {
                    struct pollfd data;
                    data.fd = fileno(fp);
                    data.events = POLLIN;
                    return (poll(&data, 1, 0) > 0);
                }
                ssize_t read_bytes(char *buffer, size_t byte_count)
                {
                    start_read();
                    return fread(buffer, 1, byte_count, fp);
                }
                void check_for_errors(void)
                {
                }

                void add_to_body(const char *to_add, size_t byte_count)
                {
                    body.write(to_add, byte_count);
                }
            };
            CannedReader *canned_reader =
                new CannedReader(catalog, catalog_file_name, url_tail);
            reader = canned_reader;

            class CannedWriter : public HTTPRawWriter
            {
            private:
                CannedReader *reader;
                enum state_type
                {
                    STATE_HEADER,
                    STATE_HEADER_LF,
                    STATE_HEADER_LF_CR,
                    STATE_BODY
                };
                state_type state;

            public:
                CannedWriter(CannedReader *reader) : reader(reader),
                    state(STATE_HEADER)  { }
                ~CannedWriter(void)  { }

                void write(const char *to_write)
                {
                    write(to_write, strlen(to_write));
                }
                void write(const char *to_write, size_t byte_count)
                {
                    if (state == STATE_BODY)
                    {
                        reader->add_to_body(to_write, byte_count);
                        return;
                    }
                    for (size_t num = 0; num < byte_count; ++num)
                    {
                        switch (to_write[num])
                        {
                        case '\r':
                            switch (state)
                            {
                            case STATE_HEADER:
                                break;
                            case STATE_HEADER_LF:
                                state = STATE_HEADER_LF_CR;
                                break;
                            case STATE_HEADER_LF_CR:
                                state = STATE_HEADER;
                                break;
                            default:
                                assert(false);
                            }
                            break;
                        case '\n':
                            switch (state)
                            {
                            case STATE_HEADER:
                                state = STATE_HEADER_LF;
                                break;
                            case STATE_HEADER_LF:
                            case STATE_HEADER_LF_CR:
                                state = STATE_BODY;
                                if (byte_count > num + 1)
                                {
                                    reader->add_to_body(
                                        &(to_write[num + 1]),
                                        byte_count - (num + 1));
                                }
                                return;
                            default:
                                assert(false);
                            }
                            break;
                        default:
                            state = STATE_HEADER;
                            break;
                        }
                    }
                }
                ssize_t read(char *buffer, size_t byte_count)
                {
                    return reader->read_bytes(buffer, byte_count);
                }
            };
            writer = new CannedWriter(canned_reader);

#ifdef SAVE_RESPONSES
            doSaveResponsesSetup();
#endif /* SAVE_RESPONSES */

            delete[] argv;
            delete[] buffer;
            return;
        }
skip:

        int incoming_pipe_fds[2];
        int retcode = pipe(incoming_pipe_fds);
        if (retcode != 0)
            error("Unable to open a pipe: %s.", strerror(errno));
        int outgoing_pipe_fds[2];
        retcode = pipe(outgoing_pipe_fds);
        if (retcode != 0)
            error("Unable to open a pipe: %s.", strerror(errno));
        pid_t fork_result = fork();
        if (fork_result == -1)
        {
            error("Unable to fork: %s.", strerror(errno));
        }
        else if (fork_result == 0)
        {
            /* This is the child. */
            close(incoming_pipe_fds[0]);
            close(1);
            int retcode = dup2(incoming_pipe_fds[1], 1);
            if (retcode == -1)
            {
                error("Unable to set standard out to a pipe: %s.",
                      strerror(errno));
            }

            close(outgoing_pipe_fds[1]);
            close(0);
            retcode = dup2(outgoing_pipe_fds[0], 0);
            if (retcode == -1)
            {
                error("Unable to set standard in to a pipe: %s.",
                      strerror(errno));
            }

            execvp(argv[0], argv);
            fprintf(stderr, "Failed attempting to execvp() on `%s': %s.\n",
                    argv[0], strerror(errno));
            exit(1);
        }
        else
        {
            /* This is the parent. */
            delete[] argv;
            delete[] buffer;
            close(incoming_pipe_fds[1]);
            close(outgoing_pipe_fds[0]);

            class ScriptReader : public HTTPSocketReader
            {
            private:
                pid_t script_pid;

            public:
                ScriptReader(int socket_num, pid_t script_pid) :
                    HTTPSocketReader(socket_num), script_pid(script_pid)
                { }
                ~ScriptReader(void)  { }

                void check_for_errors(void)
                {
                    int script_status;
                    pid_t wait_status = waitpid(script_pid, &script_status, 0);
                    if (wait_status == script_pid)
                    {
                        if (WIFEXITED(script_status))
                        {
                            if (WEXITSTATUS(script_status) != 0)
                                throw "Script returned non-zero exit status.";
                        }
                        else
                        {
                            if (WIFSIGNALED(script_status))
                            {
                                error("Script died from signal %lu.",
                                      (unsigned long)(WTERMSIG(script_status)));
                            }
                            error("Script died with exit status %lu.",
                                  (unsigned long)script_status);
                        }
                    }
                }
            };
            reader = new ScriptReader(incoming_pipe_fds[0], fork_result);

            writer = new SocketWriter(outgoing_pipe_fds[1], reader, true);

#ifdef SAVE_RESPONSES
            doSaveResponsesSetup();
#endif /* SAVE_RESPONSES */

            return;
        }
    }

    if (getenv("GOLDEN_RETRIEVER_SHOW_REAL_URL_HITS") != NULL)
        fprintf(stderr, "Golden Retriever hitting `%s'.\n", url);

    struct addrinfo *address_info_list;
    {
        char save = url[host_start + host_length];
        url[host_start + host_length] = 0;
        struct addrinfo hints;
        memset(&hints, 0, sizeof(struct addrinfo));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        LOGI("%s", &url[host_start]);
        int return_code = getaddrinfo(&(url[host_start]), NULL, &hints,
                                      &address_info_list);
        if (return_code != 0)
        {
            const char *message;
            switch (return_code)
            {
            case EAI_AGAIN:
                message =
                    "The name could not be resolved at this time.  "
                    "Future attempts may succeed.";
                break;
            case EAI_BADFLAGS:
                message = "The flags parameter had an invalid value.";
                break;
            case EAI_FAIL:
                message =
                    "A non-recoverable error occurred when attempting "
                    "to resolve the name.";
                break;
            case EAI_FAMILY:
                message = "The address family was not recognized.";
                break;
            case EAI_MEMORY:
                message =
                    "There was a memory allocation failure when trying "
                    "to allocate storage for the return value.";
                break;
            case EAI_NONAME:
                message = "The host name does not resolve.";
                break;
            case EAI_SERVICE:
                message =
                    "The service passed was not recognized for the "
                    "specified socket type.";
                break;
            case EAI_SOCKTYPE:
                message = "The intended socket type was not recognized.";
                break;
            case EAI_SYSTEM:
                message = strerror(errno);
                break;
            case EAI_OVERFLOW:
                message = "An argument buffer overflowed.";
            default:
                assert(false);
                break;
            }
            error("Host name `%.*s' not found: %s.", (int)host_length,
                  &(url[host_start]), message);
        }
        url[host_start + host_length] = save;
    }

    int socket_num =
        open_socket_with_timeout(address_info_list, timeout_seconds);
    int return_code = 0;
// #ifdef PROXYY_AUTHEN
    if(getenv("PROXY_AUTHEN") != NULL/* && atoi(getenv("PROXY_AUTHEN")) == 1 */)
    {
        LOGI("%s %s", getenv("PROXY_SERVER"), getenv("PROXY_PORT"));
        struct addrinfo hints;
        memset(&hints, 0, sizeof(struct addrinfo));
        hints.ai_family = AF_INET /*AF_UNSPEC*/;
        hints.ai_socktype = SOCK_STREAM;
        freeaddrinfo(address_info_list);
        return_code = getaddrinfo(getenv("PROXY_SERVER"), NULL, &hints, &address_info_list);
        LOGI("%d", return_code);
        ((struct sockaddr_in *)(address_info_list->ai_addr))->sin_port = htons(atoi(getenv("PROXY_PORT")));
        return_code = connect(socket_num, address_info_list->ai_addr, address_info_list->ai_addrlen);
        LOGI("%d", return_code);
        std::string autho = getenv("PROXY_USR_PWD");
        // string authorize = base64_encode((unsigned char const*)autho.c_str(), autho.size());
        std::string msg = "CONNECT api.houndify.com:443 HTTP/1.1\r\nProxy-authorization: Basic " + base64_encode((unsigned char const*)autho.c_str(), autho.size()) + "\r\n\r\n";
        LOGI("%s", msg.c_str());
        ssize_t bytes_sent = send(socket_num, msg.c_str(), msg.size(), 0);
        ssize_t bytes_recieved=0;
        char* incoming_data_buffer = (char *)malloc(1000);
        bytes_recieved = recv(socket_num, incoming_data_buffer,1000, 0);
        if (bytes_recieved == 0)
        {
            return_code = -1;
            LOGI("host shut down.");
        }
        if (bytes_recieved == -1)
        {
            return_code = -1;
            LOGI("ERROR: receive error!");
        }
        LOGI("%ld  bytes recieved", bytes_recieved);
        LOGI("%s", incoming_data_buffer);
        free(incoming_data_buffer);
    }
// #else
    else
    {
        /* @@@ */
        ((struct sockaddr_in *)(address_info_list->ai_addr))->sin_port = htons(port);
        /* @@@ */
        return_code = connect(socket_num, address_info_list->ai_addr,
                                        address_info_list->ai_addrlen);
    }
// #endif
    if (return_code != 0)
    {
        freeaddrinfo(address_info_list);
        throw "Unable to connect.";
    }

    if (!is_ssl)
    {
        freeaddrinfo(address_info_list);

        reader = new HTTPSocketReader(socket_num);

        writer = new SocketWriter(socket_num, reader, false);

#ifdef SAVE_RESPONSES
        doSaveResponsesSetup();
#endif /* SAVE_RESPONSES */

        return;
    }

    static bool initialized = false;
    if (!initialized)
    {
        LOGI("");
        SSL_library_init();
        initialized = true;
    }

    SSL_CTX *the_ctx = SSL_CTX_new(SSLv3_client_method());
    if (the_ctx == NULL)
    {
        freeaddrinfo(address_info_list);
        throw "Unable to create SSL CTX object.";
    }

    SSL *the_ssl = SSL_new(the_ctx);
    if (the_ssl == NULL)
    {
        SSL_CTX_free(the_ctx);
        freeaddrinfo(address_info_list);
        throw "Failed trying to create an SSL object";
    }

    int retcode = SSL_set_fd(the_ssl, socket_num);
    if (retcode != 1)
    {
        SSL_CTX_free(the_ctx);
        SSL_free(the_ssl);
        freeaddrinfo(address_info_list);
        throw "Failed trying to associate the SSL object with the stream.";
    }

    retcode = SSL_connect(the_ssl);
    if (retcode != 1)
    {
        LOGI("");
        SSL_CTX_free(the_ctx);
        SSL_free(the_ssl);
        close(socket_num);

        socket_num = open_socket_with_timeout(address_info_list,
                                              timeout_seconds);
        int return_code = 0;
// #ifdef PROXYY_AUTHEN
        if(getenv("PROXY_AUTHEN") != NULL)
        {
            struct addrinfo hints;
            memset(&hints, 0, sizeof(struct addrinfo));
            hints.ai_family = AF_UNSPEC;
            hints.ai_socktype = SOCK_STREAM;
            freeaddrinfo(address_info_list);
            return_code = getaddrinfo(getenv("PROXY_SERVER"), NULL, &hints, &address_info_list);
            ((struct sockaddr_in *)(address_info_list->ai_addr))->sin_port = htons(atoi(getenv("PROXY_PORT")));
            return_code = connect(socket_num, address_info_list->ai_addr, address_info_list->ai_addrlen);
            std::string autho = getenv("PROXY_USR_PWD");
            // string authorize = base64_encode((unsigned char const*)autho.c_str(), autho.size());
            std::string msg = "CONNECT api.houndify.com:443 HTTP/1.1\r\nProxy-authorization: Basic " + base64_encode((unsigned char const*)autho.c_str(), autho.size()) + "\r\n\r\n";
            LOGI("%s", msg.c_str());
            ssize_t bytes_sent = send(socket_num, msg.c_str(), msg.size(), 0);
            ssize_t bytes_recieved=0;
            char* incoming_data_buffer = (char *)malloc(1000);
            bytes_recieved = recv(socket_num, incoming_data_buffer,1000, 0);
            if (bytes_recieved == 0)
            {
                return_code = -1;
                LOGI("host shut down.");
            }
            if (bytes_recieved == -1)
            {
                return_code = -1;
                LOGI("ERROR: receive error!");
            }
            LOGI("%ld  bytes recieved", bytes_recieved);
            LOGI("%s", incoming_data_buffer);
            free(incoming_data_buffer);
        }
// #else
        else
        {
            return_code = connect(socket_num, address_info_list->ai_addr,
                                            address_info_list->ai_addrlen);
        }
// #endif
        freeaddrinfo(address_info_list);
        if (return_code != 0)
            throw "Unable to connect.";

        the_ctx = SSL_CTX_new(SSLv23_client_method());
        if (the_ctx == NULL)
            throw "Unable to create SSL CTX object.";

        the_ssl = SSL_new(the_ctx);
        if (the_ssl == NULL)
        {
            SSL_CTX_free(the_ctx);
            throw "Failed trying to create an SSL object";
        }

        int retcode = SSL_set_fd(the_ssl, socket_num);
        if (retcode != 1)
        {
            SSL_CTX_free(the_ctx);
            SSL_free(the_ssl);
            throw "Failed trying to associate the SSL object with the stream.";
        }

        retcode = SSL_connect(the_ssl);
        if (retcode != 1)
        {
            LOGI("");
            SSL_CTX_free(the_ctx);
            SSL_free(the_ssl);
            throw "SSL connection failed.";
        }
    }
    else
    {
        freeaddrinfo(address_info_list);
    }

    class SSLReader : public HTTPRawReader
    {
    private:
        SSL *the_ssl;
        SSL_CTX *the_ctx;
        int socket_num;
        std::vector<char> buffer;

    public:
        SSLReader(SSL *the_ssl, int socket_num, SSL_CTX *the_ctx) :
            the_ssl(the_ssl), socket_num(socket_num), the_ctx(the_ctx)
        {
            assert(the_ssl != NULL);
        }
        ~SSLReader(void)
        {
            SSL_CTX_free(the_ctx);
            SSL_free(the_ssl);
            close(socket_num);
        }

        char *readLine(void)
        {
            buffer.clear();
            while (true)
            {
                char local_buffer[1];
                ssize_t num = SSL_read(the_ssl, &(local_buffer[0]), 1);
                if (num != 1)
                    break;
                if (local_buffer[0] == '\r')
                    continue;
                if (local_buffer[0] == '\n')
                    break;
                buffer.push_back(local_buffer[0]);
            }
            buffer.push_back(0);
            return &(buffer[0]);
        }
        bool has_bytes(void)
        {
            if (SSL_pending(the_ssl) > 0)
                return true;
            struct pollfd data;
            data.fd = socket_num;
            data.events = POLLIN;
            return (poll(&data, 1, 0) > 0);
        }
        ssize_t read_bytes(char *buffer, size_t byte_count)
        {
            return SSL_read(the_ssl, buffer, byte_count);
        }
        void check_for_errors(void)
        {
        }
    };
    reader = new SSLReader(the_ssl, socket_num, the_ctx);

    class SSLWriter : public HTTPRawWriter
    {
    private:
        SSL *the_ssl;
        HTTPRawReader *reader;

    public:
        SSLWriter(SSL *the_ssl, HTTPRawReader *reader) : the_ssl(the_ssl),
            reader(reader)
        {
            assert(the_ssl != NULL);
            assert(reader != NULL);
        }
        ~SSLWriter(void)  { }

        void write(const char *to_write)
        {
            write(to_write, strlen(to_write));
        }
        void write(const char *to_write, size_t byte_count)
        {
            ssize_t write_result = SSL_write(the_ssl, to_write, byte_count);
            if (write_result != byte_count)
                throw "SSL socket write error.";
        }
        ssize_t read(char *buffer, size_t byte_count)
        {
            return reader->read_bytes(buffer, byte_count);
        }
    };
    writer = new SSLWriter(the_ssl, reader);

#ifdef SAVE_RESPONSES
    doSaveResponsesSetup();
#endif /* SAVE_RESPONSES */
}

#ifdef SAVE_RESPONSES
void Fetch::doSaveResponsesSetup(void)
{
    assert(reader != NULL);

    if ((getenv("GOLDEN_RETRIEVER_SAVE_PREFIX") != NULL) &&
            (getenv("GOLDEN_RETRIEVER_SAVE_CATALOG") != NULL))
    {
        class SaveReader : public HTTPRawReader
        {
        private:
            HTTPRawReader *next;
            FILE *save_fp;

        public:
            SaveReader(HTTPRawReader *next, const char *url)
            {
                assert(next != NULL);
                assert(url != NULL);

                char *prefix = getenv("GOLDEN_RETRIEVER_SAVE_PREFIX");
                char *save_file_name = new char[strlen(prefix) + 200];
                time_t the_time_t = time(NULL);
                struct tm *the_tm = localtime(&the_time_t);
                static unsigned long counter = 0;
                ++counter;
                sprintf(save_file_name,
                        "%s_%lu_%02d_%02d_%02d_%02d_%02d_%04lu.txt", prefix,
                        (unsigned long)(the_tm->tm_year + 1900),
                        (int)(the_tm->tm_mon + 1), (int)(the_tm->tm_mday),
                        (int)(the_tm->tm_hour), (int)(the_tm->tm_min),
                        (int)(the_tm->tm_sec), counter);
                save_fp = fopen(save_file_name, "w");
                if (save_fp == NULL)
                {
                    fprintf(stderr,
                            "Warning: Failed trying to open save file %s: "
                            "%s.\n", save_file_name, strerror(errno));
                }
                FILE *catalog_fp =
                    fopen(getenv("GOLDEN_RETRIEVER_SAVE_CATALOG"), "a");
                if (catalog_fp == NULL)
                {
                    fprintf(stderr,
                            "Warning: Failed trying to open save catalog file "
                            "%s: %s.\n",
                            getenv("GOLDEN_RETRIEVER_SAVE_CATALOG"),
                            strerror(errno));
                }
                else
                {
                    fprintf(catalog_fp, "  [\"%s\", \"%s\"],\n", url,
                            save_file_name);
                    fclose(catalog_fp);
                }
                delete[] save_file_name;
            }
            ~SaveReader(void)
            {
                if (save_fp != NULL)
                    fclose(save_fp);
            }

            char *readLine(void)
            {
                char *result = next->readLine();
                if (save_fp != NULL)
                    fprintf(save_fp, "%s\n", result);
                return result;
            }
            bool has_bytes(void)
            {
                return next->has_bytes();
            }
            ssize_t read_bytes(char *buffer, size_t byte_count)
            {
                ssize_t result = next->read_bytes(buffer, byte_count);
                if ((save_fp != NULL) && (result > 0))
                    fwrite(buffer, 1, result, save_fp);
                return result;
            }
            void check_for_errors(void)
            {
                next->check_for_errors();
            }
        };

        reader = new SaveReader(reader, url);
    }
}
#endif /* SAVE_RESPONSES */

void Fetch::handleReply(FetchHandler *handler)
{
    assert(reader != NULL);

    LOGI("");
    handle_first_line(reader->readLine(), handler);
    LOGI("");
    handleParametersAndBody(handler, reader);
    LOGI("");
}

void Fetch::check_for_increment_results(void)
{
    if (incremental_handler == NULL)
        return;
    while (reader->has_bytes())
    {
        char buffer[2560];
        ssize_t actual = reader->read_bytes(&(buffer[0]), 2560);
        assert(actual <= 2560);
        if (actual <= 0)
            break;
        handle_incremental_bytes(actual, &(buffer[0]));
    }
}

void Fetch::handle_incremental_bytes(size_t count, char *data)
{
    assert(incremental_handler != NULL);

    size_t count_left = count;
    char *data_left = data;
    while (count_left > 0)
    {
        switch (incremental_state)
        {
        case IS_START:
        case IS_HEADERS:
        {
            char *follow = data_left;
            size_t remainder = count_left;
            while ((remainder > 0) && (*follow != '\n'))
            {
                ++follow;
                --remainder;
            }
            if (remainder > 0)
            {
                char *line;
                size_t count;
                if (incremental_buffer.element_count > 0)
                {
                    string_buffer_append_array(&incremental_buffer,
                                               remainder, data_left);
                    string_buffer_append(&incremental_buffer, 0);
                    line = incremental_buffer.array;
                    count = incremental_buffer.element_count;
                }
                else
                {
                    line = data_left;
                    count = follow - data_left;
                    *follow = 0;
                }
                if ((count > 0) && (line[count - 1] == '\r'))
                    line[count - 1] = 0;
                if (line[0] == 0)
                {
                    incremental_handler->handleHeaderDone();
                    if (chunked)
                    {
                        chunk_count_remaining = 0;
                        incremental_state = IS_CHUNK_COUNT;
                    }
                    else
                    {
                        incremental_state = IS_BODY;
                    }
                }
                else if (incremental_state == IS_START)
                {
                    handle_first_line(line, incremental_handler);
                    incremental_state = IS_HEADERS;
                }
                else
                {
                    handle_header_line(line, incremental_handler);
                }
                incremental_buffer.element_count = 0;
                data_left = follow + 1;
                count_left = remainder - 1;
                continue;
            }
            string_buffer_append_array(&incremental_buffer, count_left,
                                       data_left);
            return;
        }
        case IS_BODY:
        {
            if (!chunked)
            {
                incremental_handler->handleContent(count_left, data_left);
                return;
            }
            if (chunk_count_remaining > count_left)
            {
                chunk_count_remaining -= count_left;
                incremental_handler->handleContent(count_left, data_left);
                return;
            }
            incremental_handler->handleContent(chunk_count_remaining,
                                               data_left);
            data_left += chunk_count_remaining;
            count_left -= chunk_count_remaining;
            chunk_count_remaining = 0;
            incremental_state = IS_CHUNK_COUNT;
            continue;
        }
        case IS_CHUNK_COUNT:
        {
            assert(chunked);
            switch (*data_left)
            {
            case '\n':
                incremental_state = IS_BODY;
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                chunk_count_remaining *= 16;
                chunk_count_remaining += *data_left - '0';
                break;
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
                chunk_count_remaining *= 16;
                chunk_count_remaining += (*data_left - 'a') + 0xa;
                break;
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
                chunk_count_remaining *= 16;
                chunk_count_remaining += (*data_left - 'A') + 0xa;
                break;
            default:
                break;
            }
            ++data_left;
            --count_left;
            continue;
        }
        default:
        {
            assert(false);
        }
        }
    }
}

void Fetch::handle_first_line(char *first_line, FetchHandler *handler)
{
    char *follow = first_line;
    while (*follow != ' ')
    {
        if (*follow == 0)
        {
            reader->check_for_errors();
            error("Bad first line in response: %s.", first_line);
        }
        ++follow;
    }
    *follow = 0;
    ++follow;
    handler->handleHTTPVersion(first_line);
    unsigned code = 0;
    for (size_t num = 0; num < 3; ++num)
    {
        if ((*follow < '0') || (*follow > '9'))
            throw "Bad first line in response.";
        code = ((code * 10) + (*follow - '0'));
        ++follow;
    }
    handler->handleStatusCode(code);
    have_content_length = (code == 204);
    if (*follow != ' ')
        throw "Bad first line in response.";
    ++follow;
    const char *reason_phrase = follow;
    while (*follow != 0)
        ++follow;
    handler->handleReasonPhrase(reason_phrase);
}

GoldenRetriever::GoldenRetriever(void) : have_content_length(false),
    content_length(0), chunked(false)
{
}

void GoldenRetriever::handle_header_line(char *this_line, HTTPHandler *handler)
{
    char *follow = this_line;
    while (*follow != ':')
        ++follow;
    *follow = 0;
    ++follow;
    while (*follow == ' ')
        ++follow;
    handler->handleParameter(this_line, follow);

    if (strcmp(this_line, "Content-Length") == 0)
    {
        content_length = 0;
        while (*follow != 0)
        {
            if ((*follow < '0') || (*follow > '9'))
                throw "Bad Content-Length field.";
            if (content_length > ((ULONG_MAX - (*follow - '0')) / 10))
                throw "Overflow in Content-Length field.";
            content_length = ((content_length * 10) + (*follow - '0'));
            ++follow;
        }
        have_content_length = true;
    }
    else if (strcmp(this_line, "Transfer-Encoding") == 0)
    {
        if (strcmp(follow, "chunked") != 0)
            throw "Unsupported Transfer-Encoding field.";
        chunked = true;
    }
}

void GoldenRetriever::handleParametersAndBody(HTTPHandler *handler,
        HTTPRawReader *reader)
{
    while (true)
    {
        char *this_line = reader->readLine();
        if (this_line[0] == 0)
            break;
        handle_header_line(this_line, handler);
    }

    handler->handleHeaderDone();

    if (chunked)
    {
        while (true)
        {
            unsigned long chunk_byte_count = 0;
            while (true)
            {
                char buffer[1];
                ssize_t num = reader->read_bytes(&(buffer[0]), 1);
                if (num < 1)
                    throw "Unexpected end of chunk header.";
                if (buffer[0] == '\r')
                    break;
                if (buffer[0] == ' ')
                    continue;
                unsigned long digit;
                if ((buffer[0] >= '0') && (buffer[0] <= '9'))
                    digit = buffer[0] - '0';
                else if ((buffer[0] >= 'a') && (buffer[0] <= 'f'))
                    digit = (buffer[0] - 'a') + 0xa;
                else if ((buffer[0] >= 'A') && (buffer[0] <= 'F'))
                    digit = (buffer[0] - 'A') + 0xa;
                else
                    throw "Bad digit in chunk header.";
                if (chunk_byte_count >= (ULONG_MAX - digit) / 16)
                    throw "Overflow in chunk header.";
                chunk_byte_count = (chunk_byte_count * 16) + digit;
            }

            if (chunk_byte_count == 0)
                break;

            {
                char buffer[1];
                ssize_t num = reader->read_bytes(&(buffer[0]), 1);
                if (num < 1)
                    throw "Unexpected end of chunk header.";
                if (buffer[0] != '\n')
                    throw "Bad end-of-line in chunk header.";
            }

            while (chunk_byte_count > 0)
            {
                char buffer[256];
                ssize_t num = reader->read_bytes(&(buffer[0]),
                                                 ((chunk_byte_count > 256) ? 256 : chunk_byte_count));
                if (num < 1)
                    break;
                handler->handleContent((size_t)num, &(buffer[0]));
                chunk_byte_count -= num;
            }

            {
                char buffer[1];
                ssize_t num = reader->read_bytes(&(buffer[0]), 1);
                if (num < 1)
                    throw "Unexpected end of chunk.";
                if (buffer[0] != '\r')
                    throw "Bad end-of-line in chunk.";
            }

            {
                char buffer[1];
                ssize_t num = reader->read_bytes(&(buffer[0]), 1);
                if (num < 1)
                    throw "Unexpected end of chunk.";
                if (buffer[0] != '\n')
                    throw "Bad end-of-line in chunk.";
            }
        }

        /* @@@ */
        /* @@@ -- Read the trailer. */
        /* @@@ */
    }
    else if (have_content_length)
    {
        while (content_length > 0)
        {
            char buffer[256];
            ssize_t num = reader->read_bytes(&(buffer[0]),
                                             ((content_length > 256) ? 256 : content_length));
            if (num < 1)
                break;
            assert((unsigned long)num <= content_length);
            handler->handleContent((size_t)num, &(buffer[0]));
            content_length -= num;
        }
    }
    else
    {
        while (true)
        {
            char buffer[256];
            ssize_t num = reader->read_bytes(&(buffer[0]), 256);
            if (num < 1)
                break;
            handler->handleContent((size_t)num, &(buffer[0]));
        }
    }
    handler->handleContentEnd();
}

void GoldenRetriever::error(const char *format, ...)
{
    va_list ap;

    va_start(ap, format);
    vfprintf(stderr, format, ap);
    va_end(ap);
    fprintf(stderr, "\n");
    throw format;
}

extern void disable_fetches(const char *reason)
{
    disable_reason = reason;
    disabled = true;
}

extern void enable_fetches(void)
{
    disabled = false;
}

extern bool fetches_are_enabled(void)
{
    return !disabled;
}

extern const char *fetch_disable_reason(void)
{
    return disable_reason;
}

extern void base64_encode_three(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    unsigned int bits1 = (unsigned char)(input[1]);
    unsigned int bits2 = (unsigned char)(input[2]);
    output[0] = base64_encode_six_bits(bits0 >> 2);
    output[1] = base64_encode_six_bits(((bits0 & 0x3) << 4) | (bits1 >> 4));
    output[2] = base64_encode_six_bits(((bits1 & 0xf) << 2) | (bits2 >> 6));
    output[3] = base64_encode_six_bits(bits2 & 0x3f);
}

extern void base64_encode_last_two(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    unsigned int bits1 = (unsigned char)(input[1]);
    output[0] = base64_encode_six_bits(bits0 >> 2);
    output[1] = base64_encode_six_bits(((bits0 & 0x3) << 4) | (bits1 >> 4));
    output[2] = base64_encode_six_bits((bits1 & 0xf) << 2);
    output[3] = '=';
    output[4] = 0;
}

extern void base64_encode_last_one(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    output[0] = base64_encode_six_bits(bits0 >> 2);
    output[1] = base64_encode_six_bits((bits0 & 0x3) << 4);
    output[2] = '=';
    output[3] = '=';
    output[4] = 0;
}

extern void base64_decode(const char *encoded_bytes, size_t encoded_byte_count,
                          uint8_t *decoded)
{
    assert(encoded_bytes != NULL);
    assert((encoded_byte_count % 4) == 0);
    assert(decoded != NULL);

    const char *follow_encoded = encoded_bytes;
    uint8_t *follow_decoded = decoded;
    for (size_t num = 0; num < (encoded_byte_count / 4) - 1; ++num)
    {
        uint8_t bits0 = base64_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64_decode_six_bits(follow_encoded[2]);
        uint8_t bits3 = base64_decode_six_bits(follow_encoded[3]);
        follow_encoded += 4;
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        *follow_decoded = ((bits2 & 0x3) << 6) | bits3;
        ++follow_decoded;
    }
    if (encoded_byte_count == 0)
        return;
    if (follow_encoded[2] == '=')
    {
        assert(follow_encoded[3] == '=');
        uint8_t bits0 = base64_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64_decode_six_bits(follow_encoded[1]);
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        assert((bits1 & 0xf) == 0);
    }
    else if (follow_encoded[3] == '=')
    {
        uint8_t bits0 = base64_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64_decode_six_bits(follow_encoded[2]);
        follow_encoded += 4;
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        assert((bits2 & 0x3) == 0);
    }
    else
    {
        uint8_t bits0 = base64_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64_decode_six_bits(follow_encoded[2]);
        uint8_t bits3 = base64_decode_six_bits(follow_encoded[3]);
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        *follow_decoded = ((bits2 & 0x3) << 6) | bits3;
        ++follow_decoded;
    }
}

extern void base64url_encode_three(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    unsigned int bits1 = (unsigned char)(input[1]);
    unsigned int bits2 = (unsigned char)(input[2]);
    output[0] = base64url_encode_six_bits(bits0 >> 2);
    output[1] = base64url_encode_six_bits(((bits0 & 0x3) << 4) | (bits1 >> 4));
    output[2] = base64url_encode_six_bits(((bits1 & 0xf) << 2) | (bits2 >> 6));
    output[3] = base64url_encode_six_bits(bits2 & 0x3f);
}

extern void base64url_encode_last_two(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    unsigned int bits1 = (unsigned char)(input[1]);
    output[0] = base64url_encode_six_bits(bits0 >> 2);
    output[1] = base64url_encode_six_bits(((bits0 & 0x3) << 4) | (bits1 >> 4));
    output[2] = base64url_encode_six_bits((bits1 & 0xf) << 2);
    output[3] = '=';
    output[4] = 0;
}

extern void base64url_encode_last_one(char *output, const char *input)
{
    unsigned int bits0 = (unsigned char)(input[0]);
    output[0] = base64url_encode_six_bits(bits0 >> 2);
    output[1] = base64url_encode_six_bits((bits0 & 0x3) << 4);
    output[2] = '=';
    output[3] = '=';
    output[4] = 0;
}

extern void base64url_decode(const char *encoded_bytes,
                             size_t encoded_byte_count, uint8_t *decoded)
{
    assert(encoded_bytes != NULL);
    assert((encoded_byte_count % 4) == 0);
    assert(decoded != NULL);

    const char *follow_encoded = encoded_bytes;
    uint8_t *follow_decoded = decoded;
    for (size_t num = 0; num < (encoded_byte_count / 4) - 1; ++num)
    {
        uint8_t bits0 = base64url_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64url_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64url_decode_six_bits(follow_encoded[2]);
        uint8_t bits3 = base64url_decode_six_bits(follow_encoded[3]);
        follow_encoded += 4;
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        *follow_decoded = ((bits2 & 0x3) << 6) | bits3;
        ++follow_decoded;
    }
    if (encoded_byte_count == 0)
        return;
    if (follow_encoded[2] == '=')
    {
        assert(follow_encoded[3] == '=');
        uint8_t bits0 = base64url_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64url_decode_six_bits(follow_encoded[1]);
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        assert((bits1 & 0xf) == 0);
    }
    else if (follow_encoded[3] == '=')
    {
        uint8_t bits0 = base64url_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64url_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64url_decode_six_bits(follow_encoded[2]);
        follow_encoded += 4;
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        assert((bits2 & 0x3) == 0);
    }
    else
    {
        uint8_t bits0 = base64url_decode_six_bits(follow_encoded[0]);
        uint8_t bits1 = base64url_decode_six_bits(follow_encoded[1]);
        uint8_t bits2 = base64url_decode_six_bits(follow_encoded[2]);
        uint8_t bits3 = base64url_decode_six_bits(follow_encoded[3]);
        *follow_decoded = (bits0 << 2) | (bits1 >> 4);
        ++follow_decoded;
        *follow_decoded = ((bits1 & 0xf) << 4) | (bits2 >> 2);
        ++follow_decoded;
        *follow_decoded = ((bits2 & 0x3) << 6) | bits3;
        ++follow_decoded;
    }
}

extern void clean_up_fetch(void)
{
    pthread_mutex_lock(&catalog_lock);

    if (catalog_index != NULL)
    {
        destroy_string_index(catalog_index);
        catalog_index = NULL;
        size_t count = all_catalogs.size();
        for (size_t num = 0; num < count; ++num)
            delete all_catalogs[num];
        all_catalogs.clear();
    }

    pthread_mutex_unlock(&catalog_lock);
}


static char base64_encode_six_bits(unsigned int input)
{
    assert(input < 64);
    if (input < 26)
        return input + 'A';
    if (input < 52)
        return (input - 26) + 'a';
    if (input < 62)
        return (input - 52) + '0';
    if (input == 62)
        return '+';
    assert(input == 63);
    return '/';
}

static uint8_t base64_decode_six_bits(char input)
{
    if ((input >= 'A') && (input <= 'Z'))
        return input - 'A';
    if ((input >= 'a') && (input <= 'z'))
        return (input - 'a') + 26;
    if ((input >= '0') && (input <= '9'))
        return (input - '0') + 52;
    if (input == '+')
        return 62;
    assert(input == '/');
    return 63;
}

static char base64url_encode_six_bits(unsigned int input)
{
    assert(input < 64);
    if (input < 26)
        return input + 'A';
    if (input < 52)
        return (input - 26) + 'a';
    if (input < 62)
        return (input - 52) + '0';
    if (input == 62)
        return '-';
    assert(input == 63);
    return '_';
}

static uint8_t base64url_decode_six_bits(char input)
{
    if ((input >= 'A') && (input <= 'Z'))
        return input - 'A';
    if ((input >= 'a') && (input <= 'z'))
        return (input - 'a') + 26;
    if ((input >= '0') && (input <= '9'))
        return (input - '0') + 52;
    if (input == '-')
        return 62;
    assert(input == '_');
    return 63;
}

static int open_socket_with_timeout(struct addrinfo *address_info_list,
                                    double timeout_seconds)
{
    int socket_num = socket(AF_INET, address_info_list->ai_socktype,
                            address_info_list->ai_protocol);
    if (socket_num == -1)
    {
        freeaddrinfo(address_info_list);
        throw "Unable to open a socket.";
    }

    if (timeout_seconds > 0)
    {
        struct timeval the_timeval;
        the_timeval.tv_sec = timeout_seconds;
        the_timeval.tv_usec =
            ((timeout_seconds - the_timeval.tv_sec) * 1000 * 1000);
        int result = setsockopt(socket_num, SOL_SOCKET, SO_RCVTIMEO,
                                &the_timeval, sizeof(struct timeval));
        if (result != 0)
        {
            fprintf(stderr,
                    "Warning: GoldenRetriever was unable to set the receive "
                    "timeout for a socket to %g seconds.\n", timeout_seconds);
        }
        result = setsockopt(socket_num, SOL_SOCKET, SO_SNDTIMEO, &the_timeval,
                            sizeof(struct timeval));
        if (result != 0)
        {
            fprintf(stderr,
                    "Warning: GoldenRetriever was unable to set the send "
                    "timeout for a socket to %g seconds.\n", timeout_seconds);
        }
    }

    return socket_num;
}

static CannedCatalog *find_catalog(const char *catalog_file_name,
                                   const char *executable_directory, size_t executable_directory_length)
{
    assert(catalog_file_name != NULL);

    CannedCatalog *result;

    pthread_mutex_lock(&catalog_lock);

    if (catalog_index == NULL)
    {
        catalog_index = create_string_index();
        if (catalog_index == NULL)
            throw "Out of memory.";
    }

    result = (CannedCatalog *)(lookup_in_string_index(catalog_index,
                               catalog_file_name));

    pthread_mutex_unlock(&catalog_lock);

    if (result != NULL)
        return result;

    char *directory_copy = new char[executable_directory_length + 1];
    memcpy(directory_copy, executable_directory, executable_directory_length);
    directory_copy[executable_directory_length] = 0;

    void *include_handler_data = create_local_file_include_handler_data(
                                     catalog_file_name, "", directory_copy);
    if (include_handler_data == NULL)
    {
        delete[] directory_copy;
        return NULL;
    }

    file_parser *the_file_parser = create_file_parser(catalog_file_name, NULL,
                                   NULL, &local_file_include_handler,
                                   &local_file_interface_include_handler, include_handler_data, NULL,
                                   NULL, true);
    if (the_file_parser == NULL)
    {
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    parser *the_parser = file_parser_parser(the_file_parser);

    open_expression *the_open_expression =
        parse_expression(the_parser, EPP_TOP, NULL, FALSE);
    if (the_open_expression == NULL)
    {
        delete_file_parser(the_file_parser);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    tokenizer *the_tokenizer = file_parser_tokenizer(the_file_parser);

    token *the_token = next_token(the_tokenizer);
    if (the_token == NULL)
    {
        delete_open_expression(the_open_expression);
        delete_file_parser(the_file_parser);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    token_kind kind = get_token_kind(the_token);

    if (kind == TK_ERROR)
    {
        delete_open_expression(the_open_expression);
        delete_file_parser(the_file_parser);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    if (kind != TK_END_OF_INPUT)
    {
        token_error(the_token, "Syntax error -- expected end of input.");
        delete_open_expression(the_open_expression);
        delete_file_parser(the_file_parser);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    verdict the_verdict = consume_token(the_tokenizer);
    if (the_verdict != MISSION_ACCOMPLISHED)
    {
        delete_open_expression(the_open_expression);
        delete_file_parser(the_file_parser);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    delete_file_parser(the_file_parser);

    declaration *built_ins_declaration =
        create_standard_built_ins_class_declaration();
    if (built_ins_declaration == NULL)
    {
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    tracer *the_tracer = create_tracer(TC_COUNT, trace_channel_names);
    if (the_tracer == NULL)
    {
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    salmon_thread *root_thread = create_salmon_thread("Root Thread",
                                 get_root_thread_back_end_data());
    if (root_thread == NULL)
    {
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    jumper *the_jumper = create_root_jumper(root_thread, the_tracer);
    if (the_jumper == NULL)
    {
        salmon_thread_remove_reference(root_thread);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    value *arguments_value = create_semi_labeled_value_list_value();
    if (arguments_value == NULL)
    {
        delete_jumper(the_jumper);
        salmon_thread_remove_reference(root_thread);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    value *element_value = c_string_to_value("???");
    if (element_value == NULL)
    {
        value_remove_reference(arguments_value, NULL);
        delete_jumper(the_jumper);
        salmon_thread_remove_reference(root_thread);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    the_verdict = add_field(arguments_value, NULL, element_value);
    value_remove_reference(element_value, NULL);
    if (the_verdict != MISSION_ACCOMPLISHED)
    {
        value_remove_reference(arguments_value, NULL);
        delete_jumper(the_jumper);
        salmon_thread_remove_reference(root_thread);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    context *top_level_context = create_top_level_context(arguments_value);
    value_remove_reference(arguments_value, NULL);
    if (top_level_context == NULL)
    {
        delete_jumper(the_jumper);
        salmon_thread_remove_reference(root_thread);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    object *built_ins_object = object_for_class_declaration(
                                   declaration_routine_declaration(built_ins_declaration),
                                   top_level_context, jumper_purity_level(the_jumper), the_tracer,
                                   root_thread, the_jumper);
    salmon_thread_remove_reference(root_thread);
    if (built_ins_object == NULL)
    {
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    context *built_ins_context =
        global_object_internal_context(built_ins_object, the_jumper);

    unbound_name_manager *the_unbound_name_manager =
        open_expression_unbound_name_manager(the_open_expression);
    assert(the_unbound_name_manager != NULL);

    quark_declaration *api_error_declaration = create_quark_declaration();
    if (api_error_declaration == NULL)
    {
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    source_location start_location;
    start_location.file_name = __FILE__;
    start_location.start_line_number = __LINE__;
    start_location.start_column_number = 1;
    start_location.end_line_number = __LINE__;
    start_location.end_column_number = 1;
    start_location.holder = NULL;

    declaration *the_declaration = create_declaration_for_quark("api_error",
                                   false, false, true, api_error_declaration,
                                   get_declaration_location(built_ins_declaration));
    if (the_declaration == NULL)
    {
        quark_declaration_remove_reference(api_error_declaration);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    the_verdict = bind_quark_name(the_unbound_name_manager, "api_error",
                                  api_error_declaration);
    if (the_verdict != MISSION_ACCOMPLISHED)
    {
        quark_declaration_remove_reference(api_error_declaration);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    statement *api_error_statement = create_declaration_statement();
    if (api_error_statement == NULL)
    {
        quark_declaration_remove_reference(api_error_declaration);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    the_verdict = declaration_statement_add_declaration(api_error_statement,
                  the_declaration);
    if (the_verdict != MISSION_ACCOMPLISHED)
    {
        delete_statement(api_error_statement);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    static_home *global_static_home = create_static_home(
                                          unbound_name_manager_static_count(the_unbound_name_manager),
                                          unbound_name_manager_static_declarations(
                                                  the_unbound_name_manager));
    if (global_static_home == NULL)
    {
        delete_statement(api_error_statement);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    context *static_global_context = create_static_context(built_ins_context,
                                     global_static_home, the_jumper);
    if (static_global_context == NULL)
    {
        delete_static_home(global_static_home);
        delete_statement(api_error_statement);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    statement_block *api_error_statement_block = create_statement_block();
    if (api_error_statement_block == NULL)
    {
        exit_context(static_global_context, the_jumper);
        delete_static_home(global_static_home);
        delete_statement(api_error_statement);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    the_verdict = append_statement_to_block(api_error_statement_block,
                                            api_error_statement);
    if (the_verdict != MISSION_ACCOMPLISHED)
    {
        delete_statement_block(api_error_statement_block);
        exit_context(static_global_context, the_jumper);
        delete_static_home(global_static_home);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    context *main_context = create_statement_block_context(
                                static_global_context, api_error_statement_block, NULL, NULL,
                                the_jumper);
    if (main_context == NULL)
    {
        exit_context(static_global_context, the_jumper);
        delete_static_home(global_static_home);
        delete_statement_block(api_error_statement_block);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    execute_statement(api_error_statement, main_context, the_jumper, NULL);

    value *top_value = evaluate_expression(
                           open_expression_expression(the_open_expression), main_context,
                           the_jumper);
    if (top_value == NULL)
    {
        exit_context(main_context, the_jumper);
        exit_context(static_global_context, the_jumper);
        delete_static_home(global_static_home);
        delete_statement_block(api_error_statement_block);
        close_object(built_ins_object, the_jumper);
        object_remove_reference(built_ins_object, the_jumper);
        exit_context(top_level_context, NULL);
        delete_jumper(the_jumper);
        delete_tracer(the_tracer);
        declaration_remove_reference(built_ins_declaration);
        delete_open_expression(the_open_expression);
        delete_local_file_include_handler_data(include_handler_data);
        delete[] directory_copy;
        return NULL;
    }

    wait_for_all_threads_to_finish();

    result = new CannedCatalog();

    result = fill_in_catalog_from_value(result, top_value, catalog_file_name,
                                        directory_copy);

    value_remove_reference(top_value, the_jumper);
    exit_context(main_context, the_jumper);
    exit_context(static_global_context, the_jumper);
    delete_static_home(global_static_home);
    delete_statement_block(api_error_statement_block);
    close_object(built_ins_object, the_jumper);
    object_remove_reference(built_ins_object, the_jumper);
    exit_context(top_level_context, NULL);
    delete_jumper(the_jumper);
    delete_tracer(the_tracer);
    declaration_remove_reference(built_ins_declaration);
    delete_open_expression(the_open_expression);
    delete_local_file_include_handler_data(include_handler_data);
    delete[] directory_copy;

    pthread_mutex_lock(&catalog_lock);

    enter_into_string_index(catalog_index, catalog_file_name, result);
    all_catalogs.push_back(result);

    pthread_mutex_unlock(&catalog_lock);

    return result;
}

static object *object_for_class_declaration(routine_declaration *declaration,
        context *the_context, purity_level *level, tracer *the_tracer,
        salmon_thread *root_thread, jumper *the_jumper)
{
    routine_instance *the_class;
    type *return_type;
    value *routine_value;
    expression *routine_expression;
    call *the_call;
    value *object_value;
    object *result;

    assert(declaration != NULL);
    assert(the_context != NULL);
    assert(level != NULL);

    the_class = create_routine_instance(declaration, level, NULL);
    if (the_class == NULL)
        return NULL;

    return_type = get_class_type(the_class);
    if (return_type == NULL)
    {
        routine_instance_remove_reference(the_class, NULL);
        return NULL;
    }

    assert(!(routine_instance_scope_exited(the_class)));
    routine_instance_set_return_type(the_class, return_type, 1, the_jumper);
    type_remove_reference(return_type, the_jumper);
    if (!(jumper_flowing_forward(the_jumper)))
    {
        routine_instance_remove_reference(the_class, the_jumper);
        return NULL;
    }

    assert(routine_instance_instance(the_class) != NULL);
    set_instance_instantiated(routine_instance_instance(the_class));

    assert(!(routine_instance_scope_exited(the_class)));
    routine_instance_set_up_static_context(the_class, the_context, the_jumper);
    if (!(jumper_flowing_forward(the_jumper)))
    {
        routine_instance_remove_reference(the_class, the_jumper);
        return NULL;
    }

    routine_value = create_routine_value(the_class);
    routine_instance_remove_reference(the_class, the_jumper);
    if (routine_value == NULL)
        return NULL;

    routine_expression = create_constant_expression(routine_value);
    value_remove_reference(routine_value, NULL);
    if (routine_expression == NULL)
        return NULL;

    the_call = create_call(routine_expression, 0, NULL, NULL, NULL);
    if (the_call == NULL)
        return NULL;

    object_value = execute_call(the_call, TRUE, NULL, NULL, 0, the_context,
                                the_jumper);
    delete_call(the_call);
    if (!(jumper_flowing_forward(the_jumper)))
    {
        assert(object_value == NULL);
        assert(jumper_target(the_jumper) == NULL);
        return NULL;
    }

    assert(object_value != NULL);
    assert(get_value_kind(object_value) == VK_OBJECT);
    result = object_value_data(object_value);
    object_add_reference(result);
    value_remove_reference(object_value, NULL);
    return result;
}

static context *global_object_internal_context(object *global_object,
        jumper *the_jumper)
{
    size_t print_field_num;
    value *print_value;
    routine_instance *the_routine_instance;

    assert(global_object != NULL);
    assert(the_jumper != NULL);

    assert(!(object_is_closed(global_object))); /* VERIFIED */
    print_field_num = object_field_lookup(global_object, "print");
    assert(print_field_num < object_field_count(global_object));

    assert(!(object_is_closed(global_object))); /* VERIFIED */
    print_value = object_field_read_value(global_object, print_field_num, NULL,
                                          the_jumper);
    if (!(jumper_flowing_forward(the_jumper)))
    {
        assert(print_value == NULL);
        return NULL;
    }

    assert(print_value != NULL);
    assert(value_is_valid(print_value)); /* VERIFIED */

    assert(get_value_kind(print_value) == VK_ROUTINE);
    the_routine_instance = routine_value_data(print_value);
    assert(the_routine_instance != NULL);

    value_remove_reference(print_value, NULL);

    assert(routine_instance_is_instantiated(the_routine_instance));
    /* VERIFIED */
    assert(!(routine_instance_scope_exited(the_routine_instance)));
    /* VERIFIED */
    return routine_instance_context(the_routine_instance);
}

static CannedCatalog *fill_in_catalog_from_value(CannedCatalog *result,
        value *top_value, const char *catalog_file_name,
        const char *executable_directory)
{
    assert(result != NULL);
    assert(top_value != NULL);

    if (get_value_kind(top_value) != VK_SEMI_LABELED_VALUE_LIST)
    {
        delete result;
        fprintf(stderr,
                "In canned API catalog file `%s', the top-level value was not a"
                " semi-labeled value list.\n", catalog_file_name);
        return NULL;
    }

    size_t component_count = value_component_count(top_value);
    for (size_t component_num = 0; component_num < component_count;
            ++component_num)
    {
        value *component_value =
            value_component_value(top_value, component_num);
        assert(component_value != NULL);

        if (get_value_kind(component_value) != VK_SEMI_LABELED_VALUE_LIST)
        {
            delete result;
            fprintf(stderr,
                    "In canned API catalog file `%s', component %lu of the "
                    "top-level value was not a semi-labeled value list.\n",
                    catalog_file_name, (unsigned long)component_num);
            return NULL;
        }

        size_t inner_count = value_component_count(component_value);
        const char *body;
        switch (inner_count)
        {
        case 2:
        {
            body = "";
            break;
        }
        case 3:
        {
            value *body_value = value_component_value(component_value, 2);
            assert(body_value != NULL);

            if (get_value_kind(body_value) != VK_STRING)
            {
                delete result;
                fprintf(stderr,
                        "In canned API catalog file `%s', component %lu of "
                        "the top-level value had a non-string third "
                        "element.\n", catalog_file_name,
                        (unsigned long)component_num);
                return NULL;
            }
            body = string_value_data(body_value);
            break;
        }
        default:
        {
            delete result;
            fprintf(stderr,
                    "In canned API catalog file `%s', component %lu of the "
                    "top-level value had %lu elements instead of 2 or 3.\n",
                    catalog_file_name, (unsigned long)component_num,
                    (unsigned long)inner_count);
            return NULL;
        }
        }

        value *url_tail_value = value_component_value(component_value, 0);
        assert(url_tail_value != NULL);

        if (get_value_kind(url_tail_value) != VK_STRING)
        {
            delete result;
            fprintf(stderr,
                    "In canned API catalog file `%s', component %lu of the "
                    "top-level value had a non-string first element.\n",
                    catalog_file_name, (unsigned long)component_num);
            return NULL;
        }

        value *file_name_value = value_component_value(component_value, 1);
        assert(file_name_value != NULL);

        if (get_value_kind(file_name_value) == VK_QUARK)
        {
            result->add_item(body, string_value_data(url_tail_value),
                             "api_error");
            continue;
        }

        if (get_value_kind(file_name_value) != VK_STRING)
        {
            delete result;
            fprintf(stderr,
                    "In canned API catalog file `%s', component %lu of the "
                    "top-level value had a non-string second element.\n",
                    catalog_file_name, (unsigned long)component_num);
            return NULL;
        }

        const char *relative_file_name = string_value_data(file_name_value);

        char *full_path = new char[
            strlen(executable_directory) + strlen(relative_file_name) + 2];
        sprintf(full_path, "%s/%s", executable_directory, relative_file_name);
        result->add_item(body, string_value_data(url_tail_value), full_path);
        delete[] full_path;
    }

    return result;
}

