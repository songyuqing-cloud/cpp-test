#include <curl/curl.h>
#include <fstream>
#include <sstream>
#include <iostream>


#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <algorithm>

#include "rapidxml-1.13/rapidxml.hpp"
#include "rapidxml-1.13/rapidxml_iterators.hpp"
#include "rapidxml-1.13/rapidxml_print.hpp"
#include "rapidxml-1.13/rapidxml_utils.hpp"

using namespace rapidxml;
using namespace std;

#define LOGI(format, ...)            printf("%s %s [%d] " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#define LOGE(format, ...)            fprintf(stderr, "%s %s[%d] " format "%s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))

#define COMPARE_NAME(x, y)      strncmp(x, y, strlen(x))
std::string strresult = "";

// callback function writes data to a std::ostream
static size_t data_write(char* buf, size_t size, size_t nmemb, void* userp)
{
    LOGI("");
    if(userp)
    {
        // LOGI("%s", (const char*)buf);
        strresult += (const char*)buf;
        return size * nmemb;
    }

    return 0;
}

struct WriteThis {
  const char *readptr;
  int sizeleft;
};

static size_t read_callback(char *ptr, size_t size, size_t nmemb, void *userp)
{
  struct WriteThis *pooh = (struct WriteThis *)userp;
  if(size*nmemb < 1)
    return 0;

  if(pooh->sizeleft) {
    // size_t
    // *(char *)ptr = pooh->readptr[0]; /* copy one single byte */
    size_t read = (pooh->sizeleft > (size * nmemb)) ? nmemb : pooh->sizeleft;
    pooh->sizeleft = (pooh->sizeleft > nmemb) ? pooh->sizeleft - nmemb : 0;
    memcpy(ptr, pooh->readptr, read);
    pooh->readptr += read;
    // pooh->readptr++;                 /* advance pointer */
    // pooh->sizeleft--;                /* less data left */
  LOGI("%d %d %d", read, pooh->sizeleft, nmemb);
    return size * nmemb;                        /* we return 1 byte at a time! */
  }

  return 0;                          /* no more data left to deliver */
}

int main(int argc, char **argv)
{
    if(argc < 2)
    {
        LOGI("usage: %s <filename>", argv[0]);
        return 1;
    }

    struct SCURLRead
    {
        const char *readptr;
        int sizeleft;
        std::string result;
    };

    SCURLRead curlData;
    LOGI("");
    CURL* curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &data_write);
    curl_easy_setopt(curl, CURLOPT_URL, "http://search.midomi.com:443/v2/?method=search&type=identify");
    // curl_easy_setopt(curl, CURLOPT_PROXY, "http://fsoft-proxy:8080");
    // curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, "longlt3:Nemchuaran3");
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "AppNumber=ZFc3qCfVzr");
    curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 50L);
    curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &curlData);

    std::streampos size;
    char * memblock;
    std::ifstream file (argv[1], std::ios::in| std::ios::binary| std::ios::ate);
    if (file.is_open())
    {
        size = file.tellg();
        memblock = new char [size];
        file.seekg (0, std::ios::beg);
        file.read (memblock, size);
        file.close();
    }
    else
    {
        LOGE("ERROR TRYING TO OPEN AUDIO FILE %s", argv[1]);
        return 1;
    }
    // struct SCURLRead pooh;
    curlData.result="";
    curlData.readptr = memblock;
    curlData.sizeleft = size;
    /* we want to use our own read function */
    curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
    /* pointer to pass to our read function */
    curl_easy_setopt(curl, CURLOPT_READDATA, &curlData);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE_LARGE, (curl_off_t)size);
    LOGI("calling curl_easy_perform...");
    curl_easy_perform(curl);
    delete [] memblock;
    if(curlData.result.size() > 0 && curlData.result.c_str() != "")
    {
        LOGI("%s", curlData.result.c_str());
        // obj.parse(&curlData.result[0], SOUNDHOUND_OBJ_RESULT_TYPE_XML);
    }
    else
    {
        LOGI("GOT NO RESULT");
    }

        LOGI("%s", strresult.c_str());

    rapidxml::xml_document<char> doc;
    doc.parse<0>((char *)&strresult[0]);
    xml_node<> * node = doc.first_node("melodis");
    node = node->first_node("tracks_grouped");
    node = node->first_node("track_name_group");
    node = node->first_node("artist_name_group");
    node = node->first_node("tracks");


    std::string mAlbum = "";
    std::string mArtist = "";
    std::string mAlbumArt = "";
    std::string mTitle = "";
    std::string mArtistType = "";
    std::string mArtistArt = "";
    std::string mAlbumDate = "";
    std::string mAlbumReview = "";
    std::string mAudioPreviewURL = "";

    for (xml_node<> *track = node->first_node("track"); track;
            track = track->next_sibling()) {
        /** attribute of track */
        for (xml_attribute<> *attr = track->first_attribute(); attr;
                attr = attr->next_attribute()) {
            //qDebug() << "[" << attr->name() << "] = " << attr->value() << "\r\n";
            if (!COMPARE_NAME("artist_display_name", attr->name()))
            {
                mArtist = std::string(attr->value());
            }
            else if (!COMPARE_NAME("album_name", attr->name()))
            {
                mAlbum = std::string(attr->value());
            }
            else if (!COMPARE_NAME("album_date", attr->name()))
            {
                mAlbumDate = std::string(attr->value());
            }
            else if (!COMPARE_NAME("track_name", attr->name()))
            {
                mTitle = std::string(attr->value());
            }
            else if (!COMPARE_NAME("album_primary_image", attr->name()))
            {
                mAlbumArt = std::string(attr->value());
            }

        }

        xml_node<> * artists = track->first_node("artists");

        for (xml_node<> *artist = artists->first_node("artist"); artist;
                artist = artist->next_sibling()) {
            /** attribute of artist */
            for (xml_attribute<> *attr = artist->first_attribute(); attr; attr =
                    attr->next_attribute()) {
                if (!COMPARE_NAME("artist_primary_image", attr->name()))
                {
                    mArtistArt = std::string(attr->value());
                }
                else if (!COMPARE_NAME("artist_name", attr->name()))
                {
                    mArtist = std::string(attr->value());
                }
            }
        }
    }
LOGI("%s", mAlbum.c_str());
LOGI("%s", mArtist.c_str());
LOGI("%s", mAlbumArt.c_str());
LOGI("%s", mTitle.c_str());
LOGI("%s", mArtistType.c_str());
LOGI("%s", mArtistArt.c_str());
LOGI("%s", mAlbumDate.c_str());
LOGI("%s", mAlbumReview.c_str());
LOGI("%s", mAudioPreviewURL.c_str());
#if 0
#endif



        double speed_upload, total_time;
        curl_easy_getinfo(curl, CURLINFO_SPEED_UPLOAD, &speed_upload);
        curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &total_time);
        LOGI("Speed: %.3f bytes/sec during %.3f seconds", speed_upload, total_time);
    LOGE("");
    curl_easy_cleanup(curl);

    return 0;
}
