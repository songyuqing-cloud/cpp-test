#include <iostream>
#include <vector>
#include "../../utils.h"

struct Vec2
{
	float x,y;
    /// cocos2d
    inline float operator* (const Vec2 &v) const
    {
        return x*v.x + y*v.y;
    }

    inline Vec2 operator- (const Vec2 &v) const
    {
        return Vec2{x-v.x, y-v.y};
    }
};

/// https://blackpawn.com/texts/pointinpoly/default.html
template <typename V>
inline bool isPointInTri(const V &p, const V &a, const V &b, const V &c)
{
    // Compute vectors
    V v0 = c - a;
    V v1 = b - a;
    V v2 = p - a;

    // Compute dot products
    float dot00 = v0 * v0;
    float dot01 = v0 * v1;
    float dot02 = v0 * v2;
    float dot11 = v1 * v1;
    float dot12 = v1 * v2;

    // Compute barycentric coordinates
    float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
    float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

    // Check if point is in triangle
    return (u >= 0) && (v >= 0) && (u + v <= 1);
}

template <typename V>
inline bool isPointInConvex(const V &p, const std::vector<V> &poly)
{
    // TRACE();
    for(int i=0; i<poly.size()-2; i++)
    {
        if(isPointInTri(p, poly[0], poly[i+1], poly[i+2]))
            return true;
    }

    return false;
}

/// raycast
inline bool isPointInPoly(Vec2 const &p, std::vector<Vec2> const &verts)
{
    // TRACE();
    size_t i, j;
    bool c = false;
    size_t nvert = verts.size();
    for (i = 0, j = nvert-1; i < nvert; j = i, i++)
    {
        const Vec2 &v1 = verts[i];
        const Vec2 &v2 = verts[j];
        float raycast_intersection_x = (v2.x-v1.x) * (v1.y-p.y) / (v1.y-v2.y) + v1.x;
        /// if p lies on the left of the edge
        if (((v1.y > p.y) != (v2.y > p.y)) && (p.x < raycast_intersection_x))
            c = !c;
    }
    return c;
}


int main(int argc, char const *argv[])
{
	TRACE();
    bool ret1, ret2;
    std::vector<Vec2> tri = {{0,0}, {1,0}, {1,1}};
    std::vector<Vec2> hexagon = {{0.25,0}, {0.75,0}, {1,0.5}, {0.75,1}, {0.25,1}, {0,0.5}};
    ::utils::Timer<std::chrono::duration<float, std::milli>> timer;
    float d1=0, d2=0;
    for(int i=0; i<100000; i++)
    {
        for(const Vec2 &p : { Vec2{-1,0.5}, Vec2{0,0.5}, Vec2{0.25,0.5}, Vec2{0.5,0.5}, Vec2{0.75,0.5}, Vec2{1,0.5}, Vec2{2,0.5} })
        {
            timer.reset();
            ret1 = isPointInConvex(p, tri);
            d1+=timer.elapse();
            timer.reset();
            ret2 = isPointInPoly(p, tri);
            d2+=timer.elapse();
        }
    }
    LOGD("%.3f : %.3f", d1, d2);
    d1=0, d2=0;
    for(int i=0; i<100000; i++)
    {
        for(const Vec2 &p : { Vec2{-1,0.5}, Vec2{0,0.5}, Vec2{0.5,0.5}, Vec2{1,0.5}, Vec2{2,0.5} })
        {
            timer.reset();
            ret1 = isPointInConvex(p, hexagon);
            d1+=timer.elapse();
            timer.reset();
            ret2 = isPointInPoly(p, hexagon);
            d2+=timer.elapse();
        }
    }
    LOGD("%.3f : %.3f", d1, d2);
	return 0;
}