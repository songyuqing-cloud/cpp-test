#include <utils/Utils>

#include <boost/timer.hpp>
#include <boost/progress.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <boost/foreach.hpp>

void read( std::istream & a_is, std::ostream &outfile, int a_cnt=1)
{
    using boost::property_tree::ptree;
    ptree l_pt, l__pt;

    COUNTER_BEGIN_STR("read_xml");
        read_xml(a_is, l_pt);
    COUNTER_END

    COUNTER_BEGIN_STR("get_child");
        l__pt = l_pt.get_child("registermap");
    COUNTER_END;

    // std::vector<std::string> l_vField = {"name", "length", "length_ext", "resetvalue", "readvalue"};
    // std::vector<std::string> l_vFieldMappingMemory = {"byteaddress", "lsb", "msb"};

    COUNTER_BEGIN_STR("parsing");
    // BOOST_FOREACH( ptree::value_type const& v, l__pt.get_child("fieldgroup") )
    for( ptree::value_type &v : l__pt.get_child("fieldgroup") )
    {
        // BOOST_FOREACH( std::string l_str, l_vField )
        if((a_cnt > 1) && v.first == "field")
        {
            for(std::string l_str : {"name", "length"})
            {
                std::string l_val = v.second.get<std::string>(l_str);
                outfile << l_str << ": " << l_val << std::endl;
            }

            for(auto v_ : v.second)
            {
                if(v_.first == "mapping")
                {
                    for(auto v__ : v_.second)
                    {
                        if(v__.first == "memory")
                        {
                            for(std::string l_str : {"byteaddress", "msb", "lsb"})
                            {
                                std::string l_val = v__.second.get<std::string>(l_str);
                                outfile << l_str << ": " << l_val << std::endl;
                            }
                        }
                    }
                }
            }

        }



        if(a_cnt > 1) outfile << std::endl << std::endl;
    }
    COUNTER_END
}

int main(int a_cnt, char **a_vars)
{
    std::ofstream outfile;
    if(a_cnt > 1)
    {
        outfile.open("result.txt", std::ios::out | std::ios::trunc);
    }

    COUNTER_BEGIN_STR("MAIN");

    std::string l_file = "../MantraC_RegisterMap.xml";

    std::ifstream l_stream (l_file/*, std::ios::in|std::ios::binary|std::ios::ate*/);

    _LOGE("Open \"%s\"", l_file.c_str());

    if(l_stream)
    {
        read(l_stream, outfile, a_cnt);
    }

    COUNTER_END;

    outfile.close();

    return 0;
}