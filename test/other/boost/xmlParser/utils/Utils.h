#ifndef UTILS_H
#define UTILS_H

#define MB_TO_BYTE(mb)  ((mb) * 0x100000)
#define BYTE_TO_MB(byte) (static_cast<double>(byte) / 0x100000)

#define SAFE_DELETE(ptr) if(ptr){ delete (ptr); (ptr) = nullptr; }

#endif