#ifndef LOG_H
#define LOG_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

namespace custom {

class Log
{
public:
    enum class _ELvl
    {
        Init = 0,
        Info,
        Default = Info,
        Debug,
        Max,
    };
    
    static inline bool canLog(_ELvl const &alvl)
    {
        static char const *lPatterns[] = {"LOG_LVL", "LOGLVL", "LOGLEVEL", "LOG_LEVEL"};
        static char *lEnvi = nullptr;
        static _ELvl lLvl = _ELvl::Default;
        
        if(!lEnvi)
        {
            for(char const *pPat : lPatterns)
            {
                lEnvi = std::getenv(pPat);
                if(lEnvi)
                {
                    lLvl = static_cast<_ELvl>(std::stoi(lEnvi));
                    break;
                }
            }
        }
        
        return (lLvl >= alvl); 
    }
};

}

#ifdef LOG_EN

    #define _LOGW(format, ...) printf("[W]%s %s [%d] " format "\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
    #define _PLOGW(format, ...) printf("[W]%s %s p[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
    #define _PPLOGW(format, ...) printf("[W]%s %s pp[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
    #define _TLOGW(format, ...) printf("[W]%s %s t[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )

    #define _LOGE(format, ...) fprintf(stderr, "[E]%s %s [%d] " format ": %s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
    #define _PLOGE(format, ...) fprintf(stderr, "[E]%s %s p[%d-%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__, strerror(errno))
    #define _PPLOGE(format, ...) fprintf(stderr, "[E]%s %s pp[%d-%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__, strerror(errno))
    #define _TLOGE(format, ...) fprintf(stderr, "[E]%s %s t[%d-%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__, strerror(errno))

    #define _LOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) printf("[I]%s %s [%d] " format "\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
    #define _PLOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) printf("[I]%s %s p[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
    #define _PPLOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) printf("[I]%s %s pp[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
    #define _TLOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) printf("[I]%s %s t[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )

    #define _LOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) printf("[D]%s %s [%d] " format "\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
    #define _PLOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) printf("[D]%s %s p[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
    #define _PPLOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) printf("[D]%s %s pp[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
    #define _TLOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) printf("[D]%s %s t[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )

#else // LOG_EN

    #define _LOGW(...)
    #define _PLOGW(...)
    #define _PPLOGW(...)
    #define _TLOGW(...)
    #define _LOGE(...)
    #define _PLOGE(...)
    #define _PPLOGE(...)
    #define _TLOGE(...)

    #define _LOGI(...)
    #define _PLOGI(...)
    #define _PPLOGI(...)
    #define _TLOGI(...)

    #define _LOGD(...)
    #define _PLOGD(...)
    #define _PPLOGD(...)
    #define _TLOGD(...)

#endif // LOG_EN

#endif // LOG_H
