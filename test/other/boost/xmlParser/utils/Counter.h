#ifndef COUNTER_H
#define COUNTER_H

#define USING_BOOST

#ifdef USING_BOOST
#include <boost/progress.hpp>
#else // USING_BOOST
#endif // USING_BOOST

#include <string>
#include "Utils.h"

namespace custom
{

class MyCounter
{
#ifdef USING_BOOST
public:
    MyCounter(std::string const &a_str="") : _name(a_str)
    {
    }
    
    virtual ~MyCounter()
    {
        
    }
    
    inline std::string const &name(void) const { return _name; }
private:
    boost::progress_timer _t;
    std::string _name;

#else // USING_BOOST

public:
    MyCounter(std::string const &a_str) {}
    virtual ~MyCounter() = default;
private:

#endif // USING_BOOST
    
};

}

#define COUNTER_BEGIN                       { custom::MyCounter l_counter
#define COUNTER_BEGIN_WITH_STRING(a_name)   { custom::MyCounter l_counter(std::string("[") + std::to_string(__LINE__) + std::string("] ") + a_name)
#define COUNTER_END                         _LOGD("%s", l_counter.name().c_str()); }

#endif // COUNTER_H