// regex_search example
#include <iostream>
#include <string>
#include <regex>
#include <vector>
#include <unistd.h>

#include "../../utils.h"

std::vector<std::string> split(const std::string& input, const std::string& regex) {
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    std::sregex_token_iterator
        first{input.begin(), input.end(), re, -1},
        last;
    return {first, last};
}

int main (int, char** argv)
{
    std::string sp {argv[1]};
    // std::string sp ("com.ford.projection");
    // std::string sp ("com.garmin.sync.garmin-app_GarminNav");
    std::regex re {"^.*\\._*(projection|applink|(radio|media)|(.*Nav|dev_nav))_*"};   // matches words beginning by "sub"
    // std::vector<std::string> arr;
    // int submatches[] = { 1, 2 };

    // using regex_token_iterator = std::regex_token_iterator<std::string::iterator>;

    // std::regex_token_iterator<std::string::iterator> first ( s.begin(), s.end(), e), last;
    // std::copy(regex_token_iterator{s.begin(), s.end(), e, submatches}, regex_token_iterator{}, std::back_inserter(arr));

    // std::cout << arr.size() << std::endl;

    // for(auto &el : arr) {
        // std::cout << el << std::endl;
    // }
    std::vector<std::string> grpMatches{"media"};
    std::smatch match;
    if(std::regex_search(sp, match, re)) {
        std::cout << "Match size = " << match.size() << std::endl;
        for(int i=0; i<match.size(); i++) {
            std::cout << i << ": '" << match.str(i) << "'" << std::endl;
        }
        // int i=match.size()-1;
        // for(; i>0 && match.str(i).empty(); i--) {}
        // std::cout << i << " " << match.str(i) << std::endl;
    }

    // // default constructor = end-of-sequence:
    // std::regex_token_iterator<std::string::iterator> rend;

    // std::cout << "entire matches:"; 
    // std::regex_token_iterator<std::string::iterator> a ( s.begin(), s.end(), e );
    // while (a!=rend) std::cout << " [" << *a++ << "]";
    // std::cout << std::endl;

    // std::cout << "2nd submatches:";
    // std::regex_token_iterator<std::string::iterator> b ( s.begin(), s.end(), e, 2 );
    // while (b!=rend) std::cout << " [" << *b++ << "]";
    // std::cout << std::endl;

    // std::cout << "1st and 2nd submatches:";
    // int submatches[] = { 1, 2 };
    // std::regex_token_iterator<std::string::iterator> c ( s.begin(), s.end(), e, submatches );
    // while (c!=rend) std::cout << " [" << *c++ << "]";
    // std::cout << std::endl;

    // std::cout << "matches as splitters:";
    // std::regex_token_iterator<std::string::iterator> d ( s.begin(), s.end(), e, -1 );
    // while (d!=rend) std::cout << " [" << *d++ << "]";
    // std::cout << std::endl;

    return 0;
}
