#pragma once

struct Obj
{
	Obj() : val_(0) {}
	virtual ~Obj() {}

	inline void doSmt();
	int val_;
};

#include "obj.cc"
