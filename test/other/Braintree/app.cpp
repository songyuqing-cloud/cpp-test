// this example should print out "Hello, World!", four times
#include <iostream>
#include "BrainTree.h"
#include <string>
#include <thread>
#include <chrono>
#include <random>

// GoHomeAndSleepTilRested
//     Fatigued? -> EnterMineAndDigForNugget
//         PocketsFull? -> VisitBankAndDepositGold
//             Wealth?  -> GoHomeAndSleepTilRested
//                   ! -> EnterMineAndDigForNugget
//         Thirsty? -> QuenchThirst
//             EnterMineAndDigForNugget


// sleep
// move to mine
// dig for nugget
// move to bank
// deposit gold
// move to home
// move to bar
// buy drink

// move to
// sleep
// dig for nugget
// deposit gold
// buy goods

static const int MAX_ENERGY = 12, MIN_ENERGY = 2;

static const int SLEEP_ENG = 4;
static const int WALK_ENG = 1;
static const int DIG_ENG = 1;
static const int GOLD_PER_DAY = 5;

struct Miner
{
    int m_energy;
    int m_Thrist;
    int m_GoldCarry;
    int m_MoneyInBank;
    std::string m_Location;

    void status()
    {
        printf("\te:%d g:%d\n", m_energy, m_GoldCarry);
    }
};

Miner g_miner;

class Sleep : public BrainTree::Node
{
public:
    Status update() override
    {
        std::cout << " .z.";
        g_miner.m_energy += SLEEP_ENG;

        if(g_miner.m_energy < MAX_ENERGY)
            return Node::Status::Running;

        return Node::Status::Success;
    }

    void initialize() override
    {
        std::cout << "ZZZ";
    }

    void terminate(Status s) override
    {
        // if(s == Node::Status::Success)
        std::cout << ". Let's grab some cashes!\n";
        g_miner.status();
    }
};

class MoveTo : public BrainTree::Node
{
public:
    MoveTo(std::string a_location, int a_movEng) : BrainTree::Node(), m_Location(a_location), m_movEng(a_movEng), m_crr(0) {}

    void initialize() override { m_crr = 0; std::cout << g_miner.m_Location; }

    void terminate(Status s) override
    {
        if(s == Node::Status::Success)
        {
            g_miner.m_Location = m_Location;
            std::cout << " " << g_miner.m_Location << "\n";
        }
    }

    Status update() override
    {
        g_miner.m_energy -= WALK_ENG;
        m_crr++;
        std::cout << " .w.";

        if(m_crr < m_movEng)
            return Node::Status::Running;

        return Node::Status::Success;
    }

private:
    std::string m_Location;
    int m_movEng;
    int m_crr;
};

class DigForNugget : public BrainTree::Node
{
public:
    void initialize() override
    {
        m_GoldCarry = 0;
        std::cout << "Digging";
    }

    void terminate(Status s) override
    {
        g_miner.m_GoldCarry += m_GoldCarry;
        m_GoldCarry = 0;

        if(s == Node::Status::Failure)
            std::cout << "\n\tTired!!!\n";
        else if(s == Node::Status::Success)
            std::cout << "\n\tGREAT!!!\n";
    }

    Status update() override
    {
        std::cout << " .d.";
        g_miner.m_energy -= DIG_ENG;
        m_GoldCarry++;

        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> dis(0, 1.f);
        float random = dis(gen);

        // std::cout << "[!] RAND: "<< random << "\n";


        if (g_miner.m_energy <= MIN_ENERGY)
        {
            return Node::Status::Failure;
        }

        if (m_GoldCarry > GOLD_PER_DAY)
        {
            return Node::Status::Success;
        }

        if (random < 0.1f)
        {
            std::cout << " OMG!!!";
            m_GoldCarry = 0;
        }

        return Node::Status::Running;
    }

private:
    int m_GoldCarry;
};

void CreatingBehaviorTreeManually()
{
    g_miner.m_Location = "Home";

    BrainTree::BehaviorTree tree;
    auto comp1 = std::make_shared<BrainTree::Sequence>();
    auto comp2 = std::make_shared<BrainTree::Selector>();

    // auto deco = std::make_shared<BrainTree::UntilSuccess>();

    auto sleep = std::make_shared<Sleep>();
    auto moveToMine = std::make_shared<MoveTo>("Mine", 3);
    auto moveToHome = std::make_shared<MoveTo>("Home", 3);
    auto digForNugget = std::make_shared<DigForNugget>();

    auto success = std::make_shared<BrainTree::Succeeder>();
    success->setChild(digForNugget);

    // deco->setChild(digForNugget);
    // comp2->addChild(digForNugget);
    // comp2->addChild(success);

    comp1->addChild(sleep);
    comp1->addChild(moveToMine);
    comp1->addChild(success);
    comp1->addChild(moveToHome);

    tree.setRoot(comp1);
    // BrainTree::Node::Status s = tree.update();
    // printf("Result: %d\n", s);

    do
    {
        // std::cout << "Started\n";
        auto status = tree.update();
        // std::cout << "Done: " << (int)(status) << "\n";
        std::cout << std::flush;
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    while( 1 );
}

// void CreatingBehaviorTreeUsingBuilders()
// {
//     auto tree = BrainTree::Builder()
//         .composite<BrainTree::Sequence>()
//             .leaf<Action>()
//             .leaf<Action>()
//         .end()
//         .build();
//     tree->update();
// }

int main()
{
    // std::random_device rd;  //Will be used to obtain a seed for the random number engine
    // std::mt19937 gen(std::random_device()); //Standard mersenne_twister_engine seeded with rd()
    // std::uniform_real_distribution<float> dis(0, 1.f);

    CreatingBehaviorTreeManually();
    // CreatingBehaviorTreeUsingBuilders();
    return 0;
}