#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <sys/syscall.h>

#include <vector>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <atomic>

#define _TLOGI(format, ...) printf("[I]%s %s t-%d [%d] " format "\n",__FILE__, __PRETTY_FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )
#define _TLOGE(format, ...) fprintf(stderr, "[E]%s %s t-%d [%d] " format ": %s\n", __FILE__, __PRETTY_FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__, strerror(errno))


static const ssize_t BUFF_SIZE = 0x1000;
static const ssize_t WSIZE = 0x10;
static const ssize_t RSIZE = 0x10 / 2;
static const ssize_t READ_NUM = 10;
static const ssize_t MASK = BUFF_SIZE - 1;

template <typename T>
class RingBuff
{
public:

    RingBuff<T>(ssize_t size) : _len(size), _mark(0), _wIdx(0), _rIdx(0), _overflow(false)
    {
        // _buff.resize(size);
    }

    ssize_t length() const
    {
        return _len;
    }

    ssize_t getFill(ssize_t w, ssize_t r, ssize_t m) const
    {
        if(w >= r)
        {
            return w - r;
        }
        else
        {
            return m - r;
        }
    }

    void debug() const
    {
        _TLOGI("W:%ld R:%ld M:%ld O:%d", _wIdx.load(), _rIdx.load(), _mark.load(), _overflow);
    }

    bool overflow() const
    {
        return _overflow;
    }

    void push(ssize_t size)
    {
        ssize_t rIdx = _rIdx;

        if((_len - _wIdx) >= size)
        {
            // todo: add check OVF
            if((_wIdx < rIdx) && ((_wIdx + size) > rIdx))
                _overflow = true;

            _wIdx += size;
        }
        else // wrap
        {
            // todo: add check OVF
            if(size >= rIdx)
                _overflow = true;

            _mark = _wIdx.load();
            _wIdx = size;
        }
    }

    ssize_t pop(ssize_t size)
    {
        // std::lock_guard<std::mutex> lock(_mutex);
        ssize_t wIdx = _wIdx;
        ssize_t fill = getFill(wIdx, _rIdx, _mark);
        if(size == 0 || size > fill)
            size = fill;

        if(size)
        {
            _rIdx += size;

            if(_rIdx > wIdx && _rIdx == _mark)
                _rIdx = 0;
        }

        return size;
    }

private:
    // std::vector<T> _buff;
    std::mutex _mutex;
    // std::condition_variable _cond;
    T *_buff;
    size_t _len;
    std::atomic<ssize_t> _mark;
    std::atomic<ssize_t> _wIdx;
    std::atomic<ssize_t> _rIdx;
    bool _overflow;
};

int main(int argc, char **argv)
{
    RingBuff<char> rb(BUFF_SIZE);
    int rs = 0;

    bool stop = false;

    std::vector<char> buff;
    buff.resize(BUFF_SIZE);

    std::thread ts[READ_NUM];

    int loopcnt = 0;
    
    if(argc > 1) 
        loopcnt = std::stoi(argv[1]);

    for(int i=0; i<READ_NUM; i++)
    {
        ts[i] = std::thread([=, &stop, &rb]() 
        {
            ssize_t cnt=0;
            // std::vector<char> buff(RSIZE);
            _TLOGI("Reader started");

            while(!stop)
            {
                if(rb.pop(RSIZE))
                    cnt++;

                std::this_thread::yield();
            }

            _TLOGI("Read cnt: %ld", cnt);
        });
    }

    std::thread wrt([=, &stop, &rb]() 
    {
        ssize_t cnt = 0;
        int rs = 0;
        // std::vector<char> buff;
        // buff.resize(WSIZE);

        _TLOGI("Writer started");

        while(!stop)
        {
            cnt++;
            rb.push(WSIZE);

            if(rb.overflow() || cnt == loopcnt)
                break;

            std::this_thread::yield();
        }

        stop = true;
        _TLOGI("WRITE cnt: %ld", cnt);
    });

    wrt.join();

    for(int i=0; i<READ_NUM; i++)
        ts[i].join();

    // _TLOGI("%ld", rb.getFill());
    rb.debug();

    return 0;
}
