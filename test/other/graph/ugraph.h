#pragma once

#include "graph.h"

template <class T> struct Uhash;
template <class T> struct Uequal_to;

/// Undirected edge hasher
template <class Vertex>
struct Uhash<Edge<Vertex>>
{
    inline size_t operator()(Edge<Vertex> const &v) const
    {
        return (std::hash<Vertex>()(v.first)) ^ std::hash<Vertex>()(v.second);
    }
};

/// Undirected edge equal
template <class Vertex>
struct Uequal_to<Edge<Vertex>>
{
    inline bool operator()(Edge<Vertex> const &e1, Edge<Vertex> const &e2) const
    {
        return (e1.first==e2.first && e1.second==e2.second) || (e1.first==e2.second && e1.second==e2.first);
    }
};

/// Undirected graph
template <class Vertex>
class UGraph : public Graph<Vertex, Uhash<Edge<Vertex>>, Uequal_to<Edge<Vertex>>>
{
// private:
public:
    using Edge_t = Edge<Vertex>;
    using Adjacent_t = Adjacent<Vertex>;
    using DGraph = Graph<Vertex, Uhash<Edge_t>, Uequal_to<Edge_t>>;
    void addEdge(Vertex const &v1, Vertex const &v2, float weight);
    void remEdge(Vertex const &v1, Vertex const &v2);
};

template <class Vertex>
void UGraph<Vertex>::addEdge(Vertex const &v1, Vertex const &v2, float weight)
{
    /// add 2 enpoints
    DGraph::addVertex(v1);
    DGraph::addVertex(v2);
    /// add new edge
    DGraph::edges_[{v1, v2}] = weight;
    /// add adjacents
    DGraph::vertices_[v1].push_back({v2, weight});
    DGraph::vertices_[v2].push_back({v1, weight});
}

template <class Vertex>
void UGraph<Vertex>::remEdge(Vertex const &v1, Vertex const &v2)
{
    DGraph::edges_.erase(DGraph::edges_.find({v1,v2}));
    /// remove adjacents
    DGraph::vertices_[v1].erase(std::find(DGraph::vertices_[v1].begin(), DGraph::vertices_[v1].end(), Adjacent<Vertex>{v2, 0}));
    DGraph::vertices_[v2].erase(std::find(DGraph::vertices_[v2].begin(), DGraph::vertices_[v2].end(), Adjacent<Vertex>{v1, 0}));
}