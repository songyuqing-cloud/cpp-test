#pragma once

#include <iostream>
#include <tuple>
#include <cmath>

struct Vec2
{
    float x, y;             ///< position
    Vec2()=default;
    Vec2(float x, float y) : x(x), y(y) {}
    Vec2(int x, int y) : x((float)x), y((float)y) {}
    double square() {return x*x + y*y;}
};

bool operator==(Vec2 const &p1, Vec2 const &p2)
{
    // return p1.x==p2.x && p1.y==p2.y;
    return std::tie(p1.x, p1.y) == std::tie(p2.x, p2.y);
}

bool operator!=(Vec2 const &p1, Vec2 const &p2)
{
    return std::tie(p1.x, p1.y) != std::tie(p2.x, p2.y);
}

Vec2 operator+(Vec2 const &p1, Vec2 const &p2)
{
    return {p1.x + p2.x, p1.y + p2.y};
}

Vec2 operator-(Vec2 const &p1, Vec2 const &p2)
{
    return {p1.x - p2.x, p1.y - p2.y};
}

// bool operator < (Vec2 const &p1, Vec2 const &p2) {
//     return std::tie(p1.x, p1.y) < std::tie(p2.x, p2.y);
// }

template <>
struct std::hash<Vec2>
{
    inline size_t operator()(const Vec2& p) const
    {
        return (std::hash<float>()(p.x)
           ^ (std::hash<float>()(p.y) << 1));
    }
};

std::ostream &operator<<(std::ostream &os, Vec2 const &v)
{
    return os << v.x << ":" << v.y;
}

template <class T> struct HeuManhattan;
template <>
struct HeuManhattan<Vec2>
{
    double operator()(Vec2 const &p1, Vec2 const &p2) const
    {
        return /*std::sqrt*/((p2 - p1).square());
    }
};