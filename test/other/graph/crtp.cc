#include "ggraph.h"
#include "vec2.h"

template <typename T>
void testAddVertex(GGraph<T> &g)
{
	g.addVertex({1,1});
}

int main(int argc, char const *argv[])
{
	DGraph<Vec2> g;
	testAddVertex(g);
	return 0;
}