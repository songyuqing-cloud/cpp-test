#include <cstring>
#include "ggraph.h"

#include "vec2.h"
#include "graph.h"
#include "ugraph.h"
#include "pathplanner.h"

#include "../../../utils/MyCounter.h"
std::unordered_map<std::thread::id, int> util::log::MyCounter::indent;
int const SIZE = 128;
int const CENTER=(SIZE)/2;

template <class Vertex, class Graph>
void test01()
{
    MY_COUNTER();
    Vertex v1{0,0}, v2{0,1}, v3{1,0};
    {
        Graph G;
        G.addEdge(v1,v2,1);
        G.addEdge(v1,v3,1);
        G.addEdge(v2,v3,1.2f);
        std::cout << "Init<Vec2>\n" << G << std::endl;

        G.remEdge(v1,v2);
        std::cout << "remEdge\n" << G << std::endl;
    }

    {
        Graph G;
        G.addEdge(v1,v2,1);
        G.addEdge(v1,v3,1);
        G.addEdge(v2,v3,1.2f);
        std::cout << "Init<Vec2>\n" << G << std::endl;

        G.remEdge(v2,v3);
        std::cout << "remEdge2\n" << G << std::endl;
    }

    {
        Graph G;
        G.addEdge(v1,v2,1);
        G.addEdge(v1,v3,1);
        G.addEdge(v2,v3,1.2f);
        std::cout << "Init<Vec2>\n" << G << std::endl;

        G.remVertex(v1);
        std::cout << "remVertex\n" << G << std::endl;
    }
}

template <class Graph>
void test02()
{
    Graph G;
    std::string v1{"A"}, v2{"B"}, v3{"C"};

    G.addEdge(v1,v2,1);
    G.addEdge(v1,v3,1);
    G.addEdge(v2,v3,1.2f);

    std::cout << G << std::endl;

    G.remEdge(v1,v2);
    std::cout << G << std::endl;

    G.addEdge(v1,v2,1.2f);
    G.remVertex(v1);
    std::cout << G << std::endl;
}

template <class Graph>
void test03(int size)
{
    MY_COUNTER();
    using Vertex = typename GraphType<Graph>::Vertex;
    Graph G;
    {
        MY_COUNTER();
        for(int i=0; i<size; i++)
        {
            for(int j=0; j<size; j++)
            {
                Vertex v = {i,j};
                Vertex vr = {i+1,j};
                Vertex vl = {i-1,j};
                Vertex vt = {i,j+1};
                Vertex vb = {i,j-1};

                if(i < (size - 1)) G.addEdge(v, vr, 1);
                if(j < (size - 1)) G.addEdge(v, vt, 1);
                if(i > 0) G.addEdge(v, vl, 1);
                if(j > 0) G.addEdge(v, vb, 1);

                // if(i < (SIZE - 1)) G.addEdge({i,j}, {i+1,j}, 1);
                // if(j < (SIZE - 1)) G.addEdge({i,j}, {i,j+1}, 1);
                // if(i > 0) G.addEdge({i,j}, {i-1,j}, 1);
                // if(j > 0) G.addEdge({i,j}, {i,j-1}, 1);
            }
        }
    }

    Vertex start{size-1,size-1}, goal{0,0};
    std::unordered_map<Vertex, Vertex> came_from;
    std::unordered_map<Vertex, float> cost_so_far;
    {
        MY_COUNTER();
        pathFinding<Vertex, Graph>(G, start, goal, came_from, cost_so_far);
    }
    auto path = reconstructPath<Vertex>(start, goal, came_from);
        // for(auto &v : path) std::cout << "(" << v << ") ";
        // std::cout << std::endl;
    {
        MY_COUNTER();
        pathFinding<Vertex, Graph, HeuManhattan>(G, start, goal, came_from, cost_so_far);
    }
    path = reconstructPath<Vertex>(start, goal, came_from);
        // for(auto &v : path) std::cout << "(" << v << ") ";
        // std::cout << std::endl;
}

int main(int argc, char **argv)
{
    int size = SIZE;
    if(argc > 1) size = std::stoi(argv[1]);
    // test01<Vec2, Graph<Vec2>> ();
    // test02<Graph<std::string>> ();
    test03<UGraph<Vec2>> (size);
    
    // std::vector<Vec2> v(5);
    // std::cout << v << std::endl;

    return 0;
}