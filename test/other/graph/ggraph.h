#pragma once

#include <iostream>
#include <vector>
#include <tuple>
#include "../../utils.h"


// using Weight = float;
float static const MAX_WEIGHT = 999999.f;
/// Edge type
template <class Vertex>
using Edge = std::pair<Vertex, Vertex>;
/// Adjacent type
template <class Vertex/*, typename Weight*/>
using Adjacent = std::pair<Vertex, float>;

template<class Vertex>
bool operator==(Adjacent<Vertex> const &a1, Adjacent<Vertex> const &a2)
{
    return a1.first == a2.first;
}

template<class Vertex>
bool operator < (Adjacent<Vertex> const &a1, Adjacent<Vertex> const &a2)
{
    return a1.second < a2.second;
}

/// Directed edge hasher
template <class Vertex>
struct std::hash<Edge<Vertex>>
{
    inline size_t operator()(Edge<Vertex> const &e) const
    {
        return (std::hash<Vertex>()(e.first) << 1) ^ std::hash<Vertex>()(e.second);
    }
};

/// Directed edge equal
template <class Vertex>
struct std::equal_to<Edge<Vertex>>
{
    inline bool operator()(Edge<Vertex> const &e1, Edge<Vertex> const &e2) const
    {
        return (e1.first==e2.first && e1.second==e2.second);
    }
};


template <class G>
class GGraph : public crtp<G, GGraph>
{
public:
    using V = typename crtp<G, GGraph>::template_type;

    G &addVertex(V const &v)
    {
        return this->underlying().addVertex(v);
    }

    G &remVertex(V const &v)
    {
        return this->underlying().remVertex(v);
    }

    G &addEdge(V const &v1, V const &v2, float w)
    {
        return this->underlying().addEdge(v1,v2,w);
    }

    G &remEdge(V const &v1, V const &v2, float w)
    {
        return this->underlying().remEdge(v1,v2,w);
    }

    // size_t vertexSize() const {return vertices_.size();}
    // size_t edgeSize() const {return edges_.size();}
    // bool hasVertex(Vertex const &v) const {return vertices_.find(v) != vertices_.end();}
    // bool hasEdge(Edge_t const &e) const {return edges_.find(e) != edges_.end();}

protected:

private:
};

template <class Vertex, class EH=std::hash<Edge<Vertex>>, class EE=std::equal_to<Edge<Vertex>>>
class DGraph : public GGraph<DGraph<Vertex> >
{
protected:
    std::unordered_map<Vertex, std::vector<Adjacent<Vertex>>> vertices_;
    std::unordered_map<Edge<Vertex>, float, EH, EE> edges_;
    
public:
    DGraph &addVertex(Vertex const &v)
    {
        LOGD("");
        return *this;
    }
};

// template <class V>
// class UnGraph : public GGraph<UnGraph<V> >
// {
// public:
//     UnGraph &addVertex(V const &v)
//     {
//         LOGD("");
//         return *this;
//     }
// };