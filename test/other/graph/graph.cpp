#include <iostream>
#include <vector>
#include <set>
#include <list>
#include <unordered_map>
#include "../../../utils/MyCounter.h"

struct Point
{
	float x,y;
	Point(float x, float y) : x(x), y(y) {} 
	Point(Point const &p) {x=p.x; y=p.y;}

	// bool operator < (Point const &p) const
	// {
	// 	float len = x*x+y*y;
	// 	float lenp = p.x*p.x+p.y*p.y;
	// 	if(len != lenp) return len < lenp;
	// 	else return x < p.x;
	// }

	Point &operator=(Point const &point)
	{
		x = point.x;
		y = point.y;
		return *this;
	}

	bool operator==(Point const &p) const
	{
		return x == p.x && y == p.y;
	}
};

namespace std {
template <>
struct hash<Point>
{
	std::size_t operator()(const Point& p) const
	{
		using std::size_t;
		using std::hash;

		return (hash<float>()(p.x)
	       ^ (hash<float>()(p.y) << 1));
	}
};

template <>
struct hash<std::pair<Point, Point>>
{
	std::size_t operator()(const std::pair<Point, Point>& p) const
	{
		using std::size_t;
		using std::hash;

		return (hash<float>()(p.first.x)
	       ^ (hash<float>()(p.first.y) << 1))
		^ (hash<float>()(p.second.x)
	       ^ (hash<float>()(p.second.y) << 1));
	}
};
}

class Edge;

class Vertex
{
public:
	Vertex() : point_(0,0) {}

	Vertex(Point const &point) 
		: point_(point) 
	{}

	Vertex &operator=(Vertex const &v)
	{
		point_ = v.point_;
		return *this;
	}

	void addProtrude(Edge *e)
	{
		protrudes_.push_back(e);
	}

	// Vertex *adjacent()
	// {
	// 	return protrudes_;
	// }

	Point point_;
	std::list<Edge*> protrudes_;
};

class Edge
{
public:
	Edge(Point const &from, Point const &to, float weight)
		: from_(from)
		, to_(to)
		, weight_(weight)
	{}

	bool operator==(Edge const &e)
	{
		return from_ == e.from_ && to_ == e.to_ && weight_ == e.weight_;
	}

	Point from_;
	Point to_;
	float weight_;
};

class Graph
{
public:
	Graph() 
		: vertices_(0x1000) 
	{}

	template <typename... Args>
	Graph(Args&&... args) 
		: vertices_(0x1000)
	{
		// addEdge(std::forward<T>(first));
		addEdge(std::forward<Args>(args)...);
	}

	Vertex &addVertex(Point const &p)
	{
		if(!vertices_.count(p)) 
			vertices_[p] = Vertex(p);
		
		return vertices_[p];
	}

	void removeVertex(Point const &p);
	void removeEdge(Edge const &e);

	// void addEdge(Point const &f, Point const &t, float w)
	// void addEdge(Edge const &e)

	template <typename T>
	void addEdge(T &&t)
	{
		Vertex &from = addVertex(t.from_);
		Vertex &to = addVertex(t.to_);
		edges_.push_front(t);
		from.addProtrude(&*edges_.begin());
	}

	template <typename T, typename... Args>
	void addEdge(T &&first, Args&&... args)
	{
		addEdge(std::forward<T>(first));
		addEdge(std::forward<T>(args)...);
	}

	template <typename T>
	void addBiEdge(T &&t)
	{
		Vertex &from = addVertex(t.from_);
		Vertex &to = addVertex(t.to_);
		edges_.push_front(t);
		from.addProtrude(&*edges_.begin());
		edges_.push_front(T{t.to_,t.from_,t.weight_});
		to.addProtrude(&*edges_.begin());
	}

	template <typename T, typename... Args>
	void addBiEdge(T &&first, Args&&... args)
	{
		addBiEdge(std::forward<T>(first));
		addBiEdge(std::forward<T>(args)...);
	}

	void dump()
	{
		for(auto &it : vertices_)
		{
			Vertex &v = it.second;
			printf("(%.2f:%.2f)", v.point_.x, v.point_.y);
			for(auto e : v.protrudes_)
			{
				printf(" - (%.2f:%.2f|%.2f)", e->to_.x, e->to_.y, e->weight_);
			}
			printf("\n");
		}
	}

	std::unordered_map<Point, Vertex> vertices_;
	std::list<Edge> edges_;
};

std::unordered_map<std::thread::id, int> custom::MyCounter::indent;

int main(int argc, char const *argv[])
{ MY_COUNTER();

	{ MY_COUNTER();
		Graph gr;
		gr.addBiEdge(Edge{{0,0},{0,1},1});
		gr.addBiEdge(Edge{{0,0},{1,1},1.2});
		gr.addBiEdge(Edge{{0,0},{1,0},1});

		gr.addBiEdge(Edge{{1,0},{1,1},1});
		gr.addBiEdge(Edge{{0,1},{1,1},1});
		gr.dump();
	}
	return 0;
}

