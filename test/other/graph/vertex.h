#pragma once

#include <vector>
#include <iostream>
#include <list>
#include <unordered_map>
#include <algorithm>

#include "vec2.h"

class Vertex
{
public:
    Vec2 p_;                        ///< position
    std::list<Vec2> adj_to_;        ///< adjacent vertices is connecting to
    /// be bypassed in undirected mode
    std::list<Vec2> adj_from_;      ///< adjacent vertices is being connected from

public:
    bool operator==(Vertex const &v) const;
    void addAdjacentTo(Vec2 const &v);
    void addAdjacentTo(Vertex const &v);

    void remAdjacentTo(Vec2 const &v);
    void remAdjacentTo(Vertex const &v);

    void addAdjacentFrom(Vec2 const &v);
    void addAdjacentFrom(Vertex const &v);

    void remAdjacentFrom(Vec2 const &v);
    void remAdjacentFrom(Vertex const &v);
};

bool Vertex::operator==(Vertex const &v) const 
{
    return p_ == v.p_;
}

void Vertex::addAdjacentTo(Vec2 const &v)
{
    auto it = std::find(adj_to_.begin(), adj_to_.end(), v);

    if(it == adj_to_.end())
    {
        adj_to_.push_back(v);
    }
}

void Vertex::addAdjacentTo(Vertex const &v)
{
    addAdjacentTo(v.p_);
}

void Vertex::remAdjacentTo(Vec2 const &v)
{
    auto it = std::find(adj_to_.begin(), adj_to_.end(), v);

    if(it != adj_to_.end())
    {
        adj_to_.erase(it);
    }
}

void Vertex::remAdjacentTo(Vertex const &v)
{
    remAdjacentTo(v.p_);
}


void Vertex::addAdjacentFrom(Vec2 const &v)
{
    auto it = std::find(adj_from_.begin(), adj_from_.end(), v);

    if(it == adj_from_.end())
    {
        adj_from_.push_back(v);
    }
}

void Vertex::addAdjacentFrom(Vertex const &v)
{
    addAdjacentFrom(v.p_);
}

void Vertex::remAdjacentFrom(Vec2 const &v)
{
    auto it = std::find(adj_from_.begin(), adj_from_.end(), v);

    if(it != adj_from_.end())
    {
        adj_from_.erase(it);
    }
}

void Vertex::remAdjacentFrom(Vertex const &v)
{
    remAdjacentFrom(v.p_);
}

std::ostream &operator<<(std::ostream &os, Vertex const &v)
{
    os << "(" << v.p_ << ")";
    // for(auto &a : v.adj_)
    // {
    //     os << "-" << a;
    // }

    return os;
}