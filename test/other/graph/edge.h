#pragma once

#include <vector>
#include <iostream>
#include <list>
#include <unordered_map>
#include <algorithm>

#include "vec2.h"

class Edge
{
public:
    Vec2 e1_, e2_;  ///< end points
    float w_;       ///< weight

public:
    // typedef std::list<Edge>::iterator __EItor;
    // typedef std::unordered_map<Vec2, Vertex>::iterator __VItor;

    // bool operator==(Edge const &e) const;
};

// bool Edge::operator==(Edge const &e) const
// {return (e1_ == e.e1_ && e2_ == e.e2_);}

std::ostream &operator<<(std::ostream &os, Edge const &e)
{
    os << "(" << e.e1_ << "-" << e.e2_ << "|" << e.w_ << ")";
    return os;
}
