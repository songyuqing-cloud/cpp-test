#pragma once
#include <vector>
#include <unordered_map>
#include <tuple>
#include <queue>
#include <algorithm>

#include "graph.h"

template <class Vertex>
struct HeuEmpty
{
    double operator()(Vertex const &v1, Vertex const &v2) const
    {
        return 0;
    }
};


template <class Vertex>
using pqueue = std::priority_queue<Adjacent<Vertex>, std::vector<Adjacent<Vertex>>,
                 std::greater<Adjacent<Vertex>>>;

template<typename Vertex>
std::vector<Vertex> reconstructPath(
    Vertex start, Vertex goal,
    std::unordered_map<Vertex, Vertex> &came_from)
{
    std::vector<Vertex> path;
    Vertex current = goal;

    while (current != start) 
    {
        path.push_back(current);
        current = came_from[current];
    }
    path.push_back(start); // optional
    std::reverse(path.begin(), path.end());

    return path;
}

template<class Vertex, class Graph, template <class> class Heuristic=HeuEmpty>
void pathFinding
    (Graph graph,
    Vertex start,
    Vertex goal,
    std::unordered_map<Vertex, Vertex> &came_from,
    std::unordered_map<Vertex, float> &cost_so_far)
{
    pqueue<Vertex> frontier;
    frontier.emplace(start, 0);
    came_from.clear(); cost_so_far.clear();

    came_from[start] = start;
    cost_so_far[start] = 0;
  
    while (!frontier.empty()) 
    {
        Vertex current = frontier.top().first;
        frontier.pop();

        if (current == goal) break;
        for (auto &[next, weight] : graph.neighbors(current)) 
        {
            /// 
            if(weight == MAX_WEIGHT) continue;
            float new_cost = cost_so_far[current] + weight;

            if ((cost_so_far.find(next) == cost_so_far.end())
                || (new_cost < cost_so_far[next])) 
            {
                cost_so_far[next] = new_cost;
                float priority = new_cost + Heuristic<Vertex>()(next, goal);
                frontier.emplace(next, priority);
                came_from[next] = current;
            }
        }
    }
}
