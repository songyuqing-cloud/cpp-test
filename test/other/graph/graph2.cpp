#include <iostream>
#include <list>
#include <unordered_map>
#include <algorithm>
#include <vector>

struct Vec2
{
float x;
float y;

bool operator==(Vec2 const &v) const {return x==v.x &&y==v.y;}
};

class Vertex;

class Edge
{
public:

    bool operator==(Edge const &e) const;
    void dump();

    // bool operator==(Edge const &e){return (f_ == e.f_ && t_ == e.t_)/* || (*f_ == *e.f_ && *t_ == *e.t_)*/;}
    // void dump()
    // {
    //     printf("(%.2f:%.2f-%.2f:%.2f-%.2f)\n", f_->p_.x, f_->p_.y, t_->p_.x, t_->p_.y, w_);
    // }

    // Vec2 f_;
    // Vec2 t_;
    std::list<Vertex>::iterator f_, t_;
    float w_;
};

class Vertex
{
public:
    typedef std::list<Edge>::iterator __EItor;
    typedef std::list<Vertex>::iterator __VItor;

    bool operator==(Vertex const &v) const {return p_ == v.p_;}

    __EItor addEdge(__EItor e)
    {
        adj_.push_back(e);
        return adj_.back();
    }

    void remEdge(__EItor const &e)
    {
        auto it = std::find(adj_.begin(), adj_.end(), e);

        if(it != adj_.end()) adj_.erase(it);

        // return it;
    }

    std::vector<std::list<Vertex>::iterator> getAdjacents()
    {
        std::vector<std::list<Vertex>::iterator> adj;

        for(auto &it : adj_) adj.push_back(it->t_);

        return adj;
    }

    void dump()
    {
        printf("(%.2f:%.2f)", p_.x, p_.y);
        for(auto &e : adj_)
        {
            printf("-(%.2f:%.2f|%.2f)", e->t_->p_.x, e->t_->p_.y, e->w_);
        }
        printf("\n");
    }


    Vec2 p_; //! position
    std::list<__EItor> adj_;
    std::list<__VItor> child_;
};

bool Edge::operator==(Edge const &e) const 
{
    return (f_ == e.f_ && t_ == e.t_) || (*f_ == *e.f_ && *t_ == *e.t_);
}

void Edge::dump()
{
    printf("(%.2f:%.2f-%.2f:%.2f-%.2f)\n", f_->p_.x, f_->p_.y, t_->p_.x, t_->p_.y, w_);
}

class Graph
{
public:
    typedef std::list<Edge>::iterator __EItor;
    typedef std::list<Vertex>::iterator __VItor;

    Graph()
    {

    }

    __VItor addVertex(Vec2 const &v)
    {
        return addVertex(Vertex{v});
    }

    __VItor addVertex(Vertex const &v)
    {
        auto it = std::find(vertices_.begin(), vertices_.end(), v);
        if(it == vertices_.end())
        {
            return vertices_.insert(vertices_.end(), Vertex{v});
            // return vertices_.end() - 1;
        }

        return it;
    }

    __VItor remVertex(__VItor &it)
    {
        return vertices_.erase(it);
    }

    __VItor remVertex(Vec2 const &v)
    {
        auto it = std::find(vertices_.begin(), vertices_.end(), Vertex{v});
        if(it != vertices_.end())
        {
            for(auto &e : it->adj_)
            {
                remEdge(e);
            }
            return vertices_.erase(it);
        }

        return it;
    }

    __EItor addEdge(Vec2 const &from, Vec2 const &to, float weight)
    {
        auto ifrom = addVertex(from);
        auto ito = addVertex(to);
        auto it = edges_.insert(edges_.end(), Edge{ifrom, ito, weight});

        ifrom->addEdge(it);

        return it;
    }

    __EItor addEdge(Edge const &e)
    {
        auto ifrom = addVertex(*(e.f_));
        auto ito = addVertex(*e.t_);
        auto it = edges_.insert(edges_.end(), Edge{ifrom, ito, e.w_});

        ifrom->addEdge(it);

        return it;
    }

    __EItor remEdge(__EItor &it)
    {
        return edges_.erase(it);
    }

    void remEdge(Vec2 const &from, Vec2 const &to)
    {
        auto ifrom = std::find(vertices_.begin(), vertices_.end(), Vertex{from});
        auto ito = std::find(vertices_.begin(), vertices_.end(), Vertex{to});

        if(ifrom == vertices_.end() || ito == vertices_.end()) return;

        Edge E{ifrom, ito, 0};
        auto it = std::find(edges_.begin(), edges_.end(), E);

        while(it != edges_.end())
        {
            it->f_->remEdge(it);
            it = edges_.erase(it);
        }

        // return it;
    }

    std::vector<std::list<Vertex>::iterator> getAdjacents(Vec2 const &v)
    {
        std::vector<std::list<Vertex>::iterator> adj;
        auto it = std::find(vertices_.begin(), vertices_.end(), Vertex{v});

        if(it != vertices_.end())
        {
            return it->getAdjacents();
        }

        return adj;
    }

    void dump()
    {
        printf("V:%ld - E:%ld\n", vertices_.size(), edges_.size());
        for(auto &v : vertices_)
        {
            v.dump();
        }
    }

    void merge(Graph const &g)
    {
        for(auto &v : g.vertices_)
            addVertex(v);

        for(auto &e : g.edges_)
            addEdge(e);
    }

    std::list<Vertex> vertices_;
    std::list<Edge> edges_;
};

void test01()
{
    Graph G;

    G.addEdge({0,0},{0,1},1);
    G.addEdge({0,0},{1,0},1);
    G.addEdge({0,1},{1,0},1.2f);
    G.addEdge({1,0},{0,1},1.2f);
    G.dump();
    // V:3 - E:4
    // (0.00:0.00)-(0.00:1.00|1.00)-(1.00:0.00|1.00)
    // (0.00:1.00)-(1.00:0.00|1.20)
    // (1.00:0.00)-(0.00:1.00|1.20)

    G.remVertex({0,0});
    G.dump();
    // V:2 - E:2
    // (0.00:1.00)-(1.00:0.00|1.20)
    // (1.00:0.00)-(0.00:1.00|1.20)

    G.addEdge({0,0},{0,1},1);
    G.addEdge({0,0},{1,0},1);
    G.addEdge({0,0},{1,0},2);
    G.dump();
    // V:3 - E:5
    // (0.00:1.00)-(1.00:0.00|1.20)
    // (1.00:0.00)-(0.00:1.00|1.20)
    // (0.00:0.00)-(0.00:1.00|1.00)-(1.00:0.00|1.00)-(1.00:0.00|2.00)

    auto adj = G.getAdjacents({0,0});
    printf("Adjs:\n");
    for(auto &v : adj)
        v->dump();
    // Adjs:
    // (0.00:1.00)-(1.00:0.00|1.20)
    // (1.00:0.00)-(0.00:1.00|1.20)
    // (1.00:0.00)-(0.00:1.00|1.20)

    G.remEdge({0,0},{1,0});
    G.remEdge({0,0},{1,1}); // invalid removal
    G.dump();
    // V:3 - E:3
    // (0.00:1.00)-(1.00:0.00|1.20)
    // (1.00:0.00)-(0.00:1.00|1.20)
    // (0.00:0.00)-(0.00:1.00|1.00)
}

void test_merge()
{
    Graph G1, G2, G3;
    G1.addEdge({0,0},{0,1},1);
    G1.addEdge({0,0},{1,0},1);
    G1.dump();

    G2.addEdge({0,1},{1,0},1.2f);
    G2.addEdge({1,0},{0,1},1.2f);
    G2.dump();

    G1.merge(G2);
    G1.dump();

    G3.addEdge({0,0},{1,1},1.2f);
    G1.merge(G3);
    G1.dump();
}

int main()
{
    test01();
    test_merge();

    return 0;
}