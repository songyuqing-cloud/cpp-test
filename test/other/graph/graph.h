#pragma once

#include <iostream>
#include <vector>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <cassert>

using Weight = float;
Weight static const MAX_WEIGHT = 999999.f;

/// Adjacent type
template <class Vertex/*, typename Weight*/>
using Adjacent = std::pair<Vertex, Weight>;

template<class Vertex>
bool operator==(Adjacent<Vertex> const &a1, Adjacent<Vertex> const &a2)
{
    return a1.first == a2.first;
}

template<class Vertex>
bool operator < (Adjacent<Vertex> const &a1, Adjacent<Vertex> const &a2)
{
    return a1.second < a2.second;
}

template <class Vertex>
using Edge = std::pair<Vertex, Vertex>;

/// Directed edge hasher
template <class Vertex>
struct std::hash<Edge<Vertex>>
{
    inline size_t operator()(Edge<Vertex> const &e) const
    {
        return (std::hash<Vertex>()(e.first) << 1) ^ std::hash<Vertex>()(e.second);
    }
};

/// Directed edge equal
template <class Vertex>
struct std::equal_to<Edge<Vertex>>
{
    inline bool operator()(Edge<Vertex> const &e1, Edge<Vertex> const &e2) const
    {
        return (e1.first==e2.first && e1.second==e2.second);
    }
};

/// Directed graph
template <class Vertex, class EdgeHash=std::hash<Edge<Vertex>>, class EdgeEqual=std::equal_to<Edge<Vertex>>>
class Graph
{
public:
    using Edge_t = Edge<Vertex>;
    using Adjacent_t = Adjacent<Vertex>;
// struct Face
// {
//     std::vector<Vertex> points_;
// };

protected:
    std::unordered_map<Vertex, std::vector<Adjacent_t>> vertices_;
    std::unordered_map<Edge_t, Weight, EdgeHash, EdgeEqual> edges_;
    // std::unordered_map<Vertex, std::list<Adjacent_t>> faces_;

public:
    inline size_t vertexSize() const {return vertices_.size();}
    inline size_t edgeSize() const {return edges_.size();}
    inline bool hasVertex(Vertex const &v) const {return vertices_.find(v) != vertices_.end();}
    inline bool hasEdge(Edge_t const &e) const {return edges_.find(e) != edges_.end();}

    void addVertex(Vertex const &v);
    void addEdge(Vertex const &v1, Vertex const &v2, float weight);

    void remVertex(Vertex const &p);
    void remEdge(Vertex const &v1, Vertex const &v2);

    std::vector<Adjacent_t> const &neighbors(Vertex const &v) {return vertices_[v];}
    // float cost(Vertex const &p1, Vertex const &p2) {return std::find(vertices_[p1].begin(),vertices_[p1].end(),p2).second;}

    template <class,class,class>
    friend std::ostream &operator<<(std::ostream &os, Graph<Vertex, EdgeHash, EdgeEqual> const &g);
};


template <class Vertex, class EdgeHash, class EdgeEqual>
void Graph<Vertex, EdgeHash, EdgeEqual>::addVertex(Vertex const &v)
{
    vertices_.insert({v,{}});
}

template <class Vertex, class EdgeHash, class EdgeEqual>
void Graph<Vertex, EdgeHash, EdgeEqual>::addEdge(Vertex const &v1, Vertex const &v2, float weight)
{
    /// add 2 enpoints
    addVertex(v1);
    addVertex(v2);
    /// add new edge
    edges_[{v1, v2}] = weight;
    /// add adjacents
    auto &end1_adj = vertices_[v1];
    auto ait = std::find_if(end1_adj.begin(), end1_adj.end(), [v2](Adjacent_t const &adj){return adj.first == v2;});
    if(ait == end1_adj.end())
        end1_adj.push_back({v2, weight});
    else
        ait->second = weight;

    auto &end2_adj = vertices_[v2];
    ait = std::find_if(end2_adj.begin(), end2_adj.end(), [v1](Adjacent_t const &adj){return adj.first == v1;});
    if(ait == end2_adj.end())
        vertices_[v2].push_back({v1, MAX_WEIGHT});
}

template <class Vertex, class EdgeHash, class EdgeEqual>
void Graph<Vertex, EdgeHash, EdgeEqual>::remVertex(Vertex const &v)
{
    auto vit = vertices_.find(v);

    if(vit != vertices_.end())
    {
        /// remove all relation vertices & edges
        for(auto &[adj_vertex, list] : vertices_[v])
        {
            (void)(list);
            auto &adj_list = vertices_[adj_vertex];
            /// remove adjacents
            adj_list.erase(std::find_if(adj_list.begin(), adj_list.end(), [v](Adjacent_t const &adj){return adj.first == v;}));
            /// remove protrudes
            auto eit = edges_.find({adj_vertex, v});
            if(eit != edges_.end()) edges_.erase(eit);

            eit = edges_.find({v, adj_vertex});
            if(eit != edges_.end()) edges_.erase(eit);
        }

        vertices_.erase(vit);
    }
}

template <class Vertex, class EdgeHash, class EdgeEqual>
void Graph<Vertex, EdgeHash, EdgeEqual>::remEdge(Vertex const &v1, Vertex const &v2)
{
    auto eit = edges_.find({v1, v2});

    if(eit != edges_.end())
    {
        /// remove/update all adjacents
        auto &adjs1 = vertices_[v1];
        auto &adjs2 = vertices_[v2];
        /// get adjacents
        auto a12 = std::find_if(adjs1.begin(), adjs1.end(), [v2](Adjacent_t const &adj){return adj.first == v2;});
        auto a21 = std::find_if(adjs2.begin(), adjs2.end(), [v1](Adjacent_t const &adj){return adj.first == v1;});
        assert(a12 != adjs1.end());
        assert(a21 != adjs2.end());
        /// no connected edge
        if(a21->second == MAX_WEIGHT)
        {
            adjs1.erase(a12);
            adjs2.erase(a21);
        }
        else
        {
            a12->second = MAX_WEIGHT;
        }
        /// remove edge
        edges_.erase(eit);
    }
}

template <class Vertex, class EdgeHash, class EdgeEqual>
std::ostream &operator<<(std::ostream &os, Graph<Vertex, EdgeHash, EdgeEqual> const &g)
{
    os <<"V: " << g.vertices_.size();
    for(auto &[vertex, adj_list] : g.vertices_)
    {
        os << "\n(" << vertex << ")";
        for(auto &[v, w] : adj_list)
        {
            // if(adj.second < MAX_WEIGHT)
            os << "-(" << v << "|";
            (w < MAX_WEIGHT) ? os << w : os << "INF" ;
            os << ")";
        }
    }

    os <<"\nE: " << g.edges_.size();
    for(auto &[end_point, weight] : g.edges_)
    {
        os << "\n(" << end_point.first << "-" << end_point.second << "|" << weight << ")";
    }

    return os;
}

template <typename G>
struct GraphType;

template <template <class, class...> class G, class V, class ...Ts>
struct GraphType<G<V,Ts...>>
{
    using Vertex = V;
};