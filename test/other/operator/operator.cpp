#include <iostream>
#include <cstdio>


#include <test/macro.h>

template<typename _Tp> class Example
{
public:
    
    Example &operator<<(_Tp aTp)
    {
        _LOGI("0x%lx", aTp);
        return (*this);
    }
};

template<typename _Tp, typename T2=int> static inline
Example<_Tp> operator << (const Example<_Tp>& m, T2 val)
{
    _LOGI("0x%lx", val);
    return m;
}

template<typename _Tp, typename T2=int> static inline
Example<_Tp> operator , (const Example<_Tp>& m, T2 val)
{
    _LOGI("0x%lx", val);
    return m;
}

int main()
{
    using namespace std;
    
    Example<double> e;
    
    e << (double)1 << 2, 3, 4;
    
    _LOGI("");
    
    return 0;
}