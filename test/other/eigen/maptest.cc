#include <iostream>
#include <random>
#include <string>
#include <Eigen/Core>
#include <igl/triangle/triangulate.h>

#include "../../../../cpp-test/test/utils.h"

template <typename T>
T ranPoint(T const &t1, T const &t2, T const &t3)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(0, 1.f);
    float u = 1;
    float v = 1;
    while(u+v>=1 && u+v==0)
    {
        u = dis(gen);
        v = dis(gen);
    }
    return t1 + u*(t2-t1) + v*(t3-t1);
}

void triangulateTest()
{
    using clock = std::chrono::high_resolution_clock;
    using namespace Eigen;
    using namespace std;
    TIMER(main);
    LOGD("");
    auto beg = clock::now();
    // Input polygon
    Eigen::MatrixXd V;
    Eigen::MatrixXi E;
    Eigen::MatrixXd H;

    // Triangulated interior
    Eigen::MatrixXd V2;
    Eigen::MatrixXi F2;

    // Create the boundary of a square
    V.resize(4,2);
    E.resize(4,2);
    H.resize(1,2);

    // V << -0.5,-0.5, 0.5,-0.5, 0.5,0.5, -0.5, 0.5,
       // -1,-1, 1,-1, 1,1, -1, 1;
    // E << 0,1, 1,2, 2,3, 3,0,
    //    4,5, 5,6, 6,7, 7,4;
    std::vector<float> verts_data;
    std::vector<int> indices_data;
    V <<2784.00000,1728.00000, 0.000000000,1728.00000, 0.000000000,0.000000000, 2784.00000,0.000000000;
    E<<0,1,1,2,2,3,3,0;
    verts_data.push_back(2784.00000);
    verts_data.push_back(1728.00000);
    verts_data.push_back(0.000000000);
    verts_data.push_back(1728.00000);
    verts_data.push_back(0.000000000);
    verts_data.push_back(0.000000000);
    verts_data.push_back(2784.00000);
    verts_data.push_back(0.000000000);

    indices_data.push_back(0);
    indices_data.push_back(1);
    indices_data.push_back(1);
    indices_data.push_back(2);
    indices_data.push_back(2);
    indices_data.push_back(3);
    indices_data.push_back(3);
    indices_data.push_back(0);

    int rows = verts_data.size()/2;
    int cols = 2;
    /// construct eigen
    Eigen::MatrixXf Vc = Eigen::Map<Eigen::Matrix<float,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >(verts_data.data(), rows, cols);
    Eigen::MatrixXi Ec = Eigen::Map<Eigen::Matrix<int,Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> >(indices_data.data(), rows, cols);

    std::cout << V << std::endl;
    std::cout << Vc << std::endl;
    std::cout << E << std::endl;
    std::cout << Ec << std::endl;
    H << 0,0;
    // Triangulate the interior
    igl::triangle::triangulate(Vc,Ec,H,"a9999999",V2,F2);
    LOGD("%.3f", __timer_main.elapse());
    std::cout << V2.rows() << std::endl;
    std::cout << F2.rows() << std::endl;
}

void testCommaInit()
{
    Eigen::MatrixXd V;
    // V.resize(2,2);
    // V << 1,1;
    // V();
    std::cout << V << std::endl;
}

int main()
{
    triangulateTest();
    // testCommaInit();

    return 0;
}