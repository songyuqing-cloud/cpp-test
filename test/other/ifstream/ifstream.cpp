#include <vector>
#include <list>
#include <cassert>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>

#define _LOGE(format, ...) fprintf(stderr, "[E]%s %s [%d] " format ": %s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
#define _LOGI(format, ...) printf("[I]%s %s [%d] " format "\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

int main()
{
    {
        std::remove("t1.txt");
        std::ofstream l_out("t1.txt");

        l_out << "line1\n";
        l_out << "line2\n";
    }

    {
        std::remove("t2.txt");
        std::ofstream l_out("t2.txt");

        std::ifstream l_in("t1.txt");

        std::string str;
        l_in >> str;
        _LOGI("%s", str.data());
        l_out << str;
        l_out << '\n';

        l_in >> str;
        _LOGI("%s", str.data());
        l_out << str;
        l_out << '\n';
    }

    return 0;
}