#include <iostream>
#include <cstring>      // Needed for memset
#include <sys/socket.h> // Needed for the socket functions
#include <netdb.h>      // Needed for the socket functions
#include <cstdlib>
#include <string>
#include <cstdio>
#include "base64.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <limits.h>
#include <errno.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <sstream>
#include "openssl/ssl.h"
#include "pthread.h"
using namespace std;

#define LOGI(format,...) printf("%s[%s][%i] " format "\n", __FILE__, __FUNCTION__, __LINE__,##__VA_ARGS__)

int main(int argc, char* argv[])
{
    addrinfo host_info;       // The struct that getaddrinfo() fills up with data.
    addrinfo *host_info_list; 
    int socketfd;
    char* msg = NULL;
    char* msg2 = NULL;
    int status;
    int len;

    memset(&host_info, 0, sizeof host_info);

    host_info.ai_family = AF_INET;//AF_UNSPEC;
    host_info.ai_socktype = SOCK_STREAM;

    //PROXY IP = proxy.fing.edu.uy ; PORT = 3128 ; //HTTP1.0 proxy

    status = getaddrinfo("fsoft-proxy", NULL, &host_info, &host_info_list);
    LOGI("%d", status);
    ((struct sockaddr_in *)(host_info_list->ai_addr))->sin_port = htons(8080);
    socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype, 
    host_info_list->ai_protocol);
    LOGI("%d", socketfd);

    if (socketfd == -1)  LOGI("ERROR: socket error ");

    LOGI("Connect()ing...");

    status = connect(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if (status == -1)  LOGI("ERROR: connect error");
    LOGI("%d", status);
    msg = new char[9999];
    // string autho = argv[1] + ":" + argv[2];
    string autho = "longlt3:Nemchuaran2";
    string authorize = base64_encode((unsigned char const*)autho.c_str(), autho.size());
    authorize = "Proxy-authorization: Basic " + authorize + "\r\n";
    LOGI("%s", authorize.c_str());
    strcpy(msg,"CONNECT www.google.com.vn:443 HTTP/1.1\r\n");
    strcat(msg, authorize.c_str());
    strcat(msg,"\r\n");

    LOGI("%s", msg);
    ssize_t bytes_sent;
    len = strlen(msg);
    bytes_sent = send(socketfd, msg, len, 0);

    ssize_t bytes_recieved=0;
    LOGI("Waiting to recieve data...");

    char* incoming_data_buffer = new char[9999];
    bytes_recieved = recv(socketfd, incoming_data_buffer,9999, 0);

    if (bytes_recieved == 0) LOGI("host shut down.");
    if (bytes_recieved == -1) LOGI("ERROR: receive error!");
    LOGI("%ld  bytes recieved", bytes_recieved);
    LOGI("%s", incoming_data_buffer);

    SSL_library_init();
    SSL_CTX *the_ctx = SSL_CTX_new(SSLv3_client_method());
    if (the_ctx == NULL)
    {
        LOGI("");
        return 1;
    }
    SSL *the_ssl = SSL_new(the_ctx);
    if (the_ssl == NULL)
    {
        LOGI("");
        return 1;
    }
    int retcode = SSL_set_fd(the_ssl, socketfd);
    if (retcode != 1)
    {
        LOGI("");
        return 1;
    }
    retcode = SSL_connect(the_ssl);
    if (retcode != 1)
    {
        LOGI("");
        return 1;
    }
    LOGI("");




    SSL_CTX_free(the_ctx);
    SSL_free(the_ssl);
    close(socketfd);

    socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype, 
    host_info_list->ai_protocol);

    if (socketfd == -1)  LOGI("ERROR: socket error ");
    LOGI("%d", socketfd);
    status = connect(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    strcpy(msg,"CONNECT www.google.com.vn:443 HTTP/1.1\r\n");
    strcat(msg,"\r\n");
    len = strlen(msg);
    bytes_sent = send(socketfd, msg, len, 0);
    bytes_recieved = recv(socketfd, incoming_data_buffer,9999, 0);

    if (bytes_recieved == 0) LOGI("host shut down.");
    if (bytes_recieved == -1) LOGI("ERROR: receive error!");
    LOGI("%ld  bytes recieved", bytes_recieved);
    LOGI("%s", incoming_data_buffer);



    msg2 = new char[300];
    strcpy(msg2,"GET http://www.google.com/ HTTP/1.0\r\n\r\n");

    LOGI("Message sent to google: %s", msg2);

    len = strlen(msg2);
    bytes_sent = send(socketfd, msg2, len, 0);

    LOGI("bytes_sent: %ld", bytes_sent);

    bytes_recieved=0;
    LOGI("Waiting to recieve data ...");

    char* incoming_data_buffer2 = new char[1000];
    bytes_recieved = recv(socketfd, incoming_data_buffer2,1000, 0);


    if (bytes_recieved == 0) LOGI("host shut down.");
    if (bytes_recieved == -1) LOGI("ERROR: recieve error!");
    LOGI("%ld  bytes recieved", bytes_recieved);
    LOGI("%s", incoming_data_buffer);
    return 0;
}
