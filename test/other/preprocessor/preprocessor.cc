#define DEVICEHELPER "DeviceHelper"

#define LOG_I(tag, message) LOG_WITH_LEVEL(fnv::sync::core::LogSeverity::Info, tag, message)

#define LOG_WITH_LEVEL(level, tag, message) LOG_WITH_LEVEL_LINE(level, tag, message, __LINE__)

#define LOG_WITH_LEVEL_LINE(level, tag, message, line)  \
  do {                                                        \
    if (level >= fnv::sync::core::Log::mMinSeverity) {                     \
        std::stringstream __buffer;                          \
        __buffer << message;                                 \
        fnv::sync::core::Log::print(level, tag, __buffer, line); \
    }                                                         \
  } while (false)

void abc() {
    LOG_I(DEVICEHELPER, "processing commander request: " << request);
}

void abc()
{
    do 
    { 
        if (fnv::sync::core::LogSeverity::Info >= fnv::sync::core::Log::mMinSeverity) 
        { 
            std::stringstream __buffer;
            __buffer << "processing commander request: " << request;
            fnv::sync::core::Log::print(fnv::sync::core::LogSeverity::Info, "DeviceHelper", __buffer, 17); 
        } 
    } while (false);
}