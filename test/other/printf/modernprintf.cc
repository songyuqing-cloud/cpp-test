#include <string>
#include <cstdio>
#include <cassert>

#include "../../utils.h"
/// https://docs.microsoft.com/en-us/archive/msdn-magazine/2015/may/windows-with-c-adding-compile-time-type-checking-to-printf
/// print
template <typename T>
T Argument(T value) noexcept
{
  return value;
}

template <typename T>
T const * Argument(std::basic_string<T> const & value) noexcept
{
  return value.data();
}

template <typename ... Args>
void Print(char const * const format,
           Args const & ... args) noexcept
{
  printf(format, Argument(args) ...);
}

inline void Print(char const * const value) noexcept
{
  Print("%s", value);
}

inline void Print(wchar_t const * const value) noexcept
{
  Print("%ls", value);
}

template <typename T>
void Print(std::basic_string<T> const & value) noexcept
{
  Print(value.c_str());
}

/// format
template <typename ... Args>
int StringPrint(char * const buffer,
                size_t const bufferCount,
                char const * const format,
                Args const & ... args) noexcept
{
  int const result = snprintf(buffer,
                              bufferCount,
                              format,
                              Argument(args) ...);
  assert(-1 != result);
  return result;
}

template <typename ... Args>
int StringPrint(wchar_t * const buffer,
                size_t const bufferCount,
                wchar_t const * const format,
                Args const & ... args) noexcept
{
  int const result = swprintf(buffer,
                              bufferCount,
                              format,
                              Argument(args) ...);
  assert(-1 != result);
  return result;
}

template <typename T, typename ... Args>
std::basic_string<T> Format(
            T const * const format,
            Args const & ... args)
{
  std::basic_string<T> buffer;
  size_t const size = StringPrint(&buffer[0],
                                  buffer.size() + 1,
                                  format,
                                  args ...);
  if (size > 0)
  {
    buffer.resize(size);
    StringPrint(&buffer[0], buffer.size() + 1, format, args ...);
  }

  return buffer;
}

///
template <typename P>
void Append(P target, char const * const value, size_t const size)
{
  target("%.*s", size, value);
}

void Append(std::string & target,
  char const * const value, size_t const size)
{
  target.append(value, size);
}

template <typename P, typename ... Args>
void AppendFormat(P target, char const * const format, Args ... args)
{
  target(format, args ...);
}

template <typename ... Args>
void AppendFormat(std::string & target,
  char const * const format, Args ... args)
{
  int const back = target.size();
  int const size = snprintf(nullptr, 0, format, args ...);
  target.resize(back + size);
  snprintf(&target[back], size + 1, format, args ...);
}

template <typename Target>
void WriteArgument(Target & target, std::string const & value)
{
  Append(target, value.c_str(), value.size());
}

template <typename Target>
void WriteArgument(Target & target, int const value)
{
  AppendFormat(target, "%d", value);
}

template <typename Target, unsigned Count>
void WriteArgument(Target & target, char const (&value)[Count])
{
  Append(target, value, Count - 1);
}

template <typename Target, unsigned Count>
void WriteArgument(Target & target, wchar_t const (&value)[Count])
{
  AppendFormat(target, "%.*ls", Count - 1, value);
}

namespace Internal {

constexpr unsigned CountPlaceholders(char const * const format)
{
  return (*format == '%') +
    (*format == '\0' ? 0 : CountPlaceholders(format + 1));
}

template <typename Target>
void Write(Target & target, char const * const value, size_t const size)
{
  Append(target, value, size);
}

template <typename Target, typename First, typename ... Rest>
void Write(Target & target, 
    char const * const value,
    size_t const size, 
    First const & first, 
    Rest const & ... rest)
{
  // Magic goes here
  size_t placeholder = 0;
  while (value[placeholder] != '%')
  {
    ++placeholder;
  }
  assert(value[placeholder] == '%');
  Append(target, value, placeholder);
  WriteArgument(target, first);
  Write(target, value + placeholder + 1, size - placeholder - 1, rest ...);
}
} /// internal

template <typename Target, unsigned Count, typename ... Args>
void Write(Target & target,
  char const (&format)[Count], Args const & ... args)
{
  assert(Internal::CountPlaceholders(format) == sizeof ... (args));
  Internal::Write(target, format, Count - 1, args ...);
}

int main(int argc, char const *argv[])
{
  std::string str = Format("%s\n", __FILE__);
  Print("%s", str);
  Print("%s", Format("%s\n",__FUNCTION__));
  Print(Format("%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__));

  Write(printf, "% % %\n", 10, 20, "helloworld");
  std::string text;
  Write(text, "Hello %\n", 2015);
  Print("%s", text);
  return 0;
}