#include "../../utils.h"

struct A
{
	A(){LOGPD("%p", this);}
	~A(){LOGPD("%p", this);}
	A(A const &a){LOGPD("%p:%p", this, &a);}
	A(A &&a){LOGPD("%p:%p", this, &a);}
	A &operator=(A a){LOGPD("%p:%p", this, &a); return *this;}
};

struct B
{
	B(){LOGPD("%p", this);}
	~B(){LOGPD("%p", this);}
	B(B const &b){LOGPD("%p %p", this, &b);}
	B(B &&b){LOGPD("%p %p", this, &b);}
	B &operator=(B b){LOGPD("%p %p", this, &b); return *this;}
};

struct C
{
	C(A a, B const &b): a_(std::move(a)), b_(b){LOGPD("%p:%p", &a_, &b_);}
	~C(){LOGPD("%p", this);}
	C(C const &){LOGPD("%p", this);}
	C(C &&){LOGPD("%p", this);}
	C &operator=(C){LOGPD("%p", this); return *this;}

	A a_;
	B b_;
};

int main(int argc, char const *argv[])
{
	{
		TRACE();
		C c(A{}, B{});
	}
	{
		TRACE();
		A a;B b;
		C c(a, b);
	}
	{
		TRACE();
		A a, a1;
		LOGD("");
		a = A{};
		LOGD("");
		a = a1;
		LOGD("");
	}

	return 0;
}