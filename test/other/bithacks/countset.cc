#include <iostream>
#include <bitset>
#include "../../utils.h"

#define CHAR_BIT 8

uint64_t countsetfrom(uint64_t v, uint64_t pos)
{
	// uint64_t v;       // Compute the rank (bits set) in v from the MSB to pos.
	// unsigned int pos; // Bit position to count bits upto.
	uint64_t r;       // Resulting rank of bit at pos goes here.

	// // Shift out bits after given position.
	// r = v >> (sizeof(v) * CHAR_BIT - pos);
	// // Count set bits in parallel.
	// // r = (r & 0x5555...) + ((r >> 1) & 0x5555...);
	// r = r - ((r >> 1) & ~0UL/3);
	// // r = (r & 0x3333...) + ((r >> 2) & 0x3333...);
	// r = (r & ~0UL/5) + ((r >> 2) & ~0UL/5);
	// // r = (r & 0x0f0f...) + ((r >> 4) & 0x0f0f...);
	// r = (r + (r >> 4)) & ~0UL/17;
	// // r = r % 255;
	// r = (r * (~0UL/255)) >> ((sizeof(v) - 1) * CHAR_BIT);

	std::bitset<0x1000> bits(v);

	LOGD("%ld: %s", bits.size(), bits.to_string().data());

	return (bits >> pos).count();

	// return r;
}


uint64_t countrank(uint64_t v, int16_t pos)
{
	uint64_t r;       // Resulting rank of bit at pos goes here.

	// Shift out bits after given position.
	r = v >> (sizeof(v) * CHAR_BIT - pos);
	// Count set bits in parallel.
	// r = (r & 0x5555...) + ((r >> 1) & 0x5555...);
	r = r - ((r >> 1) & ~0UL/3);
	// r = (r & 0x3333...) + ((r >> 2) & 0x3333...);
	r = (r & ~0UL/5) + ((r >> 2) & ~0UL/5);
	// r = (r & 0x0f0f...) + ((r >> 4) & 0x0f0f...);
	r = (r + (r >> 4)) & ~0UL/17;
	// r = r % 255;
	r = (r * (~0UL/255)) >> ((sizeof(v) - 1) * CHAR_BIT);

	return r;
}


int countConsSet(uint32_t v, int pos)
{
	// unsigned int v;  // find the number of trailing zeros in 32-bit v 
	// int r;           // result goes here

	v >>= pos;
	v = ~v;

	static const int MultiplyDeBruijnBitPosition[32] = 
	{
	  0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8, 
	  31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
	};
	return MultiplyDeBruijnBitPosition[((uint32_t)((v & -v) * 0x077CB531U)) >> 27];
}

int countConsSet2(uint64_t v, int pos)
{
	return ffsll(v >> (pos + 1));
}

int main(int argc, char const *argv[])
{
	/* code */
	uint32_t val = 0xf0ff;
	// LOGD("%ld", countsetfrom(val, 0));

	// LOGD("%d", countConsSet(val, 0));
	// LOGD("%d", countConsSet(val, 1));
	// LOGD("%d", countConsSet(val, 2));
	// LOGD("%d", countConsSet(val, 3));
	// LOGD("%d", countConsSet(val, 4));
	// LOGD("%d", countConsSet(val, 9));
	// LOGD("%d", countConsSet(val, 12));

	// LOGD("%ld", countrank(val, 8));
	// LOGD("%ld", countrank(val, 12));
	// LOGD("%ld", countrank(val, 16));
	// LOGD("%ld", countrank(val, 20));
	// LOGD("%ld", countrank(val, 24));
	// LOGD("%ld", countrank(val, 28));
	// LOGD("%ld", countrank(val, 32));
	// LOGD("%ld", countrank(val, 64));


	// LOGD("%ld", countset(0x00FF00FF00FF00FF, 48));
	// LOGD("%ld", countset(0x00FF00FF00FF00FF, 56));

	// countsetfrom(val, 0);
	size_t const idx = 23;
	size_t const ef = 0x8000000000c00000u;
	size_t const tt_cnt = 0;
	size_t const tpos = idx % (63);

	size_t const next_idx = idx + tt_cnt;
    size_t const next_tpos = next_idx % (63);
	size_t const kMask = 0x8000000000000000u;

	// LOGD("%d", ffsll(0x40000000u));
	// LOGD("%d", ffsll(0xffffffffc0000000u));
	// size_t const temp = 0x8000001ffffffff0 & ~(0x8000000000000000u);
	// size_t const next_tpos = 37;

	size_t temp = tt_cnt ? ef : ef & ~(1ul << tpos);
	// temp = next_tpos ? ((temp >> (next_tpos)) | (temp << (63 - (next_tpos))) | kMask) : temp;
	size_t rtemp = next_tpos ? (((temp & (~kMask)) >> (next_tpos)) | (temp << (63 - (next_tpos))) | kMask) : temp;
	size_t temp1 = (temp & (~kMask)) >> (23);
	size_t temp2 = temp << (63 - (next_tpos));
	LOGD("%016lx %016lx %016lx %016lx", temp, temp1, temp2, rtemp);
	LOGD("%d", ffsll(rtemp));


	// temp1 = temp >> (next_tpos);
	// temp2 = temp << (63 - next_tpos);
	// LOGD("%lx %lx %lx", temp1, temp2, temp1 | temp2 | kMask);
	// LOGD("%d", ffsll(temp1 | temp2 | kMask));

	// 1000000000000000000000000001111111111111111111111111111111110000
	// 1111111111111111111111111111111111000000000000000000000000000000

	// 1011111111111111111111111111111111100000000000000000000000000000
	// 1111111111111111111111111111111111000000000000000000000000000000
	// LOGD("%d", countConsSet2(0b011, 0));

	return 0;
}


