print("start")

local BinaryFormat = package.cpath:match("%p[\\|/]?%p(%a+)")
if BinaryFormat == "dll" then
    function os.name()
        return "Windows"
    end
elseif BinaryFormat == "so" then
    function os.name()
        return "Linux"
    end
elseif BinaryFormat == "dylib" then
    function os.name()
        return "MacOS"
    end
end
BinaryFormat = nil

print(os.name())

io.stdout:write("helloworld\n")

local ffi = require("ffi")

local ptl_shared

if os.name() == "Windows" then
  ptl_shared = ffi.load("./ffi_ptl_lib.dll")
else
  ptl_shared = ffi.load("./libffi_ptl_lib.so")
end

io.stdout:write("helloworld\n")

ffi.cdef[[
  /* From our library */
  typedef struct PPObj PPObj;
  PPObj *new_ppobj(const char* name, const int age);
  void delete_ppobj(PPObj *p);
  char* str_val(const PPObj* p);
  int int_val(const PPObj* p);

  /* From the C library */
  void free(void*);
]]

C = ffi.C

local PPObjWrapper = {}
PPObjWrapper.__index = PPObjWrapper

local function PPObj(...)
    local self = {super = ptl_shared.new_ppobj(...)}
    ffi.gc(self.super, ptl_shared.delete_ppobj)
    return setmetatable(self, PPObjWrapper)
end

function PPObjWrapper.str_val(self)
    local name = ptl_shared.str_val(self.super)
    ffi.gc(name, C.free)
    return ffi.string(name)
end

function PPObjWrapper.int_val(self)
  return ptl_shared.int_val(self.super)
end

local obj = PPObj("Mark Twain", 74)

io.stdout:write(string.format("'%d' is %s years old\n",
                       obj:int_val(),
                       obj:str_val()))

print(obj:int_val())
print(obj:str_val())

io.stdout:write("helloworld\n")
