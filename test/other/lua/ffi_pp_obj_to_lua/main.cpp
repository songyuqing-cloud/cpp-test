#include <lua.hpp>
#include <utils/Utils>
#include <iostream>

// #include "obj.h"

#include <sol.hpp>

int main(int argc, char const *argv[])
{
    file_utils::init();

    _LOGD("");
        lua_State* L = luaL_newstate();
        luaL_openlibs(L); // load default Lua libs
        std::string lscript = GET_PATH("ffi_pp_obj_to_lua.lua");
        _LOGD("%s", lscript.data());
        // lscript = "ffi_pp_obj_to_lua.lua";
        if (luaL_loadfile(L, lscript.data()))
        {
            _LOGE("Error loading script");
        }
    _LOGD("");
        // lua_pcall(L, 0, 0, 0); // run script
        if(lua_pcall(L, 0, 0, 0))
        {
            std::cerr << "-- " << lua_tostring(L, -1) << std::endl;
        }
    _LOGD("");

    sol::state l_sol;
    l_sol.open_libraries( sol::lib::base, sol::lib::coroutine, sol::lib::string, sol::lib::io );

    l_sol.script( "print('bark bark bark!')" );

    return 0;
}
