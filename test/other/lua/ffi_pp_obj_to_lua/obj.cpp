#include "obj.h"

#include <cstring>
#include <cassert>
// #include <cstdlib>

PPObj::PPObj(int _a_int, const std::string &_a_str)
    : _m_int(_a_int)
    , _m_str(_a_str)
{

}

void PPObj::info() const
{
    printf("%s %d\n", _m_str.data(), _m_int);
}

extern "C" ffi_ptl_lib_DLL void *new_ppobj(const char* _a_str, int _a_int)
{
    printf("[i] new_ppobj\n");
    assert(_a_str != NULL);
    PPObj *p = new PPObj(_a_int, _a_str);
    return reinterpret_cast<void*>(p);
}

extern "C" ffi_ptl_lib_DLL void delete_ppobj(PPObj *p)
{
    printf("[i] delete_ppobj\n");
    delete p;
}

extern "C" ffi_ptl_lib_DLL int int_val(const PPObj* p)
{
    printf("[i] int_val\n");
    assert(p != NULL);
    return p->_m_int;
}

extern "C" ffi_ptl_lib_DLL char *str_val(const PPObj* p)
{
    printf("[i] str_val: %s\n", (&(p->_m_str[0])));
    assert(p != NULL);
    return strdup(p->_m_str.data());
    // return &(p->_m_str[0]);
}
