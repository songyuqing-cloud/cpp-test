#ifndef _OBJ_H_
#define _OBJ_H_

#include <string>
// #include <vector>

// #include <lua.hpp>
#if defined _WINDOWS
#ifndef ffi_ptl_lib_DLL_H
#define ffi_ptl_lib_DLL_H

#ifdef SHARED_EXPORTS_BUILT_AS_STATIC
#  define ffi_ptl_lib_DLL
#  define FFI_PTL_LIB_NO_EXPORT
#else
#  ifndef ffi_ptl_lib_DLL
#    ifdef ffi_ptl_lib_EXPORTS
        /* We are building this library */
#      define ffi_ptl_lib_DLL __declspec(dllexport)
#    else
        /* We are using this library */
#      define ffi_ptl_lib_DLL __declspec(dllimport)
#    endif
#  endif

#  ifndef FFI_PTL_LIB_NO_EXPORT
#    define FFI_PTL_LIB_NO_EXPORT
#  endif
#endif

#ifndef FFI_PTL_LIB_DEPRECATED
#  define FFI_PTL_LIB_DEPRECATED __declspec(deprecated)
#endif

#ifndef FFI_PTL_LIB_DEPRECATED_EXPORT
#  define FFI_PTL_LIB_DEPRECATED_EXPORT ffi_ptl_lib_DLL FFI_PTL_LIB_DEPRECATED
#endif

#ifndef FFI_PTL_LIB_DEPRECATED_NO_EXPORT
#  define FFI_PTL_LIB_DEPRECATED_NO_EXPORT FFI_PTL_LIB_NO_EXPORT FFI_PTL_LIB_DEPRECATED
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define FFI_PTL_LIB_NO_DEPRECATED
#endif

#endif
#else // win
#define ffi_ptl_lib_DLL
#endif


class ffi_ptl_lib_DLL PPObj
{
public:
    PPObj() = default;
    PPObj(int, const std::string &);
    virtual ~PPObj() = default;

    void info() const;

    int _m_int;
    std::string _m_str;
};

#endif
