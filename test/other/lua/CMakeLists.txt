# At LEAST 2.8 but newer is better
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
PROJECT(lua_sample VERSION 0.1)

find_package(PkgConfig REQUIRED)

# CMakeFiles/${TARGET}.dir/flags.make
# CMakeFiles/${TARGET}.dir/link.txt

# Compiler options
if(MSVC)
  if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /NODEFAULTLIB:msvcrt /NODEFAULTLIB:libcmt")
  else()
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /NODEFAULTLIB:libcmt")
  endif()
  ADD_DEFINITIONS(-D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS
                  -wd4251 -wd4244 -wd4334 -wd4005 -wd4820 -wd4710
                  -wd4514 -wd4056 -wd4996 -wd4099 -DSOL_USING_CXX_LUA=1 -DSOL_USING_CXX_LUAJIT=1)
else()
  if(CMAKE_BUILD_TYPE MATCHES Debug)
  endif()
  set(CMAKE_C_FLAGS_DEBUG "-g -Wall")
  set(CMAKE_CXX_FLAGS_DEBUG ${CMAKE_C_FLAGS_DEBUG})
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wno-deprecated-declarations -Wno-reorder")
  if(CLANG)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
  endif()
endif(MSVC)

# print to console
MESSAGE( STATUS "Building with system: " ${CMAKE_SYSTEM_NAME})

# set environments
SET(PRJ_ROOT ${CMAKE_CURRENT_SOURCE_DIR})


# cxx flags
# SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-deprecated-declarations -Wno-reorder")
# ADD_SUBDIRECTORY(/mnt/d/workspace/sol2/single)

#TARGET-------------------------------------------------------------------------
# set target
# ADD_SUBDIRECTORY(create_class_using_luabind)
# ADD_SUBDIRECTORY(exposing_cpp_class_using_luabind)
# ADD_SUBDIRECTORY(exposing_cpp_function_to_lua)
# ADD_SUBDIRECTORY(serialization)
# ADD_SUBDIRECTORY(luajit)
# ADD_SUBDIRECTORY(ffi_pp_obj_to_lua)
# ADD_SUBDIRECTORY(sol2)
ADD_SUBDIRECTORY(sol2_class)


