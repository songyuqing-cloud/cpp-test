require ('os')
require ('math')
local util = require ('util')
local Vec2 = require('Vec2')

local MapCircle = class()

function MapCircle:ctor(a_num_cities, a_width, a_height, a_rad, a_ratio)
	self.m_rad = a_rad or (math.min(a_width, a_height) / 2)
	self.m_size = Vec2.new(a_width, a_height)
	self.m_cities = get_circle(a_num_cities, self.m_size / 2, self.m_rad)
	self.m_ratio = a_ratio or 10
	self.m_map = {}
	self.m_map_size = (self.m_size / self.m_ratio + 1):floor_complex()
	self.m_len = self:len()

	for r=0, self.m_map_size.y - 1 do
		for c=1, self.m_map_size.x do
			self.m_map[r * (self.m_map_size.x) + c] = '.'
		end
	end

	for i=1, #self.m_cities do
		local idx = self:to_map_idx(self.m_cities[i])
		self.m_map[idx] = i
	end

	self.m_map[self:to_map_idx(Vec2.new(0,0))] = 'o'
end

function MapCircle:len()
	return (self.m_cities[2] - self.m_cities[1]):len() * #self.m_cities
end

function MapCircle:__tostring()
	return util.to_string_ordered(self.m_cities, ",", "", "")
end

function MapCircle:to_map_idx(a_city)
	local ct = (a_city + (self.m_size / 2)):floor_complex()
	local ret = (ct.x + 1) + (self.m_map_size.y - 1 - ct.y) * self.m_map_size.x

	return (ret)
end

function MapCircle:display(a_ratio)

	local num_sep = self.m_map_size.x
	local ret = ""
	local sep = ""
	for i=1, #self.m_map do
		ret = ret..sep..tostring(self.m_map[i])
		if math.fmod(i, num_sep) ~= 0 then
			if type(self.m_map[i]) == "number" and self.m_map[i] > 9 then
				sep = ""
			else sep = " " end
		else sep = "\n" end
	end
	print( ret )
end

return MapCircle