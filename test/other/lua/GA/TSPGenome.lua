local util = require ('util')
local TSPGenome = class()

function TSPGenome:ctor(a_num)
	self.m_cities = {}
	self.m_fitness = 0
	self:grab_permutation(a_num)
end

function TSPGenome:grab_permutation( a_num )
	for i=1, a_num do
		self.m_cities[i] = i
	end

	util.shuffle(self.m_cities, 10)
end

function TSPGenome:find_city(a_city)
	for i=1, #self.m_cities do
		if a_city == self.m_cities[i] then return i end
	end

	return 0
end

function TSPGenome:clone()
	local ret = TSPGenome.new(#self.m_cities)

	ret.m_fitness = self.m_fitness

	for i=1, #self.m_cities do
		ret.m_cities[i] = self.m_cities[i]
	end

	return ret
end

-- function TSPGenome:update_fitness()
-- 	for i=1, #self.m_cities -1 do
-- 		m_fitness = m_fitness + Vec2.new():len()
-- 	end
-- end

-- function TSPGenome:mutate(a_mutate_rate)
-- 	for i=1, #self.m_bits do
-- 		if math.random() > a_mutate_rate then
-- 			self.m_bits[i] = self.m_bits[i] ^ 1
-- 		end
-- 	end
-- end

-- function TSPGenome:crossover(a_gen, a_crossover_rate)
-- 	local ret1, ret2 = TSPGenome.new(#self.m_bits), TSPGenome.new(#self.m_bits)

-- 	if (math.random() > a_crossover_rate) or (self == a_gen) then
-- 		ret1 = self;
-- 		ret2 = a_gen;
-- 	else
-- 		local cp = math.random(1, #self.m_bits)

-- 		for i=1, cp do
-- 			ret1.m_bits[i] = self.m_bits[i]
-- 			ret2.m_bits[i] = a_gen.m_bits[i]
-- 		end

-- 		for i=cp+1, #self.m_bits do
-- 			ret1.m_bits[i] = a_gen.m_bits[i]
-- 			ret2.m_bits[i] = self.m_bits[i]
-- 		end
-- 	end

-- 	return ret1, ret2
-- end

function TSPGenome:__tostring()
	return util.to_string_ordered(self.m_cities, "-", "", "")
end

-- function TSPGenome:__lt(a_gen)
-- 	return self.m_fitness < a_gen.m_fitness
-- end

-- function TSPGenome:__le(a_gen)
-- 	return self.m_fitness <= a_gen.m_fitness
-- end

return TSPGenome