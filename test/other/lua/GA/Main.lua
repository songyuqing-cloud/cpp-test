require ('os')
require ('math')

local util = require ('util')
-- local Genome = require ('Genome')
local Vec2 = require('Vec2')
local MapCircle = require ('MapCircle')
local GABob = require ('GABob')
local TSPGA = require ('TSPGA')

math.randomseed(os.time())

local CROSS_OVER_RATE = 0.75
local MUTATION_RATE = 0.2
local POP_SIZE = 128
local CHROMO_LEN = 64



-- local tbl = {}
-- for i=1, CHROMO_LEN do tbl[i] = i end
-- print(util.to_string_ordered(tbl))

-- local beg = 24 -- 5
-- local size = 22 -- 2
-- local direction = 39 -- -3

-- util.shift(tbl, beg, size, direction)
-- print(util.to_string_ordered(tbl))



-- exit(0)

-- local bob = GABob.new(CROSS_OVER_RATE, MUTATED_RATE, POP_SIZE, CHROMO_LEN)
-- bob:epoch()

-- local map = {}
-- local size, rat = 32, 1

-- map = MapCircle.new(32, size, size, size/2, rat)
-- print(map:len())
-- map:display(1)

local map_size = 128
local tsp = TSPGA.new(POP_SIZE, CHROMO_LEN, map_size, CROSS_OVER_RATE, MUTATION_RATE)
while tsp:epoch() == 1 and tsp.m_gen_cnt < 4000 do
	-- print(tsp.m_gen_cnt, tsp.m_best, tsp.m_map:len())
end
print(tsp.m_gen_cnt, tsp.m_best, tsp.m_map:len())
print(tsp.m_fitess)


