require ('os')
require ('math')

local util = require ('util')
local TSPGenome = require ('TSPGenome')
local Vec2 = require('Vec2')
local MapCircle = require ('MapCircle')

local TSPGA = class()

function TSPGA:ctor(a_pop, a_num_cities, a_size, a_crossover_rate, a_mutation_rate)
	local rat = 1
	self.m_num_cities = a_num_cities
	self.m_map = MapCircle.new(a_num_cities, a_size, a_size, a_size/2, rat)
	self.m_gens = {}
	self.m_fitess = nil
	self.m_mutation_rate = a_mutation_rate
	self.m_crossover_rate = a_crossover_rate

	self.m_worst = 0
	self.m_best = 999999999
	self.m_gen_cnt = 0
	self.m_total_fitness_score = 0

	for i=1, a_pop do
		self.m_gens[i] = TSPGenome.new(self.m_num_cities)
		self:update_fitness(i)
	end

	self:convert_fitness()
end

function TSPGA:__tostring()
	return util.to_string_ordered(self.m_gens, "\n", "{", "}")
end

function TSPGA:convert_fitness()
	self.m_total_fitness_score = 0
	for i=1, #self.m_gens do
		self.m_gens[i].m_fitness = self.m_worst - self.m_gens[i].m_fitness
		self.m_total_fitness_score = self.m_total_fitness_score + self.m_gens[i].m_fitness
	end
end

function TSPGA:update_fitness(idx, a_diff)
	local genome = self.m_gens[idx]
	a_diff = a_diff or 0
	genome.m_fitness = 0

	for j=1, self.m_num_cities-1 do
		genome.m_fitness = genome.m_fitness +
		(self.m_map.m_cities[genome.m_cities[j]] -
		 self.m_map.m_cities[genome.m_cities[j+1]]):len()
	end

	genome.m_fitness = genome.m_fitness + (self.m_map.m_cities[genome.m_cities[1]] - self.m_map.m_cities[genome.m_cities[self.m_num_cities]]):len()

	-- print(self.m_worst, genome.m_fitness, self.m_gens[idx].m_fitness)
	if self.m_worst <= genome.m_fitness then
		self.m_worst = genome.m_fitness
	end

	if self.m_best > genome.m_fitness then
		self.m_fitess = genome
		self.m_best = genome.m_fitness
	end
end

function TSPGA:roulette_wheel_selection()
	-- local gen = {}
	-- TODO
	local slice = math.random() * self.m_total_fitness_score
	local total = 0
	local selected_gen = 1

	for i=1, #self.m_gens do
		total = total + self.m_gens[i].m_fitness

		if total > slice then
			selected_gen = i
			break;
		end
	end

	return self.m_gens[selected_gen]
end

function TSPGA:epoch()
	local gens = {}
	for i=1, #self.m_gens/8 do
		table.insert(gens, self.m_fitess)
	end

	while #gens < #self.m_gens do
		-- roulette whell selection
		local p1 = self:roulette_wheel_selection()
		local p2 = self:roulette_wheel_selection()

		-- -- crossover
		-- if p1 ~= p2 then
		-- 	p1,p2 = self:crossover_mpx(p1, p2)
		-- end

		if (p1 ~= p2) and (math.random() <= self.m_crossover_rate) then
			p1, p2 = p1:clone(), p2:clone()
			self:crossover_pmx(p1, p2)
		else
			p1, p2 = p1:clone(), p2:clone()
		end

		-- mutate
		if math.random() <= self.m_mutation_rate then
			self:mutate_im(p1, p2)
		end

		table.insert(gens, p1)
		table.insert(gens, p2)
	end

	self.m_gens = gens
	self.m_gen_cnt = self.m_gen_cnt + 1

	-- update fitness
	self.m_worst = 0

	for i=1, #self.m_gens do
		self:update_fitness(i)
	end
	self:convert_fitness()

	-- print (self.m_best, self.m_fitess)
	if self.m_best <= (self.m_map.m_len + util.EPSILON) then return 0 end

	return 1
end

function TSPGA:crossover_pmx(a_gen1, a_gen2)
	local r1 = math.random(1, self.m_num_cities)
	local r2 = math.random(1, self.m_num_cities)
	while r1 == r2 do r2 = math.random(1, self.m_num_cities) end
	local from, to = 0,0
	if r1 < r2 then from, to = r1, r2
	else from, to = r2, r1 end

	for i=from, to do
		local ct1, ct2 = a_gen1.m_cities[i], a_gen2.m_cities[i]
		local i1, i2 = a_gen1:find_city(ct1), a_gen1:find_city(ct2)
		a_gen1.m_cities[i1], a_gen1.m_cities[i2] = a_gen1.m_cities[i2], a_gen1.m_cities[i1]

		local i1, i2 = a_gen2:find_city(ct1), a_gen2:find_city(ct2)
		a_gen2.m_cities[i1], a_gen2.m_cities[i2] = a_gen2.m_cities[i2], a_gen2.m_cities[i1]
	end
end

function TSPGA:crossover_mpx(a_gen1, a_gen2)
	local ret1, ret2 = TSPGenome.new(self.m_num_cities), TSPGenome.new(self.m_num_cities)

	for i=1, self.m_num_cities do
		if (math.random() <= self.m_crossover_rate) then
			ret1.m_cities[i] = a_gen1.m_cities[i]
			ret2.m_cities[i] = a_gen2.m_cities[i]
		else
			ret1.m_cities[i] = a_gen2.m_cities[i]
			ret2.m_cities[i] = a_gen1.m_cities[i]
		end
	end

	return ret1, ret2
end
-- TODO: obx, pbx



function TSPGA:mutate_em(a_gen1, a_gen2)
	local r1, r2 = math.random(1, self.m_num_cities), math.random(1, self.m_num_cities)
	while r1 == r2 do r2 = math.random(1, self.m_num_cities) end

	a_gen1[r1], a_gen1[r2] = a_gen1[r2], a_gen1[r1]
	a_gen2[r1], a_gen2[r2] = a_gen2[r2], a_gen2[r1]
end

function TSPGA:mutate_sm(a_gen1, a_gen2)
	local min_span_size = 3
	local span_size = math.random(min_span_size, self.m_num_cities - min_span_size - 1)
	local pos = math.random(1, self.m_num_cities - span_size)

	util.shuffle(a_gen1.m_cities, span_size, pos)
	util.shuffle(a_gen2.m_cities, span_size, pos)
end

function TSPGA:mutate_dm(a_gen1, a_gen2)
	local min_span_size = 3
	local span_size = math.random(min_span_size, self.m_num_cities - min_span_size - 1)
	local pos = math.random(1, self.m_num_cities - span_size)
	local direction = math.random(1, self.m_num_cities) - pos

	util.shift(a_gen1.m_cities, pos, span_size, direction)
	util.shift(a_gen2.m_cities, pos, span_size, direction)
end

function TSPGA:mutate_im(a_gen1, a_gen2)
	local span_size = 1
	local pos = math.random(1, self.m_num_cities - span_size)
	local direction = math.random(1, self.m_num_cities) - pos

	util.shift(a_gen1.m_cities, pos, span_size, direction)
	util.shift(a_gen2.m_cities, pos, span_size, direction)
end

return TSPGA