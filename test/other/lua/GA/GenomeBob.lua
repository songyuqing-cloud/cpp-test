local util = require ('util')
local GenomeBob = class()

function GenomeBob:ctor(a_numbit)
	self.m_bits = {}
	self.m_fitness = 0

	for i=1, a_numbit do
		self.m_bits[#self.m_bits+1] = math.random(0,1)
	end
end

function GenomeBob:mutate(a_mutate_rate)
	for i=1, #self.m_bits do
		if math.random() > a_mutate_rate then
			self.m_bits[i] = self.m_bits[i] ^ 1
		end
	end
end

function GenomeBob:crossover(a_gen, a_crossover_rate)
	local ret1, ret2 = GenomeBob.new(#self.m_bits), GenomeBob.new(#self.m_bits)

	if (math.random() > a_crossover_rate) or (self == a_gen) then
		ret1 = self;
		ret2 = a_gen;
	else
		local cp = math.random(1, #self.m_bits)

		for i=1, cp do
			ret1.m_bits[i] = self.m_bits[i]
			ret2.m_bits[i] = a_gen.m_bits[i]
		end

		for i=cp+1, #self.m_bits do
			ret1.m_bits[i] = a_gen.m_bits[i]
			ret2.m_bits[i] = self.m_bits[i]
		end
	end

	return ret1, ret2
end

function GenomeBob:__tostring()
	return util.to_string_ordered(self.m_bits, "", "", "")
end

function GenomeBob:__lt(a_gen)
	return self.m_fitness < a_gen.m_fitness
end

-- function GenomeBob:__le(a_gen)
-- 	return self.m_fitness <= a_gen.m_fitness
-- end

return GenomeBob