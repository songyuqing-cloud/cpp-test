
local util = {}
util.__index = util

local __h2b = {
['0']='0000', ['1']='0001',
['2']='0010', ['3']='0011',
['4']='0100', ['5']='0101',
['6']='0110', ['7']='0111',
['8']='1000', ['9']='1001',
['A']='1010', ['B']='1011', ['a']='1010', ['b']='1011',
['C']='1100', ['D']='1101', ['c']='1100', ['d']='1101',
['E']='1110', ['F']='1111', ['e']='1110', ['f']='1111'
}

function hex2bin(n)
  return n:gsub(".", __h2b)
end

function dec2bin(n)
  return hex2bin(('%X'):format(n))
end

function class()
	local _class = {}
	_class.__index = _class

	_class.new = function(...)
		local ins = {}
		setmetatable(ins, _class)

		if ins.ctor ~= nil then
			ins:ctor(...)
		end

		return ins
	end

	return _class
end

util.iq_sort = function (a_tbl, a_low, a_high)
	a_low = a_low or 1
	a_high = a_high or #a_tbl

	local stack = {}
	table.insert(stack, a_low)
	table.insert(stack, a_high)

	while #stack > 0 do
		local l, h = stack[#stack-1], stack[#stack]
		table.remove(stack, #stack)
		table.remove(stack, #stack)

		if l < h then
			local pi, pv = h, a_tbl[h]
			local j=l
			-- partition
			for i=l, a_high-1 do
				if a_tbl[i] < pv then
					-- a_tbl[i], a_tbl[j] = a_tbl[j], a_tbl[i] -- swap
					util.swap(a_tbl, i, j)
					j = j + 1
				end
			end

			-- a_tbl[j], a_tbl[pi] = a_tbl[pi], a_tbl[j]
			util.swap(a_tbl, pi, j)

			table.insert(stack, l)
			table.insert(stack, j-1)

			table.insert(stack, j+1)
			table.insert(stack, h)
		end
	end
end

util.to_string_ordered = function ( a_tbl, a_sep, a_beg, a_end, a_num_sep)
	a_beg = a_beg or ""
	a_end = a_end or ""
	a_sep = a_sep or " "
	a_num_sep = a_num_sep or #a_tbl
	local ret = a_beg
	local sep = ""
	for i=1, #a_tbl do
		ret = ret..sep..tostring(a_tbl[i])
		if math.fmod(i, a_num_sep) ~= 0 then sep = a_sep
		else sep = "\n" end
	end
	return ret..a_end
end

util.to_string_unordered = function ( a_tbl, a_sep, a_beg, a_end )
	a_beg = a_beg or ""
	a_end = a_end or ""
	a_sep = a_sep or " "
	local ret = a_beg
	local sep = ""
	for k in pairs(a_tbl) do
		ret = ret..sep..tostring(a_tbl[k])
		sep = a_sep
	end
	return ret..a_end
end

util.swap = function (a_tbl, a_pos1, a_pos2)
	a_tbl[a_pos1], a_tbl[a_pos2] = a_tbl[a_pos2], a_tbl[a_pos1]
end

util.shuffle = function (a_tbl, a_num, a_beg, a_size)
	a_beg = a_beg or 1
	if a_beg == #a_tbl then return end
	a_size = a_size or #a_tbl - a_beg
	local _end = a_beg + a_size

	for i=1, a_num do
		local id1, id2 = math.random(a_beg, _end), math.random(a_beg, _end)
		if id1 ~= id2 then util.swap(a_tbl, id1, id2) end
	end
end

util.shift = function (a_tbl, a_beg, a_size, a_direction)

	-- convert shift's direction to right shifting
	if a_direction < 0 then
		a_beg =  a_beg + a_direction
		a_direction, a_size = a_size, a_direction * -1
	end

	local _end = a_beg + a_size - 1
	local new_end = _end + a_direction

	if new_end > #a_tbl then new_end = #a_tbl end
	-- print(a_beg, a_size, a_direction, _end, new_end)

	local cnt=0
	while cnt < a_size do
		for i=_end-cnt, new_end-cnt-1 do
			util.swap(a_tbl, i, i+1)
		end
		cnt = cnt + 1
	end
end

util.FLOAT_ZERO = 1.0e-15
util.EPSILON = 1.0e-5

util.DEGREE_5 = math.pi / 36
util.DEGREE_10 = util.DEGREE_5 * 2
util.DEGREE_15 = util.DEGREE_5 * 3
util.DEGREE_30 = util.DEGREE_15 * 2
util.DEGREE_45 = util.DEGREE_15 * 3
util.DEGREE_90 = util.DEGREE_45 * 2
util.DEGREE_135 = util.DEGREE_45 * 3

return util