require ('os')
require ('math')
local util = require ('util')

local Vec2 = class()

function Vec2:ctor(a_x, a_y)
	self.x, self.y = a_x, a_y
end

function Vec2:len()
	return math.sqrt(self.x * self.x + self.y * self.y)
end

function Vec2:len2()
	return self.x * self.x + self.y * self.y
end

function Vec2:__tostring()
	return util.to_string_ordered({self.x, self.y}, ",", "(", ")")
end

function Vec2:__add(a_param)
	if type(a_param) == "number" then
		return Vec2.new(self.x + a_param, self.y + a_param)
	else
		return Vec2.new(self.x + a_param.x, self.y + a_param.y)
	end
end

function Vec2:__sub(a_param)
	if type(a_param) == "number" then
		return Vec2.new(self.x - a_param, self.y - a_param)
	else
		return Vec2.new(self.x - a_param.x, self.y - a_param.y)
	end
end

function Vec2:__mul(a_param)
	if type(a_param) == "number" then
		return Vec2.new(self.x * a_param, self.y * a_param)
	else
		return Vec2.new(self.x * a_param.x, self.y * a_param.y)
	end
end

function Vec2:__div(a_param)
	if type(a_param) == "number" then
		return Vec2.new(self.x / a_param, self.y / a_param)
	else
		return Vec2.new(self.x / a_param.x, self.y / a_param.y)
	end
end

function Vec2:__eq(a_vec)
	return self.x == a_vec.x and self.y == a_vec.y
end

function Vec2:floor()
	return Vec2.new(math.floor(self.x), math.floor(self.y))
end

function Vec2:floor_complex()
	local x,x2 = math.modf(self.x)
	local y,y2 = math.modf(self.y)
	if x2 >= 0.5 then x = x + 1 end
	if y2 >= 0.5 then y = y + 1 end

	return Vec2.new(x, y)
end

function Vec2:dot(a_vec)
	return self.x * a_vec.x + self.y * a_vec.y
end

function Vec2:cross(a_vec)
	return Vec2.new(self.x * a_vec.y, self.y * a_vec.x)
end

function Vec2:normalize()
	local n = self:len()
	if n == 1 then return Vec2.new(self.x, self.y) end
	n = 1/n
	return Vec2.new(self.x * n, self.y * n)
end

function Vec2:min()
	return math.min(self.x, self.y)
end

function Vec2:rotate(a_radian)
	return Vec2.new(self.x * math.cos(a_radian) - self.y * math.sin(a_radian),
	                self.x * math.sin(a_radian) + self.y * math.cos(a_radian))
end

function get_circle(a_num_points, a_centre, a_radius, a_start_radian)
	a_start_radian = a_start_radian or math.pi/2
	local ret = {}
	local frag = 2 * math.pi / a_num_points
	for i=1, a_num_points do
		local x = math.cos(a_start_radian)
		local y = math.sin(a_start_radian)
		if math.abs(x) < util.FLOAT_ZERO then x = 0 end
		if math.abs(y) < util.FLOAT_ZERO then y = 0 end

		table.insert(ret, Vec2.new(x * a_radius, y * a_radius))
		a_start_radian = a_start_radian - frag
	end
	return ret
end

return Vec2
