require ('os')
require ('math')
local util = require ('util')
local GenomeBob = require ('GenomeBob')
local Vec2 = require('Vec2')

local GABob = class()

function GABob:__tostring()
	return util.to_string_ordered(self.m_gens, ",", "{", "}")
end

function GABob:ctor(a_crossover_rate, a_mutate_rate, a_pop_size, a_chromo_len, a_gene_len)
	self.m_gens = {}
	self.m_crossover_rate = a_crossover_rate
	self.m_mutate_rate = a_mutate_rate
	self.m_pop_size = a_pop_size
	self.m_chromo_len = a_chromo_len
	self.m_gens_len = a_gene_len
	self.m_total_fitness_score = 0.0
	self.m_generation = 0
	self.m_busy = false

	self:create_start_population()
end

function GABob:create_start_population()
	for i=1, self.m_pop_size do
		table.insert(self.m_gens, GenomeBob.new(self.m_chromo_len))
	end
end

function GABob:roulette_wheel_selection()
	local gen = {}
	local slice = math.random() * self.m_total_fitness_score
	local total = 0
	local selected_gen = 1

	for i=1, #self.m_gens do
		total = total + self.m_gens[i].m_fitness

		if total > slice then
			selected_gen = i
			break;
		end
	end

	return self.m_gens[selected_gen]
end

function GABob:update_fitness_scores()
	self.m_total_fitness_score = 0.0
	for k in pairs(self.m_gens) do
		self.m_gens[k].m_fitness = math.random()
		self.m_total_fitness_score = self.m_total_fitness_score + self.m_gens[k].m_fitness
	end
end

function GABob:epoch()
	self:update_fitness_scores()

	-- local new_babies = 0
	local new_gens = {}

	while #new_gens < #self.m_gens - 1 do
		local mum = self:roulette_wheel_selection()
		local dad = self:roulette_wheel_selection()

		local baby1, baby2 = {}, {}

		baby1, baby2 = mum:crossover(dad, self.m_crossover_rate);

		baby1:mutate(self.m_mutate_rate)
		baby2:mutate(self.m_mutate_rate)

		table.insert(new_gens, baby1)
		table.insert(new_gens, baby2)
		-- new_babies = new_babies + 2
	end

	self.m_gens = new_gens
end

return GABob