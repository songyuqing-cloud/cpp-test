#define LOG_EN 1
// #include <lua.hpp>
#include <utils/Utils>
// #include <iostream>

// #include "obj.h"
#define LUA_VERSION_NUM 501
#include <sol.hpp>

#define TRY(prog) \
try { prog; } \
catch (std::exception const &e) { _LOGE("%s", e.what()); }

struct Player
{
    int m_val;
    void talk() { printf("Player is talking: %d\n", m_val); }
};

int main(int argc, char const *argv[])
{
    file_utils::init();

    sol::state l_sol;
    l_sol.open_libraries( sol::lib::base, sol::lib::ffi, sol::lib::jit, sol::lib::io );

    l_sol.new_usertype<Player>( "Player"
        , "talk", &Player::talk
        , "m_val", &Player::m_val
        );

    TRY(l_sol.script_file("class.lua"_fp));

    Player l_p = {10};

    _LOGD("enter: 0x%d", l_sol["state_chase_ball"]["enter"].valid());
    _LOGD("do_smt: 0x%d", l_sol["state_chase_ball"]["do_smt"].valid());

    l_sol["state_chase_ball"]["enter"](&l_p);
    l_sol["state_chase_ball"]["execute"](&l_p);



    // sol::object l_sol_obj = l_sol["p1"];
    // _LOGD("%d", static_cast<bool>(l_sol_obj));

    return 0;
}
