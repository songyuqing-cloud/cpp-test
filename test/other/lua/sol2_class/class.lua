State = {}
State.__index = State

function State:new(o)
    o = o or {}
    setmetatable(o, self)
    return o
end

function State:enter()
    print("[L] State:enter()")
end

function State:execute()
    print("[L] State:execute()")
end

function State:exit()
    print("[L] State:exit()")
end


state_chase_ball = State:new()

state_chase_ball["enter"] = function(a_field_player)
-- function state_chase_ball:enter(a_field_player)
    print ("[Lua]: enter chase_ball");
    a_field_player:talk()
end

state_chase_ball["execute"] = function (a_field_player)
-- function state_chase_ball:execute(a_field_player)
    print ("[Lua]: execute chase_ball");
    print (a_field_player);
    print (a_field_player.m_val);
    -- a_field_player:talk()
end

-- state_chase_ball["execute"] = function(a_field_player)
--     -- if the ball is within kicking range the player changes state to KickBall.
--     if a_field_player:BallWithinKickingRange() then
--         a_field_player:GetFSM():ChangeState(KickBall:Instance());

--     elseif a_field_player:isClosestTeamMemberToBall() then
--         a_field_player:Steering():SetTarget(a_field_player:Ball():Pos());

--     else
--         -- if the player is not closest to the ball anymore, he should return back
--         -- to his home region and wait for another opportunity
--         a_field_player:GetFSM():ChangeState(ReturnToHomeRegion:Instance());
--     end
-- end

-- state_chase_ball["exit"] = function(a_field_player)
--     a_field_player:Steering():SeekOff();
-- end
