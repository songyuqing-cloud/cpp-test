cmake_minimum_required(VERSION 3.10)

# app name
# SET(APP_NAME ccul)
# SET(TARGET_LIB ffi_ptl_lib)
SET(TARGET_EXE sol2_class)
find_package(PkgConfig REQUIRED)

# source
SET(TARGET_EXE_SRC
    main.cpp
)

# SET(TARGET_LIB_SRC
#     obj.cpp
# )
# SET(TARGET_LIB_HEADER
#     obj.h
# )

# IF(NOT DEFINED {CMAKE_CURRENT_SOURCE_DIR})
#     MESSAGE(FATAL_ERROR "Please set evironment LUAJIT_ROOT")
#     # some more commands
# ELSE()
#     MESSAGE(STATUS "Using LUAJIT_ROOT: ${CMAKE_CURRENT_SOURCE_DIR}")
# ENDIF()

IF(NOT DEFINED CMAKE_BUILD_TYPE)
    MESSAGE(FATAL_ERROR "Please set CMAKE_BUILD_TYPE")
    # some more commands
ELSE()
    MESSAGE(STATUS "Build type: ${CMAKE_BUILD_TYPE}")
ENDIF()



#NON_TARGET---------------------------------------------------------------------
# -D
ADD_DEFINITIONS(-DLOG_EN -DSOL_USING_CXX_LUAJIT=1)

# -I
INCLUDE_DIRECTORIES(
    ../../../..
#     PkgConfig::luajit
)

# -L
LINK_DIRECTORIES(
    ${CMAKE_CURRENT_SOURCE_DIR}/src
)

#TARGET-------------------------------------------------------------------------
# set target
ADD_EXECUTABLE(${TARGET_EXE} ${TARGET_EXE_SRC})
# ADD_LIBRARY(${TARGET_LIB} SHARED ${TARGET_LIB_SRC} )

# -l
if(MSVC)
# include (GenerateExportHeader)

# GENERATE_EXPORT_HEADER(${TARGET_LIB}
#     BASE_NAME ${TARGET_LIB}
#     EXPORT_MACRO_NAME ${TARGET_LIB}_DLL
#     EXPORT_FILE_NAME ${TARGET_LIB}_exports.h
#     STATIC_DEFINE SHARED_EXPORTS_BUILT_AS_STATIC)

TARGET_LINK_LIBRARIES(${TARGET_EXE}
    lua51
    luajit
    # ${TARGET_LIB}
)

else()
TARGET_LINK_LIBRARIES(${TARGET_EXE}
    luajit-5.1
)
SET_TARGET_PROPERTIES(${TARGET_EXE} PROPERTIES LINK_FLAGS "-Wl,-rpath,./")

# flags
TARGET_COMPILE_OPTIONS(${TARGET_EXE} PUBLIC
    -std=c++14
    -I/usr/include/luajit-2.1
    # -fno-exceptions
)

# TARGET_COMPILE_OPTIONS(${TARGET_LIB} PUBLIC
#     -std=c++11
# )
endif(MSVC)



# MESSAGE( ${CMAKE_CURRENT_SOURCE_DIR} " " ${CMAKE_CURRENT_BINARY_DIR})
if (MSVC)
# ADD_CUSTOM_COMMAND(TARGET ${TARGET_EXE} PRE_BUILD
 ADD_CUSTOM_TARGET(copy_res_${TARGET_EXE} ALL
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/class.lua ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/lua51.dll ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
    DEPENDS ${TARGET_EXE}
)
else()
# ADD_CUSTOM_COMMAND(TARGET ${TARGET_EXE} PRE_BUILD
#     COMMENT " Running PRE_BUILD action "
# )

# ADD_CUSTOM_COMMAND(TARGET ${TARGET_EXE} POST_BUILD
#     COMMENT " Running POST_BUILD action "
# )

# ADD_CUSTOM_TARGET(copy_res_${TARGET_EXE} ALL
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/*.lua ${CMAKE_CURRENT_BINARY_DIR}/
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/src/*.so ${CMAKE_CURRENT_BINARY_DIR}/
#     COMMAND ${CMAKE_COMMAND} -E create_symlink libluajit.so ${CMAKE_CURRENT_BINARY_DIR}/libluajit-5.1.so.2
#     COMMENT " Resources copied"
#     DEPENDS ${TARGET_EXE}
# )

# dependencies
# ADD_DEPENDENCIES(${TARGET_EXE} copy_res_${TARGET_EXE})
endif(MSVC)

# if(MSVC)
# else()
# ADD_CUSTOM_COMMAND(TARGET ${TARGET} POST_BUILD
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_BINARY_DIR}/${TARGET} ../
# )
# endif(MSVC)

# if(MSVC)
# ADD_CUSTOM_TARGET(copy_res_${TARGET}
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/luajit.lua ../
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/sum.lua ../
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/test.lua ../
# )
# else()
# ADD_CUSTOM_TARGET(copy_res_${TARGET}
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/luajit.lua ../
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/sum.lua ../
#     COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/test.lua ../
# )
# endif(MSVC)

# dependencies
# ADD_DEPENDENCIES(${TARGET} copy_res_${TARGET})
