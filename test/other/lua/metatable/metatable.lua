
local setmetatableindex_
setmetatableindex_ = function(t, index)
    if type(t) == "userdata" then
        local peer = tolua.getpeer(t)
        if not peer then
            peer = {}
            tolua.setpeer(t, peer)
        end
        setmetatableindex_(peer, index)
    else
        local mt = getmetatable(t)
        if not mt then mt = {} end
        if not mt.__index then
            mt.__index = index
            setmetatable(t, mt)
        elseif mt.__index ~= index then
            setmetatableindex_(mt, index)
        end
    end
end
setmetatableindex = setmetatableindex_

Set = {}
-- Set.mt = {}    -- metatable for sets

function class(...)
	-- o = o or {}   -- create object if user does not provide one
	-- setmetatable(o, self)
	-- self.__index = self
	-- return o

	local cls = {}
	cls.__index = cls

	setmetatable(cls, {__index = cls.super})

	cls.new = function(...)
        local instance = {}
        setmetatable(instance, {__index = cls})
        -- instance.class = cls
        -- instance:ctor(...)
        return instance
    end

	return cls
end

local Set = class()

function Set:do_smt( ... )
	print ("do smt")
	for _, msg in  ipairs{...} do
		print (msg)
	end
	print ("end")
end

s = Set:new({100})

Set = nil

-- print(s.w)
-- print(s.w2)
s:do_smt()