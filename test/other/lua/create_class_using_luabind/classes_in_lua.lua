 -- Meta class
Animal = {noise = ""}
Animal.__index = Animal
-- Base class method new

function Animal:new(o, noise)
    o = o or {}
    setmetatable(o, self)

    self.noise = noise
    return o
end

function Animal:speak()
    print(self.noise);
end

Dog = Animal:new()
Dog.__index = Dog

-- Derived class method new

function Dog:new(o)
    o = o or Animal:new(o, "gruh")
    setmetatable(o, self)

    return o
end

-- Creating an object
my_dog = Dog:new(nil)
my_dog:speak()

os.exit()