//include the libraries
#pragma comment(lib, "lua5.1.lib")
#pragma comment(lib, "luabind.lib")
//#pragma comment(lib, "lua.lib")
//#pragma comment(lib, "lualib.lib")
#pragma warning (disable : 4786)

#include <utils/Utils>

extern "C"
{
  #include <lua.h>
  #include <lualib.h>
  #include <lauxlib.h>
}

#include <string>
#include <iostream>
using namespace std;

//include the luabind headers. Make sure you have the paths set correctly
//to the lua, luabind and Boost files.
#include <luabind/luabind.hpp>
using namespace luabind;

#include "LuaHelperFunctions.h"



int main()
{
  //create a lua state
  lua_State* pLua = luaL_newstate();

  //open the lua libaries - new in lua5.1
  luaL_openlibs(pLua);

  //open luabind
  open(pLua);

  _LOGD("");

  //load and run the script
  try
  {
    RunLuaScript(pLua, "classes_in_lua.lua");
  }
  catch(const exception &e)
  {
    _LOGE("%s", e.what());
  }

  _LOGD("");
  lua_close(pLua);

  _LOGD("");
  return 0;
}