#ifndef ANIMAL_H
#define ANIMAL_H

#include <string>
#include <iostream>


class Base
{
public:
  Base(std::string _a_msg) { _m_hello_world = _a_msg; }
  std::string _m_hello_world;
};


class Animal : public Base
{
private:

  int          m_iNumLegs;

  std::string  m_NoiseEmitted;

public:

  Animal(std::string NoiseEmitted,
         int         NumLegs):m_iNumLegs(NumLegs),
                              m_NoiseEmitted(NoiseEmitted), Base("HELLOWORLD")
  {}

  virtual ~Animal(){}

  virtual void Speak()const
  {std::cout << "\n[C++]: " << m_NoiseEmitted << " " << _m_hello_world << std::endl;}

  int          NumLegs()const{return m_iNumLegs;}

};




#endif