#include <lua.hpp>
#include <utils/Utils>

#include "LuaScript.h"

#include "LuaFunc.h"


static int cpp_write(lua_State* L) {
    const char* str = lua_tostring(L, 1); // get function argument
    _LOGD("%s", str);
    return 0; // nothing to return!
}

static int cpp_sum(lua_State* L) {
    // const char* str = lua_tostring(L, 1); // get function argument
  LUA_STACK_TRACE(L);
  int a = (int)lua_tonumber(L, 1);
  int b = (int)lua_tonumber(L, 2);
  lua_pushnumber(L, a + b);
  // _LOGD("%d %d", a, b);
  return 1; // calling C++ function with this argument...
}

int main(int argc, char const *argv[])
{

  std::string _l_script_name = "luajit.lua";
  // _LOGD("Running: %s", script_name.data());
  file_utils::init();

  _LOGD("%s", GET_PATH(_l_script_name).data());
  LuaScript _l_lua_scr(GET_PATH(_l_script_name));

  // LUA_STACK_TRACE(_l_lua_scr.lua_state());
  _LOGD("%d", _l_lua_scr.get<int>("player.pos.X"));
  // LUA_STACK_TRACE(_l_lua_scr.lua_state());
  _LOGD("%d", _l_lua_scr.get<int>("player.pos.Y"));
  _LOGD("%s", _l_lua_scr.get<std::string>("player.filename").data());
  _LOGD("%d", _l_lua_scr.get<int>("player.HP"));

  // LUA_STACK_TRACE(_l_lua_scr.lua_state());
  lua_State *_l_lua_state = _l_lua_scr.lua_state();

  _LOGD("%d", lua_gettop(_l_lua_state));

  _LOGD("%d", lua_sum(200,200));

  lua_State* L = luaL_newstate();
  luaL_openlibs(L); // load default Lua libs
  if (luaL_loadfile(L, GET_PATH("cpp_sum.lua").data())) {
      std::cout<<"Error loading script"<<std::endl;
  }

  lua_pushcfunction(L, cpp_write);
  lua_setglobal(L, "cpp_write"); // this is how function will be named in Lua
  lua_pushcfunction(L, cpp_sum);
  lua_setglobal(L, "cpp_sum"); // this is how function will be named in Lua
  lua_pcall(L, 0, 0, 0); // run script

  return 0;
}
