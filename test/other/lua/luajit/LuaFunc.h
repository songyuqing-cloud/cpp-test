#ifndef _LUA_FUNC_H_
#define _LUA_FUNC_H_

#include <lua.hpp>
#include <utils/Utils>

#define LUA_STACK_TRACE(lua_state) { _LOGD("stack_trace"); stack_trace(lua_state); }

void stack_trace(lua_State *L)
{
    int i;
    int top = lua_gettop(L);
    printf("---- Begin Stack ----\n");
    printf("Stack size: %i\n\n", top);
    for (i = top; i >= 1; i--)
    {
        int t = lua_type(L, i);
        switch (t)
        {
            case LUA_TSTRING:
            {
                printf("%i -- (%i) ---- `%s'", i, i - (top + 1), lua_tostring(L, i));
                break;
            }

            case LUA_TBOOLEAN:
            {
                printf("%i -- (%i) ---- %s", i, i - (top + 1), lua_toboolean(L, i) ? "true" : "false");
                break;
            }

            case LUA_TNUMBER:
            {
                printf("%i -- (%i) ---- %g", i, i - (top + 1), lua_tonumber(L, i));
                break;
            }

            default:
            {
                printf("%i -- (%i) ---- %s", i, i - (top + 1), lua_typename(L, t));
                break;
            }
        }
        printf("\n");
    }
    printf("---- End Stack ----\n");
    printf("\n");
}

int lua_sum(int x, int y) {
    lua_State* L = luaL_newstate();
    if (luaL_loadfile(L, get_path("sum.lua").data()) || lua_pcall(L, 0, 0, 0)) {
        std::cout<<"Error: failed to load sum.lua"<<std::endl;
        return 0;
    }
    lua_getglobal(L, "sum");
    lua_pushnumber(L, x);
    lua_pushnumber(L, y);

    std::cout<<"loaded"<<std::endl;
    lua_pcall(L, 2, 1, 0);

    int result = (int)lua_tonumber(L, -1);
    lua_pop(L, 1);
    return result;
}

#endif
