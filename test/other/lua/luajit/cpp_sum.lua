local ffi = require("ffi")
ffi.cdef[[
void Sleep(int ms);
int poll(struct pollfd *fds, unsigned long nfds, int timeout);
]]

local sleep
if ffi.os == "Windows" then
  function sleep(s)
    ffi.C.Sleep(s*1000)
  end
else
  function sleep(s)
    ffi.C.poll(nil, 0, s*1000)
  end
end

for i=1,160 do
  io.write("."); io.flush()
  sleep(0.01)
end
io.write("\n")

cpp_write("Hello, world!")
cpp_write("The square root of 2 is "..math.sqrt(2))
x = 42
cpp_write("We can use variables too, x = "..x)

x = cpp_sum(10,20)

print(x)


print (x.."")
