
Set = {}

function Set:new (o)
	o = o or {}   -- create object if user does not provide one
	setmetatable(o, self)
	self.__index = self
	return o
end

Set.__tostring = function (s)
	local ret = "{"
	local sep = ""

	for _, v in ipairs(s) do
		ret = ret..sep..v
		sep = ","
	end
	return ret.."}"
end

function Set:swap(i, j)
	local tmp = self[i]
	self[i] = self[j]
	self[j] = tmp
end

function Set:partition(low, high)
	local pi = high
	local pv = self[pi]

	local inc = low

	for i=low, high - 1 do
		if self[i] <= pv then
			self:swap(i, inc)
			inc = inc + 1
		end
	end

	self:swap(pi, inc)

	return inc
end

function Set:qsort(low, high)
	if low >= high - 1 then return end
	
	local pi = self:partition(low, high)
	-- print(low, pi, high)
	self:qsort(low, pi - 1)
	self:qsort(pi + 1, high)

	return self
end

math.randomseed(os.time())
local t = {}
local max = 1000

for cnt=1, max do
	for i=1, max do
		t[i] = math.random(max)
	end

	a = Set:new(t)
	-- print(a)
	a:qsort(1, #a)
	-- print(a)

end