// #include <LogMngr.hpp>

#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <cerrno>

#include <sys/syscall.h>
#include <sys/select.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h> // close
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/*
* File server.c
*/

#include <thread>
#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
#include <vector>
#include <atomic>
#include <mutex>
#include <cassert>
// #include "concurrentqueue.h"

#define _TLOGI(format, ...) printf("[I]%s %s t-%d [%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )
#define _TLOGE(format, ...) fprintf(stderr, "[E]%s %s t-%d [%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__, strerror(errno))

static const uint32_t MAX_THREAD_NUM = 32;

static const uint32_t SLEEPMS = 0;

static const uint32_t QUEUE_SIZE = 0x1000;
static const uint32_t CHUNK_SIZE = 0x10000;
// static const ssize_t WSIZE = 0x10;
// static const ssize_t RSIZE = 0x10 / 2;
static const uint32_t CONS_NUM = 4;
static const uint32_t PROD_NUM = 4;


uint32_t next_power_of_2(uint32_t val)
{
    val--;
    val |= val >> 1;
    val |= val >> 2;
    val |= val >> 4;
    val |= val >> 8;
    val |= val >> 16;
    val++;
    return val;
}

bool power_of_2(uint32_t val)
{
    return (val & (val - 1)) == 0;
}

class CCQueueIndex
{
private:
    struct _Position
    {
        uint64_t m_mask : 32;
        uint64_t m_position : 32;
    } uint64_t;

public:
    CCQueueIndex(uint32_t a_queue_size) : m_tail(0), m_head(0), m_min_tail({0}), m_min_head({0})
    {
        m_queue_size = power_of_2(a_queue_size) ? a_queue_size : next_power_of_2(a_queue_size);
    }

    void update_min(uint32_t a_val, std::atomic<_Position> &a_min_val)
    {
        _Position min_val = a_min_val.load(std::memory_order_relaxed);

        for(;;)
        {
            uint32_t volatile diff = a_val - min_val.m_position;

            if(diff == 0)
            {
                diff = 1;
                for(;;)
                {
                    while((min_val.m_mask >> diff) & 1)
                        diff++;
                    
                    _Position next_val;
                    next_val.m_position = min_val.m_position + diff;
                    next_val.m_mask = min_val.m_mask >> diff;

                    if(a_min_val.compare_exchange_strong(min_val, next_val, std::memory_order_relaxed))
                        return;
                }
            }
            else
            {
                _Position next_val;
                next_val.m_position = min_val.m_position;
                next_val.m_mask = min_val.m_mask | (1U << (diff));

                if(a_min_val.compare_exchange_strong(min_val, next_val, std::memory_order_relaxed))
                    return;
            }
        }
    }

    bool try_pop(uint32_t &a_head)
    {
        a_head = m_head.load();

        for(;;)
        {
            // empty
            if(a_head == m_min_tail.load(std::memory_order_relaxed).m_position)
                return false;

            if(m_head.compare_exchange_strong(a_head, a_head+1, std::memory_order_relaxed))
                return true;
        }
    }

    bool complete_pop(uint32_t a_head)
    {
        if((a_head - m_min_head.load(std::memory_order_relaxed).m_position) > (MAX_THREAD_NUM - 1))
            return false;

        update_min(a_head, m_min_head);
        return true;
    }

    bool try_push(uint32_t &a_tail)
    {
        a_tail = m_tail.load(std::memory_order_relaxed);

        for(;;)
        {
            // full
            if(a_tail == (m_min_head.load(std::memory_order_relaxed).m_position + m_queue_size))
                return false;

            if(m_tail.compare_exchange_strong(a_tail, a_tail + 1, std::memory_order_relaxed))
                return true;
        }
    }

    bool complete_push(uint32_t a_tail)
    {
        if((a_tail - m_min_tail.load(std::memory_order_relaxed).m_position) > (MAX_THREAD_NUM - 1))
            return false;

        update_min(a_tail, m_min_tail);
        return true;
    }

    uint32_t available() const
    {
        return m_tail.load(std::memory_order_relaxed) - m_min_head.load(std::memory_order_relaxed).m_position;
    }

    inline uint32_t wrap(uint32_t a_index) const
    {
        return a_index & (m_queue_size - 1);
    }

    inline uint32_t queue_size() const { return m_queue_size; }

private:

    std::atomic<uint32_t> m_tail, m_head;
    std::atomic<_Position> m_min_tail, m_min_head;

    uint32_t m_queue_size;
    std::mutex m_mutex;
};

class BSDLFQ
{
public:
    BSDLFQ(uint32_t a_queue_size) : m_prod_tail(0), m_prod_head(0), m_cons_tail(0), m_cons_head(0)
    {
        m_queue_size = power_of_2(a_queue_size) ? a_queue_size : next_power_of_2(a_queue_size);
    }

    bool try_pop(uint32_t &a_cons_head)
    {
        a_cons_head = m_cons_head.load(std::memory_order_relaxed);
    
        for(;;)
        {
            if (a_cons_head == m_prod_tail.load(std::memory_order_relaxed))
                return false;

            if(m_cons_head.compare_exchange_strong(a_cons_head, a_cons_head + 1, std::memory_order_relaxed))
                return true;
        }

        return false;
    }

    bool complete_pop(uint32_t a_cons_head)
    {
        while (m_cons_tail.load(std::memory_order_relaxed) != a_cons_head);
        m_cons_tail.fetch_add(1, std::memory_order_relaxed);

        return true;
    }

    bool try_push(uint32_t &a_prod_head)
    {
        a_prod_head = m_prod_head.load(std::memory_order_relaxed);
    
        for(;;)
        {
            if (a_prod_head == (m_cons_tail.load(std::memory_order_relaxed) + m_queue_size))
            {
                return false;
            }

            if(m_prod_head.compare_exchange_strong(a_prod_head, a_prod_head + 1, std::memory_order_relaxed))
            {
                return true;
            }
        }

        return false;
    }

    bool complete_push(uint32_t a_prod_head)
    {
        while (m_prod_tail.load(std::memory_order_relaxed) != a_prod_head);
        m_prod_tail.fetch_add(1, std::memory_order_relaxed);

        return true;
    }

    uint32_t available() const
    {
        return m_prod_tail.load(std::memory_order_relaxed) - m_cons_tail.load(std::memory_order_relaxed);
    }

    inline uint32_t wrap(uint32_t a_index) const
    {
        return a_index & (m_queue_size - 1);
    }

    inline uint32_t queue_size() const { return m_queue_size; }

private:

    std::atomic<uint32_t> m_prod_tail, m_prod_head, m_cons_tail, m_cons_head;

    uint32_t m_queue_size;
};

void test_ccqidx(int a_loopcnt)
{
    CCQueueIndex cb(QUEUE_SIZE);

    std::thread cons[CONS_NUM];
    std::thread prod[PROD_NUM];

    bool stop = false;
    
    a_loopcnt = a_loopcnt / PROD_NUM;
    std::atomic<int> read_failed_cnt, write_failed_cnt;
    read_failed_cnt.store(0); write_failed_cnt.store(0);

    std::vector<char> buff;
    buff.resize(cb.queue_size() * CHUNK_SIZE);

    for(uint32_t i=0; i<PROD_NUM; i++)
    {
        prod[i] = std::thread([=, &buff, &stop, &cb, &write_failed_cnt]()
        {
            int cnt=0;

            _TLOGI("WRITE_%d started on CPU:%d",i, sched_getcpu());
            while(true)
            {
                uint32_t idx=0;
                if(cb.try_push(idx))
                {
                    char *ptr = buff.data() + cb.wrap(idx) * CHUNK_SIZE;
                    memset(ptr, (char)idx, CHUNK_SIZE);

                    while(!cb.complete_push(idx));
                    if(++cnt == a_loopcnt)
                        break;
                }
                else
                {
                    write_failed_cnt++;
                }

                !SLEEPMS ?
                    std::this_thread::yield()
                    : std::this_thread::sleep_for(std::chrono::milliseconds(SLEEPMS));
            }

            // _TLOGI("WRITE cnt: %d", cnt);
            // cb.debug();
        });
    }

    for(uint32_t i=0; i<CONS_NUM; i++)
    {
        cons[i] = std::thread([=, &buff, &stop, &cb, &read_failed_cnt]()
        {
            int cnt=0;

            _TLOGI("READ_%d started on CPU:%d",i, sched_getcpu());
            while(cb.available() || !stop)
            {
                uint32_t idx=0;
                if(cb.try_pop(idx))
                {
                    uint32_t wrap = cb.wrap(idx);
                    char *ptr = buff.data() + wrap * CHUNK_SIZE;
                    if((char)wrap != *ptr || memcmp(ptr, ptr+1, CHUNK_SIZE-1))
                        _TLOGI("[FAILED] %d - %d", (char)wrap, *ptr);

                    while(!cb.complete_pop(idx));
                    cnt++;
                }
                else
                    read_failed_cnt++;

                // _TLOGI("%d", cb.available());

                !SLEEPMS ?
                    std::this_thread::yield()
                    : std::this_thread::sleep_for(std::chrono::milliseconds(SLEEPMS));
            }
                
            // _TLOGI("READ  cnt: %d", cnt);
            // cb.debug();
        });
    }

    std::thread tracking([&cb, &stop]()
    {
        while(!stop)
        {
            _TLOGI("A: %d", cb.available());
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
    });

    for(uint32_t i=0; i<PROD_NUM; i++)
        prod[i].join();
        
    stop = true;

    for(uint32_t i=0; i<CONS_NUM; i++)
        cons[i].join();

    tracking.join();
    
    _TLOGI("write failed: %d | read failed: %d", write_failed_cnt.load(), read_failed_cnt.load());
}

void test_bsdlfq(int a_loopcnt)
{
    BSDLFQ cb(QUEUE_SIZE);

    std::thread cons[CONS_NUM];
    std::thread prod[PROD_NUM];

    bool stop = false;
    
    a_loopcnt = a_loopcnt / PROD_NUM;
    std::atomic<int> read_failed_cnt, write_failed_cnt;
    read_failed_cnt.store(0); write_failed_cnt.store(0);

    std::vector<char> buff;
    buff.resize(cb.queue_size() * CHUNK_SIZE);

    for(uint32_t i=0; i<PROD_NUM; i++)
    {
        prod[i] = std::thread([=, &buff, &stop, &cb, &write_failed_cnt]()
        {
            int cnt=0;

            _TLOGI("WRITE_%d started on CPU:%d",i, sched_getcpu());
            while(true)
            {
                uint32_t idx=0;
                if(cb.try_push(idx))
                {
                    char *ptr = buff.data() + cb.wrap(idx) * CHUNK_SIZE;
                    memset(ptr, (char)idx, CHUNK_SIZE);

                    cb.complete_push(idx);
                    if(++cnt == a_loopcnt)
                        break;
                }
                else
                {
                    write_failed_cnt++;
                }

                !SLEEPMS ?
                    std::this_thread::yield()
                    : std::this_thread::sleep_for(std::chrono::milliseconds(SLEEPMS));
            }

            // _TLOGI("WRITE cnt: %d", cnt);
            // cb.debug();
        });
    }

    for(uint32_t i=0; i<CONS_NUM; i++)
    {
        cons[i] = std::thread([=, &buff, &stop, &cb, &read_failed_cnt]()
        {
            int cnt=0;

            _TLOGI("READ_%d started on CPU:%d",i, sched_getcpu());
            while(cb.available() || !stop)
            {
                uint32_t idx=0;
                if(cb.try_pop(idx))
                {
                    uint32_t wrap = cb.wrap(idx);
                    char *ptr = buff.data() + wrap * CHUNK_SIZE;
                    if((char)wrap != *ptr || memcmp(ptr, ptr+1, CHUNK_SIZE-1))
                        _TLOGI("[FAILED] %d - %d", (char)wrap, *ptr);

                    cb.complete_pop(idx);
                    cnt++;
                }
                else
                    read_failed_cnt++;

                // _TLOGI("%d", cb.available());

                !SLEEPMS ?
                    std::this_thread::yield()
                    : std::this_thread::sleep_for(std::chrono::milliseconds(SLEEPMS));
            }
                
            // _TLOGI("READ  cnt: %d", cnt);
            // cb.debug();
        });
    }

    std::thread tracking([&cb, &stop]()
    {
        while(!stop)
        {
            _TLOGI("A: %d", cb.available());
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
    });

    for(uint32_t i=0; i<PROD_NUM; i++)
        prod[i].join();
        
    stop = true;

    for(uint32_t i=0; i<CONS_NUM; i++)
        cons[i].join();

    tracking.join();
    
    _TLOGI("write failed: %d | read failed: %d", write_failed_cnt.load(), read_failed_cnt.load());
}

int main(int argc, char **argv)
{
    int loopcnt = 0;
    
    if(argc > 1) 
        loopcnt = std::stoi(argv[1]);

    using namespace std::chrono;

    high_resolution_clock::time_point tbeg;

    tbeg = high_resolution_clock::now();
    if(argc > 2)
        test_bsdlfq(loopcnt);
    else
        test_ccqidx(loopcnt);

    _TLOGI("%.2f ms", std::chrono::duration <double, std::milli> (std::chrono::high_resolution_clock::now() - tbeg).count());

    // std::this_thread::sleep_for(std::chrono::milliseconds(500));

    // tbeg = high_resolution_clock::now();
    // test_bsdlfq(loopcnt);
    // _TLOGI("%.2f ms", std::chrono::duration <double, std::milli> (std::chrono::high_resolution_clock::now() - tbeg).count());

    return 0;
}
