#include <cassert>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <ctime>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>

#include <algorithm>

#include <thread>
#include <chrono>

#define MAX_THREAD_NUM          4

#define ONE_MB      0x100000
#define ONE_GB      (ONE_MB * 0x400)

#define MAX_LINE_LENGTH (ONE_MB)                 // 1Mb

#define LINE_NUM (ONE_GB / MAX_LINE_LENGTH * 5) // 10Gb
// #define LINE_NUM (100) // 10Gb


#define MIN_TAPE_NUM            2


// #include <sstream>
// namespace std {
//     int stoi(const std::string &a_str)
//     {
//         return std::atoi(a_str.data());
//     }

//     template<typename T>
//     std::string to_string(const T &n) {
//         std::stringstream s;
//         s << n;
//         return s.str();
//     }
// }


#define _LOGD(format, ...) printf("%s %s [%d][D]'" format "'\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

namespace custom
{

class MyCounter
{
public:
    MyCounter()
    {
        name = "";
        tbeg = std::chrono::high_resolution_clock::now();
    }

    MyCounter(std::string const &a_str) : name(a_str)
    {
        tbeg = std::chrono::high_resolution_clock::now();
    }

    virtual ~MyCounter()
    {
        // auto diff = std::chrono::high_resolution_clock::now() - tbeg;
        _LOGD("%.2f ms", std::chrono::duration <double, std::milli> (std::chrono::high_resolution_clock::now() - tbeg).count());
    }

    std::string name;
    std::chrono::high_resolution_clock::time_point tbeg;

};

}

#define COUNTER_BEGIN                       { custom::MyCounter l_counter(std::to_string(__LINE__) + " : ")
#define COUNTER_BEGIN_STR(a_name)           { custom::MyCounter l_counter(std::to_string(__LINE__) + " : " + a_name)
#define COUNTER_END                         _LOGD("%s", l_counter.name.c_str()); }


void gen_random_data(std::string const &a_file, size_t a_line_num)
{
    std::ofstream l_file;
    l_file.open (a_file, std::ios::out | std::ios::binary);

    for(size_t i=0; i<a_line_num; i++)
    {
        char l_tmp = std::rand() % ('Z' - 'A') + 'A';

        size_t l_length = std::rand() % MAX_LINE_LENGTH + 1;
        std::vector<uint8_t> l_data(l_length, l_tmp);

        l_file.write(reinterpret_cast<char *>(&l_data[0]), l_length);
        l_file << '\n';
    }

    l_file.close();
}

std::ifstream::pos_type fsize(const std::string &filename)
{
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    return in.tellg();
}

void store(std::string const &a_file_name, std::vector<std::string> const &a_buffers)
{
    std::ofstream l_ofs;
    l_ofs.open (a_file_name, std::ios::out | std::ios::app | std::ios::binary);

    for(size_t i=0; i<a_buffers.size(); i++)
    {
        l_ofs.write(a_buffers[i].data(), a_buffers[i].size());
        l_ofs << '\n';
    }

    l_ofs.close();
}

bool is_empty(std::ifstream& pFile)
{
    return pFile.peek() == std::ifstream::traits_type::eof();
}

size_t merge_step(std::ifstream *a_ifs, std::ofstream &a_ofs, size_t a_tape_num, size_t a_rec_num, size_t a_crr_rec_num)
{
    assert(a_ifs);

    size_t l_rec_count[a_tape_num];
    std::memset(l_rec_count, 0, sizeof(l_rec_count));

    size_t l_ret=0;

    std::string l_str_out[a_tape_num];

    for(size_t k=0; k<a_crr_rec_num; k++)
    {

        int idx=-1;

        for(size_t i=0; i<a_tape_num; i++)
        {

            if(l_str_out[i].empty() && (l_rec_count[i]<(a_crr_rec_num/a_tape_num)) && !is_empty(a_ifs[i]))
            {
                l_rec_count[i]++;
                std::getline(a_ifs[i], l_str_out[i], '\n');
                // _LOGD("R[%d]: %d %s", i, l_rec_count[i], l_str_out[i].data());
                l_str_out[i]+="\n";
            }

        }

        for(int i=0; i<a_tape_num; i++)
        {
            if(!l_str_out[i].empty())
            {
                if(idx==-1) idx=i;
                else if(l_str_out[i] < l_str_out[idx]) idx=i;
            }
        }

        if(idx>=0)
        {
            std::string const &str = l_str_out[idx];
            // _LOGD("W_[%ld]: %s", idx, str.data());
            a_ofs.write(str.data(), str.size());
            l_str_out[idx].clear();
            idx = -1;
        }
        else
        {
            break;
        }

    }

    for(auto tmp : l_rec_count)
    {
        // _LOGD("l_rec_count: %d", tmp);
        l_ret+=tmp;
    }

    return l_ret;

}

void merge_all(int a_id, std::vector<std::string> const &a_tapes, std::string const &a_final, size_t a_rec_num, size_t a_total_rec_num)
{

    int t=0;
    _LOGD("[%d] l_total_rec: %ld", a_id, a_total_rec_num);

    size_t l_crr_rec_num = a_rec_num;

    size_t const l_tapes_num = a_tapes.size() / 2;

    while(l_crr_rec_num < a_total_rec_num)
    {
        l_crr_rec_num *= l_tapes_num;
        t^=1;

        _LOGD("[%d] l_crr_rec_num: %ld", a_id, l_crr_rec_num);

        std::ifstream l_ifs[l_tapes_num];

        // open in / out
        for(int i=0; i<l_tapes_num; i++)
        {
            const int in_id = i + l_tapes_num*(t^1);
            const int out_id = i + l_tapes_num*t;


            const std::string l_in_file = a_tapes[in_id];
            l_ifs[i].open(l_in_file, std::ios::in | std::ios::binary);


            const std::string l_out_file = a_tapes[out_id];
            // std::ofstream l_ofs(l_out_file, std::ios::out | std::ios::binary);
            std::remove(l_out_file.data());

            _LOGD("[%d] %d|%d %s %s", a_id, in_id, out_id, l_in_file.data(), l_out_file.data());
        }

        for(size_t r=0; r<a_total_rec_num;)
        {

            for(int i=0; i<l_tapes_num; i++)
            {
                const int out_id = i + l_tapes_num*t;
                std::string l_out_file = a_tapes[out_id];

                std::ofstream l_ofs;

                if(l_crr_rec_num >= (a_total_rec_num))
                {

                    if(a_id >= 0)
                    {
                        l_out_file = "._t_" + std::to_string(a_id) + ".tmp";
                    }
                    else
                    {
                        l_out_file = a_final;
                    }

                }
                else
                {
                    l_out_file = a_tapes[out_id];
                }

                l_ofs.open(l_out_file, std::ios::app | std::ios::binary);

                r+=merge_step(l_ifs, l_ofs, l_tapes_num, a_rec_num, l_crr_rec_num);
                _LOGD("[%d] write to %s, r:%ld/%ld", a_id, l_out_file.data(), r, a_total_rec_num);
                assert(r <= a_total_rec_num);

                if(l_crr_rec_num  >= a_total_rec_num || r==a_total_rec_num)
                {
                    _LOGD("[%d] DONE", a_id);
                    break;
                }

            }

        }

        // std::this_thread::sleep_for(std::chrono::milliseconds(1));

    }
}


int main(int argc, char ** argv)
{

    if(argc < 4)
    {
        _LOGD("Usage: {in name} {out name} {Mem limit in byte} [thread nums (>0)] [gen n line data]");
        return 1;
    }

    size_t l_tapes_num = MIN_TAPE_NUM;
    size_t l_thread_num = 1;
    size_t l_line_num = 0;

    std::string l_input = argv[1];
    std::string l_output = argv[2];
    size_t l_mem_limit = std::stoull(argv[3]);

    // if(argc > 4)
    // {
    //     l_tapes_num = std::stoi(argv[4]);
    // }

    if(argc > 4)
    {
        l_thread_num = std::stoi(argv[4]);
    }

    if(argc > 5)
    {
        l_line_num = std::stoul(argv[5]);
    }

    assert(l_tapes_num > 1);
    assert(l_thread_num);

    if(l_thread_num > MAX_THREAD_NUM) l_thread_num = MAX_THREAD_NUM;

    _LOGD("Mem: %.2f GB", (l_mem_limit * 1.f) / ONE_GB);
    _LOGD("tapes: 0x%lx", l_tapes_num);
    _LOGD("threads: 0x%lx", l_thread_num);


    if(l_line_num)
    {
        COUNTER_BEGIN_STR("gen_random_data");
            std::srand(std::time(0));
            gen_random_data(l_input, l_line_num);
        COUNTER_END;
    }

    size_t l_fsize = fsize(l_input);
    _LOGD("file size: %.2f GB", l_fsize * 1.f / ONE_GB);

    if(l_fsize == 0)
    {
        _LOGD("EMPTY");
        return 1;
    }

    std::remove(l_output.data());

    if(l_mem_limit > l_fsize)
    {
        std::vector<std::string> l_buffers;
        l_buffers.reserve(l_fsize * 2 / MAX_LINE_LENGTH);
        std::string l_buff;
        l_buff.reserve(MAX_LINE_LENGTH);

        std::ifstream l_ifs(l_input, std::ios::in | std::ios::binary);

        while(std::getline(l_ifs, l_buff, '\n'))
        {
            l_buffers.push_back(l_buff);
            l_buff.clear();
        }

        if(l_buffers.size() == 0)
        {
            _LOGD("EMPTY");
            return 1;
        }
        else
        {
            std::sort(l_buffers.begin(), l_buffers.end());

            store(l_output , l_buffers);

            _LOGD("DONE");
        }

        return 0;
    }

    size_t l_rec_num = l_mem_limit / MAX_LINE_LENGTH;
    assert(l_rec_num > 1);

    if(l_rec_num/l_thread_num < 2) l_thread_num = 1;
    l_rec_num /= l_thread_num;

    size_t l_total_rec[MAX_THREAD_NUM];
    std::memset(l_total_rec, 0, sizeof(l_total_rec));

    _LOGD("l_rec_num: %ld", l_rec_num);

    std::vector<std::string> l_tapes;

    std::thread l_threads[MAX_THREAD_NUM];

    for(int i=0; i<l_tapes_num*l_thread_num; i++)
    {
        l_tapes.push_back("._ta_" + std::to_string(i) + ".tmp");
            std::remove(l_tapes.back().data());
    }

    for(int i=0; i<l_tapes_num*l_thread_num; i++)
    {
        l_tapes.push_back("._tb_" + std::to_string(i) + ".tmp");
            std::remove(l_tapes.back().data());
    }

    COUNTER_BEGIN_STR("total");
    COUNTER_BEGIN_STR("read-sort-write");
    // read - sort - write
    {
        std::ifstream l_ifs(l_input, std::ios::in | std::ios::binary);

        bool l_is_run = true;

        std::vector<std::string> l_buffers;
        l_buffers.reserve(l_rec_num * 2);
        std::string l_buff;
        l_buff.reserve(MAX_LINE_LENGTH);

        for(int t=0; l_is_run; t++)
        {
            l_buffers.clear();

            // read
            for(int i=0; i<l_rec_num; i++)
            {
                l_buff.clear();

                if( !std::getline(l_ifs, l_buff, '\n') )
                {
                    l_is_run = false;
                    break;
                }

                l_buffers.push_back(l_buff);

            }

            // sort
            if(l_buffers.size())
            {
                std::sort(l_buffers.begin(), l_buffers.end());
            }

            // write
            if(t == l_tapes_num*l_thread_num) t=0;
            store(l_tapes[t] , l_buffers);
            l_total_rec[t/2] += l_buffers.size();
        }

    }
    COUNTER_END;

    for(int i=0; i<l_thread_num; i++)
        _LOGD("total rec[%d]: %ld", i, l_total_rec[i]);

    COUNTER_BEGIN_STR("merge-all");
    // merge
    for(int tid=0; tid<l_thread_num; tid++)
    {
        l_threads[tid] = std::thread([=, &l_tapes](int a_id)
            {
                // size_t l_t_total_rec = l_total_rec[a_id];
                size_t l_t_total_rec;
                // l_t_total_rec = l_total_rec[a_id];

                std::vector<std::string> l_t_tapes;

                for(int i=0; i<l_tapes_num; i++)
                {
                    int idx = i + (a_id * l_tapes_num);
                    l_t_tapes.push_back(l_tapes[idx]);
                }

                for(int i=0; i<l_tapes_num; i++)
                {
                    int idx = i + l_tapes_num*l_thread_num + (a_id * l_tapes_num);
                    l_t_tapes.push_back(l_tapes[idx]);
                }

                // return 0;
                std::string l_final_out;

                if(l_thread_num > 1)
                {
                    l_final_out = "._t_" + std::to_string(a_id) + ".tmp";
                }
                else
                {
                    l_final_out = l_output;
                }

                merge_all(a_id, l_t_tapes, l_final_out, l_rec_num, l_total_rec[a_id]);

                for(auto f : l_t_tapes)
                {
                    std::remove(f.data());
                }
            }, tid);

        // l_threads[tid] = std::thread([=, &l_tapes](int a_id) {}, tid);
    }

    for(int tid=0; tid<l_thread_num; tid++)
    {
        l_threads[tid].join();
    }

    COUNTER_END;

    // final merge
    if(l_thread_num > 1)
    {
        COUNTER_BEGIN_STR("final-merge");
        std::vector<std::string> l_t_tapes;
        size_t ttec=0;

        for(int i=0; i<l_thread_num; i++)
        {
            ttec += l_total_rec[i];
            l_t_tapes.push_back("._t_" + std::to_string(i) + ".tmp");

            _LOGD("%s", l_t_tapes.back().data());
        }

        for(int i=0; i<l_thread_num; i++)
        {
            l_t_tapes.push_back(".__t_" + std::to_string(i) + ".tmp");

            _LOGD("%s", l_t_tapes.back().data());
        }


        l_rec_num*=l_thread_num;
        _LOGD("%ld %ld", l_t_tapes.size(), l_rec_num);

        merge_all(-1, l_t_tapes, l_output, l_rec_num, ttec);

        for(auto f : l_t_tapes)
        {
            std::remove(f.data());
        }
        COUNTER_END;
    }

    COUNTER_END;


    return 0;
}


