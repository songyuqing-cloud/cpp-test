#include <cassert>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <ctime>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>

#include <algorithm>

#include <thread>
#include <chrono>

#define MAX_THREAD_NUM          8

#define ONE_MB      0x100000
#define ONE_GB      (ONE_MB * 0x400)

#define MAX_LINE_LENGTH (2)                 // 1Mb

#define MIN_TAPE_NUM            2

#define TEMP_0      ".0.tmp"
#define TEMP_1      ".1.tmp"

#define RIDX(x) (x)
#define WIDX(x) (x^1)

#define LOG_EN
#include "../../../utils/Utils"

std::streampos fsize(const std::string &filename)
{
    std::fstream in(filename, std::fstream::in | std::fstream::ate | std::fstream::binary);
    return in.tellg();
}

bool is_empty(std::ifstream& pFile)
{
    return pFile.peek() == std::ifstream::traits_type::eof();
}

void gen_random_data(std::string const &a_file, size_t a_line_num)
{
    std::ofstream l_file;
    l_file.open (a_file, std::ios::out | std::ios::binary);

    for(size_t i=0; i<a_line_num; i++)
    {
        char l_tmp = std::rand() % ('Z' - 'A' + 1) + 'A';

        size_t l_length = std::rand() % MAX_LINE_LENGTH + 1;
        std::vector<uint8_t> l_data(l_length, l_tmp);

        l_file.write(reinterpret_cast<char *>(&l_data[0]), l_length);
        l_file << '\n';
    }

    l_file.close();
}

struct ESWorker
{

    int m_id;

    size_t m_mem_limit;

    std::string m_in;
    std::string m_out;

    std::streampos m_end_pos;
    std::streampos m_beg_pos;

    std::thread m_worker;
    std::vector<std::tuple<std::streampos, std::streampos>> m_chunks_info;

    struct _Record
    {
        size_t m_chunk_idx;
        std::streampos m_roff;
        std::string m_line;
        // std::fstream *m_stream;
    };

    ~ESWorker()
    {
        m_worker.join();
    }

    void divide()
    {
        size_t const l_rec_limit = m_mem_limit / MAX_LINE_LENGTH;

        std::fstream l_in(m_in, std::fstream::in | std::fstream::binary );
        l_in.seekg(m_beg_pos);

        std::fstream l_out("." + std::to_string(m_id) + TEMP_0, std::fstream::out | std::fstream::binary );
        l_out.seekp(0);

        std::vector<std::string> l_buffers;
        l_buffers.reserve(l_rec_limit);

        std::string l_buff;
        l_buff.reserve(MAX_LINE_LENGTH);

        _LOGD("T%d B:%d E:%d", m_id, (int)(m_beg_pos), (int)(m_end_pos));

        for(size_t c=0; (l_in.tellg() + (std::streampos)1) < (m_end_pos) && l_in >> l_buff; c++)
        {
            if(l_buff.empty()) continue;

            if(c == l_rec_limit)
            {
                std::sort(l_buffers.begin(), l_buffers.end());
                std::streampos beg = l_out.tellp();
                // if(beg) beg = int(beg) + 1;

                // _LOGD("writing: %d", l_buffers.size());

                for(std::string const &line : l_buffers)
                {
                    l_out << line << '\n';
                    // _LOGD("%d: %s", (int)(l_out.tellp()), line.data());
                }

                std::streampos end = l_out.tellp();
                m_chunks_info.push_back(std::make_tuple(beg, end));

                l_buffers.clear();

                c = 0;

                _LOGD("T%d c:%d | r:%d, e:%d", m_id, m_chunks_info.size() - 1, static_cast<int>(beg), static_cast<int>(end));
            }

            l_buffers.push_back(l_buff);
            l_buff.clear();
        }

        if(l_buffers.size())
        {
            _LOGD("Last writing : %d", l_buffers.size());

            std::sort(l_buffers.begin(), l_buffers.end());
            std::streampos beg = l_out.tellp();

            for(std::string const &line : l_buffers)
            {
                l_out << line << '\n';
            }

            std::streampos end = l_out.tellp();
            m_chunks_info.push_back(std::make_tuple(beg, end));

            _LOGD("c:%d | r:%d, e:%d", m_chunks_info.size() - 1, static_cast<int>(beg), static_cast<int>(end));
        }

        // for(int i=0; i<m_chunks_info.size(); i++)
        // {
        //     std::string line;

        //     std::streampos r = std::get<0>(m_chunks_info[i]);
        //     std::streampos e = std::get<1>(m_chunks_info[i]);
        //     l_out.seekg(r);

        //     // while(r < e && r != -1)
        //     // {
        //     //     l_out >> line;
        //     //     _LOGD("[%d] %d|%d: %s", i, (int)r, (int)e, line.data());
        //     //     r = l_out.tellg();
        //     // }

        //     _LOGD("[%d] %d|%d", i, (int)r, (int)e);
        // }

    }

    // void merge_step(std::priority_queue<
    //         _Record,
    //         std::vector<_Record>,
    //         std::function<bool(const _Record&, const _Record&)>
    //         > *l_pqueue, size_t a_tape_num)
    // {
    //     assert(l_pqueue);

    // }

    void merge(std::string const &a_tmp0, std::string const &a_tmp1)
    {
        std::priority_queue<
            _Record,
            std::vector<_Record>,
            std::function<bool(const _Record&, const _Record&)>
            >
                l_pqueue([](const _Record &a, const _Record &b) { return a.m_line > b.m_line; });

        size_t const l_rec_limit = m_mem_limit / MAX_LINE_LENGTH;

        assert (l_rec_limit > 1);
        _LOGI("T%d rec limit: %d", m_id, l_rec_limit);

        std::fstream l_temps[2];

        std::string l_ftmps[2] = {a_tmp0, a_tmp1};

        l_temps[0].open(l_ftmps[0], std::fstream::in | std::fstream::out | std::fstream::binary);
        l_temps[1].open(l_ftmps[1], std::fstream::in | std::fstream::out | std::fstream::binary | std::fstream::trunc);

        {
            size_t const l_tape_num = l_rec_limit < m_chunks_info.size() ? (l_rec_limit) : (m_chunks_info.size());
            // size_t l_chunk_id_max = l_tape_num;

            // if(l_tape_num == 1) l_tape_num = 2;
            assert(l_tape_num > 1);

        //     std::fstream l_stream_tmp;

            // if(m_chunks_info.size() > l_tape_num) l_stream_tmp.open("._" + std::to_string(m_id) + ".tmp", std::fstream::in | std::fstream::out | std::fstream::binary );

            int t = 0;
            std::string l_line;
            l_line.reserve(MAX_LINE_LENGTH);
            _LOGI("T%d tapes:%d", m_id, l_tape_num);

            // size_t l_crr = l_rec_limit;

            while(m_chunks_info.size() > 1)
            {
                std::fstream &l_in = l_temps[RIDX(t)];
                l_in.clear();

                // {

                //     std::string tmp;
                //     l_in.seekg(0);
                //     while(l_in >> tmp)
                //     {
                //         _LOGD("T%d %s", m_id, tmp.data());
                //     }
                //     l_in.seekg(0);
                // }

                std::fstream &l_out = l_temps[WIDX(t)];

                if(m_chunks_info.size() <= l_tape_num)
                {
                    l_out.close();
                    _LOGI("writing %s", m_out.data());
                    l_out.open(m_out, std::fstream::in | std::fstream::out | std::fstream::binary);
                    l_out.seekp(m_beg_pos);
                }
                else
                {
                    l_out.seekp(0);
                }


                _LOGD("T%d R: %s W: %s", m_id, l_ftmps[RIDX(t)].data(), l_ftmps[WIDX(t)].data());
                _LOGD("T%d size: %d", m_id, m_chunks_info.size());

                // merge steps.
                // while(l_chunk_id_beg < m_chunks_info.size())
                for(size_t l_chunk_id_beg = 0, l_chunk_id_max=l_tape_num; l_chunk_id_beg<m_chunks_info.size(); l_chunk_id_beg=l_chunk_id_max, l_chunk_id_max+=l_tape_num)
                {
                    // first read.
                    for(size_t i=l_chunk_id_beg; i<l_chunk_id_max && i<m_chunks_info.size(); i++)
                    {
                        l_line.clear();

                        std::streampos &beg_pos = std::get<0>(m_chunks_info[i]);
                        // _LOGD("T%d %d", m_id, (int)beg_pos);
                        l_in.clear();
                        l_in.seekg(beg_pos);
                        // _LOGD("%d", (int)l_in.tellg());
                        l_in >> l_line;
                        assert(!l_line.empty());
                        // update reading position
                        beg_pos = l_in.tellg();
                        // _LOGD("T%d P: %s [%d] %d", m_id, l_line.data(), i, (int)beg_pos);
                        l_pqueue.push({i, 0, l_line});
                    }

                    // _LOGD("%d %d q size: %d", l_chunk_id_beg, l_chunk_id_max, l_pqueue.size());

                    // merge step
                    while(!l_pqueue.empty())
                    {
                        l_line.clear();

                        _Record const &rec = l_pqueue.top();

                        _LOGD("T%d [%d] W: %s", m_id, rec.m_chunk_idx, rec.m_line.data());
                        if(!rec.m_line.empty()) l_out << rec.m_line << '\n';

                        size_t chunk_idx = rec.m_chunk_idx;
                        l_pqueue.pop();

                        // there is no chunk to read.
                        if(chunk_idx == (size_t)(-1))
                        {
                            // _LOGE("");
                            continue;
                        }

                        std::streampos &beg_pos = std::get<0>(m_chunks_info[chunk_idx]);
                        std::streampos end_pos = std::get<1>(m_chunks_info[chunk_idx]);
                        _LOGD("T%d [%d] b:%d e:%d", m_id, chunk_idx, (int)beg_pos, (int)end_pos);

                        l_in.clear();
                        l_in.seekg(beg_pos); // jump to appropriate reading position
                        l_in >> l_line;
                        // _LOGD("- R[%d]: %d %s", chunk_idx, (int)beg_pos, l_line.data());

                        beg_pos = l_in.tellg() + (std::streampos)1; // update reading position
                        // _LOGD("%s", l_line.data());
                        // assert(!l_line.empty());
                        // if(beg_pos >= end_pos)
                            // _LOGD("c:%d | r:%d, e:%d", chunk_idx, static_cast<int>(beg_pos), static_cast<int>(end_pos));
                        // assert(beg_pos <= end_pos);

                        // check end of chunk
                        if(l_line.empty() || beg_pos >= (m_end_pos) || beg_pos >= end_pos || beg_pos == 0)
                        {
                            chunk_idx = static_cast<size_t>(-1);
                        }

                        l_pqueue.push({chunk_idx, 0, l_line});

                    }

                }

                // merge chunks
                for(size_t i=0; i<m_chunks_info.size(); i++)
                {

                    for(size_t _i=0; _i<l_tape_num - 1 && i<m_chunks_info.size() - 1; _i++)
                    {
                        // _LOGD("Delete: %d", i);
                        m_chunks_info.erase(m_chunks_info.begin() + i);
                    }

                    if(i) std::get<0>(m_chunks_info[i]) = std::get<1>(m_chunks_info[i-1]);
                    else std::get<0>(m_chunks_info[i]) = 0;

                    // int r = static_cast<int>(std::get<0>(m_chunks_info[i]));
                    // int e = static_cast<int>(std::get<1>(m_chunks_info[i]));

                    // _LOGD("c:%d | r:%d, e:%d | s:%d", i, r, e, m_chunks_info.size());
                }

                // for(int i=0; i<m_chunks_info.size(); i++)
                // {
                //     // _LOGD("c:%d r:%d e:%d");
                //     int r = static_cast<int>(std::get<0>(m_chunks_info[i]));
                //     int e = static_cast<int>(std::get<1>(m_chunks_info[i]));
                //     _LOGD("c:%d | r:%d, e:%d", i, r, e);
                // }

                // update io direction
                t ^= 1;
                // l_crr *= 2;

                // break;
                // if(l_chunk_id_max > m_chunks_info.size()) l_chunk_id_max = m_chunks_info.size();
            }

            // _LOGD("%d %d", m_id, m_chunks_info.size());

        //     // std::fstream l_stream_out(m_out, std::fstream::in | std::fstream::out | std::fstream::binary );
        }

    }

    void exec()
    {
        m_worker = std::thread([this]()
        {
            // std::string name = std::to_string(m_id);
            std::remove(("." + std::to_string(m_id) + TEMP_0).data());
            std::remove(("." + std::to_string(m_id) + TEMP_1).data());

            if(m_chunks_info.empty())
            {
                _LOGI("T%d starting", m_id);
                divide();
                merge("." + std::to_string(m_id) + TEMP_0, "." + std::to_string(m_id) + TEMP_1);
            }
            else
            {
                _LOGI("chunks: %d", m_chunks_info.size());
                merge(m_in, "." + std::to_string(m_id) + TEMP_0);
            }

            _LOGI("T%d DONE", m_id);

        });
    }

};

int main(int argc, char ** argv)
{
    if(argc < 4)
    {
        _LOGI("Usage: {in name} {out name} {Mem limit in byte} [thread nums (>0)] [gen n line data]");
        return 1;
    }

    size_t l_thread_num = 1;
    size_t l_line_num = 0;

    std::string l_input = argv[1];
    std::string l_output = argv[2];
    size_t l_mem_limit = std::stoull(argv[3]);

    if(argc > 4)
    {
        l_thread_num = std::stoi(argv[4]);
    }

    if(argc > 5)
    {
        l_line_num = std::stoul(argv[5]);
    }

    assert(l_thread_num);

    if(l_thread_num > MAX_THREAD_NUM) l_thread_num = MAX_THREAD_NUM;

    _LOGI("Mem: %.2f GB", (l_mem_limit * 1.f) / ONE_GB);
    _LOGI("threads: 0x%lx", l_thread_num);


    if(l_line_num)
    {
        COUNTER_BEGIN_STR("gen_random_data");
            std::srand(std::time(0));
            gen_random_data(l_input, l_line_num);
        COUNTER_END;
    }

    size_t l_fsize = fsize(l_input);
    _LOGI("file size: %ld", l_fsize);

    if(l_fsize == 0)
    {
        _LOGI("EMPTY");
        return 1;
    }

    std::string l_temp_out = (l_thread_num == 1) ? (l_output) : ("." + l_output + ".tmp");

    std::remove(l_output.data());
    std::remove((l_temp_out).data());

    {
        std::fstream f(l_output, std::ios::out);
        std::fstream ft(l_temp_out, std::ios::out);
    }

////////////////////////////////////////////////////////////////////////////////

    // find offset & length
    std::vector<std::tuple<std::streampos, std::streampos>> l_v;
    size_t beg = 0;
    for(size_t i=0; i<l_thread_num; i++)
    {
        std::fstream ifs(l_input, std::ios::in);
        size_t end = l_fsize/l_thread_num * (i+1);
        ifs.seekg(end);
        std::string line;
        ifs >> line;
        end = (size_t)(ifs.tellg());

        if(end == (size_t)(-1)) end = l_fsize;
        else end++;

        l_v.push_back(std::make_tuple((std::streampos)beg, (std::streampos)end));
        beg = end;
    }

    for(auto tmp : l_v)
    {
        _LOGI("%ld %ld", (int)std::get<0>(tmp), (int)std::get<1>(tmp));
    }

    // return 0;

    {
        ESWorker *ws[l_thread_num + 1];
        for(int i=0; i<(int)l_thread_num; i++)
        {
            ws[i] = new ESWorker{
                .m_id = i,
                .m_mem_limit = l_mem_limit / l_thread_num,
                .m_in = l_input,
                .m_out = l_temp_out,
                .m_end_pos = std::get<1>(l_v[i]),
                .m_beg_pos = std::get<0>(l_v[i])
            };

            ws[i]->exec();
            delete ws[i];
        }

        // for(int i=0; i<l_thread_num; i++) delete ws[i] ;
    }

    // _LOGD("");
    // {
    //     ESWorker ws = {
    //         .m_id = 0,
    //         .m_mem_limit = l_mem_limit,
    //         .m_in = l_input,
    //         .m_out = l_temp_out,
    //         .m_length = l_fsize - 102,
    //         .m_beg_pos = 102
    //     };

    //     ws.exec();

    //     ESWorker ws1 = {
    //         .m_id = 1,
    //         .m_mem_limit = l_mem_limit,
    //         .m_in = l_input,
    //         .m_out = l_temp_out,
    //         .m_length = 102,
    //         .m_beg_pos = 0
    //     };

    //     ws1.exec();
    // }

    {
        // return 1;

        if(l_v.size() == 1) return 0;
        // assert(l_v.size())

        _LOGI("Final merge");

        ESWorker fn = {
            .m_id = (int)(l_thread_num + 1),
            .m_mem_limit = l_mem_limit,
            .m_in = l_temp_out,
            .m_out = l_output,
            .m_end_pos = l_fsize,
            .m_beg_pos = 0
        };

        fn.m_chunks_info = l_v;

        // fn.m_chunks_info.push_back(std::make_tuple(0, 102));
        // fn.m_chunks_info.push_back(std::make_tuple(102, l_fsize));
        fn.exec();
    }

    return 0;
}

