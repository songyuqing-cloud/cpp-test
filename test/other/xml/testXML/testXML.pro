#-------------------------------------------------
#
# Project created by QtCreator 2016-02-20T16:34:27
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = testXML
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    rapidxml-1.13/rapidxml.hpp \
    rapidxml-1.13/rapidxml_iterators.hpp \
    rapidxml-1.13/rapidxml_print.hpp \
    rapidxml-1.13/rapidxml_utils.hpp

