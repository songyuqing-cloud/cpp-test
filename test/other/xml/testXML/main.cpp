#include <QCoreApplication>
#include <QDebug>
#include <QString>

#include "rapidxml-1.13/rapidxml.hpp"
#include "rapidxml-1.13/rapidxml_iterators.hpp"
#include "rapidxml-1.13/rapidxml_print.hpp"
#include "rapidxml-1.13/rapidxml_utils.hpp"

using namespace rapidxml;

#define COMPARE_NAME(x, y)      strncmp(x, y, strlen(x))

int parserXML(
    const QString filePath,
    QString & mAlbum,
    QString & mArtist,
    QString & mAlbumArt,
    QString & mTitle,
    QString & mArtistType,
    QString & mArtistArt,
    QString & mAlbumDate,
    QString & mAlbumReview,
    QString & mAudioPreviewURL
)
{
    rapidxml::file<char> xmlFile(filePath.toLatin1().data());
    rapidxml::xml_document<char> doc;

char *xmldata = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
"<melodis search_id=\"f4a6b14b4bf5c6e28b037b2bf0504d2b_1455956788.5197_external\">"
"<messages>"
"</messages>"
"<tracks_grouped>"
"<track_name_group track_name=\"Just Give Me a Reason\"><artist_name_group artist_name=\"P!nk\"><tracks>"
"<track album_id=\"300991623072419315\" track_id=\"100537198754027486\" artist_id=\"200396414561662823\" artist_name=\"P!nk\" artist_display_name=\"P!nk, Nate Ruess\" album_name=\"Knuffelrock 2013\" album_date=\"\" track_name=\"Just Give Me a Reason\" album_primary_image=\"http://static.midomi.com/s/s/images/000/000/000/000/274/541/42/400_000000000000274541421500x1500_72dpi_RGB_q70.jpg\" audio_preview_url=\"http://www.amazon.com/gp/dmusic/aws/sampleTrack.html?clientid=SoundHound&amp;ASIN=B0099G51W8\" video_url=\"http://api.midomi.com:443/v2/?method=getVideos&amp;artist_name=P%21nk&amp;track_name=Just+Give+Me+a+Reason\" lyrics_provider=\"\" lyrics_url=\"http://www.google.com/m?q=lyrics+Just+Give+Me+a+Reason+P%21nk\" purchase_url=\"http://api.midomi.com:443/v2/?method=getTrackBuyLinks&amp;track_id=100537198754027486&amp;store=amazon\" social_posts_count=\"0\" social_posts_available=\"0\" social_posts_lines=\"1\" omr_fingerprint_id=\"100537198754027486\">"
"<artists>"
"<artist artist_primary_image=\"http://static.midomi.com/a/pic200/drq300/q342/200_q34298v1dik.jpg\" artist_id=\"200396414561662823\" artist_name=\"P!nk\" similar_artist_count=\"30\" has_social_channels=\"1\" has_twitter_social=\"1\" has_facebook_social=\"1\">"
"</artist>"
"<artist artist_primary_image=\"http://static.midomi.com/a/pic200/drq300/q369/200_q36916mdxey.jpg\" artist_id=\"200632986407261342\" artist_name=\"Nate Ruess\" similar_artist_count=\"6\" has_social_channels=\"0\" has_twitter_social=\"0\" has_facebook_social=\"0\">"
"</artist>"
"</artists>"
"<external_links>"
"</external_links>"
"</track>"
"</tracks>"
"</artist_name_group></track_name_group></tracks_grouped>"
"</melodis>\0";


    doc.parse<0>(xmlFile.data());
    xml_node<> * node = doc.first_node("melodis");
    node = node->first_node("tracks_grouped");
    node = node->first_node("track_name_group");
    node = node->first_node("artist_name_group");
    node = node->first_node("tracks");


    mAlbum = "";
    mArtist = "";
    mAlbumArt = "";
    mTitle = "";
    mArtistType = "";
    mArtistArt = "";
    mAlbumDate = "";
    mAlbumReview = "";
    mAudioPreviewURL = "";

    for (xml_node<> *track = node->first_node("track"); track;
            track = track->next_sibling()) {
        /** attribute of track */
        for (xml_attribute<> *attr = track->first_attribute(); attr;
                attr = attr->next_attribute()) {
            //qDebug() << "[" << attr->name() << "] = " << attr->value() << "\r\n";
            if (!COMPARE_NAME("artist_display_name", attr->name()))
            {
                mArtist = QString(attr->value());
            }
            else if (!COMPARE_NAME("album_name", attr->name()))
            {
                mAlbum = QString(attr->value());
            }
            else if (!COMPARE_NAME("album_date", attr->name()))
            {
                mAlbumDate = QString(attr->value());
            }
            else if (!COMPARE_NAME("track_name", attr->name()))
            {
                mTitle = QString(attr->value());
            }
            else if (!COMPARE_NAME("album_primary_image", attr->name()))
            {
                mAlbumArt = QString(attr->value());
            }

        }

        xml_node<> * artists = track->first_node("artists");

        for (xml_node<> *artist = artists->first_node("artist"); artist;
                artist = artist->next_sibling()) {
            /** attribute of artist */
            for (xml_attribute<> *attr = artist->first_attribute(); attr; attr =
                    attr->next_attribute()) {
                if (!COMPARE_NAME("artist_primary_image", attr->name()))
                {
                    mArtistArt = QString(attr->value());
                }
                else if (!COMPARE_NAME("artist_name", attr->name()))
                {
                    mArtist = QString(attr->value());
                }
            }
        }
    }


    return 0;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString mAlbum;
    QString mArtist;
    QString mAlbumArt;
    QString mTitle;
    QString mArtistType;
    QString mArtistArt;
    QString mAlbumDate;
    QString mAlbumReview;
    QString mAudioPreviewURL;

    parserXML(
        "out.xml",
        mAlbum,
        mArtist,
        mAlbumArt,
        mTitle,
        mArtistType,
        mArtistArt,
        mAlbumDate,
        mAlbumReview,
        mAudioPreviewURL
    );

    qDebug() << "mAlbum = " << mAlbum << "\r\n";
    qDebug() << "mArtist = " << mArtist << "\r\n";
    qDebug() << "mAlbumArt = " << mAlbumArt << "\r\n";
    qDebug() << "mTitle = " << mTitle << "\r\n";
    qDebug() << "mArtistType = " << mArtistType << "\r\n";
    qDebug() << "mArtistArt = " << mArtistArt << "\r\n";
    qDebug() << "mAlbumDate = " << mAlbumDate << "\r\n";
    qDebug() << "mAlbumReview = " << mAlbumReview << "\r\n";
    qDebug() << "mAudioPreviewURL = " << mAudioPreviewURL << "\r\n";

    return a.exec();
}
