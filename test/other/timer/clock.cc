#include <iostream>
#include <memory>
#include <vector>
#include <list>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <unordered_map>
#include <thread>
#include <atomic>
#include <functional>
#include <array>
#include <algorithm>
#include <set>
#include <cassert>
#include <sstream>


#define LOGD(format, ...) printf("(D)(%.9f)(%s)(%s:%s:%d)(" format ")\n", timestamp(), to_string(tid()).data(), fileName(__FILE__).data(), __FUNCTION__, __LINE__, ##__VA_ARGS__)

template <typename T>
inline std::string to_string(T val)
{
    std::stringstream ss;
    ss << val;
    return ss.str();
}

inline std::thread::id tid()
{
    return std::this_thread::get_id();
}

inline std::string fileName(const std::string &path)
{
    ssize_t pos = path.find_last_of("/");
    if(pos > 0)
        return path.substr(pos + 1);
    return path;
}

template <typename T=double, typename D=std::ratio<1,1>, typename C=std::chrono::system_clock>
T timestamp(typename C::time_point &&t = C::now())
{
    return std::chrono::duration_cast<std::chrono::duration<T,D>>(std::forward<typename C::time_point>(t).time_since_epoch()).count();
}


template <typename D=std::chrono::duration<double, std::ratio<1>>, typename C=std::chrono::steady_clock>
class Counter
{
public:
    using Clock = C;
    using Duration = D;
    using Type = typename Duration::rep;
    using Ratio = typename Duration::period;
    using Timepoints = std::pair<typename Clock::time_point, typename Clock::time_point>;
    std::list<Timepoints> tps_lst;

    Counter() = default;
    ~Counter() = default;
    Counter(const Timepoints &tps) : tps_lst({tps})
    {}

    void start(typename Clock::time_point tp=Clock::now())
    {
        tps_lst.push_back({tp, tp});
    }

    Duration stop()
    {
        tps_lst.back().second = Clock::now();
        return tps_lst.back().second - tps_lst.back().first;
    }

    Duration elapse() const
    {
        return Duration{Clock::now() - tps_lst.back().first};
    }

    Duration last() const
    {
        return tps_lst.back().second - tps_lst.back().first;
    }

    Duration total() const
    {
        Duration ret;
        for (auto &tps : tps_lst)
        {
            ret += tps.second - tps.first;
        }

        return ret;
    }
}; /// Counter

template <class Cnt=Counter<>>
class Timer
{
private:
    using Clock = typename Cnt::Clock;
    const typename Clock::time_point begin_ = Clock::now();
    std::unordered_map<std::string, Cnt> counters_ = {{"", Cnt{{begin_,begin_}}}};

public:
    Timer() = default;
    ~Timer() = default;
    Timer(const std::set<std::string> &tp_lst)
    {
        for (const auto &tp_id : tp_lst)
            counters_[tp_id].start(begin_);
    }

    Cnt const &get(const std::string tp_id="") const
    {
        return counters_.at(tp_id);
    }

    Cnt &get(const std::string tp_id="")
    {
        return counters_[tp_id];
    }

}; /// Timer

template <class T>
class Guard
{
public:
    Guard(T &t) : t_(t) {t_.start();}
    ~Guard() {t_.stop();}
private:
    T &t_;
}; /// Guard ref

template <class T>
class Guard<T*>
{
public:
    Guard(T *t) : t_(t) {t_->start();}
    ~Guard() {t_->stop();}
private:
    T *t_;
}; /// Guard pointer


int main()
{
    Timer<> timer{};
    const Timer<> &const_timer = timer;
    {
        Guard<Counter<>> guard{timer.get("my timer")};
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    printf("total default: %.9f\n", const_timer.get().total().count());
    printf("last my timer: %.9f\n", const_timer.get("my timer").last().count());
    timer.get("my timer").start();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    printf("stop my timer: %.9f\n", timer.get("my timer").stop().count());
    printf("last my timer: %.9f\n", const_timer.get("my timer").last().count());
    printf("total my timer: %.9f\n", const_timer.get("my timer").total().count());

    printf("stop default: %.9f\n", timer.get().stop().count());
    printf("total default: %.9f\n", const_timer.get().total().count());

    return 0;
}
