#include <ctime>
#include <iostream>
#include <cstdio>

using namespace std;

#define PRINTF 			std::printf

#define LOGI(format,...)	PRINTF("%s[%s][%i] " format "\n", __FILE__, __FUNCTION__, __LINE__,##__VA_ARGS__)

struct MyCounter
{
	MyCounter(char const *name)
	{
		LOGI("%s", name);
		_name = name;
		beg = std::clock();
	}

	~MyCounter()
	{
		LOGI("%s: %ld", _name.c_str(), std::clock() - beg);
	}

	clock_t beg;
	std::string _name;
};

int main(void)
{


	string str = "Aa";
	if(str == "aa")
	{
		LOGI("");
	}

	if(str == "Aa")
	{
		LOGI("");
	}

	if(str == "bb")
	{
		LOGI("");
	}

	if(str != "bb")
	{
		LOGI("");
	}

	str="";
	char *a = &str[0];
	if(a)
	{
		LOGI("%s", a);
	}
	else
	{
		LOGI("");
	}

	return 0;

	clock_t begin = clock();
	static MyCounter *cnt = new MyCounter("COUNTER");
	// clock_t val;
	while(true)
	{
		// val = clock() - begin;
		if((clock() - begin) * 1000 / CLOCKS_PER_SEC >= 1000)
		{
			delete cnt;
		// 	begin = clock();
			cnt = new MyCounter("COUNTER");
			begin = clock();
		}
	}
	return 0;
}
