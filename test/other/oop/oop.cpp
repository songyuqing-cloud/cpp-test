#define LOG_EN

#include "../../../utils/Utils"

using namespace std;

static int eiisnan (short unsigned int *x)
{
  int i;

  if( x[i] != 0 )
    return(1);
}

static int eiisinf (unsigned short *x)
{
  if (eiisnan (x))
    return (0);

  if ((x[1] & 0x7fff) == 0x7fff)
    return (1);
}

static void toe64(short unsigned int *a, short unsigned int *b)
{
  register unsigned short *p, *q;
  unsigned short i;

  q = b + 4;

  if (eiisinf (a));

  for( i=0; i<4; i++ )
    *q-- = *p++;
}

static int asctoeg(short unsigned int *y, int oprec)
{
  unsigned short yy[13];
  char *s;

  while( *s == ' ' )
    ++s;

  toe64( yy, y );
}

long double _strtold (char *s, char **se)
{
  long double x;
  asctoeg( (unsigned short *)&x, 64 );
}


#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

int main(void)
{
    // parsing with error handling
    const char *p = "111.11 -2.22 Nan nan(2) inF 0X1.BC70A3D70A3D7P+6  1.18973e+4932zzz";
    printf("Parsing '%s':\n", p);
    char *end;
    for (double f = strtod(p, &end); p != end; f = strtod(p, &end))
    {
        printf("'%.*s' -> ", (int)(end-p), p);
        p = end;
        if (errno == ERANGE){
            printf("range error, got ");
            errno = 0;
        }
        printf("%f\n", f);
    }

    // parsing without error handling
    printf("\"  -0.0000000123junk\"  -->  %g\n", strtod("  -0.0000000123junk", NULL));
    printf("\"junk\"                 -->  %g\n", strtod("junk", NULL));
}