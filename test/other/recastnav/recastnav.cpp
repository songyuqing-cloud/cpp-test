#include <cstdio>
#include <iostream>

inline void rcVcopy(float* dest, const float* v)
{
    dest[0] = v[0];
    dest[1] = v[1];
    dest[2] = v[2];
}

void dump(float *p, int size1, int size2, int index=0)
{
    for(int i=index; i<size1; i++)
    {
        for(int j=0; j<size2; j++)
            printf("%.2f ", p[i*size2 + j]);
        printf(";");
    }
    printf("\n");
}

static void dividePoly(const float* in, int nin,
                      float* out1, int* nout1,
                      float* out2, int* nout2,
                      float x, int axis)
{
    float d[12];
    for (int i = 0; i < nin; ++i)
        d[i] = x - in[i*3+axis];

    // dump(d, 3, 1, 0);

    int m = 0, n = 0;
    for (int i = 0, j = nin-1; i < nin; j=i, ++i)
    {
        bool ina = d[j] >= 0;
        bool inb = d[i] >= 0;

        printf("\n[i,j] %d:%d\n",i,j);
        printf("    != %.2f %.2f : %d %d\n", d[j], d[i], ina, inb);

        if (ina != inb)
        {
            float s = d[j] / (d[j] - d[i]);
            out1[m*3+0] = in[j*3+0] + (in[i*3+0] - in[j*3+0])*s;
            out1[m*3+1] = in[j*3+1] + (in[i*3+1] - in[j*3+1])*s;
            out1[m*3+2] = in[j*3+2] + (in[i*3+2] - in[j*3+2])*s;


            dump(out1, m+1, 3, m);

            rcVcopy(out2 + n*3, out1 + m*3);
            n++;
            m++;
            // add the i'th point to the right polygon. Do NOT add points that are on the dividing line
            // since these were already added above
            if (d[i] > 0)
            {
                rcVcopy(out1 + m*3, in + i*3);
                m++;
            }
            else if (d[i] < 0)
            {
                rcVcopy(out2 + n*3, in + i*3);
                n++;
            }
        }
        else if(d[i] != 0) // same side
        {
            // add the i'th point to the right polygon. Addition is done even for points on the dividing line
            if (d[i] > 0)
            {
                rcVcopy(out1 + m*3, in + i*3);
                m++;
            }
            rcVcopy(out2 + n*3, in + i*3);
            n++;
        }
    }

    *nout1 = m;
    *nout2 = n;
}



int main(int argc, char const *argv[])
{
    float buf[7*3*4];
    float *in = buf, *inrow = buf+7*3, *p1 = inrow+7*3, *p2 = p1+7*3;
    int nvrow, nvIn = 3;

    // v0
    in[0] = 0, in[1] = 0, in[2] = 0;
    in[3] = 1, in[4] = 0, in[5] = 1;
    in[6] = 1, in[7] = 0, in[8] = -1;

    dividePoly(in, nvIn, inrow, &nvrow, p1, &nvIn, -1, 2);
    dump(inrow, nvrow, 3, 0);
    dump(p1, nvIn, 3, 0);

    return 0;
}