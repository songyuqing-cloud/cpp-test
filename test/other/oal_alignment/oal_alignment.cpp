#define LOG_EN
#include "../../../utils/Utils"

static const uint32_t CACHELINE_SIZE = 0x1000;
static const uint32_t PAGE_SIZE = 0x1000;
static const uint32_t MINIMUM_ALIGN = 0x1000;

#define ALIGN2_CACHELINE                     0x00000100           ///< Memory is aligned on a cache line
#define ALIGN2_PAGE                          0x00000200           ///< Memory is aligned on a page
#define BYTE_N                               0x00000400
#define ALIGN2_BYTE(N)                       (BYTE_N | (N << 16)) ///< Memory is aligned on an N-byte boundary
#define OAL_MEMORY_FLAG_CONTIGUOUS           0x01                 ///< Memory block is physically contiguous
#define OAL_MEMORY_FLAG_NONSWAPABLE          0x02                 ///< Memory allocated will never be swapped
#define OAL_MEMORY_FLAG_ZERO                 0x04                 ///< Memory is initialized with value zero
#define OAL_MEMORY_FLAG_ALIGN(ALIGN2)        (0x08 | (ALIGN2))    ///< Memory is aligned as specified

uint32_t AlignmentGet(uint32_t aFlags)
{
  uint32_t ret = 0;
  _LOGD("0x%08lx", aFlags);
  if ((aFlags & OAL_MEMORY_FLAG_ALIGN(ALIGN2_CACHELINE)) ==
        OAL_MEMORY_FLAG_ALIGN(ALIGN2_CACHELINE))
  {
    _LOGD("0x%08lx", aFlags);
    ret = CACHELINE_SIZE;
  }
  else if ((aFlags & OAL_MEMORY_FLAG_ALIGN(ALIGN2_PAGE)) ==
      OAL_MEMORY_FLAG_ALIGN(ALIGN2_PAGE))
  {
    _LOGD("0x%08lx", aFlags);
    ret = PAGE_SIZE;
  }
  else if ((aFlags & OAL_MEMORY_FLAG_ALIGN(BYTE_N)) ==
      OAL_MEMORY_FLAG_ALIGN(BYTE_N))
  {
    uint32_t alignment = (uint32_t)((aFlags & 0x00FF0000) >> 16);
    _LOGD("0x%08lx", alignment);
    ret = 1 << alignment;
  }

  return ret;
}

int main()
{
    uint32_t _l_flag_alignment = AlignmentGet(OAL_MEMORY_FLAG_ALIGN(ALIGN2_PAGE));
    _LOGD("0x%08lx", _l_flag_alignment);
    _l_flag_alignment = AlignmentGet(OAL_MEMORY_FLAG_ALIGN(ALIGN2_CACHELINE));
    _LOGD("0x%08lx", _l_flag_alignment);
    _l_flag_alignment = AlignmentGet(OAL_MEMORY_FLAG_ALIGN(BYTE_N));
    _LOGD("0x%08lx", _l_flag_alignment);
    _l_flag_alignment = AlignmentGet(OAL_MEMORY_FLAG_ALIGN(ALIGN2_BYTE(12)));
    _LOGD("0x%08lx", _l_flag_alignment);

    return 0;
}