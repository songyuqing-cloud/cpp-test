#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>

#include "suitesparse/cholmod.h"

#ifdef USE_GPU
#define CHOLMOD(__name) cholmod_l_##__name
#else
#define CHOLMOD(__name) cholmod_##__name
#endif

int main (void)
{
    struct timeval t1, t2;
    double elapsedTime;

    // printf("%ld %ld %ld\n\n", sizeof(int), sizeof(long int), sizeof(long));

    const char* matFile = "/mnt/GeneralData/workspace/vSLAM/res/nd6k.mtx";
    FILE* fp = fopen(matFile, "r");
    assert(fp != NULL);

    cholmod_sparse *A ;
    cholmod_dense *x, *b;
    cholmod_factor *L ;
    cholmod_common common;
    cholmod_common* c = &common;
    CHOLMOD(start) (c) ; /* start CHOLMOD */
    c->useGPU = 1;
    c->supernodal = CHOLMOD_SUPERNODAL;

    A = CHOLMOD(read_sparse) (fp, c) ; /* read in a matrix */
    CHOLMOD(print_sparse) (A, "A", c) ; /* print the matrix */
    fclose(fp);

    if (A == NULL || A->stype == 0) /* A must be symmetric */
    {
        CHOLMOD(free_sparse) (&A, c) ;
        CHOLMOD(finish) (c) ;
        return (0) ;
    }

    b = CHOLMOD(ones) (A->nrow, 1, A->xtype, c) ; /* b = ones(n,1) */

    gettimeofday(&t1, NULL);
    L = CHOLMOD(analyze) (A, c) ; /* analyze */
    CHOLMOD(factorize) (A, L, c) ; /* factorize */
    x = CHOLMOD(solve) (CHOLMOD_A, L, b, c) ; /* solve Ax=b */
    gettimeofday(&t2, NULL);
    elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
    printf("Time: %.4f ms\n", elapsedTime);
    CHOLMOD(gpu_stats)(c);
    CHOLMOD(free_factor) (&L, c) ; /* free matrices */
    CHOLMOD(free_sparse) (&A, c) ;
    CHOLMOD(free_dense) (&x, c) ;
    CHOLMOD(free_dense) (&b, c) ;
    CHOLMOD(finish) (c) ; /* finish CHOLMOD */

    // free(c);
    return (0) ;
}
