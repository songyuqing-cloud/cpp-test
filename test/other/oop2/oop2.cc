#include <cstdio>
#include <typeinfo>
#define LOGD(format, ...) printf("%s(%s:%d)" format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)

struct A;
struct Aimpl;
struct B;

struct A
{
	A(Aimpl *aimpl);
	virtual ~A();
	Aimpl *aimpl_;
};

struct Aimpl
{
	Aimpl(A *a);
	~Aimpl();
	A *a_;
};

struct B : public A
{
	B();
	virtual ~B();
};

A::A(Aimpl *aimpl) : aimpl_(aimpl) {}
A::~A() 
{
	LOGD("%p %p %d %s", aimpl_, aimpl_->a_, !dynamic_cast<B*>(aimpl_->a_), typeid(*(aimpl_->a_)).name());
	delete aimpl_;
}

Aimpl::Aimpl(A *a) : a_(a) {}
Aimpl::~Aimpl()
	{LOGD("[OOPS] %p %s", a_, typeid(*a_).name());}

B::B() : A(new Aimpl(this))
	{LOGD("%p %p %s", this, aimpl_->a_, typeid(*(this)).name());}
B::~B()
{
	LOGD("%p %p %d %s", aimpl_, aimpl_->a_, !dynamic_cast<B*>(aimpl_->a_), typeid(*(aimpl_->a_)).name());
}

int main(int argc, char const *argv[])
{
	LOGD("");
	auto obj = new B();
	LOGD("%s", typeid(*(obj)).name());
	LOGD("%s", typeid(*(obj->aimpl_->a_)).name());
	delete obj;
	return 0;
}