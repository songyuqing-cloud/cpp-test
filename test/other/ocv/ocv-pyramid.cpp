
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>

#include <iostream>
#include "opencv2/opencv.hpp"
// #include "opencv2/core/cuda.hpp"
// using namespace std;
// #include "opencv2/gpu/gpu.hpp"
// #include "opencv2/imgproc/imgproc.hpp"
// #include "opencv2/highgui/highgui.hpp"

using namespace cv;
#define HAVE_OPENCV_GPU 1

void createLaplacePyrGpu(const Mat &img, int levels, std::vector<Mat> &pyr)
{
 #ifdef HAVE_OPENCV_GPU

    pyr.resize(levels + 1);

    std::vector<cuda::GpuMat> gpu_pyr(levels + 1);

    gpu_pyr[0].upload(img);
    for (int i = 0; i < levels; ++i){
        cuda::pyrDown(gpu_pyr[i], gpu_pyr[i + 1]);
    }


    // cuda::GpuMat tmp,tmp2;
    // for (int i = 0; i < levels; ++i)
    // {
    //     cuda::pyrUp(gpu_pyr[i + 1], tmp);

    //     cuda::subtract(gpu_pyr[i], tmp, gpu_pyr[i]);

    //     gpu_pyr[i].download(pyr[i]);
    // }

    gpu_pyr[0].download(pyr[0]);
    gpu_pyr[1].download(pyr[1]);
    gpu_pyr[levels].download(pyr[levels]);
#else
    (void)img;
    (void)levels;
    (void)pyr;
#endif
}
int main( int argc, char** argv )
{

  /// General instructions
  printf( "\n Zoom In-Out demo  \n " );
  printf( "------------------ \n" );
  printf( " * [+] -> Zoom in  \n" );
  printf( " * [-] -> Zoom out \n" );
  printf( " * [ESC] -> Close program \n \n" );

  char* window_name_1 = "Pyramids Demo";
  char* window_name_2 = "Resize Demo";

  cv::Mat src, dst_pyr, tmp_pyr;
  cv::Mat dst_resize, tmp_resize;

  /// Test image - Make sure it s divisible by 2^{n}
  src = cv::imread( argv[1] );
  if( !src.data )
    { printf(" No data! -- Exiting the program \n");
      return -1; }

int levels = 2;

int width = src.cols;
int height = src.rows;

int dx = ((1 << levels) - width % (1 << levels)) % (1 << levels);
int dy = ((1 << levels) - height % (1 << levels)) % (1 << levels);

int border_top = 19;
int border_left = 19;
int border_bottom = 19;
int border_right = 19;

cv::copyMakeBorder(src, dst_pyr, border_top, border_bottom, border_left, border_right, BORDER_REFLECT + BORDER_ISOLATED);

std::vector<cv::Mat> pyrs;

  cv::imshow( "src", src );
  cv::imshow( "dst_pyr", dst_pyr );

// createLaplacePyrGpu(dst_pyr, levels, pyrs);

//   cv::imshow( "pyrs[0]", pyrs[0] );
//   cv::imshow( "pyrs[1]", pyrs[1] );
//   cv::imshow( "pyrs[2]", pyrs[2] );

  while(cv::waitKey(10) != 27);

  // tmp_pyr = src;
  // dst_pyr = tmp_pyr;

  // dst_resize = src;
  // tmp_resize = dst_resize;

  // /// Create window
  // cv::namedWindow( window_name_1, CV_WINDOW_AUTOSIZE );
  // cv::imshow( window_name_1, dst_pyr );

  // cv::namedWindow( window_name_2, CV_WINDOW_AUTOSIZE );
  // cv::imshow( window_name_2, dst_pyr );

  // double t1, t2;

  // /// Loop
  // while( true )
  // {
  //   int c;
  //   c = cv::waitKey(10);

  //   if( (char)c == 27 )
  //     { break; }
  //   if( (char)c == '+' )
  //     { 
  //     t1 = (double)getTickCount();
  //     cv::pyrUp( tmp_pyr, dst_pyr, Size( tmp_pyr.cols*2, tmp_pyr.rows*2 ) );
  //     t1 = 1000*((double)getTickCount()-t1) / getTickFrequency() ;
  //     t2 = (double)getTickCount();
  //     cv::resize( tmp_resize, dst_resize, Size( tmp_resize.cols*2, tmp_resize.rows*2 ) );
  //     t2 = 1000*((double)getTickCount()-t2) / getTickFrequency() ;
  //     printf( "** Zoom In: Image x 2 \n" );
  //     std::cout << "pyrUp: " << t1 << "ms" << std::endl;
  //     std::cout << "resize up: " << t2 << "ms" << std::endl;
  //      std::cout << std::endl;
  //     }
  //   else if( (char)c == '-' )
  //    { 
  //      t1 = (double)getTickCount();
  //      cv::pyrDown( tmp_pyr, dst_pyr, Size( tmp_pyr.cols/2, tmp_pyr.rows/2 ) );
  //      t1 = 1000*((double)getTickCount()-t1) / getTickFrequency() ;
  //      t2 = (double)getTickCount();
  //      cv::resize( tmp_resize, dst_resize, Size( tmp_resize.cols/2, tmp_resize.rows/2 ) );
  //      t2 = 1000*((double)getTickCount()-t2) / getTickFrequency() ;

  //      printf( "** Zoom Out: Image / 2 \n" );
  //      std::cout << "pyrDown: " << t1 << "ms" << std::endl;
  //      std::cout << "resize down: " << t2 << "ms" << std::endl;
  //      std::cout << std::endl;
  //    }

  //   cv::imshow( window_name_1, dst_pyr );
  //   cv::imshow( window_name_2, dst_resize );
  //   tmp_pyr = dst_pyr;
  //   tmp_resize = dst_resize;
  // }
  return 0;
}