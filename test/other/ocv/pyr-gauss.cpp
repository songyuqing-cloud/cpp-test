#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>

#include <iostream>
#include "opencv2/opencv.hpp"
#include <string>
#include <chrono>
#include <unordered_map>
#include <thread>
#include <sstream>
#include "MyCounter.hpp"
#include "ORBextractor.h"

std::unordered_map<std::thread::id, int> custom::MyCounter::indent;



const int nFeatures = 1000;
const float fScaleFactor = 1.2;
const int nLevels = 8;
const float fIniThFAST = 20;
const float fMinThFAST = 7;


cv::Mat gpu_gauss_resize(cv::Mat &a_frame, float a_scale, std::vector<cv::KeyPoint> &a_keypoints)
{ MY_COUNTER();
	cv::Mat ret;
	cv::cuda::GpuMat inputDevice;
	cv::cuda::GpuMat outputDevice;
	{MY_COUNTER();
		inputDevice.create(a_frame.size(), a_frame.type());
		outputDevice.create(a_frame.size(), a_frame.type());
	}
	{MY_COUNTER();
	inputDevice.upload(a_frame);
	}
	{MY_COUNTER();
	cv::cuda::resize(inputDevice, outputDevice, cv::Size(), a_scale, a_scale, CV_INTER_CUBIC);
	cv::Ptr<cv::cuda::Filter> gauss = cv::cuda::createGaussianFilter(inputDevice.type(), outputDevice.type(), cv::Size(7, 7), 2, 2, cv::BORDER_REFLECT_101);
	gauss->apply(outputDevice, outputDevice);
	cv::Ptr<cv::cuda::FastFeatureDetector> detector = cv::cuda::FastFeatureDetector::create(20,true);
	// cv::Ptr<cv::cuda::ORB> detector = cv::cuda::ORB::create();
	detector->detect(outputDevice, a_keypoints);
	}
	{MY_COUNTER();
	outputDevice.download(ret);
	}
	return ret;
}

cv::Mat cpu_gauss_resize(cv::Mat &a_frame, float a_scale, std::vector<cv::KeyPoint> &a_keypoints)
{ MY_COUNTER();
	cv::Mat ret(a_frame.size(), a_frame.type());
	cv::resize(a_frame, ret, cv::Size(), a_scale, a_scale, CV_INTER_CUBIC);
	cv::GaussianBlur(ret, ret, cv::Size(7, 7), 2, 2, cv::BORDER_REFLECT_101);
	// vector<cv::KeyPoint> vKeysCell;
    FAST(ret, a_keypoints, 20,true);

	return ret;
}

void orb_slam2_extractor(cv::Mat &im, std::vector<cv::KeyPoint> &mvKeys)
{ MY_COUNTER();
	cv::Mat mDescriptors;
	// cv::Mat im;

	ORB_SLAM2::ORBextractor *mpORBextractorLeft = new ORB_SLAM2::ORBextractor(nFeatures,fScaleFactor,nLevels,fIniThFAST,fMinThFAST);
	(*mpORBextractorLeft)(im,cv::Mat(),mvKeys,mDescriptors);
	

}


int main(int argc, char const *argv[])
{
	cv::Mat frame = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);
	std::vector<cv::KeyPoint> keypoints;

	// { 
	// 	cv::Mat output = cpu_gauss_resize(frame, 1.0, keypoints);
	// 	cv::drawKeypoints(output, keypoints, output);
	// 	cv::imshow("cpu", output);
	// }

	orb_slam2_extractor(frame, keypoints);

	// { 
	// 	keypoints.reserve(0x1000);
	// 	cv::Mat output = gpu_gauss_resize(frame, 1.0, keypoints);
	// 	// for(auto &kp : keypoints)
	// 	// 	_LOGD("%f", kp.angle);
	// 	cv::drawKeypoints(output, keypoints, output);
	// 	cv::imshow("gpu", output);
	// }



	while(cv::waitKey(10) != 27);

	return 0;
}