#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>

#include <iostream>
#include "../../../utils/MyCounter.h"
#include "opencv2/opencv.hpp"

std::unordered_map<std::thread::id, int> custom::MyCounter::indent;

int main(int argc, char const *argv[])
{
	/* code */
	cv::Mat frame = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);

	std::vector<cv::KeyPoint> keypoints;
	cv::cuda::GpuMat d_img;
	d_img.upload(frame);

	cv::Ptr<cv::cuda::ORB> detector = cv::cuda::ORB::create();
	detector->detect(d_img, keypoints);
	cv::drawKeypoints(frame, keypoints, frame);

	cv::imshow("", frame);

	while(cv::waitKey(10) != 27);
	return 0;
}

