
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>

#include <iostream>
#include "opencv2/opencv.hpp"

int main(int argc, char const *argv[])
{
	cv::Mat _a(10,10,CV_8U);

	cv::Mat _b = _a.rowRange(0,5).colRange(0,5);

	_b.at<uint8_t>(4,4) = 255;

	cv::imshow( "A", _a );
	cv::imshow( "B", _b );
	while(cv::waitKey(10) != 27);

	return 0;
}