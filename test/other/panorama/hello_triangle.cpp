#include <iostream>
#include <vector>
#include <chrono>
#include <thread>

#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#if !(defined USE_EIGEN)
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#else
#include <Eigen/Dense>
#endif

#include <stb_image.h>
#include <learnopengl/filesystem.h>
#include <learnopengl/shader.h>
#include <learnopengl/camera.h>

#define _LOGI(format, ...) printf("%s %s [%d][I]'" format "'\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#define _LOGD(format, ...) printf("%s %s [%d][D]'" format "'\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )


const double M_PI = 3.1415926535897932384626433832795;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

const std::string vertexShaderSource = "\n"
    "// precision mediump float;\n"
    "attribute vec3 aPos;\n"
    "attribute vec2 aTexCoord;\n"
    "varying vec2 TexCoord;\n"
    "uniform mat4 MVP;\n"
    "void main()\n"
    "{\n"
    "   TexCoord = aTexCoord;\n"
    "   gl_Position = MVP * vec4(aPos, 1.0);\n"
    "   // gl_Position = vec4(aPos, 1.0);\n"
    "}\0";

const std::string fragmentShaderSource = "\n"
    "// precision mediump float;\n"
    "varying vec2 TexCoord;\n"
    "uniform sampler2D texture;\n"
    "void main()\n"
    "{\n"
    "   gl_FragColor = texture2D(texture, TexCoord);\n"
    "   // gl_FragColor = vec4(1.0, 0.5, 0.2, 1.0);\n"
    "}\n\0";

int sphereMapping(uint32_t aRows, uint32_t aCols, float aRad, std::vector<float> &aVertices, std::vector<uint32_t> &aIndicies)
{
    assert(aRows > 1);
    assert(aCols > 2);

    int lRet = 0;
    float deltaAlpha = (2.0 * M_PI) / aCols;
    float deltaBeta = M_PI / (aRows);

    const uint32_t vert_num = (aCols * 2) + (aRows - 1) * (aCols + 1);
    _LOGD("vert_num: %d", vert_num);

    aVertices.reserve(vert_num * 5);

    // top
    for(uint32_t col=0; col<aCols; col++)
    {
        float x = 0;
        float y = aRad;
        float z = 0;
        float u = (static_cast<float>(col) / static_cast<float>(aCols)) + (0.5f / aCols);
        float v = 1;

        aVertices.push_back(x);
        aVertices.push_back(y);
        aVertices.push_back(z);
        aVertices.push_back(u);
        aVertices.push_back(v);
    }

    for(uint32_t row=1; row < (aRows); row++)
    {
        float beta = row * deltaBeta;
        float y = aRad * cos(beta);
        float tv = 1 - static_cast<float>(row) / static_cast<float>(aRows);

        for(uint32_t col=0; col < aCols + 1; col++)
        {
            float alpha = col * deltaAlpha;
            float x = aRad * sin(beta) * cos(alpha);
            float z = aRad * sin(beta) * sin(alpha);

            float tu = (static_cast<float>(col) / static_cast<float>(aCols));
            // _LOGD("%.2f", tu);
            // aVertices.push_back(glm::vec3(x,y,z));
            // aTexCoords.push_back(glm::vec2(tu, tv));
            aVertices.push_back(x);
            aVertices.push_back(y);
            aVertices.push_back(z);
            aVertices.push_back(tu);
            aVertices.push_back(tv);
        }
    }

    // bottom
    for(uint32_t col=0; col<aCols; col++)
    {
        float x = 0;
        float y = aRad * (-1);
        float z = 0;
        float u = (static_cast<float>(col) / static_cast<float>(aCols)) + (0.5f / aCols);
        float v = 0;

        aVertices.push_back(x);
        aVertices.push_back(y);
        aVertices.push_back(z);
        aVertices.push_back(u);
        aVertices.push_back(v);
    }

    // indicies
    const uint32_t index_num = (aCols * 2) * (aRows - 1);
    _LOGD("index_num: %d", index_num);
    aIndicies.reserve(index_num * 3);
    // top
    for (uint32_t col = 0; col < aCols; col++)
    {
        uint32_t const tm = col;
        uint32_t const bl = aCols + col;
        uint32_t const br = aCols + col + 1;

        aIndicies.push_back(tm);
        aIndicies.push_back(bl);
        aIndicies.push_back(br);
    }

    for (uint32_t row=0; row < (aRows - 2); ++row)
    {
        for (uint32_t col = 0; col < aCols; col++)
        {
            uint32_t const idx = aCols + ((aCols + 1) * (row));
            uint32_t const tl = idx + col;
            uint32_t const bl = idx + (aCols + 1) + col;
            uint32_t const br = idx + (aCols + 1) + col + 1;
            uint32_t const tr = idx + col + 1;

            aIndicies.push_back(tl);
            aIndicies.push_back(bl);
            aIndicies.push_back(br);

            aIndicies.push_back(tl);
            aIndicies.push_back(tr);
            aIndicies.push_back(br);
        }
    }

    // bottom
    for (uint32_t col = 0; col < aCols; col++)
    {
        uint32_t const last_index = vert_num - (aCols * 2) - 1;
        uint32_t const tl = col;
        uint32_t const tr = col + 1;
        uint32_t const bm = aCols + 1 + col;

        aIndicies.push_back(last_index + tl);
        aIndicies.push_back(last_index + tr);
        aIndicies.push_back(last_index + bm);
    }


    // for(int i=0; i<aIndicies.size(); i+=3)
    // {
    //     _LOGD("%d, %d, %d", aIndicies[i],
    //       aIndicies[i + 1],
    //       aIndicies[i + 2]);
    // }

    return lRet;
}

int cylinderMapping(float aAspect, uint32_t aCols, float aHeight, std::vector<float> &aVertices, std::vector<uint32_t> &aIndicies)
{
    assert(aAspect > 0);
    assert(aCols > 2);

    int lRet = 0;
    float deltaAlpha = (2.0 * M_PI) / aCols;
    // float deltaBeta = PI / (aRows - 1);

    const uint32_t vert_num = (aCols + 1) * 2;
    _LOGD("vert_num: %d", vert_num);
    aVertices.reserve(vert_num * 5);
    const uint32_t index_num = aCols * 2;
    _LOGD("index_num: %d", index_num);
    aIndicies.reserve(index_num * 3);

    // for(uint32_t row=0; row < aRows; row++)
    {

        float const circuit = aHeight * aAspect;
        float const rad = circuit / M_PI / 2;
        _LOGD("circuit: %.4f, rad: %.4f", circuit, rad);

        for(uint32_t col=0; col < aCols + 1; col++)
        {
            float alpha = col * deltaAlpha;
            float x = rad * cos(alpha);
            float z = rad * sin(alpha);

            float tu = static_cast<float>(col) / static_cast<float>(aCols);
            // _LOGD("%.2f", tu);

            // top
            aVertices.push_back(x);
            aVertices.push_back(aHeight / 2);
            aVertices.push_back(z);
            aVertices.push_back(tu);
            aVertices.push_back(1);

            // bot
            aVertices.push_back(x);
            aVertices.push_back(aHeight / (-2));
            aVertices.push_back(z);
            aVertices.push_back(tu);
            aVertices.push_back(0);

            if(col > 0)
            {
                uint32_t tl = (col - 1) * 2;
                uint32_t bl = (col - 1) * 2 + 1;
                uint32_t br = col * 2 + 1;
                uint32_t tr = col * 2;

                aIndicies.push_back(tl);
                aIndicies.push_back(bl);
                aIndicies.push_back(br);

                aIndicies.push_back(tl);
                aIndicies.push_back(tr);
                aIndicies.push_back(br);

                // _LOGD("%d, %d, %d : %d, %d, %d", aIndicies[aIndicies.size() - 6],
                //   aIndicies[aIndicies.size() - 5],
                //   aIndicies[aIndicies.size() - 4],
                //   aIndicies[aIndicies.size() - 3],
                //   aIndicies[aIndicies.size() - 2],
                //   aIndicies[aIndicies.size() - 1]);
            }

        }
    }

    // for (uint32_t row = 1; row < aRows; ++row)
    // {
    //     for (uint32_t col = 0; col < aCols; col++)
    //     {
    //         float const tl = (aCols + 1) * (row - 1) + col;
    //         float const bl = (aCols + 1) * row + col;
    //         float const br = (aCols + 1) * (row) + col + 1;
    //         float const tr = (aCols + 1) * (row - 1) + col + 1;

    //         aIndicies.push_back(tl);
    //         aIndicies.push_back(bl);
    //         aIndicies.push_back(br);

    //         aIndicies.push_back(tl);
    //         aIndicies.push_back(tr);
    //         aIndicies.push_back(br);

    //         _LOGD("%d, %d, %d : %d, %d, %d", aIndicies[aIndicies.size() - 6],
    //               aIndicies[aIndicies.size() - 5],
    //               aIndicies[aIndicies.size() - 4],
    //               aIndicies[aIndicies.size() - 3],
    //               aIndicies[aIndicies.size() - 2],
    //               aIndicies[aIndicies.size() - 1]);

    //     }
    // }

    return lRet;
}

int prog(std::string const &aVsPlainSrc, std::string const &aFsPlainSrc)
{
    // build and compile our shader program
    // ------------------------------------
    // vertex shader

    char const *lVs = aVsPlainSrc.data();
    char const *lFs = aFsPlainSrc.data();

    int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &lVs, NULL);
    glCompileShader(vertexShader);
    // check for shader compile errors
    int success;
    const uint32_t LOG_SIZE = 1024;
    char infoLog[LOG_SIZE];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, LOG_SIZE, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // fragment shader
    int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &lFs, NULL);
    glCompileShader(fragmentShader);
    // check for shader compile errors
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, LOG_SIZE, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // link shaders
    int shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    // check for linking errors
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, LOG_SIZE, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return shaderProgram;
}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    // glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    // glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    // glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    // glfwSetCursorPosCallback(window, mouse_callback);
    // glfwSetScrollCallback(window, scroll_callback);

    // tell GLFW to capture our mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glEnable(GL_DEPTH_TEST);
    // glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    // glEnable(GL_CULL_FACE);

    int shaderProgram = prog(vertexShaderSource, fragmentShaderSource);





    // _LOGD("");




    // SPHERE
    std::vector<float> vVertices;
    std::vector<uint32_t> vIndices;

    // sphereMapping(90, 90, 1, vVertices, vIndices);
    cylinderMapping(1, 32, 2.f, vVertices, vIndices);

    _LOGD("%d %d", vVertices.size() / 5, vIndices.size() / 3);

    // for(int i=0; i<vVertices.size(); i+=5)
    // {
    //     _LOGD("%.2f %.2f %.2f %.2f %.2f", vVertices[i], vVertices[i+1], vVertices[i+2], vVertices[i+3], vVertices[i+4]);
    // }

    // for(auto elem : vTextcoords)
    // {
    //     _LOGD("%.2f %.2f", elem.x, elem.y);
    // }










    // _LOGD("");
    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    // std::vector<float> vertices = {
    //     -0.5f,0.5f,0, 0,1.0,
    //     0.5f,0.5f,0, 1.0,1.0,
    //     0.5f,-0.5f,0, 1.0,0,
    //     -0.5f,-0.5f,0, 0,0,
    // };
    // std::vector<uint32_t> indices = {  // note that we start from 0!
    //     0, 1, 3,  // first Triangle
    //     1, 2, 3   // second Triangle
    // };

    int alPos = glGetAttribLocation(shaderProgram, "aPos");
    int alTexCoord = glGetAttribLocation(shaderProgram, "aTexCoord");
    _LOGD("alPos: %d", alPos);
    _LOGD("alTexCoord: %d", alTexCoord);

    unsigned int VBO, EBO;
    glGenBuffers(1, &VBO);

    // vVertices.clear();
    // vVertices = {
    //   -1,-1, 0,
    //   -1, 1, 0,
    //    1, 1, 0,
    // };

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vVertices.size() * sizeof(float), vVertices.data(), GL_STATIC_DRAW);
    // glVertexAttribPointer(alPos, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    // glEnableVertexAttribArray(alPos);

    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, vIndices.size() * sizeof(uint32_t), vIndices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // unsigned int rboDepth;
    // glGenRenderbuffers(1, &rboDepth);
    // glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
    // glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, SCR_WIDTH, SCR_HEIGHT);
    // glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);
    // glDrawBuffers(0,0);



    // _LOGD("");


    // load and create a texture
    // -------------------------
    unsigned int texture1;
    glGenTextures(1, &texture1);
    glBindTexture(GL_TEXTURE_2D, texture1);
    // set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // load image, create texture and generate mipmaps
    int width, height, nrChannels;
    unsigned char *data;
    stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
    data = stbi_load(FileSystem::getPath("resources/textures/grid.jpg").c_str(), &width, &height, &nrChannels, 0);

    // if(0)
    {
        // std::ifstream ifs("earth.dat", std::ios::binary);
        // width = 1280;
        // height = 640;
        // nrChannels = 3;
        // data = new unsigned char[width * height * nrChannels];
        // ifs.read((char *)data, width * height * nrChannels);
    }

    if (data)
    {
        // _LOGI("");
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        // std::ofstream ofs("earth.dat", std::ios::binary);
        // ofs.write((char const *)data, width * height * nrChannels);

        _LOGI("%d %d %d", width, height, nrChannels);
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    // stbi_image_free(data);


    // _LOGD("");


    glUseProgram(shaderProgram);
        int textID = glGetUniformLocation(shaderProgram, "texture");
        _LOGD("textID: %d", textID);
        glUniform1i(textID, 0);

    // CAMERA
    float fov = 60.f;

#if !(defined USE_EIGEN)
    glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f, 3.0);
    glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f, 0.0f);

    glm::mat4 projection = glm::perspective(glm::radians(fov), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
    glm::mat4 view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);

    glm::mat4 model;
    // glm::mat4 MVP = projection * view * model;
    // glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "MVP"), 1, GL_FALSE, &MVP[0][0]);
#else
    using namespace Eigen;

    typedef Eigen::Matrix<float,4,4> Matrix4;
    typedef Eigen::Matrix<float,3,1> Vector3;

    Vector3 cameraPos(0,0,3);
    Vector3 cameraFront(0,0,-1);
    Vector3 cameraUp(0,1,0);

    // Projection
    float aspect = (float)SCR_WIDTH / (float)SCR_HEIGHT;
    float zFar = 100.f;
    float zNear = 0.1f;

    Transform<float,3,Projective> tr;
    tr.matrix().setZero();
    float radf = M_PI * fov / 180.0;
    float tan_half_fovy = std::tan(radf / 2.0);
    tr(0,0) = 1.0 / (aspect * tan_half_fovy);
    tr(1,1) = 1.0 / (tan_half_fovy);
    tr(2,2) = - (zFar + zNear) / (zFar - zNear);
    tr(3,2) = - 1.0;
    tr(2,3) = - (2.0 * zFar * zNear) / (zFar - zNear);

    Matrix4 projection = tr.matrix();

    // View
    Vector3 f = (cameraFront).normalized();
    Vector3 u = cameraUp.normalized();
    Vector3 s = f.cross(u).normalized();
    u = s.cross(f);
    Matrix4 view = Matrix4::Zero();
    view(0,0) = s.x();
    view(0,1) = s.y();
    view(0,2) = s.z();
    view(0,3) = -s.dot(cameraPos);
    view(1,0) = u.x();
    view(1,1) = u.y();
    view(1,2) = u.z();
    view(1,3) = -u.dot(cameraPos);
    view(2,0) = -f.x();
    view(2,1) = -f.y();
    view(2,2) = -f.z();
    view(2,3) = f.dot(cameraPos);
    view.row(3) << 0,0,0,1;
    // Model
    Matrix4 model = Matrix4::Identity(4,4);
#endif

    int ulMVP = glGetUniformLocation(shaderProgram, "MVP");
    _LOGD("ulMVP: %d", ulMVP);



    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // input
        // -----
        processInput(window);

        // render
        // ------
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        {
            glUseProgram(shaderProgram);

#if !(defined USE_EIGEN)
            model = glm::rotate(model, glm::radians(1.0f), glm::vec3(0.0, 1.0, 0.0));
            glm::mat4 MVP = projection * view * model;
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "MVP"), 1, GL_FALSE, &MVP[0][0]);
#else
            Affine3f aff; aff = AngleAxis<float>(M_PI * 1.f / 180.0, Vector3f(0,1,0));
            model = model * aff.matrix();

            Matrix4 MVP = projection * view * model;
            glUniformMatrix4fv(ulMVP, 1, GL_FALSE, MVP.data());
#endif
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture1);


            glEnableVertexAttribArray(alPos);
            glEnableVertexAttribArray(alTexCoord);
            glBindBuffer(GL_ARRAY_BUFFER, VBO);

            glVertexAttribPointer(alPos, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
            glVertexAttribPointer(alTexCoord, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

            glDrawElements(GL_TRIANGLES, vIndices.size(), GL_UNSIGNED_INT, (void*)0);
            // glDrawArrays(GL_TRIANGLES, 0, 3);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glDisableVertexAttribArray(alPos);
        }
        // glBindVertexArray(0); // no need to unbind it every time

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();

        std::this_thread::sleep_for(std::chrono::milliseconds(17));
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    // glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}
