#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <atomic>
#include <memory>
#include <functional>
#include <vector>
#include "sys/types.h"
#include "sys/sysinfo.h"

#define LOG(format, ...) printf("(%s:%d)\t" format "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)

struct MyUsage {

    struct Mem_ {
        double total;
        double totalUsage;
        double progUsage;

        friend Mem_ operator-(const Mem_ &usg1, const Mem_ &usg2) {
            Mem_ ret;

            ret.total = usg1.total - usg2.total;
            ret.totalUsage = usg1.totalUsage - usg2.totalUsage;
            ret.progUsage = usg1.progUsage - usg2.progUsage;
            return ret;
        }
    };

    MyUsage() = default;

    

    friend MyUsage operator-(const MyUsage &usg1, const MyUsage &usg2) {
        MyUsage ret;

        ret.virtual_ = usg1.virtual_ - usg2.virtual_;
        ret.physical_ = usg1.physical_ - usg2.physical_;

        return ret;
    }

    void update()
    {
        struct sysinfo memInfo;
        sysinfo (&memInfo);
        size_t totalVirtualMem = memInfo.totalram;
        //Add other values in next statement to avoid int overflow on right hand side...
        totalVirtualMem += memInfo.totalswap;
        totalVirtualMem *= memInfo.mem_unit;

        long long virtualMemUsed = memInfo.totalram - memInfo.freeram;
        //Add other values in next statement to avoid int overflow on right hand side...
        virtualMemUsed += memInfo.totalswap - memInfo.freeswap;
        virtualMemUsed *= memInfo.mem_unit;

        virtual_.total = totalVirtualMem * 1.f;
        virtual_.totalUsage = virtualMemUsed * 1.f;
        virtual_.progUsage = getVirtProg() * 1.f;

        long long totalPhysMem = memInfo.totalram;
        //Multiply in next statement to avoid int overflow on right hand side...
        totalPhysMem *= memInfo.mem_unit;

        long long physMemUsed = memInfo.totalram - memInfo.freeram;
        //Multiply in next statement to avoid int overflow on right hand side...
        physMemUsed *= memInfo.mem_unit;
        physical_.total = totalPhysMem * 1.f;
        physical_.totalUsage = physMemUsed * 1.f;
        physical_.progUsage = getPhysProg() * 1.f;
    }

    void dump()
    {
        dump(virtual_);
        dump(physical_);
    }


    void dump(Mem_ meminfo)
    {
        LOG("%f\t%f\t%f", meminfo.total, meminfo.totalUsage, meminfo.progUsage);
    }

    int parseLine(char* line) {
        // This assumes that a digit will be found and the line ends in " Kb".
        int i = strlen(line);
        const char* p = line;
        while (*p <'0' || *p > '9') p++;
        line[i-3] = '\0';
        i = atoi(p);
        return i;
    }

    int getVirtProg() { //Note: this value is in KB!
        FILE* file = fopen("/proc/self/status", "r");
        int result = -1;
        char line[128];

        while (fgets(line, 128, file) != NULL){
            // LOG("%s", line);
            if (strncmp(line, "VmSize:", 7) == 0){
                result = parseLine(line);
                break;
            }
        }
        fclose(file);
        return result;
    }

    int getPhysProg(){ //Note: this value is in KB!
        FILE* file = fopen("/proc/self/status", "r");
        int result = -1;
        char line[128];

        while (fgets(line, 128, file) != NULL){
            // LOG("%s", line);
            if (strncmp(line, "VmRSS:", 6) == 0){
                result = parseLine(line);
                break;
            }
        }
        fclose(file);
        return result;
    }

    Mem_ virtual_;
    Mem_ physical_;
};



int main(int argc, char const *argv[])
{
    /// 100 Kb
    constexpr size_t size = 0x400 * 1000; 
    MyUsage myusg1, myusg2;
    myusg1.update();
    (myusg1 - myusg2).dump();
    // myusg1.update();

    {
        LOG("+stack: %ld", size);
        char stk[size];
        myusg2.update();
        (myusg2 - myusg1).dump();
        myusg1.update();
    }
    myusg2.update();
    LOG("- stack: %ld", size);
    (myusg2 - myusg1).dump();

    myusg1.update();
    {
        LOG("+heap: %ld", size);
        char *vir = new char [size * 10];
        myusg2.update();
        (myusg2 - myusg1).dump();
        delete vir;
        myusg1.update();
    }
    myusg2.update();
    LOG("- heap: %ld", size);
    (myusg2 - myusg1).dump();

    return 0;
}
