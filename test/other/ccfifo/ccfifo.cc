
// #pragma once

#include <set>
#include <cassert>
#include <memory>
#include <atomic>
#include <functional>
#include <cstdio>
#include <cstring>
#include <chrono>
#include <list>
#include <vector>
#include <iostream>
#include <algorithm>
#include <thread>
#include <mutex>
#include <queue>
#include <unordered_map>
#include <condition_variable>
#include <sstream>

// extern char *__progname;
template <typename T=double, typename D=std::ratio<1,1>, typename C=std::chrono::system_clock>
T timestamp(typename C::time_point &&t = C::now())
{
    return std::chrono::duration_cast<std::chrono::duration<T,D>>(std::forward<typename C::time_point>(t).time_since_epoch()).count();
}

template <typename D=std::chrono::duration<double, std::ratio<1>>, typename C=std::chrono::steady_clock>
struct Counter {
    using Tp = std::chrono::time_point<C,D>;
    Tp begin = std::chrono::time_point_cast<D>(C::now());

    D restart()
    {
        D ret = elapse();
        begin = std::chrono::time_point_cast<D>(C::now());
        return ret;
    }

    D elapse() const
    {
        return (std::chrono::time_point_cast<D>(C::now()) - begin);
    }
};

template <typename T>
inline std::string to_string(T val)
{
    std::stringstream ss;
    ss << val;
    return ss.str();
}

inline std::string tid()
{
    static const thread_local auto ret=to_string(std::this_thread::get_id());
    return ret;
}

#define LOG_(format, ...) printf("[D](%s)%s:%s:%d:" format "\n", tid().data(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)

template <typename T>
struct SPSCFifo {
    /// Note that capacity must be a power of 2 number.
    SPSCFifo(size_t capacity) {m_buffer.resize(capacity);}

    template <typename U>
    bool push(U &&t)
    {
        size_t prod = m_prod.load(std::memory_order_relaxed);
        size_t cons = m_cons.load(std::memory_order_relaxed);
        bool ret = false;
        if(prod - cons < m_buffer.size())
        {
            m_buffer[wrap(prod)] = std::forward<U>(t);
            m_prod.store(prod + 1, std::memory_order_release); /// release
            ret = true;
        }

        return ret;
    }

    bool pop(T &t)
    {
        size_t cons = m_cons.load(std::memory_order_relaxed);
        size_t prod = m_prod.load(std::memory_order_acquire); /// acquire
        bool ret = false;
        if(prod > cons)
        {
            t = std::move(m_buffer[wrap(cons)]);
            m_cons.store(cons + 1, std::memory_order_relaxed);
            ret = true;
        }

        return ret;
    }

    inline bool empty()
    {
        return m_prod.load(std::memory_order_relaxed) == m_cons.load(std::memory_order_relaxed);
    }

    size_t wrap(size_t index)
    {
        return index & (m_buffer.size() - 1);
    }

    std::atomic<size_t> m_prod{0}, m_cons{0};
    std::vector<T> m_buffer;
};


int main(int argc, char **argv)
{
    Counter<> counter;
    constexpr size_t total_size = 100000;
    std::condition_variable cv;
    std::mutex mtx;

    {
        // SPSCFifo<size_t> spscfifo(0x100);
        std::queue<size_t> spscfifo;
        std::vector<size_t> store_buff(total_size, 0);
        std::atomic<bool> isDone{false};

        counter.restart();
        std::thread producer([&](){
            size_t count = 0;
            while(count < total_size)
            {
                std::unique_lock<std::mutex> lock(mtx);
                spscfifo.push(count);
                count++;
            }
            LOG_("%ld", count);
            isDone.store(true);
        });

        std::thread consumer([&](){
            size_t count=0;
            while(!spscfifo.empty() || !isDone.load(std::memory_order_relaxed))
            {
                std::unique_lock<std::mutex> lock(mtx);
                if(!spscfifo.empty())
                {
                    store_buff[count] = spscfifo.front();
                    count++;
                    spscfifo.pop();
                }
                else
                {
                    lock.unlock();
                    std::this_thread::yield();
                }
            }
            LOG_("%ld %d", count, spscfifo.empty());
        });

        producer.join();
        consumer.join();

        double total_time = counter.elapse().count();

        for(size_t i=0; i<total_size; i++)
        {
            if(store_buff[i] != i) LOG_("%ld %ld", store_buff[i], i);
            assert(store_buff[i] == i);
        }
        LOG_("std::queue: %.9f", total_time);
    }

    {
        SPSCFifo<size_t> spscfifo(0x100);
        std::vector<size_t> store_buff(total_size, 0);
        std::atomic<bool> isDone{false};

        counter.restart();
        std::thread producer([&](){
            size_t count = 0;
            while(count < total_size)
            {
                if(spscfifo.push(count))
                {
                    count++;
                }
                else
                    std::this_thread::yield();
            }
            LOG_("%ld", count);
            isDone.store(true);
        });

        std::thread consumer([&](){
            size_t val = 0, count=0;
            while(!spscfifo.empty() || !isDone.load(std::memory_order_relaxed))
            {
                if(spscfifo.pop(val))
                {
                    store_buff[count] = val;
                    count++;
                }
                else
                {
                    std::this_thread::yield();
                }
            }
            LOG_("%ld %d", count, spscfifo.empty());
        });

        producer.join();
        consumer.join();

        double total_time = counter.elapse().count();

        for(size_t i=0; i<total_size; i++)
        {
            if(store_buff[i] != i) LOG_("%ld %ld", store_buff[i], i);
            assert(store_buff[i] == i);
        }
        LOG_("lockfree: %.9f", total_time);
    }

    {
        // SPSCFifo<size_t> spscfifo(0x100);
        std::queue<size_t> spscfifo;
        std::vector<size_t> store_buff(total_size, 0);
        std::atomic<bool> isDone{false};

        counter.restart();
        std::thread producer([&](){
            size_t count = 0;
            while(count < total_size)
            {
                std::unique_lock<std::mutex> lock(mtx);
                spscfifo.push(count);
                cv.notify_all();
                count++;
            }
            LOG_("%ld", count);
            isDone.store(true);
            {
                std::unique_lock<std::mutex> lock(mtx);
                cv.notify_all();
            }
        });

        std::thread consumer([&](){
            size_t count=0;
            while(!spscfifo.empty() || !isDone.load(std::memory_order_relaxed))
            {
                std::unique_lock<std::mutex> lock(mtx);
                cv.wait(lock, [&]{return !spscfifo.empty() || isDone.load(std::memory_order_relaxed);});
                if(!spscfifo.empty())
                {
                    store_buff[count] = spscfifo.front();
                    count++;
                    spscfifo.pop();
                }
            }
            LOG_("%ld %d", count, spscfifo.empty());
        });

        producer.join();
        consumer.join();

        double total_time = counter.elapse().count();

        for(size_t i=0; i<total_size; i++)
        {
            if(store_buff[i] != i) LOG_("%ld %ld", store_buff[i], i);
            assert(store_buff[i] == i);
        }
        LOG_("condvar std::queue: %.9f", total_time);
    }

    {
        SPSCFifo<size_t> spscfifo(0x100);
        std::vector<size_t> store_buff(total_size, 0);
        std::atomic<bool> isDone{false};

        counter.restart();
        std::thread producer([&](){
            size_t count = 0;
            while(count < total_size)
            {
                if(spscfifo.push(count))
                {
                    count++;
                    std::unique_lock<std::mutex> lock(mtx);
                    cv.notify_all();
                }
                else
                    std::this_thread::yield();
            }
            LOG_("%ld", count);
            isDone.store(true);
            {
                std::unique_lock<std::mutex> lock(mtx);
                cv.notify_all();
            }
        });

        std::thread consumer([&](){
            size_t val = 0, count=0;
            while(!spscfifo.empty() || !isDone.load(std::memory_order_relaxed))
            {
                {
                    std::unique_lock<std::mutex> lock(mtx);
                    cv.wait(lock, [&]{return !spscfifo.empty() || isDone.load(std::memory_order_relaxed);});
                }

                if(spscfifo.pop(val))
                {
                    store_buff[count] = val;
                    count++;
                }
            }
            LOG_("%ld %d", count, spscfifo.empty());
        });

        producer.join();
        consumer.join();

        double total_time = counter.elapse().count();

        for(size_t i=0; i<total_size; i++)
        {
            if(store_buff[i] != i) LOG_("%ld %ld", store_buff[i], i);
            assert(store_buff[i] == i);
        }
        LOG_("condvar lockfree: %.9f", total_time);
    }

    return 0;
}
