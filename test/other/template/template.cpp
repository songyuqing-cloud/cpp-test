
#include <iostream>
#include <chrono>
#include <functional>
#include "../../utils.h"

template<class Instance, class Class, typename RET, class... Args> 
std::function<RET (Args...)> invoke (Instance &object, RET (Class::*method) (Args ...), Args ...args) 
{
    LOGPD("");
    (object .* method) (args...);
}

struct A_Struct
{
    void dosmt(){LOGD("");}
};

template<typename T> struct Type_ { };

template <int obj_type>
struct SGProp
{
    // SGProp(void *obj) : obj_(obj)
    // {}

    template <typename T> T get(int prop_type)
    {
        return get_as(Type_<T>(), prop_type);
    }

private:
    template<typename T> T get_as(Type_<T>, int)
    {
        T ret;
        std::cout << std::is_same_v<T, int> << std::endl;
        return ret;
    }
    // template <typename T> void set(int, const T&);

    // template <int obj_type>
    int get_as(Type_<int>, int prop_type)
    {
        return prop_type * obj_type;
    }

    // double get_as(Type_<double>, int prop_type)
    // {
    //     return prop_type * obj_type * 0.1f;
    // }

    // void *obj_=nullptr;
};

// template <int obj_type>
// double SGProp<obj_type>::get_as(SGProp<obj_type>::Type_<double>, int prop_type)
// {
//     return prop_type * obj_type;
// }


// int SGProp<obj_type>::get_as(SGProp<obj_type>::Type_<int>, int)

// template<int obj_type> template<>
// double SGProp<obj_type>::get_as(Type_<double>, int prop_type)
// {
//     return prop_type * obj_type;
// }

// template<>
// template <int obj_type> template<>
// double SGProp<obj_type>::get_as(Type_<double>, int prop_type)
// {
//     return prop_type * obj_type;
// }

// template <class C> class Ex
// {
//    template<typename T> struct type { };

// public:
//    template <class T> void get_as() {
//      get_as(type<T>());
//    }

// private:
//    template<typename T> void get_as(type<T>) {
//     LOGD("");
//    }

//    void get_as(type<double>) {
//     LOGD("");
//    }
// };

// template <class C> template <>
// void Ex<C>::get_as<double>()
// {
//     LOGD("");
// }

int main()
{
    // A<std::chrono::milliseconds, std::chrono::high_resolution_clock::time_point> a;
    // A_Struct a;
    // invoke(a, &A_Struct::dosmt);

    SGProp<10> prop;
    LOGD("%d", prop.get<int>(20));
    // LOGD("%f", prop.get<double>(20));
    LOGD("%d", prop.get<char>(20));
    LOGD("%d", SGProp<50>().get<int>(20));

    // Ex<int> ex;
    // ex.get_as<int>();
    // ex.get_as<double>();

    return 0;
}
