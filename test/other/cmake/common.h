#pragma once

#include <cstdio>

#define LOG_(format, ...) printf("%s:%s:%d " format "\n", __FILE__, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
