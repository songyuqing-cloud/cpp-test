#if defined LIB_C
#include <libC.h>
#include <libC_impl.h>
#error "libA::libC is not PRIVATE"
#endif

#include <dlfcn.h>
#include <common.h>
#include <libA.h>

int main(int argc, char const *argv[])
{
    libA::process();
    void* handle = dlopen("./liblibB.so", RTLD_LAZY);

    LOG_("%p", dlsym(handle, "_ZN4libB7processEv"));
    LOG_("%p", dlsym(handle, "_ZN4libC7processEv"));

    dlclose(handle);
    return 0;
}