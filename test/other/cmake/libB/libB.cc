#include <common.h>
#include <libB.h>
#include <libB_impl.h>
#include <libC.h>
#include <config.h>

namespace libB
{
    void MY_PUBLIC_API process()
    {
        LOG_("");
        impl::process();
    }

namespace impl
{
    void process()
    {
        LOG_("");
    }
}
}

