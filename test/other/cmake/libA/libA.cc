#include <common.h>

#include <libA.h>
#include <libA_impl.h>
#include <libC.h>

namespace libA
{
    void process()
    {
        LOG_("");
        impl::process();
    }
}

namespace libA::impl
{
    void process()
    {
        LOG_("");
    }
}
