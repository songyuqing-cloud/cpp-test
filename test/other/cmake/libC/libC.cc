#include <libC.h>
#include <libC_impl.h>
#include <common.h>

#ifdef LIB_C
#error "libC definition is NOT AN INTERFACE!!!"
#endif

#include <config.h>

namespace libC
{
    void MY_PUBLIC_API process()
    {
        LOG_("");
        impl::process();
    }
namespace impl
{
    void process()
    {
        LOG_("");
    }
}
}
