cmake_minimum_required(VERSION 3.16 FATAL_ERROR)
project(appB VERSION 0.1 LANGUAGES CXX)
################################################################################
find_package(smc QUIET)
message (SMC: ${smc_FOUND})
################################################################################


set(target appB)
add_executable(${target})

target_sources(${target}
PRIVATE
    main.cc
)


# inherit all PUBLIC properties from libA: 
#   target_include_directories
#   target_link_libraries
target_link_libraries(${target} 
PRIVATE
#     smc::libC
#     $<IF:$<smc_FOUND:oitroioi>,libB>
#     $<IF:$<smc_FOUND>,oitroioi,libB>

#     $IF:${smc_FOUND},oitroioi,libB>
    smc::libB
    infGlobal
)

target_link_options(${target} PRIVATE 
                    "-Wl,-rpath,.")
target_compile_features(${target} PRIVATE cxx_std_17)

add_custom_target(${target}.bin
    ALL
    COMMAND             ${CMAKE_OBJCOPY} -O binary ${target}.out ${target}.bin
    DEPENDS             ${target}
    WORKING_DIRECTORY   "${CMAKE_BINARY_DIR}"
)

set_target_properties(${target}
    PROPERTIES
        OUTPUT_NAME ${target}.out
        ADDITIONAL_CLEAN_FILES ${target}.bin
)