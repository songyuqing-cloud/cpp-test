#if !defined LIB_C
#error "libB::libC is not PUBLIC"
#endif

#include <libC.h>
#include <libC_impl.h>
#include <common.h>
#include <libB.h>

int main(int argc, char const *argv[])
{
    libB::process();
    libC::process();
    return 0;
}