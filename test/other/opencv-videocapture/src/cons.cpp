#include "opencv2/opencv.hpp"

#include <thread>
#include <vector>
#include <string>
#include <cstring>
#include <cerrno>

#include <helper.inl>

int main(int argc, char** argv)
{
    sockaddr_in svr_addr = helper::to_sockaddr((argc > 1) ? argv[1] : HOST, (argc > 2) ? std::stoi(argv[2]) : PORT);
    int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    
    if ( fd <= 0 )
    {
        return 1;
    }

    // set non-blocking io
    if ( fcntl( fd, F_SETFL, O_NONBLOCK, 1 ) == -1 )
    {
        _LOGE("");
        return 1;
    }

    // std::vector < int > compression_params;
    // compression_params.push_back(cv::IMWRITE_JPEG_QUALITY);
    // compression_params.push_back(ENCODE_QUALITY);


    cv::Mat frame;
    // std::vector < uchar > encoded;
    const float SMOOTHING = 0.95;
    double fps = 0;
    sockaddr_in client;
    std::vector<uchar> buffer, frame_buff;
    buffer.reserve(PACK_SIZE);

    char cmd = 's';
    sendto( fd, &cmd, 1, 0, (sockaddr*)&svr_addr, sizeof(sockaddr_in) );
    ssize_t frm_recv = 0;

    // while (helper::udp_recv<uchar>(fd, buffer, &client, 50000) <= 0)
        // sendto( fd, &cmd, 1, 0, (sockaddr*)&svr_addr, sizeof(sockaddr_in) );

    // namedWindow("edges",1);

    // State state = State::IDLE;
    // Cmd state = Cmd::STOP;
    uint32_t crr_frm = 0;
    for(;;)
    {
        Counter counter;

        ssize_t recv = helper::udp_recv<uchar>(fd, buffer, &client, 50000);
        if(recv <=0) continue;

        uint32_t frm_size, idx, frm_idx, frm_pack_size = recv - HEADER_SIZE;

        helper::parse_header((char*)buffer.data(), &frm_size, &idx, &frm_idx);

        if(crr_frm < frm_idx || crr_frm > frm_idx * 2)
        {
            frm_recv = 0;
            crr_frm = frm_idx;
        }

        frame_buff.resize(frm_size);
        memcpy(frame_buff.data() + (idx * PAYLOAD_SIZE), buffer.data() + HEADER_SIZE, frm_pack_size);
        frm_recv += frm_pack_size;
        _LOGD("%d : %d", idx, frm_pack_size);

        if(frm_recv < frm_size) continue;

        _LOGD("%d : %d", frm_recv, frm_size);
        // frm_recv = 0;
        frame = cv::imdecode(frame_buff, cv::IMREAD_GRAYSCALE);

        // frame_buff.clear();
        // _LOGD("%dx%d", frame.cols, frame.rows);

        double ttrack=counter.elapse();
        fps = (fps*SMOOTHING) + ((1000/ttrack) * (1.0-SMOOTHING));

        std::string msg = std::to_string(fps);
        int baseLine = 0;
        cv::Size textSize = cv::getTextSize(msg, 1, 1, 1, &baseLine);
        cv::Point textOrigin(frame.cols - 2*textSize.width - 10, frame.rows - 2*baseLine - 10);
        cv::putText( frame, msg, textOrigin, 1, 1, cv::Scalar(0,255,0));

        cv::imshow(argv[0], frame);
        if(cv::waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor

    cmd = 'u';
    sendto( fd, &cmd, 1, 0, (sockaddr*)&svr_addr, sizeof(sockaddr_in) );

    // while (helper::udp_recv<uchar>(fd, buffer, &client, 50000) <= 0)
        // sendto( fd, &cmd, 1, 0, (sockaddr*)&svr_addr, sizeof(sockaddr_in) );

    helper::udp_deinit(fd);

    return 0;
}