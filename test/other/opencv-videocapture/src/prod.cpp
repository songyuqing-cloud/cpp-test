#include "opencv2/opencv.hpp"

#include <helper.inl>

int main(int argc, char** argv)
{
    // "http://192.168.100.23:8080/video/mjpeg"
    cv::VideoCapture cap(argv[1]); // open the default camera

    if (std::strlen(argv[1]) > 1)
        cap.open(argv[1]);
    else
        cap.open(std::stoi(argv[1])); // open the default camera

    if(!cap.isOpened())  // check if we succeeded
        return -1;
    sockaddr_in svr_addr = helper::to_sockaddr((argc>2) ? argv[2] : HOST, (argc>3) ? std::stoi(argv[3]) : PORT);
    int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    
    if ( fd <= 0 )
    {
        return 1;
    }

    // set non-blocking io
    if ( fcntl( fd, F_SETFL, O_NONBLOCK, 1 ) == -1 )
    {
        _LOGE("");
        return 1;
    }

    std::vector < int > compression_params;
    compression_params.push_back(cv::IMWRITE_JPEG_QUALITY);
    compression_params.push_back(ENCODE_QUALITY);

    cv::Mat frm_org, gray;
    std::vector < uchar > buffer;
    const float SMOOTHING = 0.9;
    double fps = 0;

    _LOGD("");
    // std::vector<char> buffer;
    // buffer.resize();
    cv::namedWindow(argv[0],1);
    Counter counter;
    for(int frm_cnt=0;; frm_cnt++)
    {
        ssize_t sent = 0;
        int i;
        // Mat frame;
        cap >> frm_org; // get a new frame from camera
        cv::cvtColor(frm_org, gray, cv::COLOR_BGR2GRAY);
        // GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        // Canny(edges, edges, 0, 30, 3);
        cv::imencode(".jpg", gray, buffer/*, compression_params*/);
        // cv::Mat frm_decoded = cv::imdecode(buffer, cv::IMREAD_GRAYSCALE);
        helper::udp_send_frame(fd, (sockaddr*)&svr_addr, buffer, frm_cnt);

        double ttrack=counter.elapse();
        fps = (fps*SMOOTHING) + ((1000/ttrack) * (1.0-SMOOTHING));

        std::string msg = std::to_string(fps);
        int baseLine = 0;
        cv::Size textSize = cv::getTextSize(msg, 1, 1, 1, &baseLine);
        cv::Point textOrigin(frm_org.cols - 2*textSize.width - 10, frm_org.rows - 2*baseLine - 10);
        cv::putText( frm_org, msg, textOrigin, 1, 1, cv::Scalar(0,255,0));

        cv::imshow(argv[0], frm_org);
        if(cv::waitKey(30) == 27) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor

    close(fd);

    return 0;
}