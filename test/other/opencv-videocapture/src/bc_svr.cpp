// #include "opencv2/opencv.hpp"

#include <helper.inl>

int main(int argc, char **argv)
{
    // VideoCapture cap(0); // open the default camera
    // if(!cap.isOpened())  // check if we succeeded
        // return -1;

    uint16_t port = 65500;
    if(argc > 1) port = std::stoi(argv[1]);

    sockaddr_in svr_addr;
    int fd = helper::udp_svr_init(&svr_addr, port, O_NONBLOCK);

    if(fd < 0) return 1;

    std::thread([fd, svr_addr]()
    {
        sockaddr_in client;
        // int client_address_size = sizeof(client);
        std::vector<sockaddr_in> clt_addr_list;

        std::vector<char> buffer;
        buffer.reserve(0x8000);
        bool stop = false;
        _LOGI("Server is listenning on %d", ntohs(svr_addr.sin_port));

        while(!stop)
        {
            ssize_t recv = helper::udp_recv<char>(fd, buffer, &client, 50000);

            if(recv !=0)
            {
                _LOGD("Recv %ld byte from (%s:%d)", recv, inet_ntoa(client.sin_addr), ntohs(client.sin_port));
            }

            if(recv == 1)
            {
                switch(buffer[0])
                {
                    case 's': // subscribe
                    { 
                        clt_addr_list.push_back(client); 
                        _LOGI(" Add (%s:%d)", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
                    } 
                    break; 

                    case 'u': // unsubscribe
                    {
                        clt_addr_list.erase(
                            std::remove_if(clt_addr_list.begin(), clt_addr_list.end(), // disconnect
                                [client](sockaddr_in aClt) 
                                {
                                    return ((aClt.sin_addr.s_addr == client.sin_addr.s_addr) && (aClt.sin_port == client.sin_port));
                                }), clt_addr_list.end()
                        );
                        _LOGI(" Remove (%s:%d)", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
                    }
                    break; 

                    default:
                    { 
                        // _LOGI("Unkown code: %d", buffer[0]);
                        std::string msg ("Command list\n"
                            "s: Subscribe\n"
                            "u: Unsubscribe\n"
                            );
                        sendto( fd, msg.data(), msg.size(), 0, (sockaddr*)&client, sizeof(sockaddr_in) );
                    }
                    break;
                }
            }
            else if (recv > 1)
            {
                for (auto clt : clt_addr_list)
                {
                    sendto( fd, buffer.data(), recv, 0, (sockaddr*)&clt, sizeof(sockaddr_in) );
                    _LOGD(" Sent to (%s:%d)", inet_ntoa(clt.sin_addr), ntohs(clt.sin_port));
                }
            }
            // std::this_thread::yield();
            // std::this_thread::sleep_for(std::chrono::microseconds(5));
        }
    }).join();
    
    helper::udp_deinit(fd);
    _LOGI("Stopped");

    return 0;
}