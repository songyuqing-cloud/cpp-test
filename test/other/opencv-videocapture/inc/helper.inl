#ifndef HELPER_INL_
#define HELPER_INL_

#include <algorithm>
#include <thread>
#include <vector>
#include <string>
#include <cerrno>
#include <cstring>


#include <sys/select.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h> // close
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// #define _LOGD(format, ...) printf("[Dbg] %s %s [%d] " format "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__)
#define _LOGD(...)
#define _LOGI(format, ...) printf("[I] %s %s [%d] " format "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__)
#define _LOGE(format, ...) fprintf(stderr, "[E] %s %s [%d]" format " %s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
// using namespace cv;

const int HEADER_SIZE = 12;
const int PAYLOAD_SIZE = 0x1000;
const int PACK_SIZE = PAYLOAD_SIZE + HEADER_SIZE; // TODO

const int ENCODE_QUALITY = 80;
const char *HOST = "localhost";
const uint16_t PORT = 65500;

// enum class Cmd : uint8_t { START = 0, STOP };

class Counter
{
public:
    Counter() 
    {
        m_start = std::chrono::high_resolution_clock::now();
    }

    double elapse()
    {
        auto start = m_start;
        m_start = std::chrono::high_resolution_clock::now();
        return std::chrono::duration <double, std::milli> (std::chrono::high_resolution_clock::now() - start).count();
    }

private:
    std::chrono::high_resolution_clock::time_point m_start;
};

namespace helper {
  
template <typename T>
inline ssize_t udp_recv(int a_sock, std::vector<T> &a_buffer, sockaddr_in *ap_client, int a_timeout=1000)
{
    ssize_t ret = 0;
    struct timeval timeout = {0, a_timeout}; // make select() return once per second
    int client_address_size = sizeof(sockaddr_in);

    fd_set read_set;
    FD_ZERO(&read_set);
    FD_SET(a_sock, &read_set);

    if (select(a_sock+1, &read_set, NULL, NULL, &timeout) >= 0)
    {
        if (FD_ISSET(a_sock, &read_set))
        {
            ret = recvfrom(a_sock, a_buffer.data(), a_buffer.capacity() * sizeof(T), 0, (sockaddr*)ap_client,
                        (socklen_t*)&client_address_size);
        }
    }

    return ret;
}

inline int udp_svr_init(sockaddr_in *ap_svr_addr, uint16_t a_port, int a_flag=0)
{
    int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    
    if ( fd <= 0 )
    {
        return fd;
    }

    // sockaddr_in svr_addr;
    ap_svr_addr->sin_family = AF_INET;
    ap_svr_addr->sin_addr.s_addr = INADDR_ANY;
    ap_svr_addr->sin_port = htons( a_port );

    // set non-blocking io
    if ( fcntl( fd, F_SETFL, a_flag, 1 ) == -1 )
    {
        _LOGD( "failed to make socket non-blocking" );
        return fd;
    }
    
    if ( ::bind( fd, (sockaddr *)ap_svr_addr, sizeof(sockaddr) ) < 0 )
    {
        _LOGE("");
        // m_error = SOCKET_ERROR_BIND_IPV4_FAILED;
        return fd;
    }

    return fd;
}

inline sockaddr_in to_sockaddr(char const *ap_host, uint16_t a_port)
{
	struct hostent *psvr;
	sockaddr_in addr;

	psvr = gethostbyname(ap_host);

    addr.sin_family = AF_INET;
    addr.sin_port = htons( a_port );
    bcopy((char *)psvr->h_addr, (char *)&addr.sin_addr.s_addr, psvr->h_length);

    return addr;
}

inline void udp_deinit(int fd)
{
	close(fd);
}

template <typename _T>
void put(_T a_val, char *ap_buffer)
{
    char *ptr = (char*)&a_val;

    for(size_t i=0; i<sizeof(_T); i++)
        ap_buffer[i] = *(ptr + i);
}

template <typename _T>
_T get(char const *ap_buffer)
{
    _T ret;
    char *ptr = (char*)&ret;

    for(size_t i=0; i<sizeof(_T); i++)
        *(ptr + i) = ap_buffer[i];

    return ret;
}

// template <typename _T>
void prepare_header(char *ap_buffer, uint32_t a_total, uint32_t a_idx, uint32_t a_frm_idx)
{
    put<uint32_t>(a_total, ap_buffer);
    put<uint32_t>(a_idx, ap_buffer + sizeof(uint32_t));
    put<uint32_t>(a_frm_idx, ap_buffer + sizeof(uint32_t) * 2);
}

// template <typename _T>
void parse_header(char const *ap_buffer, uint32_t *ap_total, uint32_t *ap_idx, uint32_t *ap_frm_idx)
{
    *ap_total = get<uint32_t>(ap_buffer);
    *ap_idx = get<uint32_t>(ap_buffer + sizeof(uint32_t));
    *ap_frm_idx = get<uint32_t>(ap_buffer + sizeof(uint32_t) * 2);
}

// pack_totalsize:4 | pack_index:4 | pack_size:4 | pack_payload
template <typename _T>
void udp_send_frame(int fd, sockaddr const *ap_addr, std::vector<_T> const &a_buffer, uint32_t a_frm_idx)
{
    std::vector<char> buffer;
    uint32_t frame_size = a_buffer.size() * sizeof(_T);
    uint32_t frame_pack_size = PAYLOAD_SIZE;
    uint32_t frame_pack_count = frame_size / frame_pack_size;

    buffer.reserve(PACK_SIZE);

    for(int i=0; i < frame_pack_count; i++)
    {
        prepare_header(buffer.data(), frame_size, i, a_frm_idx);
        memcpy(buffer.data() + HEADER_SIZE, (char*)a_buffer.data() + (i * frame_pack_size), frame_pack_size);
        sendto(fd, buffer.data(), PACK_SIZE, 0, ap_addr, sizeof(sockaddr) );
    }

    // last pack
    if(frame_pack_count * frame_pack_size < frame_size)
    {
        int last_frame_pack_size = frame_size - (frame_pack_count * frame_pack_size);

        prepare_header(buffer.data(), frame_size, frame_pack_count, a_frm_idx);
        memcpy(buffer.data() + HEADER_SIZE, (char*)a_buffer.data() + (frame_pack_count * frame_pack_size), last_frame_pack_size);
        sendto(fd, buffer.data(), last_frame_pack_size + HEADER_SIZE, 0, ap_addr, sizeof(sockaddr) );
    }
}


}
#endif // helper