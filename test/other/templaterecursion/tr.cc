#include <iostream>
#include "../../utils.h"

template <typename T>
T recurs(T first)
{
	return first + 1;
}

template <typename T, typename ... Args>
T recurs(T first, T seconds, Args ... rest)
{
	// first + 10;
	return recurs(seconds + first, rest ... );
}

// template <typename T, typename ... Args>
// T recurs(T first, Args ... rest)
// {
// 	recurs();
// }

int main(int argc, char const *argv[])
{
	/* code */
	int ret = recurs(100, 1000, 10000);
	LOGD("%d", ret);
	return 0;
}