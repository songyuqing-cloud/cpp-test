#ifndef QTHREADTEST_H
#define QTHREADTEST_H

#include <QThread>

#include </home/longlt3/workspace/tmp/MyCounter.h>

class MyTest;

class QThreadTest : public QThread
{
    Q_OBJECT
public:
    QThreadTest();
    ~QThreadTest();
    Q_SIGNAL void doSmt(QThreadTest *context);
    Q_SIGNAL void doSmt2(QThreadTest *context);
    // Q_SLOT void doSmt3(QThreadTest *context);

    void run(void);

};

class MyTest : public QObject
{
    Q_OBJECT
public:
    Q_SLOT void doSmt(QThreadTest *context);
    void doRequest(void);
    static void *threadEntry(void *context);
    void run(void);

};


#endif // QTHREADTEST_H
