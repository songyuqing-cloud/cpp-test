TEMPLATE = app

TARGET = QThreadTest

QT       -= gui

SOURCES *= \
    QThreadTest.cpp \

HEADERS *= \
    QThreadTest.h \

DEFINES += \
    __DEBUG_LOGI__ \
    __DRAFT__

OBJECTS_DIR = tmp/
MOC_DIR     = tmp/

# LIBS += \

# linux-g++:QMAKE_CXXFLAGS *= '-std=c++11'
