#include <QThreadTest.h>
#include <QCoreApplication>
#include <pthread.h>

QThreadTest::QThreadTest()
{
    LOGI("");
    connect(this, SIGNAL(doSmt2(QThreadTest *)), this, SIGNAL(doSmt(QThreadTest *)));
    // connect(this, SIGNAL(doSmt2(QThreadTest *)), this, SLOT(doSmt3(QThreadTest *)));

}

QThreadTest::~QThreadTest()
{
     LOGI("");
     // wait();
}

void QThreadTest::run(void)
{
    LOGI("");
    // while(true);
    emit doSmt2(this);
    LOGI("");
    while(1);
    // doSmt3(this);
}

// void QThreadTest::doSmt(QThreadTest *context)
// {
//     LOGI("");
// }

void MyTest::doSmt(QThreadTest *context)
{
    LOGI("");
    delete context;
    LOGI("");
}

void MyTest::doRequest(void)
{
    // LOGI("");
    // pthread_t thread;
    // pthread_create(&thread, NULL, &threadEntry, this);

    QThreadTest *thread = new QThreadTest();
    connect(thread, SIGNAL(doSmt(QThreadTest *)), this, SLOT(doSmt(QThreadTest *)));
    thread->start();
}

void *MyTest::threadEntry(void *context)
{
    LOGI("");
    ((MyTest *)context)->run();
    return NULL;
}

void MyTest::run(void)
{
    LOGI("");
    while(1);
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    MyTest qt;

    qt.doRequest();

    LOGI("");
    app.exec();

    return 0;
}
