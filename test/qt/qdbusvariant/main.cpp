#include <QVariant>
#include <QString>
#include <QVector>
#include <QDBusArgument>
#include <QtDBus/QtDBus>

#include <QDBusVariant>

#include <vector>

#include </opt/MyCounter.h>

class MyClass
{
public:
    int valint;
    QString valStr;
};

Q_DECLARE_METATYPE(MyClass)

inline const QDBusArgument& operator>>(const QDBusArgument& a_args, MyClass& obj)
{
    a_args.beginStructure();
    a_args >> obj.valint;
    a_args >> obj.valStr;
    a_args.endStructure();
    return a_args;
}

inline QDBusArgument& operator<<(QDBusArgument& a_args, const MyClass& obj)
{
    a_args.beginStructure();
    a_args << obj.valint;
    a_args << obj.valStr;
    a_args.endStructure();
    return a_args;
}


int main()
{
    LOGI("");

    qRegisterMetaType<MyClass>("MyClass");
    qDBusRegisterMetaType<MyClass>();

    std::vector<MyClass> v1;
    MyClass m1 {1, "abcdef"};
    MyClass m2 {2, "abcdef"};
    v1.push_back(m1);
    v1.push_back(m2);

    QVariant vObj;
    vObj.setValue(QVector<MyClass>::fromStdVector(v1));
    QDBusVariant obj(vObj);

    QVector<MyClass> v2;
    obj.variant().value<QDBusArgument>() >> v2;
    LOGI("%d", v2.size());
    for(auto v : v2)
    {
        LOGI("%d", v.valint);
    }


    LOGI("");



    return 0;
}
