TEMPLATE = app

TARGET = myapp

QT       -= gui
QT       += dbus

SOURCES *= \
    main.cpp \

# HEADERS *= \
#     main.h \

DEFINES += \
    __DEBUG_LOGI__ \
    __DRAFT__

OBJECTS_DIR = tmp/
MOC_DIR     = tmp/

# LIBS += \

QMAKE_CXXFLAGS += '-std=c++11'
