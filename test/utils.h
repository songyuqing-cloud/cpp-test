#pragma once

#include <vector>
#include <chrono>
#include <thread>
#include <unordered_map>
#include <string>
#include <sstream>
#include <error.h>
#include <cstdio>
#include <cstring>

#define LOGPD(format, ...) printf("[D](%.9f)(%s)(%s:%s:%d):" format "\n", ::utils::timestamp<std::chrono::high_resolution_clock, std::milli>(std::chrono::high_resolution_clock::now()), ::utils::tid().data(), __FILE__, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOGD(format, ...) printf("[D](%.9f)(%s)(%s:%s:%d):" format "\n", ::utils::timestamp<std::chrono::high_resolution_clock, std::milli>(std::chrono::high_resolution_clock::now()), ::utils::tid().data(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define LOGE(format, ...) printf("[E](%.9f)(%s)(%s:%s:%d):" format "%s\n", ::utils::timestamp<std::chrono::high_resolution_clock, std::milli>(std::chrono::high_resolution_clock::now()), ::utils::tid().data(), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))

#define TIMER(ID) ::utils::Tracer tracer_##ID__(#ID)
#define TRACE() ::utils::Tracer tracer__(std::string(__FUNCTION__) + ":" + std::to_string(__LINE__) + "(" + ::utils::tid() + ")")

template<typename T>
struct Trait;

template <template <class, class...> class G, class V, class ...Ts>
struct Trait<G<V,Ts...>>
{
    using template_type = V;
};

template <typename T, template<typename ...> class crtpType>
struct crtp
{
    T& underlying() { return static_cast<T&>(*this); }
    T const& underlying() const { return static_cast<T const&>(*this); }

    using template_type = typename Trait<T>::template_type;
private:
    crtp(){}
    friend crtpType<T>;
};


namespace utils {

inline std::string tid()
{
    std::stringstream ss;
    ss << std::this_thread::get_id();
    return ss.str();
}

template <typename C=std::chrono::high_resolution_clock, typename D=std::ratio<1,1>>
double timestamp(typename C::time_point const &t)
{
    return std::chrono::duration_cast<std::chrono::duration<double,D>>(t.time_since_epoch()).count();
}

struct Tracer
{
    using Clock = std::chrono::high_resolution_clock;

    Tracer() : id_(""), begin_(Clock::now()) {}
    Tracer(std::string id) : id_(std::move(id)), begin_(Clock::now()) {printf(" (%.9f)%s\n", ::utils::timestamp<Clock, std::milli>(Clock::now()), id_.data());}
    ~Tracer()
    {
        if(!id_.empty())
            printf(" (%.9f)~%s: %.3f (ms)\n", ::utils::timestamp<Clock, std::milli>(Clock::now()), id_.data(), elapse<double,std::milli>());
    }

    template <typename T=double, typename D=std::milli>
    T reset()
    {
        T ret = elapse<T,D>();
        begin_ = Clock::now();
        return ret;
    }

    template <typename T=double, typename D=std::milli>
    T elapse() const
    {
        using namespace std::chrono;
        return duration_cast<std::chrono::duration<T,D>>(Clock::now() - begin_).count();
    }

    template <typename T=double, typename D=std::milli>
    std::chrono::duration<T,D> duration() const
    {
        using namespace std::chrono;
        auto ret = duration_cast<std::chrono::duration<T,D>>(Clock::now() - begin_);
        return ret;
    }

    Clock::time_point begin_;
    std::string id_;
};
}