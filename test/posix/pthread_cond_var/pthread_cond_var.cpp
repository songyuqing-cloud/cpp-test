#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <thread>
#include <chrono>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <sys/syscall.h>

#define _TLOGI(format, ...) printf("[I]%s %s t-%d [%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )
#define _TLOGE(format, ...) fprintf(stderr, "[E]%s %s t-%d [%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__, strerror(errno))


class CircularBuffer
{
public:
	CircularBuffer(size_t size) : _size (size)
	{
		pthread_mutex_init(&_mutex, nullptr); _TLOGE("mutex init");
		pthread_cond_init(&_cond, nullptr); _TLOGE("cond init");
	}

	virtual ~CircularBuffer()
	{
		pthread_mutex_destroy(&_mutex);
		pthread_cond_destroy(&_cond);
	}

	size_t getAvai() const
	{
		return _avai;
	}

	bool write(size_t size)
	{
		bool ret = true;
		_write += size;

		if(_write >= _size)
		{
			_write -= _size;
		}

		{
			pthread_mutex_lock(&_mutex);
			_avai += size;
			if(_avai > _size)
			{
				_avai = _size;
				ret = false;
			}
			pthread_cond_signal(&_cond); 
			pthread_mutex_unlock(&_mutex);
		}

		return ret;
	}

	size_t read(size_t size)
	{
		pthread_mutex_lock(&_mutex); 
		while(_avai == 0) pthread_cond_wait(&_cond, &_mutex); 
		pthread_mutex_unlock(&_mutex); 

		if(size > _avai)
			size = _avai;

		_read += size;
		if(_read >= _size)
			_read -= _size;

		_avai -= size;

		return size;
	}

private:
	// std::vector<char> _buffer;
	// std::vector<Consumer> _cons;

	pthread_cond_t _cond;
	pthread_mutex_t _mutex;
	size_t _size;
	size_t _write, _read;
	size_t _avai;
};




static const size_t CB_SIZE = 10;
static const size_t W_SIZE=1;
static const size_t R_SIZE=1;
static const size_t W_SLEEPMS = 1;
static const size_t R_SLEEPMS = 1;

static CircularBuffer sCB(CB_SIZE);
static bool sWorking = true;



void *handler(void *arg)
{
	int64_t id = reinterpret_cast<int64_t>(arg);
	_TLOGI("%ld started", id);
	size_t size = 0;
	int cnt = 0;

	for(cnt=0; sWorking; cnt++)
	{
		if(id == 1)
		{
			if(!sCB.write(W_SIZE))
			{
				_TLOGI("%ld [ERROR] Overflow!!!", id);
				sWorking = false;
			}
			else _TLOGI("%ld write %ld", id, W_SIZE);
			std::this_thread::sleep_for(std::chrono::milliseconds(W_SLEEPMS));
		}
		else
		{
			size = sCB.read(R_SIZE);
			_TLOGI("%ld read %ld", id, size);
			std::this_thread::sleep_for(std::chrono::milliseconds(R_SLEEPMS));
			// std::this_thread::yield();
		}
	}

	_TLOGI("%ld finished: %d loops", id, cnt);
	return nullptr;
}

int main()
{
	pthread_t t1,t2;
	pthread_create(&t1, nullptr, &handler, reinterpret_cast<void*>(1)); _TLOGE("Create 1");
	pthread_create(&t2, nullptr, &handler, reinterpret_cast<void*>(2)); _TLOGE("Create 2");

	pthread_join(t1, nullptr);
	pthread_join(t2, nullptr);

	return 0;
}