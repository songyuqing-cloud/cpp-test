#include <fcntl.h>
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */
#include <sys/stat.h>  /* Primitive System Data Types */
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string>     /* String handling */
#include <semaphore.h>  /* Semaphore */
#include <mqueue.h>
#include <time.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>

#define FIFO_NAME "my_fifo"
#define LOGI(format, ...)            printf("%s %s [%d] " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#define LOGE(format, ...)            fprintf(stderr, "%s %s [%d] " format " %s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
sem_t mSemRequest; /*!< semaphore request */
sem_t mSemRequest2; /*!< semaphore request */

void *read_thread(void *arg)
{
    LOGI("");
    // sem_wait(&mSemRequest);
    usleep(500000);
    int fd = open(FIFO_NAME, O_WRONLY |O_NONBLOCK );
    LOGE("open %d", fd);
    while(true)
    {
        // char buff[512];
        LOGI("write (%d) :", fd);
        int rs =  write(fd, "hello world", strlen("hello world"));
        LOGE("size (%d) :", rs);

        usleep(500000);
        if(rs == 0)
        {
            LOGI("");
            break;
        }
    }
    close(fd);
    return NULL;
}

void *read_thread2(void *arg)
{
    LOGI("");
    signal(SIGPIPE, SIG_IGN);
    // sem_wait(&mSemRequest2);
    int fd = open(FIFO_NAME, O_RDONLY);
    LOGE("open %d", fd);
    int count = 0;
    while(true)
    {
        char buff[512];
        int rs =  read(fd, buff, 512);
        buff[rs] = '\0';
        LOGI("%s: %d", buff, rs);
        // usleep(500000);
        if(count++ == 10)
        {
            break;
        }
        if(rs == 0)
        {
            break;
        }
    }
    close(fd);
    return NULL;
}


int main(void)
{
    LOGI("");
    int rs;
    char *buffer = new char [1764000];
    sem_init( &mSemRequest, 0, 0);
    sem_init( &mSemRequest2, 0, 0);
    pthread_t mWorker1; /*!< worker thread */
    pthread_t mWorker2; /*!< worker thread */

    unlink(FIFO_NAME);
    rs = mkfifo(FIFO_NAME, 0777);
    LOGE("mkfifo %d", rs);

    pthread_create(&mWorker1, NULL, &read_thread, NULL);
    pthread_create(&mWorker2, NULL, &read_thread2, NULL);
    // sem_post(&mSemRequest);
    // sem_post(&mSemRequest2);
    // usleep(500000);

    // int fd = 0;
    // int fd = open(FIFO_NAME, O_WRONLY | O_NONBLOCK);
    // usleep(1000000);
    // LOGE("open %d", fd);
    // int flags = fcntl(fd, F_GETFL, 0);
    // LOGI("flags %x", flags);
    // usleep(500000);

    // rs = write(fd, "hello world", strlen("hello world"));
    // LOGE("write %d", rs);
    // sem_post(&mSemRequest);
    // sem_post(&mSemRequest2);
    // usleep(5000000);

    // rs = write(fd, "HELLO", strlen("HELLO"));
    // LOGE("write %d", rs);
    // sem_post(&mSemRequest);
    // sem_post(&mSemRequest2);
    // usleep(500000);

    // LOGE("");
    // sem_post(&mSemRequest);
    // sem_post(&mSemRequest2);

    // close(fd);
    pthread_join(mWorker1, NULL);
    pthread_join(mWorker2, NULL);
    unlink(FIFO_NAME);
    // LOGE("");
    return 0;
}
