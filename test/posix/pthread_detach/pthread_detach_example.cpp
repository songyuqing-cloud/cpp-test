#include <pthread.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <sys/types.h>  /* Primitive System Data Types */
#include <string.h>     /* String handling */

#define LOGI(format, ...)            printf("%s[%s][%d] " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#define LOGE(format, ...)            fprintf(stderr, "%s[%s][%d] " format "%s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))

#define handle_error_en(en, msg) \
           do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)


class Base
{
public:

    Base()
    {
        LOGI("");
        pthread_create(&mWorker, NULL, &workerThreadEntry, this);
    }

    static void *workerThreadEntry(void *context)
    {
        LOGI("");
        return ((Base*)context)->workerThread();
    }

    virtual void *workerThread(void)
    {
        LOGI("");

    }

    pthread_t mWorker;
};

class Con1 : public Base
{
public:
    Con1() /*:Base()*/
    {
        LOGI("");

    }

    virtual void *workerThread(void)
    {
        LOGI("");
        while(1);
    }
};

class Con2 : public Base
{
public:
    Con2() /*:Base()*/
    {
        LOGI("");

    }

    virtual void *workerThread(void)
    {
        LOGI("");
        while(1);
    }
};

int main()
{
    Con1 con1;
    Con2 con2;
    while(1);
    return 0;
}

// void my_handler1(int sig)
// {
//     // printf("my_handle1: Got signal %d, tid: %lu\n",sig,pthread_self());
//     LOGI("");
//     //exit(0);
// }

// void cleanup(void *arg)
// {
//     LOGI("");
// }

// class Ex
// {
//     Ex() {}
//     ~Ex() {}
// };

// static void *
// thread_func(void *ignored_argument)
// {
//     int s;

//     /* Disable cancellation for a while, so that we don't
//        immediately react to a cancellation request */

//     // s = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
//     // if (s != 0)
//     //     handle_error_en(s, "pthread_setcancelstate");

//     // LOGI("thread_func(): started; cancellation disabled\n");
//     // sleep(5);
//     // LOGI("thread_func(): about to enable cancellation\n");

//     // s = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
//     // if (s != 0)
//     //     handle_error_en(s, "pthread_setcancelstate");

//     /* sleep() is a cancellation point */
//     LOGI("");

//     // struct sigaction my_action;
//     // my_action.sa_handler = my_handler1;
//     // my_action.sa_flags = SA_RESTART;
//     // sigaction(SIGUSR2, &my_action, NULL);

//     pthread_cleanup_push(&cleanup, NULL);

//     sleep(15);        /* Should get canceled while we sleep */
//     /* Should never get here */

//     LOGI("");
//     pthread_cleanup_pop(0);
//     return NULL;
// }

// int
// main(void)
// {
//     pthread_t thr;
//     void *res;
//     int s;

//     /* Start a thread and then send it a cancellation request */

//     s = pthread_create(&thr, NULL, &thread_func, NULL);
//     LOGE("");

//     pthread_detach(thr);
//     // {
//         LOGE("");
//     // }


//     LOGI("");
//     s = pthread_cancel(thr);
//     // s = pthread_kill(thr, SIGUSR1);
//     // if (s != 0)
//     //     LOGE("");

//     sleep(1);           /* Give thread a chance to get started */
//     /* Join with thread to see what its exit status was */

//     // s = pthread_join(thr, &res);
//     // if (s != 0)
//         LOGE("");

//     if (res == PTHREAD_CANCELED)
//         LOGI("");
//     else
//         LOGI("");
//     exit(EXIT_SUCCESS);
// }
