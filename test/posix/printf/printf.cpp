#include <cstdio>
#include <string>

int main(int arc, char **argv)
{
	int indent = std::stoi(argv[1]);
	printf("s%*s%se\n", indent, "",argv[2]);
	return 0;
}