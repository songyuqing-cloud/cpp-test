#include <cstdio>
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include <grp.h>
#include <pwd.h>
#include <cstring>

#define LOGD(format, ...) printf("[D]%s(%s:%d)" format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)

int main(int argc, char const *argv[])
{
    int pid = std::stoi(argv[1]);

    // LOGD("%d", rs);
    int rs = getpgid(pid);
    LOGD("%d", rs);
    // int rs = getpgrp(pid);

    return 0;
}
