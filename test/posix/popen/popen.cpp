#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <error.h>
#include <cassert>
#include <iostream>
// #include <stdpair>
#define LOG_EN
#include "../../../utils/Utils"

std::pair<int, std::string> exec(const std::string &cmd)
{
    std::array<char, 128> buffer;
    std::string ret_str;
    FILE *pipe = popen(cmd.data(),"r");
    if (pipe != nullptr) while (!feof(pipe)) if (fgets(buffer.data(), 128, pipe) != nullptr) ret_str += buffer.data();

    int ret_code = 0;
    if(pclose(pipe) != EXIT_SUCCESS) ret_code = 1;
    return {ret_code, ret_str};
}

int main(int, char ** argv)
{
    // auto ret = exec(argv[1]);
    // _LOGI("%d:%s", ret.first, ret.second.data());

    // char * const command[] = {"ls", "-l", "/bin/sh", NULL};
    execl("/bin/ls", "ls", "-l", NULL);

    return 0;
}