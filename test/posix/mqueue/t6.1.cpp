/*
 *  takeone.c
 *
 *  simply request a message from a queue, and displays queue
 *  attributes.
 *
 *
 *  Created by Mij <mij@bitchx.it> on 07/08/05.
 *  Original source file available on http://mij.oltrelinux.com/devel/unixprg/
 *
 */

#include <stdio.h>
/* mq_* functions */
#include <mqueue.h>
/* exit() */
#include <stdlib.h>
/* getopt() */
#include <unistd.h>
/* ctime() and time() */
#include <time.h>
/* strlen() */
#include <string.h>
#include <stdint.h>

#define LOGI(format, ...)            printf("%s[%d][%s] " format "\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__ )

#define LOGE(format, ...)            fprintf(stderr, "%s[%d][%s] " format "%s\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__, strerror(errno))
 
/* name of the POSIX object referencing the queue */
#define MSGQOBJ_NAME    /*"/myqueue123"*/ "/msgq_houndify"
/* max length of a message (just for this process) */
#define MAX_MSG_LEN     10000
#define MAX_DATA_LENGTH             0x0FF0


#include <vector>
#include <string>

class IOStream
{
public:
    IOStream() : _buffer(), _it(_buffer.begin()) {}
    ~IOStream() {}

    inline uint32_t buffSize()
    {
        return (uint32_t)_buffer.size();
    }

    inline std::vector<uint8_t>& getBuffer()
    {
        return _buffer;
    }

    inline void setBuffer(const std::vector<uint8_t>& data)
    {
        _buffer = data;
    }

    inline void clear()
    {
        _buffer.clear();
        _it = _buffer.begin();
    }

    inline void reset()
    {
        _it = _buffer.begin();
    }

    inline IOStream& operator<<(int8_t arg)
    {
        _buffer.push_back((uint8_t)arg);
        return *this;
    }

    inline IOStream& operator>>(int8_t arg)
    {
        if(_it < _buffer.end())
        {
            arg = (int8_t)(*_it);
            _it++;
        }
        return *this;
    }

    inline IOStream& operator<<(uint8_t arg)
    {
        _buffer.push_back((uint8_t)arg);
        return *this;
    }

    inline IOStream& operator>>(uint8_t arg)
    {
        if(_it < _buffer.end())
        {
            arg = (uint8_t)(*_it);
            _it++;
        }
        return *this;
    }


    inline IOStream& operator<<(int32_t arg)
    {
        uint8_t* ptr = (uint8_t*)(&arg);

        for (uint32_t i = 0; i < sizeof(arg); ++i)
        {
            _buffer.push_back(ptr[i]);
        }

        return *this;
    }

    inline IOStream& operator>>(int32_t arg)
    {
        uint8_t* ptr = (uint8_t*)(&arg);

        for (uint32_t i = 0; (_it != _buffer.end()) && (i < sizeof(arg)); ++i, ++_it)
        {
            ptr[i] = *_it;
        }

        return *this;
    }


    inline IOStream& operator<<(uint32_t arg)
    {
        uint8_t* ptr = (uint8_t*)(&arg);

        for (uint32_t i = 0; i < sizeof(arg); ++i)
        {
            _buffer.push_back(ptr[i]);
        }

        return *this;
    }

    inline IOStream& operator>>(uint32_t arg)
    {
        uint8_t* ptr = (uint8_t*)(&arg);

        for (uint32_t i = 0; (_it != _buffer.end()) && (i < sizeof(arg)); ++i, ++_it)
        {
            ptr[i] = *_it;
        }

        return *this;
    }


#ifdef __SYSTEM_X64__
    inline IOStream& operator<<(uint64_t arg)
    {
        uint8_t* ptr = (uint8_t*)(&arg);

        for (uint32_t i = 0; i < sizeof(arg); ++i)
        {
            _buffer.push_back(ptr[i]);
        }

        return *this;
    }

    inline IOStream& operator>>(uint64_t arg)
    {
        uint8_t* ptr = (uint8_t*)(&arg);

        for (uint32_t i = 0; (_it != _buffer.end()) && (i < sizeof(arg)); ++i, ++_it)
        {
            ptr[i] = *_it;
        }

        return *this;
    }

#endif

    inline IOStream& operator<<(std::vector<uint8_t> const &arg)
    {

        return *this;
    }

    inline IOStream& operator>>(std::vector<uint8_t> &arg)
    {

        return *this;
    }

    // inline IOStream& operator<<(std::string const &arg)
 //    {
 //     *this << (uint32_t)(arg.size());

 //     for (uint32_t i = 0; i < arg.size(); ++i)
    //     {
    //         _buffer.push_back(arg[i]);
    //     }
 //     return *this;
 //    }

 //    inline IOStream& operator>>(std::string &arg) const
 //    {
 //     uint32_t argLen = 0;
 //     *this >> argLen;
 //     for (uint32_t i = 0; (_it != _buffer.end()) && (i < argLen); ++i, ++_it)
    //     {
    //         arg.push_back(*_it);
    //     }

    //     return *this;
 //    }

private:
    std::vector<uint8_t>            _buffer;
    std::vector<uint8_t>::iterator  _it;
};

int main(int argc, char *argv[]) {
    mqd_t msgq_id;
    char msgcontent[MAX_MSG_LEN];
    int msgsz;
    unsigned int sender;
    struct mq_attr msgq_attr;


    /* opening the queue        --  mq_open() */
    msgq_id = mq_open(MSGQOBJ_NAME, O_RDWR);
    if (msgq_id == (mqd_t)-1) {
        perror("In mq_open()");
        exit(1);
    }

    /* getting the attributes from the queue        --  mq_getattr() */
    mq_getattr(msgq_id, &msgq_attr);
    LOGI("Queue \"%s\":\n\t- stores at most %ld messages\n\t- large at most %ld bytes each\n\t- currently holds %ld messages\n", MSGQOBJ_NAME, msgq_attr.mq_maxmsg, msgq_attr.mq_msgsize, msgq_attr.mq_curmsgs);

    /* getting a message */
    msgsz = mq_receive(msgq_id, msgcontent, MAX_MSG_LEN, &sender);
    if (msgsz == -1) {
        perror("In mq_receive()");
        exit(1);
    }
    // printf("Received message (%d bytes) from %d: %s\n", msgsz, sender, msgcontent);

    typedef struct _MsgChunk_t
    {
        int msgType;
        int clientIndex;
        std::vector<uint8_t> data;

        inline _MsgChunk_t &operator<<(IOStream &stream)
        {
            stream >> (msgType);
            stream >> clientIndex;
            stream >> data;

            return *this;
        }

        inline _MsgChunk_t &operator>>(IOStream &stream)
        {
            stream << msgType;
            stream << clientIndex;
            stream << data;

            return *this;
        }

    } MsgChunk_t;
    
    IOStream stream;
    std::vector<uint8_t> buffer = stream.getBuffer();
    buffer.reserve(msgsz);
    memcpy(&buffer[0], msgcontent, msgsz);
    MsgChunk_t msg;
    msg << stream;
    LOGI("%d %d %d", msg.clientIndex, msg.msgType, msg.data.size());

    // struct myinfo
    // {
    //     int x;
    //     uint8_t data[MAX_DATA_LENGTH];
    //     inline myinfo const &operator>>(char *ptr) const
    //     {
    //         memcpy(ptr, this, sizeof(myinfo));
    //     }
        
    //     inline myinfo const &operator<<(char *ptr)
    //     {
    //         memcpy(this, ptr, sizeof(myinfo));
    //     }
    // };
    
    // myinfo info;
    // // info = (myinfo*)msgcontent;
    // info << msgcontent;
    // LOGI("%d %s", info.x, (char*)(info.data));
    
    /* closing the queue    --  mq_close() */
    mq_close(msgq_id);


    return 0;
}
