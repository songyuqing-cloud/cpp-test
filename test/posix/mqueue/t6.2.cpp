/*
 *  dropone.c
 *
 *  drops a message into a #defined queue, creating it if user
 *  requested. The message is associated a priority still user
 *  defined
 *
 *
 *  Created by Mij <mij@bitchx.it> on 07/08/05.
 *  Original source file available on http://mij.oltrelinux.com/devel/unixprg/
 *
 */

#include <stdio.h>
/* mq_* functions */
#include <mqueue.h>
#include <sys/stat.h>
/* exit() */
#include <stdlib.h>
/* getopt() */
#include <unistd.h>
/* ctime() and time() */
#include <time.h>
/* strlen() */
#include <string.h>
#include <stdint.h>
#include <errno.h>


#define LOGI(format, ...)            printf("%s[%d][%s] " format "\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__ )

#define LOGE(format, ...)            fprintf(stderr, "%s[%d][%s] " format "%s\n", __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__, strerror(errno))

/* name of the POSIX object referencing the queue */
#define MSGQOBJ_NAME    "/myqueue123"
/* max length of a message (just for this process) */
#define MAX_MSG_LEN     70

#define MAX_DATA_LENGTH             0x0FF0

struct myinfo
    {
        int x;
        uint8_t data[255];

        inline myinfo const &operator>>(char *ptr) const
        {
            memcpy(ptr, this, sizeof(myinfo));
        }

        inline myinfo const &operator<<(char *ptr)
        {
            memcpy(this, ptr, sizeof(myinfo));
        }
    };

int main(int argc, char *argv[]) {
    mqd_t msgq_id;
    unsigned int msgprio = 0;
    pid_t my_pid = getpid();
    char msgcontent[MAX_MSG_LEN];
    int create_queue = 0;
    char ch;            /* for getopt() */
    time_t currtime;

    struct mq_attr mqattr;
    mqattr.mq_maxmsg = 2;
    mqattr.mq_msgsize = sizeof(myinfo *) - 1;
    /* accepting "-q" for "create queue", requesting "-p prio" for message priority */
    // while ((ch = getopt(argc, argv, "qi:")) != -1) {
    //     switch (ch) {
    //         case 'q':   /* create the queue */
                create_queue = argc - 1;
    //             break;
    //         case 'p':   /* specify client id */
    //             msgprio = (unsigned int)strtol(optarg, (char **)NULL, 10);
    //             printf("I (%d) will use priority %d\n", my_pid, msgprio);
    //             break;
    //         default:
    //             printf("Usage: %s [-q] -p msg_prio\n", argv[0]);
    //             exit(1);
    //     }
    // }
                msgprio = 1;
    /* forcing specification of "-i" argument */
    if (msgprio == 0) {
        printf("Usage: %s [-q] -p msg_prio\n", argv[0]);
        exit(1);
    }

    /* opening the queue        --  mq_open() */
    mq_unlink(MSGQOBJ_NAME);
    if (create_queue)
    {
        LOGI("");
        /* mq_open() for creating a new queue (using default attributes) */
        // msgq_id = mq_open(MSGQOBJ_NAME, O_RDWR | O_CREAT , S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH, NULL);
        msgq_id = mq_open(MSGQOBJ_NAME, O_RDWR | O_CREAT | O_EXCL , 0644, &mqattr);
    }
    else
    {
        /* mq_open() for opening an existing queue */
        msgq_id = mq_open(MSGQOBJ_NAME, O_RDWR);
    }

    if (msgq_id == (mqd_t)-1)
    {
        LOGE("");
        exit(1);
    }

    // mq_setattr(msgq_id, &mqattr, NULL);


    LOGI("%ld\n", sizeof(myinfo));
    /* producing the message */
    currtime = time(NULL);
    snprintf(msgcontent, MAX_MSG_LEN, "Hello from process %u (at %s).", my_pid, ctime(&currtime));

    myinfo *info = new myinfo();
    info->x = 10;
    // info.data = (void*)msgcontent;
    // info.data = (uint8_t*)malloc(sizeof(msgcontent));
    strcpy((char*)info->data, msgcontent);
    LOGI("%ld", sizeof(myinfo));
    LOGI("%s\n", (char*)info->data);
    // char buffer[0xFF];
    // info >> buffer;
    // myinfo info2;
    // info2 << buffer;
    // LOGI("%d %s", info2.x, (char*)(info2.data));
    /* sending the message      --  mq_send() */
    // mq_send(msgq_id, msgcontent, strlen(msgcontent)+1, msgprio);
    int send = mq_send(msgq_id, reinterpret_cast<char const *>(&info), sizeof(myinfo *), 0);
    LOGI("%d", send);
    mq_close(msgq_id);

    msgq_id = mq_open(MSGQOBJ_NAME, O_RDWR);
    struct mq_attr msgq_attr;
    mq_getattr(msgq_id, &msgq_attr);

    LOGI("\n\tQueue \"%s\":\n\t- stores at most %ld messages\n\t- large at most %ld bytes each\n\t- currently holds %ld messages\n", MSGQOBJ_NAME, msgq_attr.mq_maxmsg, msgq_attr.mq_msgsize, msgq_attr.mq_curmsgs);
    myinfo *in2;
    int rec = mq_receive(msgq_id, reinterpret_cast<char *>(&in2), msgq_attr.mq_msgsize, NULL);
    if(rec == -1)
    {
        LOGE("");
    }

    LOGI("%d %d %s", rec, in2->x, in2->data);
    /* closing the queue        -- mq_close() */
    mq_close(msgq_id);
    // while(1);

    return 0;
}
