#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <limits.h>
#include <errno.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <sstream>

#define LOGI(format,...) printf("%s[%s][%i] " format "\n", __FILE__, __FUNCTION__, __LINE__,##__VA_ARGS__)

int main(void)
{
    struct addrinfo *address_info_list;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    int return_code = getaddrinfo("fsoft-proxy", NULL, &hints,
                                  &address_info_list);
    LOGI("%d", return_code);
    int socket_num = socket(AF_INET, address_info_list->ai_socktype, address_info_list->ai_protocol);
    LOGI("%d", socket_num);
    
    double timeout_seconds = 5;
    struct timeval the_timeval;
        the_timeval.tv_sec = timeout_seconds;
        the_timeval.tv_usec =
            ((timeout_seconds - the_timeval.tv_sec) * 1000 * 1000);
    int result = setsockopt(socket_num, SOL_SOCKET, SO_RCVTIMEO,
                                &the_timeval, sizeof(struct timeval));
    result = setsockopt(socket_num, SOL_SOCKET, SO_SNDTIMEO, &the_timeval,
                            sizeof(struct timeval));
    
    return_code = connect(socket_num, address_info_list->ai_addr,
                              address_info_list->ai_addrlen);
    LOGI("%d", return_code);
    
    freeaddrinfo(address_info_list);
    return 0;   
}
