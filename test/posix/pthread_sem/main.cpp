/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */
#include <string>
#include <iostream>
#include <sstream>
#include <vector>

/* prototype for thread routine */
extern "C" void *handler ( void *ptr );
#define LOGI(format, ...)            printf("%s[%s][%d] " format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )

#define LOGE(format, ...)            fprintf(stderr, "%s[%s][%d] " format "%s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
/* global vars */
/* semaphores are declared global so they can be accessed
   in main() and in thread routine,
   here, the semaphore is used as a mutex */
sem_t mutex;
int counter; /* shared variable */

std::string integerToString ( int integer )
{
std::ostringstream ss;
ss << integer;
return ss.str();
}

int main()
{
    int i[6];
    pthread_t thread_a;
    pthread_t thread_b;
    // pthread_t thread_c;
    // pthread_t thread_d;
    // pthread_t thread_e;
    // pthread_t thread_f;

    i[0] = 0; /* argument to threads */
    i[1] = 1;
    i[2] = 2;
    i[3] = 3;
    i[4] = 4;
    i[5] = 5;

    sem_init(&mutex, 0, 4);      /* initialize mutex to 1 - binary semaphore */
                                 /* second param = 0 - semaphore is local */

    /* Note: you can check if thread has been successfully created by checking return value of
       pthread_create */
    pthread_create (&thread_a, NULL, /*(void *)*/ &handler, (void *) &i[0]);
    pthread_create (&thread_b, NULL, /*(void *)*/ &handler, (void *) &i[1]);
    // pthread_create (&thread_c, NULL, /*(void *)*/ &handler, (void *) &i[2]);
    // pthread_create (&thread_d, NULL, /*(void *)*/ &handler, (void *) &i[3]);
    // pthread_create (&thread_e, NULL, /*(void *)*/ &handler, (void *) &i[4]);
    // pthread_create (&thread_f, NULL, /*(void *)*/ &handler, (void *) &i[5]);

    pthread_join(thread_a, NULL);
    pthread_join(thread_b, NULL);
    // pthread_join(thread_c, NULL);
    // pthread_join(thread_d, NULL);
    // pthread_join(thread_e, NULL);
    // pthread_join(thread_f, NULL);

    sem_destroy(&mutex); /* destroy semaphore */
    std::string msg = "type ";
    msg += integerToString(10);
    msg += " index ";
    msg += integerToString(20);
    LOGI("%s", msg.c_str());

    std::vector<int> v(10);
    v.push_back(1);
    LOGI("%d", v.size());

    char buffer[0xFF];
    sprintf(buffer, "type %d index %d", 10, 20);
    LOGI("%s", buffer);
    LOGI("%d %d", strlen(buffer), msg.size());

    int d = 10;
    LOGI("%llx - %lld - %ld - %p - %lp %llp", &d, &d, &d, &d, &d, &d);
    char *x = (char *)&d;
    LOGI("%d", *(int *)(x) );

    /* exit */
    exit(0);
} /* main() */

// extern "C"
void *handler ( void *ptr )
{
    int x;
    x = *((int *) ptr);
    printf("Thread %d: Waiting to enter critical region...\n", x);
    sem_wait(&mutex);       /* down semaphore */
    /* START CRITICAL REGION */
    printf("Thread %d: Now in critical region...\n", x);
    printf("Thread %d: Counter Value: %d\n", x, counter);
    printf("Thread %d: Incrementing Counter...\n", x);
    counter++;
    printf("Thread %d: New Counter Value: %d\n", x, counter);
    printf("Thread %d: Exiting critical region...\n", x);
    /* END CRITICAL REGION */
    sem_post(&mutex);       /* up semaphore */


    pthread_exit(0); /* exit thread */
    return NULL;
}

