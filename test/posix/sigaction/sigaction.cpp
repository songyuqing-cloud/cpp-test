#include <signal.h>

#include "../../../utils/Utils"

int main()
{
    // prepare internal signal handler
    struct sigaction sig;
    sig.sa_sigaction = nullptr;
    sig.sa_flags = SA_SIGINFO;
    _LOGI("%d", sigaction(SIGKILL, &sig, NULL));

     return 0;
}