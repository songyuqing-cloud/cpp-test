// #include <LogMngr.hpp>

#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <cerrno>

#include <sys/syscall.h>
#include <sys/select.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h> // close
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/*
* File server.c
*/

#include <thread>
#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
#include <vector>
#include <atomic>

#define _TLOGI(format, ...) printf("[I]%s %s t-%d [%d] " format "\n",__FILE__, __PRETTY_FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )
#define _TLOGE(format, ...) fprintf(stderr, "[E]%s %s t-%d [%d] " format ": %s\n", __FILE__, __PRETTY_FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__, strerror(errno))

const std::string SOCKET_NAME = "/tmp/tll.svr.dgram";
const std::string CLT = "/tmp/tll.clt.dgram";

const char *DISP_UNIT = "Mb";
const int DIVIDER = 0x100000;

int svrInit()
{
    int fd;
    sockaddr_un sockAddr;

    // unlink(SOCKET_NAME.data());

    /* Create local socket. */

    fd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (fd == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

   memset(&sockAddr, 0, sizeof(sockaddr_un));

    /* Bind socket to socket name. */

    sockAddr.sun_family = AF_UNIX;
    strncpy(sockAddr.sun_path, SOCKET_NAME.data(), sizeof(sockAddr.sun_path) - 1);

    int ret = bind(fd, (const sockaddr *) &sockAddr,
            sizeof(sockaddr_un));
    if (ret == -1) {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    // set non-blocking io
    if ( fcntl( fd, F_SETFL, O_NONBLOCK, 1 ) == -1 )
    {
        printf( "failed to make socket non-blocking\n" );
        return -1;
    }

    return fd;
}

int cltInit(sockaddr_un &sockAddr)
{
    int fd = socket(AF_UNIX, SOCK_DGRAM, 0);
    
    sockAddr.sun_family = AF_UNIX;
    strncpy(sockAddr.sun_path, SOCKET_NAME.data(), sizeof(sockAddr.sun_path) - 1);
    // memset(&sockAddr, 0, sizeof(sockaddr_un));

    // /* Bind socket to socket name. */

    // sockAddr.sun_family = AF_UNIX;
    // strncpy(sockAddr.sun_path, SOCKET_NAME.data(), sizeof(sockAddr.sun_path) - 1);

    // int ret = bind(fd, (const sockaddr *) &sockAddr,
    //         sizeof(sockaddr_un));

    // set non-blocking io
    if ( fcntl( fd, F_SETFL, O_NONBLOCK, 1 ) == -1 )
    {
        printf( "failed to make socket non-blocking\n" );
        return -1;
    }

    return fd;
}

static const ssize_t BUFF_SIZE = 0x100;
static const ssize_t WSIZE = 0x400;
static const ssize_t RSIZE = 0x400;

int main()
{
    unlink(SOCKET_NAME.data());

    ssize_t svrBW=0, cltBW=0;
    std::atomic<bool> running(true);
    typedef std::chrono::high_resolution_clock hrc;
    hrc::time_point start = hrc::now();

    // RingBuff rb(BUFF_SIZE);
    int cnt = 0;

    std::thread svr([&svrBW, &running, &cnt]()
    {
        sockaddr_in client;
        int client_address_size = sizeof(client);
        std::vector<char> buff;
        buff.resize(WSIZE);
        _TLOGI("WRITE Started");

        int fd = svrInit();

        while(fd >= 0 && running.load(std::memory_order_relaxed))
        {
            ssize_t size = recvfrom(fd, buff.data(), buff.size(), 0, (sockaddr *) &client,
                (socklen_t*)&client_address_size);

            if(size > 0)
            {
                _TLOGI("Received from (%s:%d): %ld", inet_ntoa(client.sin_addr), ntohs(client.sin_port), size);
                svrBW += size;

            }

            std::this_thread::yield();
            // std::this_thread::sleep_for(std::chrono::microseconds(5));
        }
        _TLOGI("WRITE end !!!");
    });

    std::thread clts[2];
    for(auto &clt : clts)
        clt = std::thread([&cltBW, &running, &cnt]() {
            sockaddr_un sockAddr;
            memset(&sockAddr, 0, sizeof(sockaddr_un));
            std::vector<char> buff;
            buff.resize(RSIZE);
            int fd = cltInit(sockAddr);
            _TLOGI("READ Started");
            
            while(fd >= 0 && running.load(std::memory_order_relaxed))
            {
                memset(buff.data(), cnt, buff.size());
                ssize_t size = sendto( fd, buff.data(), buff.size(), 0, (sockaddr *)&sockAddr, sizeof(sockaddr_un) );
                if(size > 0)
                {
                    // rb.pop(size);
                    cltBW += size;
                    cnt++;
                }

                // std::this_thread::yield();
                std::this_thread::sleep_for(std::chrono::microseconds(1));
            }
            _TLOGI("READ end !!!");
        });

    std::thread statistic([&svrBW, &cltBW, &running, &start, &cnt]() {
        hrc::time_point mark = hrc::now();

        while(running.load(std::memory_order_relaxed))
        {
            if(std::chrono::duration_cast<std::chrono::duration<ssize_t, std::ratio<1, 1000>>> (hrc::now() - mark).count() < 1000) 
                continue;
            
            double time = std::chrono::duration <double, std::ratio<1,1>> (hrc::now() - start).count();
            double svrBw = (1.f * svrBW) / DIVIDER;
            double cltBw = (1.f * cltBW) / DIVIDER;

            printf("%.2fs\n", time);
            printf(" Svr: %.2f (%s) : %.2f (%ss)\n", svrBw, DISP_UNIT, svrBw / time, DISP_UNIT);
            printf(" Clt: %.2f (%s) : %.2f (%ss)\n", cltBw, DISP_UNIT, cltBw / time, DISP_UNIT);
            mark = hrc::now();
        }
    });

    std::this_thread::sleep_for(std::chrono::microseconds(100000));
    running.store(false, std::memory_order_relaxed);

    svr.join();
    // clt.join();
    for(auto &clt : clts)
        clt.join();
    statistic.join();

    return 0;
}