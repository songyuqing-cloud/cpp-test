#include <cstdio>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <sstream>
#include <unordered_set>
#include <signal.h>
#include <unistd.h>

#include "../../utils.h"

int main(int argc, char const *argv[])
{
    LOGD("Parent: %d", getpid());
    pid_t cid = fork();
    if(cid == 0) /// child
    {
        LOGD("Child: %d", getpid());
        std::this_thread::sleep_for(std::chrono::seconds(60));
    }
    else
    {
        LOGD("%d", cid);
        LOGD("Parent: %d", getpid());
        std::this_thread::sleep_for(std::chrono::seconds(10));
        kill(cid, SIGKILL);
        LOGE("");
    }
    return 0;
}

