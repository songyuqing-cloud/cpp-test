#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <sched.h>
#include <unistd.h>
#include <sys/syscall.h>
// #define gettid() syscall(SYS_gettid)

#include <iostream>
#include <thread>
#include <chrono>
#include <functional>

#include "../../utilsmacro.h"

#define handle_error_en(en, msg) \
               do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

static const int MAX_THREAD = 10;
static const int LOOP_CNT = 100000;
static const int TARGET = 2;

static bool sFinished = false;

void paramInfo()
{
    int lPolicy;
    sched_param lParam;
    int rs = pthread_getschedparam(pthread_self(), &lPolicy, &lParam);
    // _TLOGE("");
    _TLOGI("policy: %d", lPolicy);
    _TLOGI("prio: %d", lParam.sched_priority);
}

void prioInfo()
{
    int lSchedPrioMin, lSchedPrioMax;
    _TLOGI("%d %d %d", SCHED_OTHER, SCHED_FIFO, SCHED_RR);
    lSchedPrioMin=sched_get_priority_min(SCHED_RR);
    lSchedPrioMax=sched_get_priority_max(SCHED_RR);
    _TLOGI("%d %d", lSchedPrioMin, lSchedPrioMax);
}

static void
display_pthread_attr(pthread_attr_t *attr, char *prefix)
{
   int s, i;
   size_t v;
   void *stkaddr;
   struct sched_param sp;

   s = pthread_attr_getdetachstate(attr, &i);
   if (s != 0)
       handle_error_en(s, "pthread_attr_getdetachstate");
   _TLOGI("%sDetach state        = %s", prefix,
           (i == PTHREAD_CREATE_DETACHED) ? "PTHREAD_CREATE_DETACHED" :
           (i == PTHREAD_CREATE_JOINABLE) ? "PTHREAD_CREATE_JOINABLE" :
           "???");

   s = pthread_attr_getscope(attr, &i);
   if (s != 0)
       handle_error_en(s, "pthread_attr_getscope");
   _TLOGI("%sScope               = %s", prefix,
           (i == PTHREAD_SCOPE_SYSTEM)  ? "PTHREAD_SCOPE_SYSTEM" :
           (i == PTHREAD_SCOPE_PROCESS) ? "PTHREAD_SCOPE_PROCESS" :
           "???");

   s = pthread_attr_getinheritsched(attr, &i);
   if (s != 0)
       handle_error_en(s, "pthread_attr_getinheritsched");
   _TLOGI("%sInherit scheduler   = %s", prefix,
           (i == PTHREAD_INHERIT_SCHED)  ? "PTHREAD_INHERIT_SCHED" :
           (i == PTHREAD_EXPLICIT_SCHED) ? "PTHREAD_EXPLICIT_SCHED" :
           "???");

   s = pthread_attr_getschedpolicy(attr, &i);
   if (s != 0)
       handle_error_en(s, "pthread_attr_getschedpolicy");
   _TLOGI("%sScheduling policy   = %s", prefix,
           (i == SCHED_OTHER) ? "SCHED_OTHER" :
           (i == SCHED_FIFO)  ? "SCHED_FIFO" :
           (i == SCHED_RR)    ? "SCHED_RR" :
           "???");

   s = pthread_attr_getschedparam(attr, &sp);
   if (s != 0)
       handle_error_en(s, "pthread_attr_getschedparam");
   _TLOGI("%sScheduling priority = %d", prefix, sp.sched_priority);

   s = pthread_attr_getguardsize(attr, &v);
   if (s != 0)
       handle_error_en(s, "pthread_attr_getguardsize");
   _TLOGI("%sGuard size          = %zu bytes", prefix, v);

   s = pthread_attr_getstack(attr, &stkaddr, &v);
   if (s != 0)
       handle_error_en(s, "pthread_attr_getstack");
   _TLOGI("%sStack address       = %p", prefix, stkaddr);
   _TLOGI("%sStack size          = 0x%zx bytes", prefix, v);
}

static void *sHandler(void *arg)
{
    _TLOGI("%ld Start", reinterpret_cast<size_t>(arg));
    if(reinterpret_cast<size_t>(arg) == TARGET) paramInfo();
    int i;
    pthread_attr_t lAttr;

    // pthread_getattr_np(pthread_self(), &lAttr);
    // display_pthread_attr(&lAttr, "\t");

    for(i=0; !sFinished && i<LOOP_CNT; i++)
    {
        std::this_thread::yield();
        // std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    sFinished = true;
    _TLOGI("%ld End: %d", reinterpret_cast<size_t>(arg), i);
    return 0;
}



int main()
{
    _LOGE("");

    pthread_t lTs[MAX_THREAD];
    pthread_attr_t lAttr;
    int lPolicy=0;
    sched_param lParam;

    int rs;

    memset(&lParam, 0, sizeof(lParam));
    memset(&lAttr, 0, sizeof(lAttr));
    rs = pthread_attr_init(&lAttr);
    _TLOGE("");

    lPolicy = SCHED_RR;
    lParam.sched_priority = 50;
    rs = pthread_setschedparam(pthread_self(), lPolicy, &lParam);
    _TLOGE("");

    prioInfo();
    paramInfo();
    lParam.sched_priority = 99;

    // rs = pthread_getschedparam(pthread_self(), &lPolicy, &lParam);
    // _TLOGE("");
    // _TLOGI("policy: %d", lPolicy);
    // _TLOGI("prio: %d", lParam.sched_priority);
    pthread_attr_setstacksize(&lAttr, 0x400000);_TLOGE("");
    pthread_attr_setschedpolicy(&lAttr, lPolicy); _TLOGE("");
    pthread_attr_setschedparam(&lAttr, &lParam); _TLOGE("");
    pthread_attr_setinheritsched(&lAttr, PTHREAD_EXPLICIT_SCHED); _TLOGE("");

    for(int i=0; i<MAX_THREAD; i++)
    {
        if(i == TARGET) 
        {   
            // display_pthread_attr(&lAttr, "\t");
            pthread_create(&lTs[i], &lAttr, &sHandler, reinterpret_cast<void*>(i));
            // _TLOGE("");
        }
        pthread_create(&lTs[i], NULL, &sHandler, reinterpret_cast<void*>(i));
    }

    for(int i=0; i<MAX_THREAD; i++)
    {
        pthread_join(lTs[i], NULL);
    }

    rs = pthread_attr_destroy(&lAttr);
    _TLOGE("");

    return 0;
}
