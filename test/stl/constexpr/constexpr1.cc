#include<iostream> 
#include "constexpr.h"

using namespace std; 

constexpr long int fib(int n)
{ 
    return (n <= 1)? n : fib(n-1) + fib(n-2);
}

void echopi();

int main (int argc, char **argv)
{ 
    // value of res is computed at compile time.  
    const long int res = fib(30); 
    cout << res << endl;

    echopi();
    printf("%.2f\n", pi);

    return 0; 
} 