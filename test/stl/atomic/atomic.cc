#include <cstdio>
#include <memory>
#include <atomic>
#include <thread>
// #include <mutex>
#include <omp.h>
#include "../../utils.h"
#include "../../timer.h"

struct Obj
{

Obj() {/*std::this_thread::sleep_for(std::chrono::milliseconds(1000)); LOGD("%p", this);*/}
~Obj() {}

void foo()
{
    LOGD("%p", this);
}

static Obj &instance1()
{
    static std::atomic<Obj*> singleton{nullptr};
    static std::atomic<bool> init{false};
    bool tmp = false;

    if (singleton.load(std::memory_order_relaxed))
        return *singleton.load(std::memory_order_acquire);

    if(!init.compare_exchange_strong(tmp, true))
    {
        while(!singleton.load(std::memory_order_relaxed)){}
    }
    else
    {
        singleton.store(new Obj{}, std::memory_order_release);
    }

    return *singleton.load(std::memory_order_acquire);
}

static Obj &instance2()
{
    static std::atomic<Obj*> singleton{nullptr};
    static std::atomic<bool> init{false};
    bool tmp = false;

    // if (singleton.load(std::memory_order_relaxed))
    //     return *singleton.load(std::memory_order_acquire);

    if(!init.compare_exchange_strong(tmp, true))
    {
        while(!singleton.load(std::memory_order_relaxed)){}
    }
    else
    {
        singleton.store(new Obj{}, std::memory_order_release);
    }

    return *singleton.load(std::memory_order_acquire);
}

};

int main(int argc, char const *argv[])
{
    // LOGD("");
    tll::time::List<> timer;
    #pragma omp parallel num_threads ( 4 )
    {
        for(int i=0; i<1000000; i++) {auto &instance = Obj::instance1(); /*LOGD("%p", &instance); *//*instance.foo();*/}
    }
    LOGD("%.9f",timer().restart().last().count());
    #pragma omp parallel num_threads ( 4 )
    {
        for(int i=0; i<1000000; i++) {auto &instance = Obj::instance2(); /*LOGD("%p", &instance); *//*instance.foo();*/}
    }
    LOGD("%.9f",timer().restart().last().count());
    return 0;
}