#include <unordered_set>
#include <thread>
#include <chrono>
#include <cstdio>

int main(int argc, char const *argv[])
{
    std::unordered_set<int> set;
    std::thread t1, t2;
    // set.insert(1);

    t1 = std::thread([&]()
    {
        while(true)
        {
            for(auto val : set) printf("%d ", val);
            printf("\n");
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    });

    t2 = std::thread([&]()
    {
        while(true)
        {
            static int i=1;
            if(i%5==0) set.erase(i-1); else set.insert(i);
            i++;
            std::this_thread::sleep_for(std::chrono::milliseconds(20));
        }
    });

    t1.join();
    t2.join();

    return 0;
}
