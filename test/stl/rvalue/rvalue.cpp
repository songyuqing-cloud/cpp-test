#include <iostream>
#include <iomanip>
#include <memory>
#include <utility>
#include <array>
#include <vector>
#include <string>
#include <cstring>

#define LOG_EN

#include "../../../utils/Utils"

using namespace std;

struct Snitch {   // Note: All methods have side effects
  Snitch() { cout << "c'tor" << endl; }
  ~Snitch() { cout << "d'tor" << endl; }

  Snitch(const Snitch&) { cout << "copy c'tor" << endl; }
  Snitch(Snitch&&) { cout << "move c'tor" << endl; }

  Snitch& operator=(const Snitch&) {
    cout << "copy assignment" << endl;
    return *this;
  }

  Snitch& operator=(Snitch&&) 
  {
    cout << "move assignment" << endl;
    return *this;
  }

  int val;

};

Snitch gsnitch;


Snitch ExampleRVO() 
{
    _LOGD("");
    return Snitch();
}

Snitch NExampleRVO() 
{
    _LOGD("");
    Snitch ret;
    ret.val = 100;

    return ret;
}

template <class T>
Snitch ReturnParameter(T &&snitch)
{
    _LOGD("");
  return std::forward<T>(snitch);
}

template<typename T> constexpr T rvalue_cast(T&& t) { return std::move(t); }

int main() 
{
  _LOGD(""); {Snitch snitch(ExampleRVO());} _LOGD("");
  _LOGD(""); {Snitch snitch(NExampleRVO());} _LOGD("");
  _LOGD(""); {Snitch snitch; snitch = ExampleRVO();} _LOGD("");
  _LOGD(""); {Snitch snitch1, snitch2; snitch1 = std::move(snitch2);} _LOGD("");
  _LOGD(""); {Snitch snitch(ReturnParameter(ExampleRVO()));} _LOGD("");
  _LOGD(""); {Snitch snitch(ReturnParameter(gsnitch));} _LOGD("");

  return 0;
}

