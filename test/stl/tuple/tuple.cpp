#include <unordered_map>
#include <string>
#include <tuple>
#include <thread>

// typedef std::tuple<int, char> my_key_t;
typedef std::tuple<pid_t, std::thread::id> my_key_t;

struct key_hash : public std::unary_function<my_key_t, std::size_t>
{
    std::size_t operator()(const my_key_t& k) const
    {
        return std::get<0>(k) ^ std::hash<std::thread::id>()(std::get<1>(k));
    }
};

int main()
{
    std::unordered_map<my_key_t, std::string, key_hash> map;



    return 0;
}
