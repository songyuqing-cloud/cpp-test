#include <cstdio>
#include <typeinfo>
// #define LOGD(format, ...) printf("%s(%s:%d)" format "\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)

#include <thread>
#include <mutex>
#include <condition_variable>

#include <chrono>

#include "../../utils.h"

bool ready=false;
bool start=false;

int main(int argc, char const *argv[])
{
    using namespace std;
    condition_variable cv, startcv;
    mutex mtx; 
    thread t1([&cv,&mtx]{
        
        TIMER(thread_1);
        unique_lock<mutex> lock(mtx);
        {
            cv.wait(lock, []{return start;});
        }

        {
            // unique_lock<mutex> lock(mtx);
            bool status = cv.wait_for(lock, chrono::seconds(1), []{return ready;});
            LOGD("%d", status);
        }
    });

    thread t2([&cv,&mtx]{

        TIMER(thread_2);
        unique_lock<mutex> lock(mtx);
        {
            cv.wait(lock, []{return start;});
        }

        {
            // std::this_thread::sleep_for(std::chrono::microseconds(1));
            // lock_guard<mutex> lock(mtx);
            ready=true;
            cv.notify_all();
        }
    });

    std::this_thread::sleep_for(std::chrono::microseconds(1));
    {
        lock_guard<mutex> lock(mtx);
        start=true;
        cv.notify_all();
    }

    t1.join();
    t2.join();

    return 0;
}