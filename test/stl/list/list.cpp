#include <map>
#include <list>
// #include <vector>
#include <iostream>

#include "../../utils.h"

template <typename T>
class StdList : private std::list<T>
{
public:
    using container_ = std::list<T>;
    using container_::size;

    auto begin() noexcept -> decltype(container_::begin()) {TRACE();
        return container_::begin();
    }
    auto begin() const noexcept -> decltype(container_::cbegin()) {TRACE();
        return container_::cbegin();
    }
    auto cbegin() const noexcept -> decltype(container_::cbegin()) {TRACE();
        return container_::cbegin();
    }
    auto end() noexcept -> decltype(container_::end()) {TRACE();
        return container_::end();
    }
    auto end() const noexcept -> decltype(container_::cend()) {TRACE();
        return container_::cend();
    }
    auto cend() const noexcept -> decltype(container_::cend()) {TRACE();
        return container_::cend();
    }

    template <class... Args>
    void emplace_back (Args&&... args) {TRACE();
        container_::emplace_back(std::forward<Args>(args)...);
    }
    void push_back(const T &t) {TRACE();
        container_::push_back(t);
    }
    typename container_::iterator erase (typename container_::iterator position) {TRACE();
        return container_::erase(position);
    }
    typename container_::iterator erase (typename container_::const_iterator first, typename container_::const_iterator last) {TRACE();
        return container_::erase(first, last);
    }

    typename container_::iterator insert (typename container_::const_iterator position, const T& val){TRACE();
        return container_::insert(position, val);
    }
    void insert (typename container_::iterator position, size_t n, const T& val) {TRACE();
        container_::insert(position, n, val);
    }
    template <class InputIterator>
    void insert (typename container_::iterator position, InputIterator first, InputIterator last) {TRACE();
        container_::insert(position, first, last);
    }
    void remove (const T& val) {TRACE();
        container_::remove(val);
    }
    template <class Predicate>
    void remove_if (Predicate pred) {
        container_::remove_if(pred);
    }

    StdList() = default;
    virtual ~StdList() = default;
    template <class InputIterator>
    StdList (InputIterator first, InputIterator last) : container_{first, last}
    {}
    StdList (std::initializer_list<T> il) : container_{il} {}
};

// template < class Key, class T >
// class StdMap : public std::map<Key, T> {
// public:
//     using container_ = std::map<Key, T>;
//     find
//     begin
//     end
//     insert
//     emplace
//     remove_if
//     erase
//     clear
//     empty
//     operator[]

//     StdMap() = default;
//     virtual ~StdMap() = default;
//     template <class InputIterator>
//     StdMap (InputIterator first, InputIterator last) : container_{first, last}
//     {}
//     StdMap (std::initializer_list<T> il) : container_{il} {}
// };

int main(int argc, char const *argv[])
{
    StdList<int> mylist;

    mylist.push_back(1);
    mylist.push_back(2);
    mylist.push_back(3);

    mylist.remove(2);
    mylist.insert(mylist.begin(), 3);



    return 0;
}