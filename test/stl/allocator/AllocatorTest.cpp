#include <memory>
#include <iostream>
#include <string>
#include <vector>
#include <list>

#include "../../utilsmacro.h"
#include "../../../utils/MyCounter.h"

#include <cstdlib>
#include <iostream>

// clone from https://docs.roguewave.com/sourcepro/11.1/html/toolsug/11-6.html
template <class T>
class my_allocator
{
public:
  typedef size_t    size_type;
  typedef ptrdiff_t difference_type;
  typedef T*        pointer;
  typedef const T*  const_pointer;
  typedef T&        reference;
  typedef const T&  const_reference;
  typedef T         value_type;

  my_allocator() {}
  my_allocator(const my_allocator&) {}

  pointer   allocate(size_type n, const void * = 0) {
              T* t = (T*) malloc(n * sizeof(T));
              _LOGD("");
              return t;
            }
  
  void      deallocate(void* p, size_type) {
              if (p) {
                free(p);
                _LOGD("");
              } 
            }

  pointer           address(reference x) const { return &x; }
  const_pointer     address(const_reference x) const { return &x; }
  my_allocator<T>&  operator=(const my_allocator&) { return *this; }
  void              construct(pointer p, const T& val) 
                    { new ((T*) p) T(val); }
  void              destroy(pointer p) { p->~T(); }

  size_type         max_size() const { return size_t(-1); }

  template <class U>
  struct rebind { typedef my_allocator<U> other; };

  template <class U>
  my_allocator(const my_allocator<U>&) {}

  template <class U>
  my_allocator& operator=(const my_allocator<U>&) { return *this; }
};

int main()
{
    std::list<int, my_allocator<int>> dll;
    dll.push_front(100);
    dll.push_front(200);

    std::cout << dll.size() << std::endl;

    return 0;
}