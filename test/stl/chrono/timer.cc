#include <thread>
#include <chrono>
#include <cstdio>
#include <omp.h>
#include <atomic>

int main(int argc, char const *argv[])
{
    using namespace std::chrono;

    auto begin = steady_clock::now();
    std::atomic<size_t> total{0};
    std::atomic<size_t> count{0};
    #pragma omp parallel num_threads ( 3 )
    {
        auto start = steady_clock::now();
        for(int i=0; i<1000000; i++) 
            {
                count.fetch_add(1, std::memory_order_relaxed);
                // std::this_thread::yield();
            }
        size_t diff = duration_cast<duration<size_t, std::ratio<1,1000000000000>>>(steady_clock::now() - start).count();
        total.fetch_add(diff, std::memory_order_relaxed);
    }

    size_t org_diff = duration<size_t, std::ratio<1, 1000000000000>>(steady_clock::now() - begin).count();
    size_t diff = (duration<size_t, std::ratio<1,1000000000000>>(total.load())).count();
    printf("count       : %ld\n", count.load());
    printf("org_diff    : %ld\n", org_diff);
    printf("diff        : %ld\n", diff);
    printf("org/count   : %ld\n", org_diff/count.load());
    printf("diff/count  : %ld\n", diff/count.load());
    printf("diff/3/count: %ld\n", diff/count.load()/3);

    return 0;
}
