#include <string>
#include <cstdio>
#include <chrono>

using namespace std::chrono;

int main(int argc, char const *argv[])
{

	// steady_clock::time_point now = steady_clock::now();
	// ssize_t tp = time_point_cast<milliseconds>(now).time_since_epoch().count();
	ssize_t system = time_point_cast<microseconds>(system_clock::now()).time_since_epoch().count();
	ssize_t steady = time_point_cast<microseconds>(steady_clock::now()).time_since_epoch().count();
	ssize_t highr = time_point_cast<microseconds>(high_resolution_clock::now()).time_since_epoch().count();

	printf("%ld\n", system);
	printf("%ld\n", steady);
	printf("%ld\n", highr);

	// using namespace std::chrono;
	using hrc = std::chrono::high_resolution_clock;

	hrc::time_point now = hrc::now();

	duration<int64_t, std::nano> d1 = hrc::now() - now;
	// auto d2 = std::chrono::duration_cast<std::chrono::milliseconds>(d1);
	auto d2 = std::chrono::duration_cast<duration<int, std::ratio<1,1000>>>(d1);

	// auto d2 = duration_cast<double, std::milli>(d1);
	// duration<int, std::ratio<1,10000>> d3 = d2;
	std::chrono::duration_cast<duration<int, std::ratio<1,1000>>>(d2);
	// int nano = std::chrono::duration<int64_t, std::nano>(t).count();

	return 0;
}