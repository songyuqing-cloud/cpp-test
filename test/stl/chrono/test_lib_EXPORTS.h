
#ifndef test_lib_EXPORTS_H
#define test_lib_EXPORTS_H

#ifdef SHARED_EXPORTS_BUILT_AS_STATIC
#  define test_lib_EXPORTS
#  define TEST_LIB_NO_EXPORT
#else
#  ifndef test_lib_EXPORTS
#    ifdef test_lib_EXPORTS
        /* We are building this library */
#      define test_lib_EXPORTS __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define test_lib_EXPORTS __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef TEST_LIB_NO_EXPORT
#    define TEST_LIB_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef TEST_LIB_DEPRECATED
#  define TEST_LIB_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef TEST_LIB_DEPRECATED_EXPORT
#  define TEST_LIB_DEPRECATED_EXPORT test_lib_EXPORTS TEST_LIB_DEPRECATED
#endif

#ifndef TEST_LIB_DEPRECATED_NO_EXPORT
#  define TEST_LIB_DEPRECATED_NO_EXPORT TEST_LIB_NO_EXPORT TEST_LIB_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef TEST_LIB_NO_DEPRECATED
#    define TEST_LIB_NO_DEPRECATED
#  endif
#endif

#endif
