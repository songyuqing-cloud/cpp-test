#include <chrono>

#include <utils/Utils>

#include "test_lib.h"

int main()
{
    _LOGD("");
    typedef std::chrono::high_resolution_clock      __hrc;

    __hrc::time_point _l_beg;
    __hrc::time_point _l_end;

    _l_beg = __hrc::now();

    custom::MyCounter::sleep(10);

    _l_end = __hrc::now();

    _LOGD("%d", _l_beg < _l_end);
    _LOGD("%d", _l_beg > _l_end);

    std::chrono::duration<double, std::ratio<60, 1>> _l_time_frame(std::chrono::seconds(1));
    std::chrono::seconds sec(1);

    _LOGD("%.2f", _l_time_frame.count());

    test();

    // __hrc::time_point _l_temp = _l_end + std::chrono::seconds(1);

    return 0;
}
