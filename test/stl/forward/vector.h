



  template<typename _Alloc>
  struct __alloc_traits
#if __cplusplus >= 201103L
    : std::allocator_traits<_Alloc>
#endif
  {
    typedef _Alloc allocator_type;
    typedef std::allocator_traits<_Alloc>           _Base_type;
    typedef typename _Base_type::value_type         value_type;
    typedef typename _Base_type::pointer            pointer;
    typedef typename _Base_type::const_pointer      const_pointer;
    typedef typename _Base_type::size_type          size_type;
    typedef typename _Base_type::difference_type    difference_type;
    // C++11 allocators do not define reference or const_reference
    typedef value_type                             &reference;
    typedef const value_type                       &const_reference;
    using _Base_type::allocate;
    using _Base_type::deallocate;
    using _Base_type::construct;
    using _Base_type::destroy;
    using _Base_type::max_size;

  private:
    template<typename _Ptr>
    using __is_custom_pointer
    = std::__and_<std::is_same<pointer, _Ptr>,
    std::__not_<std::is_pointer<_Ptr>>>;

  public:
    // overload construct for non-standard pointer types
    template<typename _Ptr, typename... _Args>
    static typename std::enable_if<__is_custom_pointer<_Ptr>::value>::type
    construct(_Alloc &__a, _Ptr __p, _Args &&... __args)
    {
      _Base_type::construct(__a, std::addressof(*__p),
                            std::forward<_Args>(__args)...);
    }

    // overload destroy for non-standard pointer types
    template<typename _Ptr>
    static typename std::enable_if<__is_custom_pointer<_Ptr>::value>::type
    destroy(_Alloc &__a, _Ptr __p)
    {
      _Base_type::destroy(__a, std::addressof(*__p));
    }

    static _Alloc _S_select_on_copy(const _Alloc &__a)
    {
      return _Base_type::select_on_container_copy_construction(__a);
    }

    static void _S_on_swap(_Alloc &__a, _Alloc &__b)
    {
      std::__alloc_on_swap(__a, __b);
    }

    static constexpr bool _S_propagate_on_copy_assign()
    {
      return _Base_type::propagate_on_container_copy_assignment::value;
    }

    static constexpr bool _S_propagate_on_move_assign()
    {
      return _Base_type::propagate_on_container_move_assignment::value;
    }

    static constexpr bool _S_propagate_on_swap()
    {
      return _Base_type::propagate_on_container_swap::value;
    }

    static constexpr bool _S_always_equal()
    {
      return _Base_type::is_always_equal::value;
    }

    static constexpr bool _S_nothrow_move()
    {
      return _S_propagate_on_move_assign() || _S_always_equal();
    }

    template<typename _Tp>
    struct rebind
    {
      typedef typename _Base_type::template rebind_alloc<_Tp> other;
    };
  };


























template<typename _Tp, typename _Alloc>
class vector
{
public:
    typedef typename __alloc_traits<_Alloc>::template rebind<_Tp>::other 
        _Tp_alloc_type;
        
    typedef typename __alloc_traits<_Tp_alloc_type>::pointer
        pointer;
        
    struct _Vector_impl
      : public _Tp_alloc_type
    {
      pointer _M_start;  
      pointer _M_finish;
      pointer _M_end_of_storage;

      _Vector_impl()
        : _Tp_alloc_type(), _M_start(), _M_finish(), _M_end_of_storage()
      { _LOGI(""); }

      _Vector_impl(_Tp_alloc_type const &__a) _GLIBCXX_NOEXCEPT
:
      _Tp_alloc_type(__a), _M_start(), _M_finish(), _M_end_of_storage()
      { _LOGI(""); }

#if __cplusplus >= 201103L
      _Vector_impl(_Tp_alloc_type &&__a) noexcept
        : _Tp_alloc_type(std::move(__a)),
          _M_start(), _M_finish(), _M_end_of_storage()
      { _LOGI(""); }
#endif

      void _M_swap_data(_Vector_impl &__x) _GLIBCXX_NOEXCEPT
      {
        std::swap(_M_start, __x._M_start);
        std::swap(_M_finish, __x._M_finish);
        std::swap(_M_end_of_storage, __x._M_end_of_storage);
      }
    };
        
  public:
    typedef _Alloc allocator_type;

    _Tp_alloc_type &
    _M_get_Tp_allocator() _GLIBCXX_NOEXCEPT
    { return *static_cast<_Tp_alloc_type *>(&this->_M_impl); }

    const _Tp_alloc_type &
    _M_get_Tp_allocator() const _GLIBCXX_NOEXCEPT
    {
      return *static_cast<const _Tp_alloc_type *>(&this->_M_impl);
    }
        
        
    typedef typename _Alloc::value_type   _Alloc_value_type;

    // typedef _Vector_base<_Tp, _Alloc>     _Base;
    // typedef typename _Base::_Tp_alloc_type    _Tp_alloc_type;
    typedef __gnu_cxx::__alloc_traits<_Tp_alloc_type> _Alloc_traits;

  public:
    typedef _Tp         value_type;
    // typedef typename _Base::pointer     pointer;
    typedef typename _Alloc_traits::const_pointer                   const_pointer;
    typedef typename _Alloc_traits::reference                       reference;
    typedef typename _Alloc_traits::const_reference                 const_reference;
    typedef __gnu_cxx::__normal_iterator<pointer, vector>           iterator;
    typedef __gnu_cxx::__normal_iterator<const_pointer, vector>     const_iterator;
    typedef std::reverse_iterator<const_iterator>                   const_reverse_iterator;
    typedef std::reverse_iterator<iterator>                         reverse_iterator;
    typedef size_t                                                  size_type;
    // typedef ptrdiff_t         difference_type;
    // typedef _Alloc          allocator_type;
    
    _Vector_impl _M_impl;

    vector() : _M_impl()
    { 
      _LOGI(""); 
    }
    
    
    
    void
    push_back(const value_type &__x)
    {
      _LOGI("");
      pointer __new_start(_Alloc_traits::allocate(_M_impl, 1));
      // if (this->_M_impl._M_finish != this->_M_impl._M_end_of_storage)
      {
        _Alloc_traits::construct(this->_M_impl, this->_M_impl._M_finish,
                                 __x);
        _LOGI("");
        // ++this->_M_impl._M_finish;
      }
      // else {
        // _M_realloc_insert(end(), __x);
      // }
    }
    
    // size_type
    // _M_check_len(size_type __n, const char *__s) const
    // {
    //   if (max_size() - size() < __n) {
    //     __throw_length_error(__N(__s));
    //   }

    //   const size_type __len = size() + std::max(size(), __n);
    //   return (__len < size() || __len > max_size()) ? max_size() : __len;
    // }
    
    template<typename... _Args>
    void
    _M_realloc_insert(iterator __position, _Args &&... __args);
};












template<typename _Tp, typename _Alloc>
    template<typename... _Args>
      void
      vector<_Tp, _Alloc>::
      _M_realloc_insert(iterator __position, _Args&&... __args)
      {
      const size_type __len = 1;
//   _M_check_len(size_type(1), "vector::_M_realloc_insert");
      const size_type __elems_before = 0;
      pointer __new_start(this->_M_allocate(__len));
      pointer __new_finish(__new_start);
//       __try
//   {
//     // The order of the three operations is dictated by the C++11
//     // case, where the moves could alter a new element belonging
//     // to the existing vector.  This is an issue only for callers
//     // taking the element by lvalue ref (see last bullet of C++11
//     // [res.on.arguments]).
    _Alloc_traits::construct(this->_M_impl,
           __new_start + __elems_before,
// #if __cplusplus >= 201103L
           std::forward<_Args>(__args)...);
// #else
           // __x);
// #endif
//     __new_finish = pointer();

//     __new_finish
//       = std::__uninitialized_move_if_noexcept_a
//       (this->_M_impl._M_start, __position.base(),
//        __new_start, _M_get_Tp_allocator());

//     ++__new_finish;

//     __new_finish
//       = std::__uninitialized_move_if_noexcept_a
//       (__position.base(), this->_M_impl._M_finish,
//        __new_finish, _M_get_Tp_allocator());
//   }
//       __catch(...)
//   {
//     if (!__new_finish)
//       _Alloc_traits::destroy(this->_M_impl,
//            __new_start + __elems_before);
//     else
//       std::_Destroy(__new_start, __new_finish, _M_get_Tp_allocator());
//     _M_deallocate(__new_start, __len);
//     __throw_exception_again;
//   }
//       std::_Destroy(this->_M_impl._M_start, this->_M_impl._M_finish,
//         _M_get_Tp_allocator());
//       _M_deallocate(this->_M_impl._M_start,
//         this->_M_impl._M_end_of_storage
//         - this->_M_impl._M_start);
//       this->_M_impl._M_start = __new_start;
//       this->_M_impl._M_finish = __new_finish;
//       this->_M_impl._M_end_of_storage = __new_start + __len;
    }