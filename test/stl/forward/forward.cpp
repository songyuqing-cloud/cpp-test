#include <iostream>
#include <memory>
#include <utility>
#include <array>
#include <vector>
#include <string>

#define LOG_EN

#include "../../../utils/Utils"

template <class T>
class Forward
{
public:
    Forward<T>() : _val(0)
    {
        printf("%s %d\n",__FUNCTION__, __LINE__);
    }

    virtual ~Forward()
    {
        printf("%s: %d\n", __FUNCTION__, _val);
    }

    Forward<T>(Forward<T> const &f) : _val(f._val)
    {
        printf("%s %d\n", __FUNCTION__, __LINE__);
    }

    Forward<T>(Forward<T> &&f) : _val(f._val)
    {
        printf("%s %d\n",__FUNCTION__, __LINE__);
    }

    T _val;
};

int main()
{   
    printf("%s %d\n", __FUNCTION__, __LINE__);
    std::vector<Forward<int> > v;
    v.reserve(4);
    {
        printf("%s %d\n", __FUNCTION__, __LINE__);
        Forward<int> f;
        printf("%s %d\n", __FUNCTION__, __LINE__);
        f._val = 10;
        printf("%s %d\n", __FUNCTION__, __LINE__);
        v.push_back(f);
        printf("%s %d\n", __FUNCTION__, __LINE__);
        f._val = 12;
        v.push_back(std::move(f));
        f._val = 14;
    }
    printf("%s %d\n", __FUNCTION__, __LINE__);
    return 0;

}