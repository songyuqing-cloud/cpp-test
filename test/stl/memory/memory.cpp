#include <memory>
#include <atomic>
#include <thread>
#include <iostream>

#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <cassert>

#include <sys/syscall.h>
#include <unistd.h> // close

// #define LOG_EN 1
// #include "../../../utils/Utils"

// #ifdef _TLOGI
// #undef _TLOGI
// #undef _TLOGE
// #endif

// #define _TLOGI(format, ...) printf("[I]%s %s t-%d [%d] " format "\n",__FILE__, __PRETTY_FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )
// #define _TLOGE(format, ...) fprintf(stderr, "[E]%s %s t-%d [%d] " format ": %s\n", __FILE__, __PRETTY_FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__, strerror(errno))


static int volatile A;
static int volatile B;

std::atomic<int> F;

void __attribute__ ((noinline)) test()
{
    A=B+1;
    F.store(1, std::memory_order_release);
    // asm volatile("mfence" ::: "memory");
    // _mm_mfence();
    B=0;
    F.load(std::memory_order_acquire);
}

// int main(int argc, char const *argv[])
// // int test()
// {
//     // _TLOGI("");
//     test();
//     // int cnt=0;
//     // const int LOOP_CNT=1000000;

    
//     return 0;
// }