	.arch armv8-a
	.file	"memory.cpp"
	.text
	.section	.rodata
	.align	3
	.type	_ZStL19piecewise_construct, %object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.align	2
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, %object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 4
_ZN9__gnu_cxxL21__default_lock_policyE:
	.word	2
	.align	3
	.type	_ZStL13allocator_arg, %object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.zero	1
	.align	3
	.type	_ZStL6ignore, %object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.zero	1
	.section	.text._ZStanSt12memory_orderSt23__memory_order_modifier,"axG",@progbits,_ZStanSt12memory_orderSt23__memory_order_modifier,comdat
	.align	2
	.weak	_ZStanSt12memory_orderSt23__memory_order_modifier
	.type	_ZStanSt12memory_orderSt23__memory_order_modifier, %function
_ZStanSt12memory_orderSt23__memory_order_modifier:
.LFB1657:
	.cfi_startproc
	sub	sp, sp, #16
	.cfi_def_cfa_offset 16
	str	w0, [sp, 12]
	str	w1, [sp, 8]
	ldr	w1, [sp, 12]
	ldr	w0, [sp, 8]
	and	w0, w1, w0
	add	sp, sp, 16
	.cfi_def_cfa_offset 0
	ret
	.cfi_endproc
.LFE1657:
	.size	_ZStanSt12memory_orderSt23__memory_order_modifier, .-_ZStanSt12memory_orderSt23__memory_order_modifier
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,8
	.local	_ZL1A
	.comm	_ZL1A,4,4
	.local	_ZL1B
	.comm	_ZL1B,4,4
	.global	F
	.bss
	.align	3
	.type	F, %object
	.size	F, 4
F:
	.zero	4
	.text
	.align	2
	.global	_Z4testv
	.type	_Z4testv, %function
_Z4testv:
.LFB2791:
	.cfi_startproc
	stp	x29, x30, [sp, -48]!
	.cfi_def_cfa_offset 48
	.cfi_offset 29, -48
	.cfi_offset 30, -40
	add	x29, sp, 0
	.cfi_def_cfa_register 29
	adrp	x0, _ZL1B
	add	x0, x0, :lo12:_ZL1B
	ldr	w0, [x0]
	add	w1, w0, 1
	adrp	x0, _ZL1A
	add	x0, x0, :lo12:_ZL1A
	str	w1, [x0]
	mov	w0, 1
	str	w0, [x29, 36]
	mov	w0, 3
	str	w0, [x29, 40]
	mov	w1, 65535
	ldr	w0, [x29, 40]
	bl	_ZStanSt12memory_orderSt23__memory_order_modifier
	str	w0, [x29, 44]
	adrp	x0, F
	add	x0, x0, :lo12:F
	ldr	w1, [x29, 36]
	stlr	w1, [x0]
	adrp	x0, _ZL1B
	add	x0, x0, :lo12:_ZL1B
	str	wzr, [x0]
	mov	w0, 2
	str	w0, [x29, 28]
	mov	w1, 65535
	ldr	w0, [x29, 28]
	bl	_ZStanSt12memory_orderSt23__memory_order_modifier
	str	w0, [x29, 32]
	adrp	x0, F
	add	x0, x0, :lo12:F
	ldar	w0, [x0]
	nop
	ldp	x29, x30, [sp], 48
	.cfi_restore 30
	.cfi_restore 29
	.cfi_def_cfa 31, 0
	ret
	.cfi_endproc
.LFE2791:
	.size	_Z4testv, .-_Z4testv
	.align	2
	.type	_Z41__static_initialization_and_destruction_0ii, %function
_Z41__static_initialization_and_destruction_0ii:
.LFB3195:
	.cfi_startproc
	stp	x29, x30, [sp, -32]!
	.cfi_def_cfa_offset 32
	.cfi_offset 29, -32
	.cfi_offset 30, -24
	add	x29, sp, 0
	.cfi_def_cfa_register 29
	str	w0, [x29, 28]
	str	w1, [x29, 24]
	ldr	w0, [x29, 28]
	cmp	w0, 1
	bne	.L7
	ldr	w1, [x29, 24]
	mov	w0, 65535
	cmp	w1, w0
	bne	.L7
	adrp	x0, _ZStL8__ioinit
	add	x0, x0, :lo12:_ZStL8__ioinit
	bl	_ZNSt8ios_base4InitC1Ev
	adrp	x0, __dso_handle
	add	x2, x0, :lo12:__dso_handle
	adrp	x0, _ZStL8__ioinit
	add	x1, x0, :lo12:_ZStL8__ioinit
	adrp	x0, :got:_ZNSt8ios_base4InitD1Ev
	ldr	x0, [x0, #:got_lo12:_ZNSt8ios_base4InitD1Ev]
	bl	__cxa_atexit
.L7:
	nop
	ldp	x29, x30, [sp], 32
	.cfi_restore 30
	.cfi_restore 29
	.cfi_def_cfa 31, 0
	ret
	.cfi_endproc
.LFE3195:
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.align	2
	.type	_GLOBAL__sub_I_F, %function
_GLOBAL__sub_I_F:
.LFB3196:
	.cfi_startproc
	stp	x29, x30, [sp, -16]!
	.cfi_def_cfa_offset 16
	.cfi_offset 29, -16
	.cfi_offset 30, -8
	add	x29, sp, 0
	.cfi_def_cfa_register 29
	mov	w1, 65535
	mov	w0, 1
	bl	_Z41__static_initialization_and_destruction_0ii
	ldp	x29, x30, [sp], 16
	.cfi_restore 30
	.cfi_restore 29
	.cfi_def_cfa 31, 0
	ret
	.cfi_endproc
.LFE3196:
	.size	_GLOBAL__sub_I_F, .-_GLOBAL__sub_I_F
	.section	.init_array,"aw",%init_array
	.align	3
	.xword	_GLOBAL__sub_I_F
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu/Linaro 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
