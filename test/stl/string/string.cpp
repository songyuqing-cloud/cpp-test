// string::find
#include <iostream>       // std::cout
#include <string>         // std::string

#define LOG_EN
#include "../../../utils/Utils"

bool execThenCheckOutput(std::string const &aCmd, std::string const &aCheckMsg = "", uint32_t aSleepMs=10)
{
  _LOGI("exec: %s, expected: %s", aCmd.data(), aCheckMsg.data());

  FILE *lPipe;
  if(!(lPipe = popen(aCmd.data(), "r")))
  {
    _LOGE("");
    return false;
  }

  // SLEEPMS(aSleepMs);

  // no check
  if(aCheckMsg == "")
  {
    _LOGD("");
    pclose(lPipe);
    return true;
  }

  std::string lBuffer;
  lBuffer.reserve(0x1000);

  while ( !feof(lPipe) )
  {
    if (fgets(&lBuffer[0], lBuffer.capacity(), lPipe) != NULL)
    {
      _LOGD("%s", lBuffer.data());
      size_t pos = lBuffer.find(aCheckMsg);
      // found
      if(pos != std::string::npos)
      {
        pclose(lPipe);
        return true;
      }

      // size_t pos = lBuffer.find(aCheckMsg);
      // found
      // if(pos != std::string::npos)
      if(strstr(lBuffer.data(), aCheckMsg.data()) != nullptr)
      {
        pclose(lPipe);
        return true;
      }

      _LOGD("pos: %ld", pos);
    }
  }

  _LOGD("");
  // not found
  pclose(lPipe);
  return false;
}

int main ()
{
  std::string str ("There are two needles in this haystack with needles.");
  std::string str2 ("two needles");

  size_t found=str.find(str2);
  if (found!=std::string::npos)
    std::cout << "Period found at: " << found << '\n';

  // let's replace the first needle:
  // str.replace(str.find(str2),str2.length(),"preposition");
  // std::cout << str << '\n';

_LOGI("%d", execThenCheckOutput("lsmod", "16384  2 i915,nouveau"));

  return 0;
}