#include <set>
#include <string>
#include <algorithm>
#include <vector>


namespace HmiAl {
template<typename T>
struct GblVehicle {
    GblVehicle(std::string str) : strVal(std::move(str)) {}
    GblVehicle(T t) : enumVal(t) {}
    GblVehicle(std::string str, T t) : strVal(std::move(str)), enumVal(t) {}
    bool operator==(const GblVehicle &other) const {
        if(strVal.empty() || other.strVal.empty()) {
            return enumVal == other.enumVal;
        } else {
            return strVal == other.strVal;
        }
    }

    std::string strVal;
    T enumVal;
};

enum class ENUM_GblVehiclePrograms {
    ENUM_GblVehicleProgramUnKnown = 0,
    ENUM_GblVehicleProgram_C344,
    ENUM_GblVehicleProgram_C346C519,
};
enum class ENUM_GblVehicleBodyType {
    ENUM_GblVehicleBodyTypeUnknown = 0,
    ENUM_GblVehicleBodyTypeSedan,
    ENUM_GblVehicleBodyTypeCoupe,
};

const std::vector<GblVehicle<ENUM_GblVehiclePrograms>> VehProgLst = {
    {"C344", ENUM_GblVehiclePrograms::ENUM_GblVehicleProgram_C344},
    {"C346C519", ENUM_GblVehiclePrograms::ENUM_GblVehicleProgram_C346C519},
};
const std::vector<GblVehicle<ENUM_GblVehicleBodyType>> VehBodyLst = {
    {"sedan", ENUM_GblVehicleBodyType::ENUM_GblVehicleBodyTypeSedan},
    {"coupe", ENUM_GblVehicleBodyType::ENUM_GblVehicleBodyTypeCoupe},
    {"abc", ENUM_GblVehicleBodyType::ENUM_GblVehicleBodyTypeUnknown},
};

}

int main() {
    using namespace HmiAl;

    // printf("size: %ld\n", VehProgLst.size());
    // for(auto it : VehProgLst) {
    //     printf("%s %d\n", it.strVal.data(), (int)it.enumVal);
    // }
    printf("size: %ld\n", VehProgLst.size());
    for(auto it : VehProgLst) {
        // printf("%s %d\n", it.strVal.data(), (int)it.enumVal);
        printf("Search by string: ");
        auto rs = std::find(VehProgLst.begin(), VehProgLst.end(), it.strVal);// VehProgLst.find(it.strVal);
        if(rs != VehProgLst.end()) {
            printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
        } else {
            printf("Not found: %s\n", it.strVal.data());
        }
        printf("Search by enum: ");
        rs = std::find(VehProgLst.begin(), VehProgLst.end(), it.enumVal);// VehProgLst.find(it.enumVal);
        if(rs != VehProgLst.end()) {
            printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
        } else {
            printf("Not found: %d\n", (int)it.enumVal);
        }
    }

    auto rs = std::find(VehProgLst.begin(), VehProgLst.end(), std::string{"C344"});
    if(rs != VehProgLst.end()) {
        printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
    } else {
        printf("Not found: %s\n", "C344");
    }

    rs = std::find(VehProgLst.begin(), VehProgLst.end(), ENUM_GblVehiclePrograms::ENUM_GblVehicleProgram_C344);
    if(rs != VehProgLst.end()) {
        printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
    } else {
        printf("Not found: %s\n", "C344");
    }
    // {
    //     printf("Search by string\n");
    //     auto rs = VehProgLst.find({"C344"});
    //     if(rs != VehProgLst.end()) {
    //         printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
    //     }
    //     rs = VehProgLst.find({"C346C519"});
    //     if(rs != VehProgLst.end()) {
    //         printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
    //     }
    //     /// invalid
    //     std::string invalidStr{"invalidVal"};
    //     rs = VehProgLst.find({invalidStr});
    //     if(rs != VehProgLst.end()) {
    //         printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
    //     } else {
    //         printf("Not existence: %s\n", invalidStr.data());
    //     }
    // }

    // {
    //     printf("Search by enum\n");
    //     auto rs = VehBodyLst.find(ENUM_GblVehicleBodyType::ENUM_GblVehicleBodyTypeSedan);
    //     if(rs != VehBodyLst.end()) {
    //         printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
    //     }
    //     rs = VehBodyLst.find({ENUM_GblVehicleBodyType::ENUM_GblVehicleBodyTypeCoupe});
    //     if(rs != VehBodyLst.end()) {
    //         printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
    //     }
    //     /// invalid
    //     ENUM_GblVehicleBodyType invalidEnum = ENUM_GblVehicleBodyType::ENUM_GblVehicleBodyTypeUnknown;
    //     rs = VehBodyLst.find({invalidEnum});
    //     if(rs != VehBodyLst.end()) {
    //         printf("%s %d\n", rs->strVal.data(), (int)rs->enumVal);
    //     } else {
    //         printf("Not existence: %d\n", (int)invalidEnum);
    //     }
    // }

    return 0;
}