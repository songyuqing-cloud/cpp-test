#include <vector>
#include <string>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <functional>

#define LOG_EN
#include "../../../utils/Utils"

template <typename T>
class StdVector : private std::vector<T>
{
public:
    using container_t = std::vector<T>;
    using iterator = typename container_t::iterator;
    using const_iterator = typename container_t::const_iterator;
    using value_type = typename container_t::value_type;

    using container_t::vector;
    using container_t::size;
    using container_t::resize;
    using container_t::empty;
    using container_t::at;
    using container_t::reserve;
    using container_t::back;
    using container_t::operator=;
    using container_t::operator[];

    // value_type& operator[] (size_t n) {
    //     return container_t::operator[](n);
    // }
    // const value_type& operator[] (size_t n) const {
    //     return container_t::operator[](n);
    // }
    StdVector& operator=(const StdVector& x) {
        container_t::operator=(x);
        return *this;
    }
    StdVector& operator=(StdVector&& x) {
        container_t::operator=(x);
        return *this;
    }
    StdVector& operator=(std::initializer_list<value_type> il) {
        container_t::operator=(il);
        return *this;
    }
    auto begin() noexcept -> decltype(container_t::begin()) {
        return container_t::begin();
    }
    auto begin() const noexcept -> decltype(container_t::cbegin()) {
        return container_t::cbegin();
    }
    auto cbegin() const noexcept -> decltype(container_t::cbegin()) {
        return container_t::cbegin();
    }
    auto end() noexcept -> decltype(container_t::end()) {
        return container_t::end();
    }
    auto end() const noexcept -> decltype(container_t::cend()) {
        return container_t::cend();
    }
    auto cend() const noexcept -> decltype(container_t::cend()) {
        return container_t::cend();
    }

    auto rbegin() noexcept -> decltype(container_t::rbegin()) {
        return container_t::rbegin();
    }
    auto rbegin() const noexcept -> decltype(container_t::crbegin()) {
        return container_t::crbegin();
    }
    auto crbegin() const noexcept -> decltype(container_t::crbegin()) {
        return container_t::crbegin();
    }
    auto rend() noexcept -> decltype(container_t::rend()) {
        return container_t::rend();
    }
    auto rend() const noexcept -> decltype(container_t::crend()) {
        return container_t::crend();
    }
    auto crend() const noexcept -> decltype(container_t::crend()) {
        return container_t::crend();
    }

    template <class... Args>
    iterator emplace (const_iterator position, Args&&... args) {TLL_GLOGD("%p: %ld", this, this->size());
        return container_t::emplace(position, std::forward<Args>(args)...);
    }
    template <class... Args>
    void emplace_back (Args&&... args) {
        container_t::emplace_back(std::forward<Args>(args)...);
    }

    void push_back(const value_type &t) {
        container_t::push_back(t);
    }
    void push_back(T &&t) {
        container_t::push_back(t);
    }
    void pop_back() {
        container_t::pop_back();
    }
    
    void push_front(const value_type &t) {
        container_t::push_front(t);
    }
    void push_front(T &&t) {
        container_t::push_front(t);
    }
    void pop_front() {
        container_t::pop_front();
    }

    iterator insert (iterator position, const value_type& val){
        return container_t::insert(position, val);
    }
    void insert (iterator position, size_t n, const value_type& val) {
        container_t::insert(position, n, val);
    }
    template <class InputIterator>
    void insert (iterator position, InputIterator first, InputIterator last) {
        container_t::insert(position, first, last);
    }

    void clear() {
        container_t::clear();
    }

    iterator erase (iterator position) {
        return container_t::erase(position);
    }
    iterator erase (const_iterator first, const_iterator last) {
        return container_t::erase(first, last);
    }

    template <class InputIterator>
    StdVector (InputIterator first, InputIterator last) : container_t{first, last}
    {}
};

struct Ex {
    Ex() = default;
    ~Ex() {_LOGI("");}
    StdVector<std::string> m_newSecondDevices {};
    StdVector<bool> m_usbConnectedList {};
};

struct DEx : private Ex {
    // DEx() : Ex() {_LOGI("");}
    // DEx = default;
    using Ex::Ex;
    ~DEx() {_LOGI("");}
};

// StdVector<std::function<void()>> &create() {
//     static StdVector<std::function<void()>> smyvec;
//     return smyvec;
// }

// const StdVector<std::string>&         getStrings(const char *key, const StdVector<std::string> & defaultValues = StdVector<std::string>()) {
//     return defaultValues;
// }

int main()
{
    
    _LOGI("");
    StdVector<std::string> vec;
    vec.empty();
    vec.push_back("1");
    vec.push_back("2");
    vec.push_back("3");
    StdVector<std::string> vec2;

    vec2 = vec;
    // // _LOGI("%ld", dosomething().size());
    // _LOGI("%ld", vec.size());
    for(auto el : vec) {
        _LOGI("%s", el.data());
    }
    for(auto el : vec2) {
        _LOGI("%s", el.data());
    }

    // auto it = vec.begin();
    // vec.erase(it);

    // vec.emplace_back("10");
    // vec.emplace_back("11");
    // vec.emplace_back("12");
    // vec.emplace_back("13");

    // for(const auto &i : vec) {
    //     _LOGI("%s", i.data());
    // }
    // StdVector<std::string> mInstalled{vec};
    // std::copy(vec.begin(), vec.end(), std::back_inserter(mInstalled));

    // _LOGI("%ld", mInstalled.size());

    // auto i = std::find(vec.begin(), vec.end(), "10");

    // Ex e;

    // StdVector<std::function<void()>> instance;

    // instance = create();

    // // e.m_usbConnectedList.data();


    // const auto &v1 = getStrings(nullptr);
    // auto v2 = getStrings(nullptr);

    DEx e;

    return 0;
}