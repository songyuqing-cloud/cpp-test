#ifndef UTILS_MACRO_H
#define UTILS_MACRO_H

#include <cerrno>
#include <sched.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <cstring>

#define PROXY_AUTHEN

#ifndef LOGI
#define LOGI(format,...) printf("%s[%s][%i] " format "\n", __FILE__, __FUNCTION__, __LINE__,##__VA_ARGS__)
#endif

#define _TLOGI(format, ...) printf("[I]%s %s t-%d [%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )

#define _LOGW(format, ...) printf("[W]%s %s [%d] " format "\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
#define _PLOGW(format, ...) printf("[W]%s %s p[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
#define _PPLOGW(format, ...) printf("[W]%s %s pp[%d-%d] " format "\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
#define _TLOGW(format, ...) printf("[W]%s %s t-%d [%d] " format "\n",__FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__ )

#define _LOGE(format, ...) fprintf(stderr, "[E]%s %s [%d] " format ": %s\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
#define _PLOGE(format, ...) fprintf(stderr, "[E]%s %s p[%d-%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__, strerror(errno))
#define _PPLOGE(format, ...) fprintf(stderr, "[E]%s %s pp[%d-%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__, strerror(errno))
#define _TLOGE(format, ...) fprintf(stderr, "[E]%s %s t-%d [%d] " format ": %s\n", __FILE__, __FUNCTION__, (int)syscall(SYS_gettid), __LINE__, ##__VA_ARGS__, strerror(errno))

#endif
