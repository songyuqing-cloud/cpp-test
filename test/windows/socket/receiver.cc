#include <winsock2.h>
#include <iostream>
#include <cstdio>
#include <Ws2tcpip.h>
#include <cstring>
#include <string>

using namespace std;

int main(int argc, char **argv)
{
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    SOCKET sock;
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    char broadcast = '1';
    //     This option is needed on the socket in order to be able to receive broadcast messages
    //   If not set the receiver will not receive broadcast messages in the local network.
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) < 0)
    {
        cout << "Error in setting Broadcast option";
        closesocket(sock);
        return 0;
    }

    struct sockaddr_in recv_addr;
    struct sockaddr_in send_addr;
    int len = sizeof(struct sockaddr_in);
    char recvbuff[50];
    int recvbufflen = 50;
    recv_addr.sin_family = AF_INET;
    recv_addr.sin_port = htons(std::stoi(argv[2]));
    inet_pton(AF_INET, argv[1], &recv_addr.sin_addr);

    if (bind(sock, (sockaddr*)&recv_addr, sizeof(recv_addr)) < 0)
    {
        cout << "Error in BINDING" << WSAGetLastError();
        closesocket(sock);
        WSACleanup();
        return 1;
    }

    char str[INET_ADDRSTRLEN];
    do
    {
        cout << "\nWaiting for message...\n";
        recvfrom(sock, recvbuff, recvbufflen, 0, (sockaddr*)&send_addr, &len);
        inet_ntop(AF_INET, &(send_addr.sin_addr), str, INET_ADDRSTRLEN);
        cout << "Received from: " << str << ":" << send_addr.sin_port << "\n";
        cout << recvbuff;
    } while (1);

    closesocket(sock);
    WSACleanup();
    return 0;
}