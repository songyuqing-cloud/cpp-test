#include <winsock2.h>
#include <iostream>
#include <cstdio>
#include <Ws2tcpip.h>
#include <cstring>
#include <string>

using namespace std;

int main(int argc, char **argv)
{
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    SOCKET sock;
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    char broadcast = '1';
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast)) < 0)
    {
        cout << "Error in setting Broadcast option";
        closesocket(sock);
        return 0;
    }

    struct sockaddr_in bc_addr;
    // struct sockaddr_in send_addr;
    int len = sizeof(struct sockaddr_in);
    char sendMSG[] = "Attention: this is a broadcast!!!";
    char recvbuff[50] = "";
    int recvbufflen = 50;
    bc_addr.sin_family = AF_INET;
    bc_addr.sin_port = htons(std::stoi(argv[2]));
    inet_pton(AF_INET, argv[1], &bc_addr.sin_addr);

    cout << "sending message: " << sendMSG;
    sendto(sock, sendMSG, (int)strlen(sendMSG) + 1, 0, (sockaddr*)&bc_addr, sizeof(bc_addr));

    closesocket(sock);
    WSACleanup();
}