ARGET = MMSTest
TEMPLATE = app

CONFIG *= warn_on

QT -= gui
QT *= core

INCLUDEPATH *= . \
	$$(ADK_PREFIX_HOST)/usr/include/libdrm


HEADERS *= \
    # MMSTestProxy.h


SOURCES *= \
    setbacklight.c \
    libbacklight.c


LIBS *= -ldrm -ludev


# OTHER_FILES *= \
    # xml/IMMSTest.xml


#
# Compiler flags:
#
linux-g++:QMAKE_CXXFLAGS *= -Wall
# linux-g++:QMAKE_CXXFLAGS *= '-std=c++11'
linux-oe-g++:QMAKE_CXXFLAGS *= -Wall
linux-oe-g++:QMAKE_CXXFLAGS *= -Wno-psabi
# linux-oe-g++:QMAKE_CXXFLAGS *= '-std=c++11'



#
# Create the moc* and *.o files in separate directory:
#
OBJECTS_DIR = tmp/
MOC_DIR     = tmp/

# linux-oe-g++
# {
#     #----------------------
#     # Installation - TARGET
#     #----------------------
#     target.path += $$OVIP_TARGET_INSTALL_PATH/usr/lib/ovip/AppFwk/Domains
#     INSTALLS += target

#     xml.path = $$OVIP_TARGET_INSTALL_PATH/usr/share/ovip/AppFwk/Conn
#     xml.files = xml/TsmServicesPlugin.xml
#     INSTALLS += xml

#     #-------------------
#     # Installation - ADK
#     #-------------------
#     ADK_UPDATE {
#         adk_target.path = $$(ADK_PREFIX_HOST)/usr/lib/ovip/AppFwk/Domains
#         adk_target.files = libTsmServicesPlugin.so
#         INSTALLS += adk_target

#         adk_service.path = $$(ADK_PREFIX_HOST)/usr/share/ovip/AppFwk/Conn
#         adk_service.files = xml/TsmServicesPlugin.xml
#         INSTALLS += adk_service

#         adk_headers.path = $$(ADK_PREFIX_HOST)/usr/include/ovip/AppFwk/Conn/TelematicServiceManager
#         adk_headers.files = $$files(pub/*.h)
#         INSTALLS += adk_headers

#         adk_prf.path = $$(ADK_PREFIX_HOST)/usr/share/qt5/mkspecs/features
#         adk_prf.files = ../features/oviptelematicservicemanager.prf
#         INSTALLS += adk_prf
#     }

# }
