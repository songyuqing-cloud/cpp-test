#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <linux/input.h>
#include <linux/init.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>


#include <asm/uaccess.h>

static int irq_num = -1;
module_param(irq_num, int, 0);
static unsigned int irq_flag = 0;
module_param(irq_flag, int, 1);
static int gpio_num = -1;
module_param(gpio_num, int, 1);

#define PRINT printk
#include "input_drv.h"

#define FIRST_MINOR 0
#define MINOR_CNT 1

#define MY_WORK_QUEUE_NAME "WQsched.c"

static dev_t dev;
static struct cdev c_dev;
static struct class *cl;
// static int status = 1, dignity = 3, ego = 5;

static struct input_dev *button_dev;

static struct workqueue_struct *my_workqueue;
static struct delayed_work      dwork_press;
static struct delayed_work      dwork_release;

static unsigned long skey;

static void key_pressed(struct work_struct *work)
{
    LOGI("");
    input_report_key(button_dev, skey, 1);
    input_sync(button_dev);
}

static void key_released(struct work_struct *work)
{
    LOGI("");
    // struct apds9960_data *data = container_of(work, struct apds9960_data, als_dwork.work);
    input_report_key(button_dev, skey, 0);
    input_sync(button_dev);
}

static int my_open(struct inode *i, struct file *f)
{
    return 0;
}
static int my_close(struct inode *i, struct file *f)
{
    return 0;
}
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
static int my_ioctl(struct inode *i, struct file *f, unsigned int cmd, unsigned long arg)
#else
static long my_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
#endif
{
    // query_arg_t q;

    switch (cmd)
    {
        // case QUERY_GET_VARIABLES:

        //     q.status = status;
        //     q.dignity = dignity;
        //     q.ego = ego;
        //     if (copy_to_user((query_arg_t *)arg, &q, sizeof(query_arg_t)))
        //     {
        //         return -EACCES;
        //     }
        //     break;
        case INPUT_PRESSED:
            LOGI("%d", arg);
            skey = arg;
            queue_delayed_work(my_workqueue, &dwork_press, msecs_to_jiffies(1000));
            break;

        case INPUT_RELEASED:
            LOGI("");
            skey = arg;
            queue_delayed_work(my_workqueue, &dwork_release, msecs_to_jiffies(1000));
            break;
        default:
            return -EINVAL;
    }

    return 0;
}

static struct file_operations query_fops =
{
    .owner = THIS_MODULE,
    .open = my_open,
    .release = my_close,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
    .ioctl = my_ioctl
#else
    .unlocked_ioctl = my_ioctl
#endif
};
static unsigned long count = 0;
static irqreturn_t test_int(int irq, void *dev_id)
{
    LOGI("%d", count++);
    return IRQ_HANDLED;
}

static int __init query_ioctl_init(void)
{
    int ret;
    struct device *dev_ret;
    LOGI("");


    button_dev = input_allocate_device();
    if (!button_dev) {
        printk(KERN_ERR "button.c: Not enough memory\n");
        return -ENOMEM;
    }
    // button_dev->evbit[0] = BIT_MASK(EV_KEY);
    // button_dev->evbit[BIT_WORD(EV_KEY)] = BIT_MASK(EV_KEY);
    // button_dev->keybit[BIT_WORD(KEY_F1)] = BIT_MASK(KEY_F1);
    set_bit(EV_KEY, button_dev->evbit);
    set_bit(KEY_F1, button_dev->keybit);
    set_bit(KEY_F2, button_dev->keybit);
    set_bit(KEY_F3, button_dev->keybit);
    set_bit(KEY_F4, button_dev->keybit);
    set_bit(KEY_A, button_dev->keybit);
    // set_bit(ID_INPUT_KEYBOARD, button_dev->propbit);

    button_dev->name="_name";
    // button_dev->phys="_phys";
    // button_dev->uniq="_uniq";

    ret = input_register_device(button_dev);
    if (ret) {
        printk(KERN_ERR "button.c: Failed to register device\n");
        input_free_device(button_dev);
        return ret;
    }

    // workqueue
    my_workqueue = create_workqueue(MY_WORK_QUEUE_NAME);
    INIT_DELAYED_WORK(&dwork_press, key_pressed);
    INIT_DELAYED_WORK(&dwork_release, key_released);
    // end workqueue
    if(irq_num > -1 && irq_flag > 0)
    {
        int _ret = 0;
        if (gpio_is_valid(gpio_num))
        {
            _ret = gpio_request(gpio_num, "apds_irq");
            LOGI("%d", _ret);
        }
        else
        {
            printk("Unable to request GPIO: %d.\n", gpio_num);
        }
/*        err = gpio_request(gpio_num, "apds_irq");
        if (err)
        {
            printk("Unable to request GPIO.\n");
            goto exit_kfree;
        }
    
        gpio_direction_input(gpio_num);
        irq = gpio_to_irq(gpio_num);
    
        if (irq < 0)
        {
            err = irq;
            printk("Unable to request gpio irq. err=%d\n", err);
            gpio_free(gpio_num);
        
            goto exit_kfree;
        }
    
        irq_num = irq;
        if (request_irq(irq_num, apds9960_interrupt, irq_flag,
            APDS9960_DRV_NAME, (void *)client)) {
            printk("%s Could not allocate %d !\n", __func__, apds_irq);
        
            goto exit_kfree;
        }*/
        int gpio3 = 64;
        LOGI("gpio_to_irq(%d): %d", gpio_to_irq(gpio3));
        // LOGI("%d:%d", gpio_to_irq(352), gpio_to_irq(320));
        // LOGI("%d:%d", gpio_to_irq(32), gpio_to_irq(288));
        // LOGI("%d:%d", gpio_to_irq(224), gpio_to_irq(192));
        // LOGI("%d:%d", gpio_to_irq(160), gpio_to_irq(128));
        // LOGI("%d:%d", gpio_to_irq(0), gpio_to_irq(294));
            
        gpio_direction_input(gpio3);
        
            
        LOGI("%d : %x", irq_num, irq_flag);
        ret = request_irq(gpio_to_irq(gpio3), test_int, irq_flag, "test_int", test_int);
        if(ret)
        {
            LOGI("%d", ret);
            // free_irq(irq_num, test_int);
            input_free_device(button_dev);
            return ret;
        }
    }

    if ((ret = alloc_chrdev_region(&dev, FIRST_MINOR, MINOR_CNT, "query_ioctl")) < 0)
    {
        return ret;
    }

    cdev_init(&c_dev, &query_fops);

    if ((ret = cdev_add(&c_dev, dev, MINOR_CNT)) < 0)
    {
        return ret;
    }

    if (IS_ERR(cl = class_create(THIS_MODULE, "char")))
    {
        cdev_del(&c_dev);
        unregister_chrdev_region(dev, MINOR_CNT);
        return PTR_ERR(cl);
    }
    if (IS_ERR(dev_ret = device_create(cl, NULL, dev, NULL, "query")))
    {
        class_destroy(cl);
        cdev_del(&c_dev);
        unregister_chrdev_region(dev, MINOR_CNT);
        return PTR_ERR(dev_ret);
    }

    return 0;

}

static void __exit query_ioctl_exit(void)
{
    LOGI("");
    input_unregister_device(button_dev);
    input_free_device(button_dev);
    destroy_workqueue(my_workqueue);
    if(irq_num > -1 && irq_flag > 0)
    {
        gpio_free(gpio_num);
        // free_irq(irq_num, test_int);
    }
    device_destroy(cl, dev);
    class_destroy(cl);
    cdev_del(&c_dev);
    unregister_chrdev_region(dev, MINOR_CNT);
}

module_init(query_ioctl_init);
module_exit(query_ioctl_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anil Kumar Pugalia <email_at_sarika-pugs_dot_com>");
MODULE_DESCRIPTION("Query ioctl() Char Driver");
