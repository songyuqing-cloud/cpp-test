#ifndef INPUT_DRV_H
#define INPUT_DRV_H
#include <linux/ioctl.h>
#include <linux/input.h>
// typedef struct
// {
//     int status, dignity, ego;
// } query_arg_t;

enum
{
    INPUT_KEY_PRESSED,
    INPUT_KEY_RELEASED
};

// #define INPUT_DRV_TO_KEY(arg)       ((arg) + KEY_F1)
// #define INPUT_DRV_TO_ARG(key)       ((key) - KEY_F1)

#define INPUT_PRESSED       _IOW('i', 1, unsigned long)
#define INPUT_RELEASED      _IOW('i', 2, unsigned long)


#define LOGI(format,...) PRINT("%s[%s][%i] " format "\n", __FILE__, __FUNCTION__, __LINE__,##__VA_ARGS__)

#endif //INPUT_DRV_H
