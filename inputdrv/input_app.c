#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdint.h>
#define PRINT printf
#include "input_drv.h"

void key_pressed(int fd, int key)
{
    if (ioctl(fd, INPUT_PRESSED, key) == -1)
    {
        perror("key_pressed");
    }
}

void key_released(int fd, int key)
{
    if (ioctl(fd, INPUT_RELEASED, key) == -1)
    {
        perror("key_released");
    }
}

int main(int argc, char *argv[])
{
    char *file_name = "/dev/query";
    int fd = open(file_name, O_RDWR);
    if (fd == -1)
    {
        perror("query_apps open");
        return 2;
    }

    if (argc == 5)
    {
        if(atoi(argv[3]) == INPUT_KEY_PRESSED)
        {
            LOGI("");
            key_pressed(fd, atoi(argv[1]));
            key_pressed(fd, atoi(argv[2]));
            key_released(fd, atoi(argv[1]));
            key_released(fd, atoi(argv[2]));
        }
        usleep((uint32_t)atoi(argv[4]) * 1000);
        LOGI("");
    }
    if (argc == 4)
    {
        if(atoi(argv[2]) == INPUT_KEY_PRESSED)
        {
            LOGI("");
            key_pressed(fd, atoi(argv[1]));
            key_released(fd, atoi(argv[1]));
        }
        usleep((uint32_t)atoi(argv[3]) * 1000);
        LOGI("");
    }
    else if(argc == 3)
    {
        if(atoi(argv[2]) == INPUT_KEY_PRESSED)
        {
            LOGI("");
            key_pressed(fd, atoi(argv[1]));
            key_released(fd, atoi(argv[1]));
        }
        LOGI("");
    }
    else
    {
        fprintf(stderr, "Usage: %s [0 | 1 | 2 | 3] [0 | 1]{1,2} [ms]\n", argv[0]);
        return 1;
    }

    close (fd);

    return 0;
}
