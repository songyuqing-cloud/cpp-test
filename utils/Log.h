#ifndef LOG_H
#define LOG_H

#if defined(_WIN32) && defined(_WINDOWS)
#include <windows.h>
#endif

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <cstdarg>

#include <sstream>

#include <thread>

#ifdef LOG_EN

#define MAX_LOG_LENGTH 0x1000

#if (defined _WIN32)

namespace std {
    int stoi(const std::string &a_str)
    {
        return std::atoi(a_str.data());
    }

    template<typename T>
    std::string to_string(const T &n) {
        std::stringstream s;
        s << n;
        return s.str();
    }
}

#endif

namespace custom {

class Log
{
public:
    enum class _ELvl
    {
        Init = 0,
        Info,
        Default = Info,
        Debug,
        Max,
    };

    static inline bool canLog(_ELvl const &alvl)
    {
        return true;
        static char const *lPatterns[] = {"LOG_LVL", "LOGLVL", "LOGLEVEL", "LOG_LEVEL"};
        static char *lEnvi = nullptr;
        static _ELvl lLvl = _ELvl::Default;
        static bool lInit = false;

        if(!lInit)
        {
            lInit = true;
            for(char const *pPat : lPatterns)
            {
                lEnvi = std::getenv(pPat);
                if(lEnvi)
                {
                    lLvl = static_cast<_ELvl>(std::stoi(lEnvi));
                    break;
                }
            }
        }

        return (lLvl >= alvl);
    }

    static void _log(const char *format, va_list args)
    {
        int bufferSize = MAX_LOG_LENGTH;
        char* buf = nullptr;

        do
        {
            buf = new (std::nothrow) char[bufferSize];
            if (buf == nullptr)
                return; // not enough memory

            int ret = vsnprintf(buf, bufferSize - 3, format, args);
            if (ret < 0)
            {
                bufferSize *= 2;

                delete [] buf;
            }
            else
                break;

        } while (true);

        strcat(buf, "\n");

#if defined(ANDROID)
        __android_log_print(ANDROID_LOG_DEBUG, "cocos2d-x debug info", "%s", buf);

#elif defined(_WIN32) && defined(_WINDOWS)

        int pos = 0;
        int len = strlen(buf);
        char tempBuf[MAX_LOG_LENGTH + 1] = { 0 };
        WCHAR wszBuf[MAX_LOG_LENGTH + 1] = { 0 };

        do
        {
            std::copy(buf + pos, buf + pos + MAX_LOG_LENGTH, tempBuf);

            tempBuf[MAX_LOG_LENGTH] = 0;

            MultiByteToWideChar(CP_UTF8, 0, tempBuf, -1, wszBuf, sizeof(wszBuf));
            OutputDebugStringW(wszBuf);
            WideCharToMultiByte(CP_ACP, 0, wszBuf, -1, tempBuf, sizeof(tempBuf), nullptr, FALSE);
            printf("%s", tempBuf);

            pos += MAX_LOG_LENGTH;

        } while (pos < len);
        // SendLogToWindow(buf);
        fflush(stdout);
#else
        // Linux, Mac, iOS, etc
        fprintf(stdout, "%s", buf);
        fflush(stdout);
#endif

        delete [] buf;
    }

    static inline void log(const char * format, ...)
    {
        va_list args;
        va_start(args, format);
        Log::_log(format, args);
        va_end(args);
    }

};

}

// #if defined(_WIN32) && defined(_WINDOWS)
// #define PRINTF Log::log
// #else
// #endif

// #define PRINTF custom::Log::log
#define PRINTF printf


    #define _LOGW(format, ...) PRINTF("%s %s [%d][W]'" format "'\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
    #define _PLOGW(format, ...) PRINTF("%s %s p[%d-%d][W]'" format "'\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
    #define _PPLOGW(format, ...) PRINTF("%s %s pp[%d-%d][W]'" format "'\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
    #define _TLOGW(format, ...) PRINTF("%s %s t[%d-%d][W]'" format "'\n",__FILE__, __FUNCTION__, (int)std::this_thread::get_id(), __LINE__, ##__VA_ARGS__ )

    #define _LOGE(format, ...) fprintf(stderr, "%s %s [%d][E]'" format ": %s'\n", __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(errno))
    #define _PLOGE(format, ...) fprintf(stderr, "%s %s p[%d-%d][E]'" format ": %s'\n", __FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__, strerror(errno))
    #define _PPLOGE(format, ...) fprintf(stderr, "%s %s pp[%d-%d][E]'" format ": %s'\n", __FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__, strerror(errno))
    #define _TLOGE(format, ...) fprintf(stderr, "%s %s t[%d-%d][E]'" format ": %s'\n", __FILE__, __FUNCTION__, (int)std::this_thread::get_id(), __LINE__, ##__VA_ARGS__, strerror(errno))

    #define _LOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) PRINTF("%s %s [%d][I]'" format "'\n",__FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__ )
    #define _PLOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) PRINTF("%s %s p[%d-%d][I]'" format "'\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
    #define _PPLOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) PRINTF("%s %s pp[%d-%d][I]'" format "'\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
    #define _TLOGI(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Info)) PRINTF("%s %s t[%d-%d][I]'" format "'\n",__FILE__, __FUNCTION__, (int)std::this_thread::get_id(), __LINE__, ##__VA_ARGS__ )

    #define _LOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) printf("%s %s [%d][D]'" format "'\n",__FILE__, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__ )
    #define _PLOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) PRINTF("%s %s p[%d-%d][D]'" format "'\n",__FILE__, __FUNCTION__, (int)getpid(), __LINE__, ##__VA_ARGS__ )
    #define _PPLOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) PRINTF("%s %s pp[%d-%d][D]'" format "'\n",__FILE__, __FUNCTION__, (int)getppid(), __LINE__, ##__VA_ARGS__ )
    #define _TLOGD(format, ...) if(custom::Log::canLog(custom::Log::_ELvl::Debug)) PRINTF("%s %s t[%d-%d][D]'" format "'\n",__FILE__, __FUNCTION__, (int)std::this_thread::get_id(), __LINE__, ##__VA_ARGS__ )

#else // LOG_EN

    #define _LOGW(...)
    #define _PLOGW(...)
    #define _PPLOGW(...)
    #define _TLOGW(...)
    #define _LOGE(...)
    #define _PLOGE(...)
    #define _PPLOGE(...)
    #define _TLOGE(...)

    #define _LOGI(...)
    #define _PLOGI(...)
    #define _PPLOGI(...)
    #define _TLOGI(...)

    #define _LOGD(...)
    #define _PLOGD(...)
    #define _PPLOGD(...)
    #define _TLOGD(...)

#endif // LOG_EN

#endif // LOG_H
