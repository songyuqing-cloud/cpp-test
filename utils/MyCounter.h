#ifndef COUNTER_H
#define COUNTER_H

#include <string>
#include <chrono>
#include <unordered_map>
#include <thread>
#include <sstream>

namespace util::log
{

template <typename T>
static inline std::string to_string(T &&a_t)
{
    std::ostringstream ss;
    ss << a_t;
    return std::string(ss.str());
}

std::string getId()
{
    return to_string(std::this_thread::get_id());
}

class MyCounter
{
public:
    MyCounter()
    {
        m_line = -1;
        m_beg = std::chrono::high_resolution_clock::now();
    }

    double elapse()
    {
        return std::chrono::duration <double, std::micro> (std::chrono::high_resolution_clock::now() - m_beg).count();
    }

    MyCounter(std::string const &a_file, std::string const &a_function, int a_line) : m_file(a_file), m_function(a_function), m_line(a_line)
    {
        m_beg = std::chrono::high_resolution_clock::now();
        auto id = std::this_thread::get_id();
        // indent[id] = 0;
        size_t time_point = std::chrono::time_point_cast<std::chrono::microseconds>(m_beg).time_since_epoch().count();

        printf("(%13ld):%s%*s ", time_point, to_string(id).data(), indent[id],"");
        printf("%s %s [%d]\n", m_file.data(), m_function.data(), m_line);
        indent[id]+=INDENT_COUNT;
    }

    virtual ~MyCounter()
    {
        if(m_line >= 0)
        {
            auto now = std::chrono::high_resolution_clock::now();
            auto id = std::this_thread::get_id();

            double diff = std::chrono::duration <double, std::micro> (now - m_beg).count();
            size_t time_point = std::chrono::time_point_cast<std::chrono::microseconds>(now).time_since_epoch().count();
            
            indent[id]-=INDENT_COUNT;
            printf("(%13ld):%s%*s ", time_point, to_string(id).data(), indent[id],"");
            printf("~%s [%d] %.2f us\n", m_function.data(), m_line, diff);
        }
    }

    std::string m_file, m_function;
    int m_line;
    std::chrono::high_resolution_clock::time_point m_beg;

    // static int indent;
    static std::unordered_map<std::thread::id, int> indent;
    static const int INDENT_COUNT=3;
};

}

#ifndef _LOGD
#define _LOGD(format, ...) printf("[D]%s(%s)%s[%d]" format "\n", util::log::getId().data(), __FILE__, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#endif

#define MY_COUNTER()           volatile util::log::MyCounter __counter(__FILE__, __FUNCTION__, __LINE__)
// #define MY_COUNTER() 

#endif // COUNTER_H
